//package clabs.butlers.facebook;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.Profile;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//
///**
// * Created by Rishabh on 26/04/15.
// */
//public class SocialLogin {
//
//    public static final int GOOGLE_LOGIN = 7;
//    public static final int FACEBOOK_LOGIN = 6;
//
//    private Activity activity;
//    private SocialLoginData loginData;
//    private CallbackManager callbackManager;
//
//    private TextView btnLoginViaFacebook;
//
//    public SocialLogin(Activity activity) {
//        this.activity = activity;
//    }
//
//    /**
//     * Method to login the user through facebook
//     */
//    public CallbackManager loginViaFacebook(int viewId) {
//
//        callbackManager = CallbackManager.Factory.create();
//
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//
//
//                        Log.v("ACCESS TOKEN", loginResult.getAccessToken().getToken());
//
//                        loginData = new SocialLoginData();
//                        loginData.setAccess_token(loginResult.getAccessToken().getToken());
//
//                        requestFacebookUserData(loginResult.getAccessToken());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                        Log.v("CANCEL", "CANCEL");
//
//                        loginData = new SocialLoginData();
//                        requestFacebookUserData(AccessToken.getCurrentAccessToken());
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                        Log.v("ERROR", "ERROR");
//
//                        Log.v("Something is wrong","wrong is something"+exception.toString());
//                        //CustomDialog.showAlertDialog(activity, "Error", "Please try other alternatives.", RequestCodes.DEFAULT);
//                    }
//                });
//
//        btnLoginViaFacebook = (TextView) activity.findViewById(viewId);
//        btnLoginViaFacebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                LoginManager.getInstance().logInWithReadPermissions(activity,
//                        Arrays.asList("email", "public_profile", "user_about_me","user_birthday"));
//            }
//        });
//
//        return callbackManager;
//    }
//
//    /**
//     * Method to request the UserData from Facebook
//     *
//     * @param token
//     */
//    public void requestFacebookUserData(AccessToken token) {
//
//        GraphRequest request = GraphRequest.newMeRequest(
//                token, new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(
//                            JSONObject object,
//                            GraphResponse response) {
//                        // Application code
//                        Log.v("GRAPH RESPONSE", response.toString());
//                        Log.v("JSON OBJECT", object.toString());
//
//                        try {
//
//                            String key;
//
//                            key = "id";
//                            loginData.setSocialUserID(getValue(key, object));
//
//                            key = "birthday";
//                            loginData.setBirthday_date(getValue(key, object));
//
//                            key = "gender";
//                            loginData.setSex(getValue(key, object));
//
//                            key = "name";
//                            String name = getValue(key, object);
//
//                            key = "email";
//                            loginData.setEmail(getValue(key, object));
//
//                            key = "first_name";
//                            loginData.setFirst_name(getValue(key, object));
//
//                            key = "last_name";
//                            loginData.setLast_name(getValue(key, object));
//
//                            key = "about";
//                            loginData.setAbout_me(getValue(key, object));
//
//                            returnFacebookUserData();
//                        } catch (JSONException jEx) {
//
//                        }
//                    }
//                });
//
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id, name, email, gender, birthday, about, first_name, last_name");
//        request.setParameters(parameters);
//        request.executeAsync();
//    }
//
//    /**
//     * Method to safely set the the values to the Fields
//     *
//     * @param key
//     * @param jObj
//     * @return
//     * @throws JSONException
//     */
//    private String getValue(String key, JSONObject jObj) throws JSONException {
//
//        if (jObj.has(key))
//            return jObj.getString(key);
//
//        return "";
//    }
//
//    /**
//     * Return the User data to the User
//     */
//    private void returnFacebookUserData() {
//
//        String pic = Profile.getCurrentProfile().getProfilePictureUri(400, 400).toString();
//
//        loginData.setPic_big(pic);
//        loginData.setUserType("f");
//
//        if (activity instanceof OnSocialLoginListener)
//            ((OnSocialLoginListener) activity).onSocialLogin(FACEBOOK_LOGIN, loginData);
//    }
//}
