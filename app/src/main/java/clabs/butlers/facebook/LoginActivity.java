//package clabs.butlers.facebook;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.view.View;
//
//import com.example.clicklabs.butlers.R;
//import com.facebook.FacebookSdk;
//
///**
//  * Developer: Rishabh (20/4/15)
//  */
//
//public class LoginActivity extends Activity implements View.OnClickListener, OnSocialLoginListener {
//
//    private static final int iFACEBOOK = R.id.sign_out;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//
//        super.onCreate(savedInstanceState);
//
//        // Initialize your Facebook SDK (>=4)
//        FacebookSdk.sdkInitialize(getApplicationContext());
//
//        // Set layout to your activity
//        setContentView(R.layout.activity_login);
//
//        // Initialize callback manager
//        initCallbackManager();
//    }
//
//    /**
//     * Method to initialize all the {@link View}s inside the Layout of this
//     * {@link Activity}
//     */
//    private void initCallbackManager() {
//
//        SocialLogin socialLogin = new SocialLogin(this);
//
//        // Just pass the id of your button, for which to handle the FACEBOOK LOGIN
//        callbackManager = socialLogin.loginViaFacebook(iFACEBOOK);
//    }
//
//    /**
//     * Handle results from activities
//     *
//     * @param requestCode
//     * @param responseCode
//     * @param data
//     */
//    protected void onActivityResult(int requestCode, int responseCode,
//                                    Intent data) {
//
//        callbackManager.onActivityResult(requestCode, responseCode, data);
//    }
//
//
//    @Override
//    public void onSocialLogin(int type, SocialLoginData loginData) {
//
//        // Create your method to perform actions after Facebook Login
//        performLoginAction(loginData);
//    }
//
//}
