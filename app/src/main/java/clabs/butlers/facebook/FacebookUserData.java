package clabs.butlers.facebook;

public class FacebookUserData {

	public String accessToken, fbId, firstName, lastName, userName, userEmail,gender;
	
	public FacebookUserData(String accessToken, String fbId, String firstName, String lastName, String userName,String gender, String userEmail){
		this.accessToken = accessToken;
		this.fbId = fbId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.userEmail = userEmail;
		this.gender = gender;
	}
	
}
