package clabs.butlers.utils;

/**
 * This file is used by all the other classes to check Internet connection 
 *
 * Project Name: - Word derby
 * Developed by ClickLabs. Developer: Raman goyal 
 * Link: http://www.click-labs.com/
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import com.example.clicklabs.butlers.R;

public class InternetConnectionStatus {

    static Context context;
    private static InternetConnectionStatus instance = new InternetConnectionStatus();
    ConnectivityManager connectManager;

    boolean connected = false;

    public static InternetConnectionStatus getInstance(Context ctx) {
        context = ctx;
        return instance;
    }

    public boolean isOnline(Context con) {
        try {
            connectManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable()
                    && networkInfo.isConnected();
            return connected;

        } catch (Exception e) {
            System.out
                    .println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }



//    public void showInternetErrorDialog(Context context) {
//        new MaterialDialog.Builder(context)
//                .title(R.string.error)
//                .content(R.string.internetConnectionError)
//                .negativeText(R.string.cancel)
//                .show();
//    }



}
