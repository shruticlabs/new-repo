package clabs.butlers.utils;

/**
 * Created by click103 on 20/3/15.
*/

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

public class CustomEditTextRegularFont extends EditText {

    public CustomEditTextRegularFont(Context context) {
        super(context);
        setFont();
    }

    public CustomEditTextRegularFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomEditTextRegularFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/sanfranciscotext_regularG3.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}