package clabs.butlers.utils;

/**
 * Created by click103 on 20/3/15.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewSemiBold extends TextView {

    public CustomTextViewSemiBold(Context context) {
        super(context);
        setFont();
    }

    public CustomTextViewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomTextViewSemiBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/sanfranciscotext_semibold.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}