package clabs.butlers.utils;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;

/**
 * Created by click103 on 13/3/15.
 */
public class Fonts {

    public static Typeface getFontTextHeavy(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_heavy.otf");
        return  tf;
    }
    public static Typeface getFontTextHeavyItalic(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_HeavyItalic.otf");
        return  tf;
    }
    public static Typeface getFontTextLight(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_Light.otf");
        return  tf;
    }
    public static Typeface getFontTextLightItalic(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_lightItalic.otf");
        return  tf;
    }
    public static Typeface getFontTextMedium(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_medium.otf");
        return  tf;
    }
    public static Typeface getFontTextRegularG3(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_regularG3.otf");
        return  tf;
    }
    public static Typeface getFontTextRegularItalic(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_regularItalic.otf");
        return  tf;
    }
    public static Typeface getFontTextSemiBold(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_semibold.otf");
        return  tf;
    }
    public static Typeface getFontTextSemiBoldItalic(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_semiboldItalic.otf");
        return  tf;
    }
    public static Typeface getFontTextThin(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_thin.otf");
        return  tf;
    }
    public static Typeface getFontTextThinItalic(Activity activity)
    {
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "fonts/sanfranciscotext_thinItalic.otf");
        return  tf;
    }



}
