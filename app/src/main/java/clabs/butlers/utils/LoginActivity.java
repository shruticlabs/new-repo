//package clabs.butlers.utils;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Intent;
//import android.content.IntentSender;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.text.Html;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.edurev.apisdata.APIs;
//import com.edurev.apisdata.ApiResponseFetcher;
//import com.edurev.appdata.AppDependencies;
//import com.edurev.listeners.OnSocialLoginListener;
//import com.edurev.modals.SocialLoginData;
//import com.edurev.modals.UserData;
//import com.edurev.plugins.CustomDialog;
//import com.edurev.utils.Font;
//import com.edurev.utils.InputHandler;
//import com.edurev.utils.Log;
//import com.edurev.utils.SocialLogin;
//import com.edurev.utils.Transition;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookSdk;
//import com.getangelsnow.serviceprovider.utilities.ASSL;
//import com.google.android.gms.auth.GoogleAuthException;
//import com.google.android.gms.auth.GoogleAuthUtil;
//import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
//import com.google.android.gms.auth.UserRecoverableAuthException;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.plus.Plus;
//import com.google.android.gms.plus.model.people.Person;
//import com.google.gson.Gson;
//import com.loopj.android.http.RequestParams;
//
//import java.io.IOException;
//
//public class LoginActivity extends Activity implements OnClickListener,
//        GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener, OnSocialLoginListener {
//
//    private static final int iBACK = R.id.rlBack;
//    private static final int iGOOGLE = R.id.btnLoginWithGooglePlus;
//    private static final int iFACEBOOK = R.id.btnLoginWithFacebook;
//    private static final int iLOGIN = R.id.btnEmailLogin;
//    private static final int iFORGOT = R.id.btnForgotPassword;
//
//    /* Request code used to invoke sign in user interactions. */
//    private static final int RC_SIGN_IN = 18;
//    private final String AUTH_TOKEN_URL = "https://accounts.google.com/o/oauth2/token";
//    private final String CLIENT_ID = "1018023892167-cktkid9jpirpbjmobem80gedjbtk6r8k.apps.googleusercontent.com";
//    private final String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
//    /* Client used to interact with Google APIs. */
//    private GoogleApiClient mGoogleApiClient;
//    /*
//     * A flag indicating that a PendingIntent is in progress and prevents us
//     * from starting further intents.
//     */
//    private boolean mIntentInProgress;
//    private CallbackManager callbackManager;
//    private EditText etEmail;
//    private EditText etPassword;
//    private Button btnLoginWithGooglePlus;
//    private Button btnLoginWithFacebook;
//    private Button btnEmailLogin;
//    private Button btnForgotPassword;
//    private RelativeLayout rlBack;
//    private TextView tvTitleBarText;
//
//    private String token;
//    private SocialLoginData loginData;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//
//        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        setContentView(R.layout.activity_login);
//
//        init();
//    }
//
//    /**
//     * Method to initialize all the {@link View}s inside the Layout of this
//     * {@link Activity}
//     */
//    private void init() {
//
//        new ASSL(this, (RelativeLayout) findViewById(R.id.rootLogin), 1134,
//                720, false);
//
//        SocialLogin socialLogin = new SocialLogin(this);
//        callbackManager = socialLogin.loginViaFacebook(R.id.btnLoginWithFacebook);
//
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this).addApi(Plus.API)
//                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
//
//        etEmail = (EditText) findViewById(R.id.etEmail);
//        etPassword = (EditText) findViewById(R.id.etPassword);
//
//        btnLoginWithGooglePlus = (Button) findViewById(iGOOGLE);
//        btnLoginWithFacebook = (Button) findViewById(iFACEBOOK);
//        btnEmailLogin = (Button) findViewById(iLOGIN);
//        btnForgotPassword = (Button) findViewById(iFORGOT);
//
//        rlBack = (RelativeLayout) findViewById(iBACK);
//        tvTitleBarText = (TextView) findViewById(R.id.tvTitleBarText);
//        tvTitleBarText.setTypeface(Font.get(this));
//
//        Font.bind(this, btnEmailLogin, btnLoginWithFacebook,
//                btnLoginWithGooglePlus, btnForgotPassword);
//
//        Font.bind(this, etEmail, etPassword);
//
//        rlBack.setOnClickListener(this);
//        btnLoginWithGooglePlus.setOnClickListener(this);
//        btnEmailLogin.setOnClickListener(this);
//        btnForgotPassword.setOnClickListener(this);
//    }
//
//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId()) {
//
//            case iBACK:
//                performBackAction();
//                break;
//
//            case iLOGIN:
//                performLoginAction();
//                break;
//
//            case iGOOGLE:
//                CustomDialog.showProgressDialog(this);
//                mGoogleApiClient.connect();
//                break;
//
//            case iFORGOT:
//                gotoForgotPasswordScreen();
//                break;
//
//            default:
//                break;
//        }
//    }
//
//    /**
//     * Method to goto HomeActivity
//     */
//    private void performLoginAction() {
//
//        if (InputHandler.areEmpty(etEmail, etPassword))
//            return;
//
//        String email = etEmail.getText().toString();
//        String password = etPassword.getText().toString();
//
//        String apiName = APIs.SIGN_IN;
//
//        RequestParams params = new RequestParams();
//        params.put("userName", email);
//        params.put("Password", password);
//
//        new ApiResponseFetcher(this, apiName, params, true) {
//
//            @Override
//            public void onResponse(String jResponse) {
//
//                UserData userData = new Gson().fromJson(jResponse,
//                        UserData.class);
//
//                gotoHomeScreen(userData);
//            }
//
//        };
//    }
//
//    /**
//     * Method to jump to home screen exploiting constraints on UserData
//     *
//     * @param userData
//     */
//    private void gotoHomeScreen(UserData userData) {
//
//        if (userData.getToken() == null) {
//
//            CustomDialog.showAlertDialog(LoginActivity.this, "Error",
//                    "Please verify your credentials", 0);
//        } else {
//
//            AppDependencies.saveAccessToken(LoginActivity.this,
//                    userData.getToken());
//
//            Bundle extras = new Bundle();
//            extras.putParcelable(UserData.class.getName(), userData);
//            Transition.transit(LoginActivity.this, HomeActivity.class, extras);
//        }
//    }
//
//    /**
//     * Method to goto {@link ForgotPasswordActivity}
//     */
//    private void gotoForgotPasswordScreen() {
//
//        String email = etEmail.getText().toString();
//
//        Bundle extras = new Bundle();
//        extras.putString("EMAIL", email);
//        Transition.transit(this, ForgotPasswordActivity.class, extras);
//    }
//
//    /**
//     * Method to go back
//     */
//    private void performBackAction() {
//        Transition.transit(this, SplashActivity.class, true);
//    }
//
//    /**
//     * Method to perform login action using facebook
//     */
//    public void performSocialLoginAction(SocialLoginData loginData) {
//
//        boolean isGooglePlusUser = loginData.getUserType().equals("g+");
//
//        String apiName = isGooglePlusUser ? APIs.GOOGLE : APIs.FACEBOOK;
//
//        RequestParams params = new RequestParams();
//        params.put("email", loginData.getEmail());
//        params.put("first_name", loginData.getFirst_name());
//        params.put("last_name", loginData.getLast_name());
//        params.put("sex", loginData.getSex());
//        params.put("access_token", loginData.getAccess_token());
//        params.put("socialUserID", loginData.getSocialUserID());
//        if (isGooglePlusUser)
//            params.put("user_type", loginData.getUserType());
//        params.put("pic_big", loginData.getPic_big());
//
//        if (!isGooglePlusUser) {
//
//            params.put("userType", loginData.getUserType());
//            params.put("birthday_date", loginData.getBirthday_date());
//            params.put("books", loginData.getBooks());
//            params.put("interests", loginData.getInterests());
//            params.put("movies", loginData.getMovies());
//            params.put("music", loginData.getMusic());
//            params.put("about_me", loginData.getAbout_me());
//        }
//
//        new ApiResponseFetcher(this, apiName, params, true) {
//
//            @Override
//            public void onResponse(String jResponse) {
//
//                UserData userData = new Gson().fromJson(jResponse,
//                        UserData.class);
//
//                gotoHomeScreen(userData);
//            }
//
//        };
//    }
//
//    /**
//     * Handle results from activities
//     *
//     * @param requestCode
//     * @param responseCode
//     * @param data
//     */
//    protected void onActivityResult(int requestCode, int responseCode,
//                                    Intent data) {
//
//        if (requestCode == RC_SIGN_IN) {
//            mIntentInProgress = false;
//
//            if (!mGoogleApiClient.isConnecting()) {
//                mGoogleApiClient.connect();
//            }
//        } else {
//
//            // callbackManager.onActivityResult(requestCode, responseCode,
//            // data);
//        }
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//
//        if (!mIntentInProgress && result.hasResolution()) {
//            try {
//                mIntentInProgress = true;
//                startIntentSenderForResult(result.getResolution()
//                        .getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
//            } catch (IntentSender.SendIntentException e) {
//                // The intent was canceled before it was sent. Return to the
//                // default
//                // state and attempt to connect to get an updated
//                // ConnectionResult.
//                mIntentInProgress = false;
//                mGoogleApiClient.connect();
//            }
//        }
//    }
//
//    /**
//     * Method to fetch user data from Google Plus
//     */
//    private void getGoogleProfileInformation() {
//
//        try {
//
//            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
//
//                Person currentPerson = Plus.PeopleApi
//                        .getCurrentPerson(mGoogleApiClient);
//
//                String id = currentPerson.getId();
//                String personName = currentPerson.getDisplayName();
//                String personPhotoUrl = currentPerson.getImage().getUrl();
//                String personGooglePlusProfile = currentPerson.getUrl();
//                String aboutMe = currentPerson.getAboutMe();
//                String birthday = currentPerson.getBirthday();
//                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
//                int gender = currentPerson.getGender();
//
//                Log.e(getClass().getSimpleName(), "Name: " + personName
//                        + ", plusProfile: " + personGooglePlusProfile
//                        + ", about me:" + aboutMe + ", email: " + email
//                        + ", birthday: " + birthday + ", gender: " + gender
//                        + ", Image: " + personPhotoUrl);
//
//                // by default the profile url gives 50x50 px image only
//                // we can replace the value with whatever dimension we want by
//                // replacing sz=X
//                personPhotoUrl = personPhotoUrl.substring(0,
//                        personPhotoUrl.length() - 2) + 400;
//
//                String[] name = personName.split(" ");
//                aboutMe = Html.fromHtml(aboutMe).toString();
//
//                loginData = new SocialLoginData(email, name[0],
//                        name.length > 1 ? name[1] : "", birthday == null ? ""
//                        : birthday, gender == 0 ? "M" : "F", "token",
//                        "", "", "", "", aboutMe == null ? "" : aboutMe,
//                        personPhotoUrl, id, "g+");
//
//                new GetTokenTask().execute(email);
//
//            } else {
//                Toast.makeText(getApplicationContext(),
//                        "Person information is null", Toast.LENGTH_LONG).show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onConnected(Bundle arg0) {
//        // TODO Auto-generated method stub
//        Toast.makeText(this, "CONNECTED", Toast.LENGTH_SHORT).show();
//
//        getGoogleProfileInformation();
//    }
//
//    @Override
//    public void onConnectionSuspended(int arg0) {
//        // TODO Auto-generated method stub
//
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    protected void onStop() {
//
//        super.onStop();
//
//        if (mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        performBackAction();
//    }
//
//    /**
//     * Method to fetch access token from Google Server
//     *
//     * @param email
//     */
//    private void getAndUseAuthTokenBlocking(String email) {
//        try {
//            // Retrieve a token for the given account and scope. It will always
//            // return either
//            // a non-empty String or throw an exception.
//            token = GoogleAuthUtil.getToken(this, email,
//                    "oauth2:https://www.googleapis.com/auth/userinfo.profile");
//            // Do work with token.
//
//            Log.v("TOKEN", token);
//
//            if (token.isEmpty()) {
//
//                // invalidate the token that we found is bad so that
//                // GoogleAuthUtil won't
//                // return it next time (it may have cached it)
//                GoogleAuthUtil.invalidateToken(this, token);
//                // consider retrying getAndUseTokenBlocking() once more
//                return;
//
//            } else {
//
//                loginData.setAccess_token(token);
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        performSocialLoginAction(loginData);
//                    }
//                });
//            }
//        } catch (GooglePlayServicesAvailabilityException playEx) {
//            Dialog alert = GooglePlayServicesUtil.getErrorDialog(
//                    playEx.getConnectionStatusCode(), this, 11);
//
//        } catch (UserRecoverableAuthException userAuthEx) {
//            // Start the user recoverable action using the intent returned by
//            // getIntent()
//            this.startActivityForResult(userAuthEx.getIntent(), 12);
//            return;
//        } catch (IOException transientEx) {
//            // network or server error, the call is expected to succeed if you
//            // try again later.
//            // Don't attempt to call again immediately - the request is likely
//            // to
//            // fail, you'll hit quotas or back-off.
//
//            return;
//        } catch (GoogleAuthException authEx) {
//            // Failure. The call is not expected to ever succeed so it should
//            // not be
//            // retried.
//
//            return;
//        }
//    }
//
//    @Override
//    public void onSocialLogin(int type, SocialLoginData loginData) {
//        performSocialLoginAction(loginData);
//    }
//
//    /**
//     * Task to fetch access token from Google Plus
//     */
//    private class GetTokenTask extends AsyncTask<String, Void, Void> {
//
//        @Override
//        protected Void doInBackground(String... strings) {
//
//            String email = strings[0];
//            getAndUseAuthTokenBlocking(email);
//
//            return null;
//        }
//    }
//}