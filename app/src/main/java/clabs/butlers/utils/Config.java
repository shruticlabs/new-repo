package clabs.butlers.utils;


import android.graphics.Typeface;
import android.os.Build;

public class Config {

    static String BASE_URL = "http://butlertip.com:3000/test/";
    static String GCM_ID = "70998168206";
    static String FLURRY_KEY = "";
    static String STRIPE_SECRET_KEY = "";
    static String STRIPE_PUBLISHABLE_KEY = "";
    static String STRIPE_CLIENT_ID = "";
    static AppMode appMode = AppMode.TEST;
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    private static final String PREF_USER_ID = "twitter_user_id";
    private static final String PREF_USER_PROFILE_IMAGE = "twitter_user_image";
    static boolean LOGIN_STATUS=true;
    public static final int ASSL_HEIGHT = 1134;
    public static final int ASSL_WIDTH = 720;
    public static final int LOGIN_VIA = 1;
    static int RECORDIN_STATUS = 0;
    static String DEVICE_TYPE="1";
    static int SERVER_TIMEOUT=10000;
    static int OFFSET=14;
    static int OFFSETOTHER=29;
    static boolean SKIP_MODE=false;
    static String APP_ACCESS_TOKEN="";
    static String USER_ID="";
    static String PRODUCT_ID="";
    static int VOICENOTEDURATION=30;




    /**
     * contants keys
     * @return
     */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }

    static public String getFlurryKey() {

        init(appMode);
        return FLURRY_KEY;
    }
    
    static public String getStripeSecretKey() {

        init(appMode);
        return STRIPE_SECRET_KEY;
    }
    static public String getStripePublishableKey() {

        init(appMode);
        return STRIPE_PUBLISHABLE_KEY;
    }
    static public String getStripeClientId() {

        init(appMode);
        return STRIPE_CLIENT_ID;
    }

    public static String getAPP_ACCESS_TOKEN() {
        return APP_ACCESS_TOKEN;
    }

    public static void setAPP_ACCESS_TOKEN(String APP_ACCESS_TOKEN) {
        Config.APP_ACCESS_TOKEN = APP_ACCESS_TOKEN;
    }

    public static String getPRODUCT_ID() {
        return PRODUCT_ID;
    }

    public static void setPRODUCT_ID(String PRODUCT_ID) {
        Config.PRODUCT_ID = PRODUCT_ID;
    }

    public static int getVOICENOTEDURATION() {
        return VOICENOTEDURATION;
    }

    public static String getUSER_ID() {
        return USER_ID;
    }

    public static void setUSER_ID(String USER_ID) {
        Config.USER_ID = USER_ID;
    }

    public static int getOFFSETOTHER() {
        return OFFSETOTHER;
    }

    public static void setOFFSETOTHER(int OFFSETOTHER) {
        Config.OFFSETOTHER = OFFSETOTHER;
    }

    public static int getOFFSET() {
        return OFFSET;
    }

    public static String getBASE_URL() {
        return BASE_URL;
    }

    public static void setBASE_URL(String BASE_URL) {
        Config.BASE_URL = BASE_URL;
    }

    public static boolean getSKIP_MODE() {
        return SKIP_MODE;
    }

    public static void setSKIP_MODE(boolean SKIP_MODE) {
        Config.SKIP_MODE = SKIP_MODE;
    }

    public static String getGCM_ID() {
        return GCM_ID;
    }

    public static String getDEVICE_TYPE() {
        return DEVICE_TYPE;
    }

    public static int getSERVER_TIMEOUT() {
        return SERVER_TIMEOUT;
    }

    public static int getRecordinStatus() {
        return RECORDIN_STATUS;
    }

    public static void setRecordinStatus(int RECORDIN_STATUS) {
        Config.RECORDIN_STATUS=RECORDIN_STATUS;
    }

    public static int getAsslHeight() {
        return ASSL_HEIGHT;
    }

    public static int getAsslWidth() {
        return ASSL_WIDTH;
    }

    /**
     * Font intialized
     * @return
     */



    public static boolean isLOGIN_STATUS() {
        return LOGIN_STATUS;
    }

    public static void setLOGIN_STATUS(boolean LOGIN_STATUS) {
        Config.LOGIN_STATUS = LOGIN_STATUS;
    }

    public static String getPrefKeyTwitterLogin() {
        return PREF_KEY_TWITTER_LOGIN;
    }

    public static String getPrefName() {
        return PREF_NAME;
    }

    public static String getPrefKeyOauthToken() {
        return PREF_KEY_OAUTH_TOKEN;
    }

    public static String getPrefKeyOauthSecret() {
        return PREF_KEY_OAUTH_SECRET;
    }

    public static String getPrefUserName() {
        return PREF_USER_NAME;
    }

    public static String getPrefUserImage() {
        return PREF_USER_PROFILE_IMAGE;
    }

    public static String getPrefUserId() {
        return PREF_USER_ID;
    }

    public static int getWebviewRequestCode() {
        return WEBVIEW_REQUEST_CODE;
    }

    /**
     * Initialize all the variable in this method
     *
     * @param appMode
     */
    public static void init(AppMode appMode) {

        switch (appMode) {
            case DEV:

                BASE_URL = "base URl for dev mode";
                FLURRY_KEY = "flurry key for dev mode";
                STRIPE_CLIENT_ID = "client id for dev mode";
                STRIPE_SECRET_KEY = "secret key for dev mode";
                STRIPE_PUBLISHABLE_KEY = "publishable key for dev mode";
                break;

            case TEST:

            	BASE_URL = "http://butlertip.com:8000/";
                FLURRY_KEY = "flurry key for test mode";
                STRIPE_CLIENT_ID = "client id for test mode";
                STRIPE_SECRET_KEY = "secret key for test mode";
                STRIPE_PUBLISHABLE_KEY = "publishable key for test mode";
                break;

            case LIVE:

            	BASE_URL = "base URl for live mode";
                FLURRY_KEY = "flurry key for live mode";
                STRIPE_CLIENT_ID = "client id for live mode";
                STRIPE_SECRET_KEY = "secret key for live mode";
                STRIPE_PUBLISHABLE_KEY = "publishable key for live mode";
                break;

        }
        
    }
    
    public enum AppMode {
        DEV, TEST, LIVE
    }
    public static boolean isLollyPop(){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP){
            return  true;

        }
        return false;

    }


}