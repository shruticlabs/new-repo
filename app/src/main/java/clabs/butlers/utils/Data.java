package clabs.butlers.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.clicklabs.butlers.R;

import java.util.regex.Pattern;

/**
 * Created by click103 on 17/3/15.
 */
public class Data {
    public static ProgressDialog progressDialog;
    public static String productId="";

    /**
     * Loading view
     *
     * @param c
     *            context from where the loading view is in voked
     * @param msg
     *            message to be shown
     */

    public static void loading_box(Context c, String msg) {
        progressDialog = new ProgressDialog(c,
                android.R.style.Theme_Translucent_NoTitleBar);
        progressDialog.show();
        progressDialog.setCancelable(false);

        progressDialog.setContentView(R.layout.loading_box);
//        TextView t1 = (TextView) progressDialog.findViewById(R.id.loading);
//        t1.setText(msg);
//        progressDialog.findViewById(R.id.rlt).setAlpha(0.9f);

    }

    /**
     * stop loading view
     */
    public static void loading_box_stop() {
        if (progressDialog != null)
            if (progressDialog.isShowing())
                progressDialog.dismiss();
    }


    /**
     * valid email check
     *
     * @param email
     *            String to be verified
     * @return return true if the email is valid false otherwise
     */
    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static final Pattern EMAIL_ADDRESS = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    /**
     * hiding keyboard
     * @param activity
     */

    public static void hideSoftKeyboard(Activity activity) {
//        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        catch(Exception e)
        {

        }
    }
}
