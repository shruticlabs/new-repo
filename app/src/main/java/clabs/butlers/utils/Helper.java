package clabs.butlers.utils;

/**
 * Class is used to allow listview in scrollview 
 * @author showket
 */
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Helper {

    /*
measures height of listview when placed inside scrollview
 */
	public static void getListViewSize(ListView myListView) {
		ListAdapter myListAdapter = myListView.getAdapter();
		if (myListAdapter == null) {

			return;
		}
		int totalHeight = 0;
		for (int size = 0; size < myListAdapter.getCount(); size++) {
			View listItem = myListAdapter.getView(size, null, myListView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = myListView.getLayoutParams();
		params.height = totalHeight
				+ (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
		myListView.setLayoutParams(params);


	}

    /*
measures height of recyclerview when placed inside scrollview
 */
    public static void getRecyclerViewSize(RecyclerView myListView) {
     RecyclerView.Adapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {

            return;
        }
        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getItemCount(); size++) {
          View listItem = myListView.getAdapter().createViewHolder(myListView,0).itemView;
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight
                + (7 * (myListAdapter.getItemCount() - 1));
        myListView.setLayoutParams(params);

        //	Log.i("height of listItem:", String.valueOf(totalHeight));
    }

    /*
measures height of recyclerview in gridview form when placed inside scrollview
 */
    public static void setRecyclerGridViewHeightBasedOnChildren(RecyclerView gridView, int columns,int margin) {
        RecyclerView.Adapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getItemCount();
        int rows = 0;

        View listItem = gridView.getAdapter().createViewHolder(gridView,0).itemView;

        listItem.measure(0,0);
        totalHeight = listItem.getMeasuredHeight();


        float x = 1;
        if( items > columns ){
            x = items/columns;


            if((items%columns)==0){
                rows=(int)x;
            }
            else {
                rows = (int) (x + 1);
            }

            if(rows>0)
            {
                totalHeight+= margin;

            }

            totalHeight *= rows-1;
        }
        else{

            totalHeight+=margin;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

/*
measures height of gridview when placed inside scrollview
 */
    public static void setGridViewHeightBasedOnChildren(GridView gridView, int columns,int margin) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);


       listItem.measure(0,0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;


            if((items%columns)==0){
                rows=(int)x;
            }
            else {
                rows = (int) (x + 1);
            }

            if(rows>0)
            {
               totalHeight+= margin;
            }
            totalHeight *= rows;
        }
        else{

            totalHeight+=margin;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }

}
