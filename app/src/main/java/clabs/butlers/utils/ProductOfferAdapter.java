package clabs.butlers.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.example.clicklabs.butlers.R;
import com.nineoldandroids.view.ViewHelper;

import pager.fragment.OffersFragmentTab;

/**
 * Created by clicklabs107 on 4/9/15.
 */

public class ProductOfferAdapter extends FragmentPagerAdapter {


    private boolean swipedLeft=false;
    private int lastPage=OffersFragmentTab.arraylist.size();
    private MyLinearLayout cur = null;
    private MyLinearLayout next = null;
    private MyLinearLayout prev = null;
    private MyLinearLayout prevprev = null;
    private MyLinearLayout nextnext = null;
    private Context context;
    private FragmentManager fm;
    private float scale;
    private boolean IsBlured;
    private  static float minAlpha=0.6f;
    private static float maxAlpha=1f;
    private static float minDegree=60.0f;
    private int counter=0;




    public ProductOfferAdapter(FragmentManager fm) {
        super(fm);

    }


    @Override
    public int getCount() {
        return OffersFragmentTab.arraylist.size();
    }


    public static float getMinDegree()
    {
        return minDegree;
    }
    public static float getMinAlpha()
    {
        return minAlpha;
    }
    public  static float getMaxAlpha()
    {
        return maxAlpha;
    }

    public ProductOfferAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.fm = fm;
        this.context = context;

    }

    @Override
    public OffersFragmentTab.OfferProductFragment getItem(int position)
    {



        // make the first pager bigger than others
        if (position == MyLinearLayout.FIRST_PAGE)
            scale = MyLinearLayout.BIG_SCALE;
        else
        {
            scale = MyLinearLayout.SMALL_SCALE;
            IsBlured=true;

        }

     //   Log.d("position", String.valueOf(position));
        OffersFragmentTab.OfferProductFragment curFragment= OffersFragmentTab.OfferProductFragment.init(context, position, scale, IsBlured);
        cur = getRootView(position);
        next = getRootView(position +1);
        prev = getRootView(position -1);

        return curFragment;
    }

//
//
//
//    public void onPageScrolled(int position, float positionOffset,
//                               int positionOffsetPixels)
//    {
//        if (positionOffset >= 0f && positionOffset <= 1f)
//        {
//            positionOffset=positionOffset*positionOffset;
//            cur = getRootView(position);
//            next = getRootView(position +1);
//            prev = getRootView(position -1);
//            nextnext=getRootView(position +2);
//
//            ViewHelper.setAlpha(cur, maxAlpha - 0.5f * positionOffset);
//          //  ViewHelper.setAlpha(next, minAlpha+0.5f*positionOffset);
//          // ViewHelper.setAlpha(prev, minAlpha+0.5f*positionOffset);
//
//
//            if(nextnext!=null)
//            {
//                ViewHelper.setAlpha(nextnext, minAlpha);
//                ViewHelper.setRotationY(nextnext, -minDegree);
//            }
//            if(cur!=null)
//            {
//                cur.setScaleBoth(MyLinearLayout.BIG_SCALE
//                        - MyLinearLayout.DIFF_SCALE * positionOffset);
//
//                ViewHelper.setRotationY(cur, 0);
//            }
//
//            if(next!=null)
//            {
//                next.setScaleBoth(MyLinearLayout.SMALL_SCALE
//                        + MyLinearLayout.DIFF_SCALE * positionOffset);
//                ViewHelper.setRotationY(next, -minDegree);
//            }
//            if(prev!=null)
//            {
//                ViewHelper.setRotationY(prev, minDegree);
//            }
//
//
//			/*To animate it properly we must understand swipe direction
//			 * this code adjusts the rotation according to direction.
//			 */
//            if(swipedLeft)
//            {
//                if(next!=null)
//                    ViewHelper.setRotationY(next, -minDegree+minDegree*positionOffset);
//                if(cur!=null)
//                    ViewHelper.setRotationY(cur, 0+minDegree*positionOffset);
//            }
//            else
//            {
//                if(next!=null)
//                    ViewHelper.setRotationY(next, -minDegree+minDegree*positionOffset);
//                if(cur!=null)
//                {
//                    ViewHelper.setRotationY(cur, 0+minDegree*positionOffset);
//                }
//            }
//        }
//        if(positionOffset>=1f)
//        {
//            ViewHelper.setAlpha(cur, maxAlpha);
//        }
//
//    }
//
//
//    public void onPageSelected(int position) {
//
///*
// * to get finger swipe direction
// */
//        if(lastPage<=position)
//        {
//            swipedLeft=true;
//        }
//        else if(lastPage>position)
//        {
//            swipedLeft=false;
//        }
//        lastPage=position;
//
////
////        String  string=getFragmentTag(3);
////
////        OffersFragmentTab tab=(OffersFragmentTab)fm.findFragmentById(R.id.pager);
////        tab.offersListView.setAdapter(tab.myOfferAdapter);
////
////        Helper.getListViewSize(tab.offersListView);
//
//    }


//    public void onPageScrollStateChanged(int state) {
//    }



    public MyLinearLayout getRootView(int position)
    {
        MyLinearLayout ly;
        try {
            ly = (MyLinearLayout)fm.findFragmentByTag(this.getFragmentTag(position)).getView().findViewById(R.id.root);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            return null;
        }
        if(ly!=null) {
            return ly;
        }
        return null;
    }

    private String getFragmentTag(int position)
    {
        return "android:switcher:" + OffersFragmentTab.viewPager.getId() + ":" + position;
    }
}
