package clabs.butlers.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;

public class CommonUtil {
	public static String GAME_IMAGE_NAME = "userImage.jpg";

	// private static String GAME_IMAGE_DIR = "/oriient/tempDir";

	public static File getTempImageFile() {
		// File cacheDir = new File(
		// android.os.Environment.getExternalStorageDirectory(),
		// GAME_IMAGE_DIR);
		File file = new File(
				android.os.Environment.getExternalStorageDirectory(),
				GAME_IMAGE_NAME);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	public static void saveTempImage(Bitmap bitmap) {
		OutputStream outStream = null;
		
		File tempFile = new File(
				android.os.Environment.getExternalStorageDirectory(),
				GAME_IMAGE_NAME);
		

		try {
			outStream = new FileOutputStream(tempFile);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}
	// public static Bitmap getUserImage(Context c) {
	// if (Data.userBitmap == null) {
	// return BitmapFactory.decodeFile(getTempImageFile()
	// .getAbsolutePath());
	// }
	// return Data.userBitmap;
	// }

}
