package clabs.butlers.utils;

/**
 * Created by click103 on 20/3/15.
 */
        import android.content.Context;
        import android.graphics.Typeface;
        import android.util.AttributeSet;
        import android.widget.TextView;

public class CustomTextViewLightFont extends TextView {

    public CustomTextViewLightFont(Context context) {
        super(context);
        setFont();
    }

    public CustomTextViewLightFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomTextViewLightFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/sanfranciscotext_Light.otf");
        setTypeface(font, Typeface.NORMAL);
    }
}