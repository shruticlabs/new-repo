package clicklabs.app.butlers;

import android.app.Application;
import android.util.Log;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import clabs.butlers.utils.MySingleton;

public class MyApplication extends Application
{
    String  difference = null;
  @Override
  public void onCreate()
  {
    super.onCreate();
     
    // Initialize the singletons so their instances
    // are bound to the application process.

      // UNIVERSAL IMAGE LOADER SETUP
      DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
              .cacheOnDisc(true).cacheInMemory(true)
              .imageScaleType(ImageScaleType.EXACTLY)
              .displayer(new FadeInBitmapDisplayer(300)).build();

      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
              getApplicationContext())
              .defaultDisplayImageOptions(defaultOptions)
              .memoryCache(new WeakMemoryCache())
              .discCacheSize(100 * 1024 * 1024).build();

      ImageLoader.getInstance().init(config);

    initSingletons();
  }
 
  protected void initSingletons()
  {
    // Initialize the instance of MySingleton
    MySingleton.initInstance();
  }
   

//    public String getExpirationTimeTodisplay(String diff) {
//        long diffInSecond =Long.parseLong(diff);
////        serverDate = serverDate.replace("T", " ");
////        serverDate = serverDate.replace("Z", "");
////        Calendar current = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
////        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
//        SimpleDateFormat dateFormatToPrrint = new SimpleDateFormat(
//                "MMM dd yyyy' at 'hh:mm a");
////        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
////        try {
////            cal.setTime(sdf.parse(serverDate));
////        } catch (ParseException e) {
////            e.printStackTrace();
////        }
////        long diffInMilis = current.getTimeInMillis() - cal.getTimeInMillis();
////        long diffInSecond = diffInMilis / 1000;
//        long prefix;
//        String suffix;
//
//        String serverDate = sdf.format(new Date(diffInSecond * 1000L));
//
//        if (diffInSecond >= 60) {
//            long diffInMinute = diffInSecond / 60;
//            if (diffInMinute >= 60) {
//                long diffInHour = diffInMinute / 60;
//                if (diffInHour >= 24) {
//
//                    try {
//                        Date myDate = sdf.parse(serverDate);
//                        String localDate = dateFormatToPrrint.format(myDate);
//                        localDate = localDate.replace("AM", "am").replace("PM",
//                                "pm");
//                        difference = localDate;
//
//                    } catch (Exception e1) {
//                        Log.e("e1", "=" + e1);
//                    }
//
//                } else {
//                    prefix = diffInHour;
//                    suffix = " hour" + (diffInHour == 1 ? "" : "s") + " ago";
//                    difference = prefix + suffix;
//                }
//            } else {
//                prefix = diffInMinute;
//                suffix = " minute" + (diffInMinute == 1 ? "" : "s") + " ago";
//                difference = prefix + suffix;
//            }
//        } else {
//
//            difference = "few seconds ago";
//        }
//
//        return difference;
//    }
public String getExpirationTimeTodisplay(String serverDate) {



    serverDate = serverDate.replace("T", " ");
    serverDate = serverDate.replace("Z", "");
    Calendar current = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
    SimpleDateFormat dateFormatToPrrint = new SimpleDateFormat(
            "MMM dd yyyy' at 'hh:mm a");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    try {
        cal.setTime(sdf.parse(serverDate));
    } catch (ParseException e) {
        e.printStackTrace();
    }
    long diffInMilis = current.getTimeInMillis() - cal.getTimeInMillis();
    long diffInSecond = diffInMilis / 1000;
    long prefix;
    String suffix;
    if (diffInSecond >= 60) {
        long diffInMinute = diffInSecond / 60;
        if (diffInMinute >= 60) {
            long diffInHour = diffInMinute / 60;
            if (diffInHour >= 24) {

                try {
                    Date myDate = sdf.parse(serverDate);
                    String localDate = dateFormatToPrrint.format(myDate);
                    localDate = localDate.replace("AM", "am").replace("PM",
                            "pm");
                    difference = localDate;

                } catch (Exception e1) {
                    Log.e("e1", "=" + e1);
                }

            } else {
                prefix = diffInHour;
                suffix = " hour" + (diffInHour == 1 ? "" : "s") + " ago";
                difference = prefix + suffix;
            }
        } else {
            prefix = diffInMinute;
            suffix = " minute" + (diffInMinute == 1 ? "" : "s") + " ago";
            difference = prefix + suffix;
        }
    } else {

        difference = "few seconds ago";
    }

    return difference;
}

    public  String currentDate()
    {
        return DateFormat.getDateTimeInstance().format(new Date());

    }
}