//package clicklabs.app.butlers;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import org.apache.http.Header;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import rmn.androidscreenlibrary.ASSL;
//import android.app.AlertDialog;
//import android.content.ActivityNotFoundException;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Environment;
//import android.provider.MediaStore;
//import android.support.v4.app.FragmentManager;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.flurry.android.FlurryAgent;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//import com.loopj.android.http.RequestParams;
//import com.squareup.picasso.Picasso;
//import com.vizavoo.utils.CircleTransform;
//import com.vizavoo.utils.Data1;
//import com.vizavoo.utils.DialogPopup;
//import com.vizavoo.utils.Httppost_Links;
//import com.vizavoo.utils.MyFragment;
//import com.vizavoo.utils.Prefs;
//import com.vizavoo.utils.Validation;
//import com.vizavoo.utils.Validation.TextType;
//import com.vizavoo.utils.WorkingModules;
//
//import eu.janmuller.android.simplecropimage.CropImage;
//
//public class ProfileEdit extends MyFragment implements WorkingModules, OnClickListener {
//
//	private View rootView;
//	private RelativeLayout topBar;
//	private EditText feedbackEdittext;
//	private RelativeLayout closeButton;
//	private TextView tittle;
//	private Button sendButton;
//	private LinearLayout mainLayer;
//	private TextView edit;
//	// private EditText email;
//	private TextView textFname;
//	private TextView textMobile;
//	private EditText mobileEditText;
//	private File mFileTemp = null;
//	private ImageView addPhoto;
//	private TextView textLname;
//	private EditText fName;
//	private EditText lName;
//	protected int keyDel;
//	protected String mobileno;
//	protected String mobileNOStored="";
//	protected int lastlength=0;
//	private String mobileNo;
//	public static final int REQUEST_CODE_GALLERY = 0x1;
//	public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
//	public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
//	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		Data1.FontFamily(getActivity());
//		rootView = inflater.inflate(R.layout.edit_profile, container, false);
//		RelativeLayout rv = (RelativeLayout) rootView.findViewById(R.id.container);
//		new ASSL(getActivity(), rv, 1134, 720, false);
//		initialization();
//		fontSet();
//
//		fName.setText(Prefs.with(getActivity()).getString("firstName", "").trim().toString());
//		lName.setText(Prefs.with(getActivity()).getString("lastName", "").trim().toString());
//
//		try{
//		  mobileNo ="("+ Prefs.with(getActivity()).getString("mobile", "").split("\\(")[1];
//		 }
//		 catch(Exception e)
//		 {
//			 mobileNo =Prefs.with(getActivity()).getString("mobile", "");
//		 }
//
//		mobileEditText.setText(mobileNo);
//		Picasso.with(getActivity()).load(Prefs.with(getActivity()).getString("profilePic", "")).placeholder(R.drawable.placeholder_profilepic)
//				.transform(new com.vizavoo.utils.CircleTransform()).resize(200, 200).centerCrop().into(addPhoto);
//
//		return rootView;
//	}
//
//
//
//
//	/**
//	 * OnStart and OnStop method of this class.
//	 */
//	// Flurry implementation
//	public void onStart() {
//		super.onStart();
//		FlurryAgent.onStartSession(getActivity(), Httppost_Links.fluryKey);
//		FlurryAgent.onEvent(getActivity().getPackageName().getClass().getName() + " started");
//	}
//
//	@Override
//	public void onStop() {
//		super.onStop();
//		FlurryAgent.onEndSession(getActivity());
//	}
//
//	@Override
//	public void initialization() {
//		addPhoto = (ImageView) rootView.findViewById(R.id.profilePic);
//		topBar = (RelativeLayout) rootView.findViewById(R.id.topBar);
//		mainLayer = (LinearLayout) rootView.findViewById(R.id.mainLayer);
//
//		tittle = (TextView) rootView.findViewById(R.id.tittle);
//
//		textFname = (TextView) rootView.findViewById(R.id.textFname);
//		fName = (EditText) rootView.findViewById(R.id.Fname);
//
//		textLname = (TextView) rootView.findViewById(R.id.textLname);
//		lName = (EditText) rootView.findViewById(R.id.Lname);
//
//		textMobile = (TextView) rootView.findViewById(R.id.textMobile);
//		mobileEditText = (EditText) rootView.findViewById(R.id.mobile);
//
//		edit = (TextView) rootView.findViewById(R.id.edit);
//
//		closeButton = (RelativeLayout) rootView.findViewById(R.id.closeLay);
//		edit.setOnClickListener(this);
//		closeButton.setOnClickListener(this);
//		addPhoto.setOnClickListener(this);
//
//
//		Data1.setTextWatcherTo(fName,lName);
//
//
//		Validation validate=new Validation();
//		validate.setValidationFilter(TextType.PhoneNumber, mobileEditText);
//
//
//	}
//
//	@Override
//	public void fontSet() {
//		tittle.setTypeface(Data1.Medium);
//		textFname.setTypeface(Data1.Medium);
//		textLname.setTypeface(Data1.Medium);
//
//		fName.setTypeface(Data1.Medium);
//		lName.setTypeface(Data1.Medium);
//		mobileEditText.setTypeface(Data1.Medium);
//		textFname.setTypeface(Data1.Medium);
//		edit.setTypeface(Data1.Medium);
//
//	}
//
//	/* Work for uploading profile picture */
//	void openPictureUploadDialogue() {
//		String state = Environment.getExternalStorageState();
//		if (Environment.MEDIA_MOUNTED.equals(state)) {
//			mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
//		} else {
//			mFileTemp = new File(getActivity().getFilesDir(), TEMP_PHOTO_FILE_NAME);
//		}
//
//		final String[] items = new String[] { "Take photo", "From Gallery" };
//
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item,
//				items);
//
//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//		builder.setTitle("Select Picture To Upload!");
//
//		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//
//			public void onClick(DialogInterface dialog, int item) {
//
//				if (item == 0) {
//
//					takePicture();
//					if (dialog != null) {
//
//						dialog.cancel();
//
//					}
//
//				} else {
//
//					openGallery();
//					if (dialog != null) {
//
//						dialog.cancel();
//
//					}
//				}
//
//			}
//
//		});
//
//		final AlertDialog dialog = builder.create();
//		dialog.show();
//
//	}
//
//	private File filePic = null;
//
//	private void takePicture() {
//
//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		try {
//			Uri mImageCaptureUri = null;
//			String state = Environment.getExternalStorageState();
//			if (Environment.MEDIA_MOUNTED.equals(state)) {
//				mImageCaptureUri = Uri.fromFile(mFileTemp);
//			} else {
//				/*
//				 * The solution is taken from here:
//				 * http://stackoverflow.com/questions
//				 * /10042695/how-to-get-camera-result-as-a-uri-in-data-folder
//				 */
//				// mImageCaptureUri =
//				// InternalStorageContentProvider.CONTENT_URI;
//			}
//			intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
//			intent.putExtra("return-data", true);
//			startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
//		} catch (ActivityNotFoundException e) {
//
//			Log.d("ERROR!", "cannot take picture", e);
//		}
//	}
//
//	private void openGallery() {
//
//		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//		photoPickerIntent.setType("image/*");
//		startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
//	}
//
//	private void startCropImage() {
//
//		Intent intent = new Intent(getActivity(), CropImage.class);
//		intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
//		intent.putExtra(CropImage.SCALE, true);
//
//		intent.putExtra(CropImage.ASPECT_X, 2);
//		intent.putExtra(CropImage.ASPECT_Y, 2);
//
//		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
//	}
//
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//		if (resultCode != getActivity().RESULT_OK) {
//
//			return;
//		}
//
//		Bitmap bitmap;
//
//		switch (requestCode) {
//
//		case REQUEST_CODE_GALLERY:
//
//			try {
//
//				InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
//				FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
//				copyStream(inputStream, fileOutputStream);
//				fileOutputStream.close();
//				inputStream.close();
//
//				startCropImage();
//
//			} catch (Exception e) {
//
//				Log.e("error", "Error while creating temp file", e);
//			}
//
//			break;
//		case REQUEST_CODE_TAKE_PICTURE:
//
//			startCropImage();
//			break;
//		case REQUEST_CODE_CROP_IMAGE:
//
//			String path = data.getStringExtra(CropImage.IMAGE_PATH);
//			if (path == null) {
//
//				return;
//			}
//
//			bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
//
//			filePic = mFileTemp;
//
//			Picasso.with(getActivity()).load(mFileTemp).skipMemoryCache().transform(new CircleTransform())
//					.resize(200, 200).centerCrop().into(addPhoto);
//			break;
//		}
//		super.onActivityResult(requestCode, resultCode, data);
//	}
//
//	public static void copyStream(InputStream input, OutputStream output) throws IOException {
//
//		byte[] buffer = new byte[1024];
//		int bytesRead;
//		while ((bytesRead = input.read(buffer)) != -1) {
//			output.write(buffer, 0, bytesRead);
//		}
//	}
//
//	@Override
//	public void onClick(View v) {
//
//		mainLayer.clearAnimation();
//		topBar.clearAnimation();
//		switch (v.getId()) {
//
//
//		case R.id.profilePic:
//
//
//				openPictureUploadDialogue();
//
//			break;
//
//		case R.id.edit:
//				int flag = 1;
//
//				if (mobileEditText.getText().length() == 0) {
//					mobileEditText.setError("Enter mobile number");
//					flag = 0;
//					mobileEditText.requestFocus();
//				}
//				else if (!Data1.isPhoneNumberValid(mobileEditText.getText().toString())
//						|| mobileEditText.getText().length() < 10) {
//					mobileEditText.setError("Enter valid mobile number");
//					mobileEditText.requestFocus();
//					flag = 0;
//				}
//
//				if (lName.getText().toString().trim().length() == 0) {
//					lName.setError("Enter Last Name");
//					flag = 0;
//					lName.requestFocus();
//				}
//
//				if (fName.getText().toString().trim().length() == 0) {
//					fName.setError("Enter First Name");
//					fName.requestFocus();
//					flag = 0;
//				}
//
//				if (flag == 1) {
//					if (!Data1.Internetcheck(getActivity())) {
//						new DialogPopup().alertPopup(getActivity(), "ERROR!",
//								getActivity().getResources().getString(R.string.noInternet), false,0);
//					} else {
//
//						try {
//							APIsaveProfileData();
//						} catch (FileNotFoundException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//
//					}
//				} else {
//					edit.setText("DONE");
//
//					fName.setEnabled(true);
//					lName.setEnabled(true);
//					mobileEditText.setEnabled(true);
//					// editable(fName, true);
//					// editable(mobile, true);
//					// editable(lName,true);
//				}
//
//
//			break;
//
//		case R.id.closeLay:
//
//			Data1.hideKeyBoard(fName, getActivity());
//
//			Profile somefrag = (Profile) getFragmentManager().findFragmentByTag("main");
//			somefrag.refreshData();
//
//			getFragmentManager().popBackStack();
//
//			break;
//
//		default:
//			break;
//		}
//
//	}
//
//	private void APIsaveProfileData() throws FileNotFoundException {
//
//		Log.e("access_token", Prefs.with(getActivity()).getString("accessToken", ""));
//		Log.e("first_name", fName.getText().toString());
//		Log.e("last_name", lName.getText().toString());
//		Log.e("last_name", ",");
//		Log.e("email", Prefs.with(getActivity()).getString("email", ""));
//		Log.e("mobile", mobileEditText.getText().toString());
//
//		AsyncHttpClient client = new AsyncHttpClient();
//		RequestParams params = new RequestParams();
//		params.put("access_token", Prefs.with(getActivity()).getString("accessToken", ""));
//		params.put("first_name", fName.getText().toString());
//		params.put("last_name", lName.getText().toString());
//		params.put("email", Prefs.with(getActivity()).getString("email", ""));
//		params.put("mobile",  Data1.GetCountryZipCode(getActivity())+mobileEditText.getText().toString());
//
//		try {
//			if (filePic != null) {
//				params.put("profile_pic", mFileTemp);
//			}
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		Data1.loading_box(getActivity(), "Loading...", false);
//		client.setTimeout(Data1.timeout);
//
//		client.post(Httppost_Links.EDIT_PROFILE, params, new AsyncHttpResponseHandler() {
//			@Override
//			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
//				Data1.loading_box_stop();
//				Log.e("json Failure", "," + arg3.toString());
//			}
//
//			@Override
//			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
//				String response = new String(arg2);
//				Log.e("Register", "," + response);
//				Data1.loading_box_stop();
//				try {
//					JSONObject data = new JSONObject(response);
//
//					if (!data.getBoolean("status")) {
//
//						new DialogPopup().alertPopup(getActivity(), "ERROR!", data.getString("error"), false,data.getInt("flag"));
//					} else {
//						JSONObject jsonData = data.getJSONObject("data");
//						Prefs.with(getActivity()).save("profilePic", jsonData.getString("image"));
//						Prefs.with(getActivity()).save("mobile", Data1.GetCountryZipCode(getActivity())+mobileEditText.getText().toString());
//						Prefs.with(getActivity()).save("firstName", fName.getText().toString());
//						Prefs.with(getActivity()).save("lastName", lName.getText().toString());
//						Data1.hideKeyBoard(fName, getActivity());
//						Profile somefrag = (Profile) getFragmentManager().findFragmentByTag("main");
//						somefrag.refreshData();
//
//						FragmentManager fragmentmanager = getFragmentManager();
//
//						int a = getFragmentManager().getBackStackEntryCount();
//						if (a >= 1) {
//							fragmentmanager.popBackStack();
//						}
//
//					}
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//
//			}
//		});
//
//	}
//
//	// flag =true for changepassword
//
//}
