package clicklabs.app.butlers;
/*
Developed by Showket

 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clicklabs.butlers.R;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.google.android.gcm.GCMRegistrar;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.facebook.FacebookLoginCallback;
import clabs.butlers.facebook.FacebookLoginHelper;
import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.CircleTransform;
import clabs.butlers.utils.CommonUtil;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.Countries;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.Fonts;
import clabs.butlers.utils.ProfilePictureView;
import material.animations.MaterialDesignAnimations;
import models.FacebookFriendListModel;
import models.PromoteUserListModel;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


public class SplashActivity extends Activity {
    private static Twitter twitter;
    private static RequestToken requestToken;
    private static SharedPreferences mSharedPreferences;
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;
    RelativeLayout toplayout, bottomlayout;
    RelativeLayout loginlayout, backbtnregister, backbtn;
    RelativeLayout registerlayout, footerLayout,followAll;

    TextView fbSignin, twtSignin, emailSignin, signin, skipTxt, alreadylabel, registerwithlabel;
    EditText loginEmail, loginPassword, byButlerUsname, byButlerEmail, byButlerPassword;
    int onResult = 1;
    int backPress = 0;
    Button countryBtn, changeProfileImage;
    RadioGroup radioSexGroup;
    Spinner countrySpinner;
    MaterialEditText userNameText, passField, emailText, contactText, stateField, addressText;
    TextView userNameLabel, aboutLabel;
    CustomTextViewRegularFont genderLabel;
    CustomTextViewRegularFont aboutText, countryLabel;
    ImageView profileImageView,checkBtn;
    ArrayList<String> coutries;
    ArrayList<String> coutriescodes;
    Bundle extras;
    Bitmap bitmap;
    private Uri mImageCaptureUri;
    private File mFileTemp;
   // ProfilePictureView profilePicture;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    RelativeLayout backBtnMain, registerLayout, splashLayout;
    LinearLayout errorLayout, passwordRow, OfferPeopleLayout;
    CustomTextViewSemiBold headerTitle;
    CustomTextViewRegularFont registerlabelTwitter, registerLabelFacebook;
    RelativeLayout NextBtnMain, nextRegisterLay, backBtnOfferList, nextOfferBtn;
    String name = "", emailStr = "", fbId = "", fbAccessToken = "", passwordStr = "", phone = "", country = "", countryCode = "", city = "", address = "", gender = "1",
            profilePic = "", aboutYou = "", picStatus = "0", deviceToken = "", twiId = "", twiAccessToken = "", twitterSecret = "";
    int registerVia = 0, loginVia = 0, LoginFlag = 0;
    CustomTextViewRegularFont facebookLogin, twitterLogin, forgotPassWord;
    CustomEditTextRegularFont searchBarOfferList;
    String proPicture;
    Button loginemailcrossbtn, registeremailcrossbtn;
//    boolean ifItsTwiterLogin = false;
//    boolean ifItsFacebookLogin = false;
    ListView offerListview;
    OfferAdapter offeradapter;
    String followStatus = "0";
    RelativeLayout butlerlogo;
    public static ArrayList<FacebookFriendListModel> facebookList;
    public static FacebookFriendListModel FACEBOOK_FRIENDS;
    public static String nextPageUrl="";
    Boolean loadingMoreFacebookFriends = true;

    View footerView;
    int fbOffSet = 29;
    int followAllflag=0;
    ArrayList<String> userIdsCommaSeparated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        getWindow().setBackgroundDrawableResource(R.drawable.splash_screen);
        loginlayout = (RelativeLayout) findViewById(R.id.loginlayout);
        NextBtnMain = (RelativeLayout) findViewById(R.id.nextButton);
        footerLayout = (RelativeLayout) findViewById(R.id.footerLayout);
        registerlayout = (RelativeLayout) findViewById(R.id.registerlayout);
        splashLayout = (RelativeLayout) findViewById(R.id.splashLayout);
        followAll = (RelativeLayout) findViewById(R.id.followAllLayy);
        fbSignin = (TextView) findViewById(R.id.signinfb);
        twtSignin = (TextView) findViewById(R.id.signintwitter);
        emailSignin = (TextView) findViewById(R.id.signingmail);
        signin = (TextView) findViewById(R.id.signin);
        skipTxt = (TextView) findViewById(R.id.skip);
        checkBtn = (ImageView) findViewById(R.id.checkBtnFollow);
        alreadylabel = (TextView) findViewById(R.id.alreadylabel);
        registerwithlabel = (TextView) findViewById(R.id.registerwithlabel);
        loginEmail = (EditText) findViewById(R.id.loginemailfield);
        loginPassword = (EditText) findViewById(R.id.loginpassfield);
        byButlerUsname = (EditText) findViewById(R.id.regusernamefield);
        byButlerEmail = (EditText) findViewById(R.id.regemailfield);
        byButlerPassword = (EditText) findViewById(R.id.regpassfield);
        toplayout = (RelativeLayout) findViewById(R.id.toplogolay);
        bottomlayout = (RelativeLayout) findViewById(R.id.bottomlayrel);
        backbtn = (RelativeLayout) findViewById(R.id.backbtn);
        backbtnregister = (RelativeLayout) findViewById(R.id.backbtnregister);
        registerLayout = (RelativeLayout) findViewById(R.id.registerLayout);
        butlerlogo = (RelativeLayout) findViewById(R.id.butlerlogo);
//        passwordRow = (LinearLayout) findViewById(R.id.row_pass);
        loginemailcrossbtn = (Button) findViewById(R.id.loginemailcrossbtn);
        registeremailcrossbtn = (Button) findViewById(R.id.registeremailcrossbtn);

        /**facebook
         *
         */
        backBtnOfferList = (RelativeLayout) findViewById(R.id.backBtnOfferList);
        nextOfferBtn = (RelativeLayout) findViewById(R.id.nextOfferBtn);
        searchBarOfferList = (CustomEditTextRegularFont) findViewById(R.id.searchBarOfferList);
        registerlabelTwitter = (CustomTextViewRegularFont) findViewById(R.id.twtlabelregister);
        registerLabelFacebook = (CustomTextViewRegularFont) findViewById(R.id.fblabelregister);
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.load_more_view, null, false);
        userIdsCommaSeparated = new ArrayList<String>();
        searchBarOfferList.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                String text = searchBarOfferList.getText().toString().toLowerCase();

                offeradapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        followAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(facebookList.size()>0) {
                    if (followAllflag == 0) {
                        checkBtn.setBackgroundResource(R.drawable.btn_chkbox_pressed);
                        followAllflag = 1;

                        for (int i = 0; i < facebookList.size(); i++) {
                            facebookList.get(i).setOffered(true);
                            if(!userIdsCommaSeparated.contains(facebookList.get(i).getFbId()))
                                userIdsCommaSeparated.add(facebookList.get(i).getFbId());
                        }

                    } else {

                        checkBtn.setBackgroundResource(R.drawable.btn_chkbox_normal);
                        followAllflag = 0;
                        for (int i = 0; i < facebookList.size(); i++)
                            facebookList.get(i).setOffered(false);
                        userIdsCommaSeparated.clear();
                    }
                    offeradapter.notifyDataSetChanged();
                }

            }


        });

        offerListview = (ListView) findViewById(R.id.offerListview);
        //Here is where the magic happens
        offerListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            //useless here, skip!
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            //dumdumdum
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //what is the bottom iten that is visible
                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already ? Load more !
                if ((lastInScreen == totalItemCount) && !(loadingMoreFacebookFriends)) {
                    if (AppStatus.getInstance(getApplicationContext())
                            .isOnline(getApplicationContext())) {
                        GetFacebookListAPi();
                        Log.v("Load more", "Load more");

                    }
                }
            }
        });
        OfferPeopleLayout = (LinearLayout) findViewById(R.id.OfferPeopleLayout);

        /**
         * Add fonts styles
         */

        skipTxt.setTypeface(Fonts.getFontTextMedium(SplashActivity.this));
        signin.setTypeface(Fonts.getFontTextMedium(SplashActivity.this));
        fbSignin.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        twtSignin.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        emailSignin.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        alreadylabel.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        registerwithlabel.setTypeface(Fonts.getFontTextLight(SplashActivity.this));
        loginEmail.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        loginPassword.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        byButlerUsname.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        byButlerEmail.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));
        byButlerPassword.setTypeface(Fonts.getFontTextRegularG3(SplashActivity.this));


        changeProfileImage = (Button) findViewById(R.id.changeProfileImageBtn);
        //userNameLabel = (TextView) findViewById(R.id.userNameLabel);
        userNameText = (MaterialEditText) findViewById(R.id.usernamefield);

        emailText = (MaterialEditText) findViewById(R.id.emailfieldRegister);

        contactText = (MaterialEditText) findViewById(R.id.contactfieldRegister);
        countryLabel = (CustomTextViewRegularFont) findViewById(R.id.countryLabel);


        addressText = (MaterialEditText) findViewById(R.id.addressfieldRegister);
        genderLabel = (CustomTextViewRegularFont) findViewById(R.id.genderLabel);
        aboutText = (CustomTextViewRegularFont) findViewById(R.id.aboutfieldRegister);

        // aboutLabel = (TextView) findViewById(R.id.aboutLabel);
        passField = (MaterialEditText) findViewById(R.id.passwordfield);
        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);
        profileImageView = (ImageView) findViewById(R.id.userImage);
        backBtnMain = (RelativeLayout) findViewById(R.id.backBtnMain);

        stateField = (MaterialEditText) findViewById(R.id.stateField);
        facebookLogin = (CustomTextViewRegularFont) findViewById(R.id.facebookLogin);
        twitterLogin = (CustomTextViewRegularFont) findViewById(R.id.twitterLogin);
        forgotPassWord = (CustomTextViewRegularFont) findViewById(R.id.forgotpwdLabel);
        headerTitle = (CustomTextViewSemiBold) findViewById(R.id.header);
        nextRegisterLay = (RelativeLayout) findViewById(R.id.nextRegister);
        errorLayout = (LinearLayout) findViewById(R.id.errorLayout);
       // profilePicture = (ProfilePictureView) findViewById(R.id.profilePicture);


        /**
         * Back from home Screen
         */

        if (Config.getSKIP_MODE()) {
            butlerlogo.setVisibility(View.GONE);
            extras = getIntent().getExtras();
            if (extras != null) {
                if (extras.getString("loginVia").equals("facebook")) {
                   // ifItsFacebookLogin = false;
                    onResult = 1;
                    new FacebookLoginHelper().openFacebookSession(SplashActivity.this, facebookLoginCallback, true);
                } else if (extras.getString("loginVia").equals("email")) {
//                    if (Config.isLollyPop()) {
//                        MaterialDesignAnimations.appllyLoadInAnimation(registerlayout);
//                        MaterialDesignAnimations.applyHideAnimation(bottomlayout);
//                    } else
                    {
                        backPress = 1;
                        LoginFlag=0;
                        showRegister();
                        toplayout.setVisibility(View.VISIBLE);
                        footerLayout.setVisibility(View.VISIBLE);
                    }
                } else if (extras.getString("loginVia").equals("twitter")) {
                   // ifItsTwiterLogin = false;
                    onResult = 2;
                    twitter = TwitterFactory.getSingleton();
                    inittwitter();
                    loginToTwitter();
                } else if (extras.getString("loginVia").equals("login")) {
//                    if (Config.isLollyPop()) {
//                        MaterialDesignAnimations.appllyLoadInAnimation(loginlayout);
//                        MaterialDesignAnimations.applyHideAnimation(bottomlayout);
//
//                    } else
//                    {
                        backPress = 2;
                        LoginFlag = 1;/**facebook listview backbutton**/


                        showLogin();
                        toplayout.setVisibility(View.VISIBLE);
                        footerLayout.setVisibility(View.VISIBLE);
                    //}

                }
            }
        }


        /**
         *
         * Check if you already logged in
         */

        try {
            RegisterWithGCM();
        } catch (Exception e) {
            Log.i("device_token ", deviceToken + "..");
        }

        SharedPreferences pref = getSharedPreferences("ButlerAppPref", 0);
        String acctoken = pref.getString("app_access_token", "");
        /***AccessToke Api Hit first time****/

        if (!Config.getSKIP_MODE()) {
            if (acctoken.equals("")) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        StartAnimations();
                    }
                }, 1000);
            } else {
                bottomlayout.setVisibility(View.GONE);
                Config.setAPP_ACCESS_TOKEN(acctoken);

                if (!AppStatus.getInstance(SplashActivity.this)
                        .isOnline(SplashActivity.this)) {
                    MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.internetConnectionError));

                } else {

                    GetAccessTokenApi();
                }

            }
        } else {
            Config.setSKIP_MODE(false);
            extras = getIntent().getExtras();
            if (extras != null) {
                if ((extras.getString("loginVia").equals("facebook")) || (extras.getString("loginVia").equals("twitter"))) {

                    toplayout.setVisibility(View.VISIBLE);
                    bottomlayout.setVisibility(View.VISIBLE);
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            StartAnimations();
//                        }
//                    }, 1000);
                }
            }
        }
        //  Log.v("APP ACCESS TOKEN",acctoken+" :check it");


        if (registerVia == 3) {
            passField.setVisibility(View.VISIBLE);
        } else {
            passField.setVisibility(View.GONE);
        }


        coutries = new ArrayList<String>();
        coutriescodes = new ArrayList<String>();
        extras = getIntent().getExtras();
        countryBtn = (Button) findViewById(R.id.countryBtn);


        /****gallery image**/
        mFileTemp = CommonUtil.getTempImageFile();
        if (mFileTemp.exists())
            mFileTemp.delete();
        mFileTemp = CommonUtil.getTempImageFile();

        /**
         * facebook login  email screen
         */
        registerLabelFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               // ifItsFacebookLogin = false;
                onResult = 1;
                new FacebookLoginHelper().openFacebookSession(SplashActivity.this, facebookLoginCallback, true);
            }
        });

        fbSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               // ifItsFacebookLogin = false;
                onResult = 1;
                new FacebookLoginHelper().openFacebookSession(SplashActivity.this, facebookLoginCallback, true);
            }
        });
        /**
         * twitter click to login
         */
        twtSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ifItsTwiterLogin = false;
                onResult = 2;
                twitter = TwitterFactory.getSingleton();
                inittwitter();
                loginToTwitter();
            }
        });
        /**
         * twitter register email screen
         */
        registerlabelTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ifItsTwiterLogin = false;
                onResult = 2;
                twitter = TwitterFactory.getSingleton();
                inittwitter();
                loginToTwitter();
            }
        });
        /**
         * email click to login
         */
        emailSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // startActivity(new Intent(SplashActivity.this,HomeActivity.class));

//                if (Config.isLollyPop()) {
//                    MaterialDesignAnimations.appllyLoadInAnimation(registerlayout);
//                    MaterialDesignAnimations.applyHideAnimation(bottomlayout);
//                } else
                {
                    backPress = 1;
                    LoginFlag=0;
                    showRegister();
                    footerLayout.setVisibility(View.VISIBLE);
                }

            }
        });
        /**
         * sign in click
         */
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (Config.isLollyPop()) {
//                    MaterialDesignAnimations.appllyLoadInAnimation(loginlayout);
//                    MaterialDesignAnimations.applyHideAnimation(bottomlayout);
//
//                } else
               // {
                    backPress = 2;
                    LoginFlag = 1;/**facebook listview backbutton**/


                    showLogin();
                    footerLayout.setVisibility(View.VISIBLE);
              //  }


            }
        });

        /**
         * Back btn offer list
         */

        backBtnOfferList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress = 3;
                footerLayout.setVisibility(View.VISIBLE);
                OfferPeopleLayout.setVisibility(View.GONE);


            }
        });
        /**
         * backbtn click
         */
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Data.hideSoftKeyboard(SplashActivity.this);
                } catch (Exception e) {

                }
//                if (Config.isLollyPop()) {
//                    MaterialDesignAnimations.appllyLoadInAnimation(bottomlayout);
//                    MaterialDesignAnimations.applyHideAnimation(loginlayout);
//                } else
                {
                    backPress = 0;
                    hideLogin();
                    footerLayout.setVisibility(View.GONE);
                }

            }
        });

        /**facebook listview backbutton**/

        backBtnOfferList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress = 1;
                footerLayout.setVisibility(View.VISIBLE);
                OfferPeopleLayout.setVisibility(View.GONE);


            }
        });


        /**facebook listview backbutton**/

        nextOfferBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Data.hideSoftKeyboard(SplashActivity.this);
                if(userIdsCommaSeparated.size()>0) {
                    if (!AppStatus.getInstance(SplashActivity.this)
                            .isOnline(SplashActivity.this)) {
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.noInternet));
                    } else {
                        FollowFriendApi();
                    }
                }
                else
                {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    finish();
                }


            }
        });

        /**
         * Register Layout Via all backbtn
         */
        backBtnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Data.hideSoftKeyboard(SplashActivity.this);
                } catch (Exception e) {

                }
                hideRegisterWithLayout();
                registerVia = 0;

            }
        });

        backbtnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Data.hideSoftKeyboard(SplashActivity.this);
                } catch (Exception e) {

                }
//                if (Config.isLollyPop()) {
//                    MaterialDesignAnimations.applyHideAnimation(registerlayout);
//                    MaterialDesignAnimations.appllyLoadInAnimation(bottomlayout);
//                } else
                {
                    backPress = 0;
                    hideRegister();
                    footerLayout.setVisibility(View.GONE);
                }
            }
        });
        /**
         * skip click
         */
        skipTxt.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {


                Config.setSKIP_MODE(true);
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                               finish();

            }
        });

        /**
         *facebook login
         */
        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ifItsFacebookLogin = true;
                onResult = 1;
                new FacebookLoginHelper().openFacebookSession(SplashActivity.this, facebookLoginCallback, true);


            }
        });

        /**
         *facebook login
         */
        twitterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ifItsTwiterLogin = true;
                onResult = 2;
                twitter = TwitterFactory.getSingleton();
                inittwitter();
                loginToTwitter();

            }
        });

        NextBtnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginFlag == 1) {
                    if (loginEmail.getText().length() == 0) {
                        loginEmail.requestFocus();
                        loginEmail.setError(getResources().getString(R.string.emailRequired));
                    } else if ((Data.validEmail(loginEmail.getText().toString())) == false) {
                        loginEmail.requestFocus();
                        loginEmail.setError(getResources().getString(R.string.emailValid));
                    } else if (loginPassword.getText().length() == 0) {
                        loginPassword.requestFocus();
                        loginPassword.setError(getResources().getString(R.string.passwordRequired));
                    } else {
                        if (!AppStatus.getInstance(SplashActivity.this)
                                .isOnline(SplashActivity.this)) {
                            MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            Data.hideSoftKeyboard(SplashActivity.this);
                            loginVia = 3;
                            LoginViaAll();
                        }


                    }
                } else {
                    if (byButlerUsname.getText().length() == 0)
                        byButlerUsname.setError(getResources().getString(R.string.userRequired));
                    else if (byButlerEmail.getText().length() == 0) {
                        byButlerEmail.setError(getResources().getString(R.string.emailRequired));
                    } else if ((Data.validEmail(byButlerEmail.getText().toString())) == false)

                    {
                        byButlerEmail.setError(getResources().getString(R.string.emailValid));
                    } else if (byButlerPassword.getText().length() == 0) {
                        byButlerPassword.setError(getResources().getString(R.string.passwordRequired));
                    } else {
                        backPress = 3;
                        Data.hideSoftKeyboard(SplashActivity.this);
                       // profilePicture.setVisibility(View.GONE);
                        userNameText.setText(byButlerUsname.getText().toString());
                        emailText.setText(byButlerEmail.getText().toString());
                        passField.setText(byButlerPassword.getText().toString());
                        splashLayout.setVisibility(View.GONE);
                        registerLayout.setVisibility(View.VISIBLE);
                        passField.setVisibility(View.VISIBLE);
                        registerVia = 3;
                        headerTitle.setText(getResources().getString(R.string.butlerRegister));

                    }
                }
            }
        });
        /**** gender click***/
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        RadioButton radioButtonMale = (RadioButton) findViewById(R.id.radioMale);
        RadioButton radioButtonFemale = (RadioButton) findViewById(R.id.radioFemale);

        radioButtonMale.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = radioSexGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioSexButton = (RadioButton) findViewById(selectedId);

                if (radioSexButton.getText().equals("Male"))
                    gender = "1";
                else
                    gender = "2";

            }

        });
        radioButtonFemale.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = radioSexGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
                if (radioSexButton.getText().equals("Male"))
                    gender = "1";
                else
                    gender = "2";


            }

        });


        /*
        REgister api call
         */
        nextRegisterLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (registerVia == 3) {
                    if (passField.getText().length() == 0) {
                        passField.setError(getResources().getString(R.string.passwordRequired));

                    } else if (passField.getText().length() < 6) {
                        passField.setError(getResources().getString(R.string.passLength));

                    }
                }

                if (userNameText.getText().length() == 0)
                    userNameText.setError(getResources().getString(R.string.userRequired));
                else if (emailText.getText().length() == 0)
                    emailText.setError(getResources().getString(R.string.emailRequired));
                else if ((Data.validEmail(emailText.getText().toString())) == false)
                    emailText.setError(getResources().getString(R.string.emailValid));
                else if (contactText.getText().length() == 0)
                    contactText.setError(getResources().getString(R.string.contactRequired));
                else if (countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString().equalsIgnoreCase("Choose country"))
                    MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.countryRequired));
                else if (stateField.getText().length() == 0)
                    stateField.setError(getResources().getString(R.string.stateRequired));
                else if (addressText.getText().length() == 0)
                    addressText.setError(getResources().getString(R.string.addressRequired));
                else {
                    if (!AppStatus.getInstance(SplashActivity.this)
                            .isOnline(SplashActivity.this)) {
                        AppStatus.errorDialog(SplashActivity.this);

                    } else {
                        Data.hideSoftKeyboard(SplashActivity.this);
                        RegisterViaAll();
                    }
                }


            }
        });
        /** chage profile image button***/
        changeProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryCameraPoup(SplashActivity.this);


            }
        });

        loginEmail.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (loginEmail.getText().toString().length() >= 1)
                    loginemailcrossbtn.setVisibility(View.VISIBLE);
                else
                    loginemailcrossbtn.setVisibility(View.GONE);

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        byButlerEmail.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (byButlerEmail.getText().toString().length() >= 1)
                    registeremailcrossbtn.setVisibility(View.VISIBLE);
                else
                    registeremailcrossbtn.setVisibility(View.GONE);

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        /**
         * clear email id in register email field
         */

        loginemailcrossbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginEmail.setText("");
                loginemailcrossbtn.setVisibility(View.GONE);

            }
        });
        registeremailcrossbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byButlerEmail.setText("");
                registeremailcrossbtn.setVisibility(View.GONE);

            }
        });

        /**
         * forgot password
         */
        forgotPassWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.hideSoftKeyboard(SplashActivity.this);
                forgotPasswordPopup(SplashActivity.this, getResources().getString(R.string.forgotPass), 1);

            }
        });

        String[] isoCountryCodes = Locale.getISOCountries();
        coutries.clear();
        coutriescodes.clear();
        coutries.add("Choose country");
        coutriescodes.add("NOTA");
        for (String countryCode : isoCountryCodes) {
            Locale locale = new Locale("", countryCode);
            String countryName = locale.getDisplayCountry();


            coutries.add(locale.getDisplayCountry());
            coutriescodes.add(countryCode);
        }

        /**
         * Set county code first time
         */
        // contactText.setText(getAreaCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()));

        ArrayAdapter<String> coutryAdapter = new ArrayAdapter<String>(SplashActivity.this, R.layout.country_spinner_item, Countries.countryNames);


        //ArrayAdapter coutryAdapter = new ArrayAdapter(SplashActivity.this, R.layout.country_spinner_item, coutries);
        coutryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(coutryAdapter);


        /*** country btn***/
        countryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                countrySpinner.performClick();


            }
        });


        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
                // On selecting a spinner item
                if (!(countrySpinner.getSelectedItem().equals("Choose country"))) {

                    String contact = contactText.getText().toString();
                    if(contact.contains(",")) {
                    if (contact.length() != 0) {

                            String previousPhone[] = contact.split(",");
                            if (previousPhone[1].length() != 0)
                                contactText.setText("+" + getAreaCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()) + ", " + previousPhone[1]);
                            else
                                contactText.setText("+" + getAreaCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()) + ", " + contactText.getText());

                        }
                    }
                    else
                        contactText.setText("+" + getAreaCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()) + ", " + contactText.getText());

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        /***about label click***/
//
        aboutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Data.hideSoftKeyboard(SplashActivity.this);
                aboutYouPopup(SplashActivity.this);


            }
        });

       Data.hideSoftKeyboard(SplashActivity.this);

    }

    FacebookLoginCallback facebookLoginCallback = new FacebookLoginCallback() {
        @Override
        public void facebookLoginDone() {
            // TODO do work after data is not null
            if (FacebookLoginHelper.USER_DATA != null) {
                headerTitle.setText("Register With facebook");
                backPress = 3;
                userNameText.setText(FacebookLoginHelper.USER_DATA.userName);
                emailText.setText(FacebookLoginHelper.USER_DATA.userEmail);
                fbId = FacebookLoginHelper.USER_DATA.fbId;
                emailStr = FacebookLoginHelper.USER_DATA.userEmail;


                RadioButton male = (RadioButton) findViewById(R.id.radioMale);
                RadioButton female = (RadioButton) findViewById(R.id.radioFemale);
                if (FacebookLoginHelper.USER_DATA.gender.equalsIgnoreCase("Male")) {
                    male.setChecked(true);
                    female.setChecked(false);
                    gender = "1";
                } else {
                    male.setChecked(false);
                    female.setChecked(true);
                    gender = "2";
                }

                fbAccessToken = FacebookLoginHelper.USER_DATA.accessToken;

//                if (ifItsFacebookLogin) {
//                    if (emailStr.equals("")) {
//                        loginVia = 1;
//                        forgotPasswordPopup(SplashActivity.this, getResources().getString(R.string.enterEmail), 2);
//
//                    } else {
                        if (!AppStatus.getInstance(SplashActivity.this)
                                .isOnline(SplashActivity.this)) {
                            AppStatus.errorDialog(SplashActivity.this);

                        } else {
                            loginVia = 1;
                            LoginViaAll();
                        }
//                    }
//                } else
//
                {

                    try {
                        profileImageView.setVisibility(View.VISIBLE);
                        String profile_pic="https://graph.facebook.com/"+FacebookLoginHelper.USER_DATA.fbId+ "/picture?width=160&height=160";
                        Picasso.with(SplashActivity.this).load(profile_pic).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);

                        // profilePicture.setVisibility(View.VISIBLE);
                      //  profilePicture.setProfileId(FacebookLoginHelper.USER_DATA.fbId);
                        registerVia = 1;
                    } catch (Exception e) {

                    }

                    /**will be done after Api Hit**/

                   // splashLayout.setVisibility(View.GONE);
                   // registerLayout.setVisibility(View.VISIBLE);

                }


            } else {
            }


        }
    };

    /**
     * change profile image from gallery**
     */
    private void showSelection(int idx) {
        Intent intent = null;
        if (idx == 0) {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            mImageCaptureUri = Uri
                    .fromFile(new File(android.os.Environment
                            .getExternalStorageDirectory(),
                            CommonUtil.GAME_IMAGE_NAME));

            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);

            try {
                intent.putExtra("return-data", true);

                startActivityForResult(intent, PICK_FROM_CAMERA);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else if (idx == 1) {
            intent = new Intent();

            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);

            startActivityForResult(
                    Intent.createChooser(intent, "Complete action using"),
                    PICK_FROM_FILE);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void inittwitter() {
            /* initializing twitter parameters from string.xml */
        initTwitterConfigs();

		/* Enabling strict mode */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


		/* Check if required twitter keys are set */
        if (TextUtils.isEmpty(consumerKey) || TextUtils.isEmpty(consumerSecret)) {
            Toast.makeText(this, "Twitter key and secret not configured",
                    Toast.LENGTH_SHORT).show();
            return;
        }

		/* Initialize application preferences */
        mSharedPreferences = getSharedPreferences(Config.getPrefName(), 0);

        boolean isLoggedIn = mSharedPreferences.getBoolean(Config.getPrefKeyTwitterLogin(), false);

        if (isLoggedIn) {
            String username = mSharedPreferences.getString(Config.getPrefUserName(), "");
            proPicture = mSharedPreferences.getString(Config.getPrefUserImage(), "");
            String twtid = mSharedPreferences.getString(Config.getPrefUserId(), "");
            headerTitle.setText("Register With twitter");
            backPress = 3;
            userNameText.setText(username);
            emailText.setText("");
            twiId = twtid;
            twiAccessToken = mSharedPreferences.getString(Config.getPrefKeyOauthToken(), "");
            twitterSecret = mSharedPreferences.getString(Config.getPrefKeyOauthSecret(), "");
//            if (ifItsTwiterLogin) {
//                if (emailStr.equals("")) {
//                    loginVia = 2;
//                    forgotPasswordPopup(SplashActivity.this, getResources().getString(R.string.enterEmail), 3);
//
//                } else {
//                    if (!AppStatus.getInstance(SplashActivity.this)
//                            .isOnline(SplashActivity.this)) {
//                        AppStatus.errorDialog(SplashActivity.this);
//
//                    } else {
//                        loginVia = 2;
//                        LoginViaAll();
//                    }
//                }
//            } else

            {
                if (!AppStatus.getInstance(SplashActivity.this)
                        .isOnline(SplashActivity.this)) {
                    AppStatus.errorDialog(SplashActivity.this);

                } else {
                    loginVia = 2;
                    LoginViaAll();
                }
                try {
                    //profileImageView.setVisibility(View.VISIBLE);
                   // profilePicture.setVisibility(View.GONE);
                    Picasso.with(SplashActivity.this).load(proPicture).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);
                    registerVia = 2;
                } catch (Exception e) {

                }

//                splashLayout.setVisibility(View.GONE);
//                registerLayout.setVisibility(View.VISIBLE);
            }


//            try {
//                profileImageView.setVisibility(View.VISIBLE);
//                profilePicture.setVisibility(View.GONE);
//                Picasso.with(SplashActivity.this).load(profileImage).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);
//                registerVia = 2;
//            } catch (Exception e) {
//
//            }
//
//            splashLayout.setVisibility(View.GONE);
//            registerLayout.setVisibility(View.VISIBLE);

        } else {


            Uri uri = getIntent().getData();

            if (uri != null && uri.toString().startsWith(callbackUrl)) {

                String verifier = uri.getQueryParameter(oAuthVerifier);

                try {

					/* Getting oAuth authentication token */
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

					/* Getting user id form access token */
                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    final String username = user.getName();
                    String url = user.getProfileImageURL();

					/* save updated token */
                    saveTwitterInfo(accessToken);


                    // userName.setText(getString(R.string.hello) + username);


                } catch (Exception e) {
                    Log.e("Failed to login Twitter!!", e.getMessage());
                }
            }

        }
    }


    //    /***
//     * shows login layout
//     */
    private void showLogin() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);

        loginlayout.setAnimation(animationFadeIn);
        loginlayout.setVisibility(View.VISIBLE);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        bottomlayout.clearAnimation();
        bottomlayout.setAnimation(animationFadeOut);
        bottomlayout.setVisibility(View.INVISIBLE);
    }

    //    /***
//     * hides login layout
//     */
    private void hideLogin() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        loginlayout.setAnimation(animationFadeOut);
        loginlayout.setVisibility(View.INVISIBLE);

        bottomlayout.clearAnimation();
        bottomlayout.setAnimation(animationFadeIn);
        bottomlayout.setVisibility(View.VISIBLE);
    }

    //
//    /***
//     * shows login layout
//     */
    private void showRegister() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);

        registerlayout.setAnimation(animationFadeIn);
        registerlayout.setVisibility(View.VISIBLE);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        bottomlayout.clearAnimation();
        bottomlayout.setAnimation(animationFadeOut);
        bottomlayout.setVisibility(View.INVISIBLE);


    }

    /**
     * hide register
     */

    private void hideRegister() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        registerlayout.setAnimation(animationFadeOut);
        registerlayout.setVisibility(View.INVISIBLE);

        bottomlayout.clearAnimation();
        bottomlayout.setAnimation(animationFadeIn);
        bottomlayout.setVisibility(View.VISIBLE);
    }


    /**
     * hide register
     */

    private void hideRegisterWithLayout() {
        final Animation animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        registerLayout.setAnimation(animationFadeOut);
        registerLayout.setVisibility(View.INVISIBLE);


        splashLayout.clearAnimation();
        splashLayout.setAnimation(animationFadeIn);
        splashLayout.setVisibility(View.VISIBLE);
        backPress = 0;
    }


    /*
    splash animation for logo
     */
    private void StartAnimations() {


        toplayout.setVisibility(View.VISIBLE);
        bottomlayout.setVisibility(View.VISIBLE);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadeout);
        butlerlogo.setAnimation(animationFadeOut);
        butlerlogo.setVisibility(View.INVISIBLE);
        Animation animup = AnimationUtils.loadAnimation(this, R.anim.alpha);
        animup.reset();
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.toplogolay);
        relativeLayout.clearAnimation();
        relativeLayout.startAnimation(animup);
        animup = AnimationUtils.loadAnimation(this, R.anim.translateup);
        animup.reset();
        Animation animdown = AnimationUtils.loadAnimation(this, R.anim.translatedown);
        animdown.reset();
        LinearLayout boRelativeLayout = (LinearLayout) findViewById(R.id.bottomlay);
        relativeLayout.clearAnimation();
        boRelativeLayout.clearAnimation();
        relativeLayout.startAnimation(animdown);
        boRelativeLayout.startAnimation(animup);


    }

    /**
     * Saving user information, after user is authenticated for the first time.
     * You don't need to show user to login, until user has a valid access toen
     */
    private void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();
            String userimage = user.getProfileImageURL();
            long userid = user.getId();

			/* Storing oAuth tokens to shared preferences */
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(Config.getPrefKeyOauthToken(), accessToken.getToken());
            e.putString(Config.getPrefKeyOauthSecret(), accessToken.getTokenSecret());
            e.putBoolean(Config.getPrefKeyTwitterLogin(), true);
            e.putString(Config.getPrefUserName(), username);
            e.putString(Config.getPrefUserImage(), userimage);
            e.putString(Config.getPrefUserId(), userid + "");
            e.commit();

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }
    /*

    Twitter Login Method
     */

    private void loginToTwitter() {
        boolean isLoggedIn = mSharedPreferences.getBoolean(Config.getPrefKeyTwitterLogin(), false);

        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);

                /**
                 *  Loading twitter login page on webview for authorization
                 *  Once authorized, results are received at onActivityResult
                 *  */
                final Intent intent = new Intent(getApplicationContext(), TwitterWebViewActivity.class);
                intent.putExtra(TwitterWebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, Config.getWebviewRequestCode());

            } catch (TwitterException e) {
                e.printStackTrace();
            }
        } else {

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (onResult == 1) {
            try {
                super.onActivityResult(requestCode, resultCode, data);
                Session.getActiveSession().onActivityResult(this, requestCode,
                        resultCode, data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (onResult == 2) {

            if (resultCode == Activity.RESULT_OK) {
                String verifier = data.getExtras().getString(oAuthVerifier);
                try {
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                    twitterSecret = accessToken.getTokenSecret();


                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    String username = user.getName();
                    proPicture = user.getProfileImageURL();
                    saveTwitterInfo(accessToken);
                    headerTitle.setText("Register With twitter");
                    backPress = 3;
                    userNameText.setText(username);
                    emailText.setText("");
                    twiId = userID + "";
                    twiAccessToken = accessToken.getToken();
//                    if (ifItsTwiterLogin) {
//                        if (emailStr.equals("")) {
//                            loginVia = 2;
//                            forgotPasswordPopup(SplashActivity.this, getResources().getString(R.string.enterEmail), 3);
//
//                        }
//                        {
//                            if (!AppStatus.getInstance(SplashActivity.this)
//                                    .isOnline(SplashActivity.this)) {
//                                AppStatus.errorDialog(SplashActivity.this);
//
//                            } else {
//                                loginVia = 2;
//                                LoginViaAll();
//                            }
//                        }
//                    } else {
                    if (!AppStatus.getInstance(SplashActivity.this)
                            .isOnline(SplashActivity.this)) {
                        AppStatus.errorDialog(SplashActivity.this);

                    } else {
                        loginVia = 2;
                        LoginViaAll();
                    }
                        try {
                           // profileImageView.setVisibility(View.VISIBLE);
                           // profilePicture.setVisibility(View.GONE);
                            Picasso.with(SplashActivity.this).load(proPicture).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);
                            registerVia = 2;
                        } catch (Exception e) {

                        }

//                        splashLayout.setVisibility(View.GONE);
//                        registerLayout.setVisibility(View.VISIBLE);
                   // }

                } catch (Exception e) {
                    Log.e("Twitter Login Failed", e.getMessage());
                }
            }
        }

        /***gallery profile pic chnage
         *
         *
         *
         */
//            if (resultCode != RESULT_OK)
//                return;

        else if (onResult == 3) {
            switch (requestCode) {

                case PICK_FROM_FILE:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(
                                data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(
                                mFileTemp);
                        CommonUtil.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    reload();
                    // new LoadBlurImageChange().execute("");
                    break;

                case PICK_FROM_CAMERA:

                    try {


                        if (data != null) {
                            Bundle extras = data.getExtras();
                            if (extras.containsKey("data")) {
                                bitmap = (Bitmap) extras.get("data");
                            } else {
                                bitmap = getBitmapFromUri();
                            }
                        } else {
                            bitmap = getBitmapFromUri();
                        }
                        reload();
                        //   new LongOperation().execute("");

                    } catch (Exception e) {
                        Log.v("camera image exception", e.toString());
                    }

                    break;
            }

        }

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (!facebookLogin) {
//            if (resultCode == Activity.RESULT_OK) {
//                String verifier = data.getExtras().getString(oAuthVerifier);
//                try {
//                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
//
//                    long userID = accessToken.getUserId();
//                    final User user = twitter.showUser(userID);
//                    String username = user.getName();
//
//
//                    saveTwitterInfo(accessToken);
//
////                Intent i = new Intent(getApplicationContext(), SplashActivity.class);
////                i.putExtra("fbUserName",username);
////                i.putExtra("fbUserEmail","");
////                i.putExtra("profilePicture","");
////                startActivity(i);
//
//
//                } catch (Exception e) {
//                    Log.e("Twitter Login Failed", e.getMessage());
//                }
//            }
//        }else {
//                /***
//                 *
//                 * facebook callback
//                 */
//                try {
//                    super.onActivityResult(requestCode, resultCode, data);
//                    Session.getActiveSession().onActivityResult(this, requestCode,
//                            resultCode, data);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (backPress == 1) {
            footerLayout.setVisibility(View.GONE);
            hideRegister();
            backPress = 0;
        } else if (backPress == 2) {
            backPress = 0;
            hideLogin();
            footerLayout.setVisibility(View.GONE);
        } else if (backPress == 3) {
            footerLayout.setVisibility(View.GONE);
            backPress = 0;
            registerVia = 0;
            hideRegisterWithLayout();
            footerLayout.setVisibility(View.GONE);


        }
        else if (backPress == 4) {
            backPress=3;
            registerLayout.setVisibility(View.VISIBLE);
            OfferPeopleLayout.setVisibility(View.GONE);
            footerLayout.setVisibility(View.GONE);
        }

        else
            finish();

    }

    public Bitmap getBitmapFromUri() {
        getContentResolver().notifyChange(mImageCaptureUri, null);
        ContentResolver cr = getContentResolver();
        Bitmap bitmap;
        try {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
                    mImageCaptureUri);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void reload() {
        picStatus = "1";

       // profileImageView.setVisibility(View.VISIBLE);
        //profilePicture.setVisibility(View.GONE);
        Picasso.with(SplashActivity.this).load(mFileTemp).skipMemoryCache()
                .transform(new CircleTransform()).resize(300, 300).centerCrop()
                .skipMemoryCache().into(profileImageView);

        // Picasso.with(profile.this).load(mFileTemp)
        // .transform(new BlurTransform(profile.this)).resize(300, 300)
        // .centerCrop().skipMemoryCache().into(reviewUserImgBlured);
    }

    /**
     * About You Popup*****
     */

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void aboutYouPopup(Activity activity) {
        final EditText aboutDesc;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.about_you_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            TextView textHead = (TextView) dialog.findViewById(R.id.headerLabel);
            textHead.setText(getResources().getString(R.string.aboutYou));
            aboutDesc = (EditText) dialog.findViewById(R.id.aboutFieldPopup);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backPopup);
            aboutDesc.setText(aboutText.getText());
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            Button doneBtn = (Button) dialog.findViewById(R.id.doneBtn);
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    aboutText.setText(aboutDesc.getText());
                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    /**
     * About You Popup*****
     */

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void galleryCameraPoup(Activity activity) {
        final CustomTextViewRegularFont cameraBtn, galleryBtn;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.gallery_camera_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            CustomTextViewSemiBold textHead = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);

            cameraBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.camerabtn);
            galleryBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.galleryBtn);
            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    onResult = 3;
                    showSelection(0);
                }
            });

            galleryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    onResult = 3;
                    showSelection(1);

                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    /**
     * About You Popup*****
     */

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void forgotPasswordPopup(Activity activity, String title, final int flag) {
        final CustomEditTextRegularFont emailField;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.forgot_password);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            CustomTextViewSemiBold textHead = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);
            CustomTextViewRegularFont textMessage = (CustomTextViewRegularFont) dialog.findViewById(R.id.textMsg);
            if (flag == 1) {
                textHead.setText(getResources().getString(R.string.forgotPass));
                textMessage.setText(getResources().getString(R.string.forgotPasstext));
            }
            else if (flag == 2) {
                textHead.setText(getResources().getString(R.string.faceBookEmail));
                textMessage.setText(getResources().getString(R.string.facebookEmailText));
            }
            else if (flag == 3) {
                textHead.setText(getResources().getString(R.string.twitterEmail));
                textMessage.setText(getResources().getString(R.string.twitterEmailText));
            }

            emailField = (CustomEditTextRegularFont) dialog.findViewById(R.id.emailField);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backPopup);
            CustomTextViewRegularFont doneBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.doneBtn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    } catch (Exception e) {
                    }

                    if (emailField.getText().length() == 0) {
                        emailField.requestFocus();
                        emailField.setError(getResources().getString(R.string.emailRequired));
                    } else if ((Data.validEmail(emailField.getText().toString())) == false) {
                        emailField.requestFocus();
                        emailField.setError(getResources().getString(R.string.emailValid));
                    } else {


                        dialog.dismiss();
                        if (flag == 1) {
                            if (!AppStatus.getInstance(SplashActivity.this)
                                    .isOnline(SplashActivity.this)) {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.noInternet));


                            } else {
                                ForgotPasswordApi(emailField.getText().toString());
                            }
                        } else {
                            emailStr = emailField.getText().toString();
                            if (!AppStatus.getInstance(SplashActivity.this)
                                    .isOnline(SplashActivity.this)) {
                                AppStatus.errorDialog(SplashActivity.this);

                            } else {
                                LoginViaAll();
                            }

                        }

                    }


                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    /**
     * Login via email api
     */
    public void RegisterViaAll() {
        Data.loading_box(SplashActivity.this, "Loading...");
        RequestParams params = new RequestParams();

        try {
            RegisterWithGCM();
        } catch (Exception e) {
            Log.i("device_token ", deviceToken + "..");
        }
        String serverUrl = "";
        if (registerVia == 1) {
            params.put("fb_id", fbId);
            params.put("fb_access_token", fbAccessToken);
            serverUrl = Config.getBaseURL() + "registerUserUsingFb";
        }
        if (registerVia == 2) {
            params.put("twitter_id", twiId);
            params.put("twitter_access_token", twiAccessToken);
            params.put("twitter_access_token_secret", twitterSecret);
            serverUrl = Config.getBaseURL() + "registerUserUsingTwitter";
        }
        if (registerVia == 3) {
            serverUrl = Config.getBaseURL() + "registerUserUsingEmail";
            params.put("password", passField.getText().toString());
        }
        try {

            if (picStatus.equalsIgnoreCase("1"))
                params.put("profile_pic", mFileTemp);
            else {
                if (registerVia == 2)
                    params.put("profile_pic", proPicture);
                else
                    params.put("profile_pic", "");
            }
            Log.i("profile_pic", "" + mFileTemp);
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        params.put("user_name", userNameText.getText().toString());
        params.put("email", emailText.getText().toString());
        params.put("phone", contactText.getText().toString());
        params.put("country", countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString());
        params.put("country_code", getCountryCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()));
        params.put("city", stateField.getText().toString());
        params.put("address", addressText.getText().toString());
        params.put("gender", gender);
        params.put("device_type", Config.getDEVICE_TYPE());
        params.put("device_token", deviceToken);
        params.put("device_name", getDeviceName());
        params.put("app_version", getVersion(getApplicationContext()) + "");
        params.put("pic_status", picStatus);
        params.put("about_you", aboutText.getText().toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(serverUrl, params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                Config.setAPP_ACCESS_TOKEN(jObj.getJSONObject("data").getString("access_token"));
                                Config.setUSER_ID(jObj.getJSONObject("data").getString("user_id"));
                                SharedPreferences pref = getSharedPreferences(
                                        "ButlerAppPref", 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("app_access_token",
                                        jObj.getJSONObject("data").getString("access_token"));

//                                JSONArray userResults = jObj.getJSONObject("data").getJSONArray("fb_user_list");
                                if (registerVia == 1 && facebookList.size() > 0) {

//                                    facebookList = new ArrayList<FacebookFriendListModel>();
//                                    for (int i = 0; i < userResults.length(); i++) {
//                                        FACEBOOK_FRIENDS = new FacebookFriendListModel(userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("fb_id"), userResults.getJSONObject(i).getString("user_id"), false);
//                                        facebookList.add(FACEBOOK_FRIENDS);
//
//                                    }

                                    backPress=4;
                                    OfferPeopleLayout.setVisibility(View.VISIBLE);
                                    offeradapter = new OfferAdapter(SplashActivity.this, facebookList);
                                    offerListview.setAdapter(offeradapter);
                                } else {
                                    editor.commit();
                                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

                                    finish();
                                }
                            } else {

                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                                Data.loading_box_stop();

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }
                });

    }

    /**
     * Function loads the users facebook profile pic
     *
     * @param userID
     */
    public Bitmap getUserPic(String userID) {
        String imageURL;
        Bitmap bitmap = null;

        imageURL = "http://graph.facebook.com/" + userID + "/picture?type=small";
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(imageURL).getContent());
        } catch (Exception e) {
            Log.d("TAG", "Loading Picture FAILED");
            e.printStackTrace();
        }
        return bitmap;
    }


    /**
     * get country code
     */

    private String getCountryCode(String countryName) {
        int index = Countries.countryNames.indexOf(countryName);
        return Countries.countryCodes.get(index).toString();
    }

    private String getAreaCode(String countryName) {
        int index = Countries.countryNames.indexOf(countryName);
        return Countries.countryAreaCodes.get(index).toString();
    }

    /**
     * Login via email api
     */
    public void LoginViaAll() {
        Data.loading_box(SplashActivity.this, "Loading...");
        RequestParams params = new RequestParams();

        try {
            RegisterWithGCM();
        } catch (Exception e) {
            Log.i("device_token ", deviceToken + "..");
        }
        String serverUrl = "";
        if (loginVia == 1) {
            params.put("fb_id", fbId);
            params.put("fb_access_token", fbAccessToken);
            serverUrl = Config.getBaseURL() + "loginUsingFb";
        }
        if (loginVia == 2) {
            params.put("twitter_id", twiId);
            params.put("twitter_access_token", twiAccessToken);
            params.put("twitter_access_token_secret", twitterSecret);
            serverUrl = Config.getBaseURL() + "loginUsingTwitter";
        }
        if (loginVia == 3) {
            emailStr = loginEmail.getText().toString();
            serverUrl = Config.getBaseURL() + "loginUsingEmail";
            params.put("password", loginPassword.getText().toString());
        }

        params.put("email", emailStr);
        params.put("device_type", Config.getDEVICE_TYPE());
        params.put("device_token", deviceToken);
        params.put("device_name", getDeviceName());
        params.put("app_version", getVersion(getApplicationContext()) + "");
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(serverUrl, params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {
                                Config.setAPP_ACCESS_TOKEN(jObj.getJSONObject("data").getString("access_token"));
                                Config.setUSER_ID(jObj.getJSONObject("data").getString("user_id"));
                                SharedPreferences pref = getSharedPreferences(
                                        "ButlerAppPref", 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("app_access_token",
                                        jObj.getJSONObject("data").getString("access_token"));
                                editor.commit();
                                /**
                                 * Call to Homeactivity upon successful login
                                 * set skip mode to false and give full access to user
                                 *
                                 */
                                Config.setSKIP_MODE(false);
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));

                                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                                finish();

                            } else {
//                                if (loginVia == 1) {
//                                    try {
//                                        profileImageView.setVisibility(View.GONE);
//                                        profilePicture.setVisibility(View.VISIBLE);
//                                        profilePicture.setProfileId(FacebookLoginHelper.USER_DATA.fbId);
//                                        registerVia = 1;
//                                    } catch (Exception e) {
//
//                                    }
//                                    splashLayout.setVisibility(View.GONE);
//                                    registerLayout.setVisibility(View.VISIBLE);
//                                }


                                if(loginVia==1 || loginVia==2)
                                {
                                    splashLayout.setVisibility(View.GONE);
                                    registerLayout.setVisibility(View.VISIBLE);
                                }



                                //MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                                Data.loading_box_stop();

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }
                });

    }


    /**
     * Login via email api
     */
    public void GetAccessTokenApi() {
        Data.loading_box(SplashActivity.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("device_type", Config.getDEVICE_TYPE());
        params.put("device_token", deviceToken);
        params.put("device_name", getDeviceName());
        params.put("app_version", getVersion(getApplicationContext()) + "");
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "accessTokenLogin", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                // Config.setAPP_ACCESS_TOKEN(jObj.getJSONObject("data").getString("access_token"));
                                Config.setUSER_ID(jObj.getJSONObject("data").getString("user_id"));
                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                finish();
                            } else {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                                Data.loading_box_stop();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }
                });

    }


    /**
     * Forgot Password APi
     */
    public void ForgotPasswordApi(String email) {
        Data.loading_box(SplashActivity.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("email", email);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.get(Config.getBaseURL() + "forgotPassword", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));

                            } else {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }


    /**
     * Register GCM for notifications and get the register id
     */
    private void RegisterWithGCM() {
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        String regId;
        regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
            GCMRegistrar.register(this, Config.getGCM_ID());
            Log.v("reg id ", ".." + regId);
            deviceToken = regId;
        } else {
            deviceToken = regId;
            Log.v("Registration", "Already registered, regId: " + regId);
        }
    }

    private String getDeviceName() {
        return android.os.Build.MODEL;
    }

    /**
     * get app version
     *
     * @param context
     * @return
     */
    public int getVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }


    /**
     * shows various orders adapter
     *
     * @author showket
     */
    public class OfferAdapter extends BaseAdapter {

        private List<FacebookFriendListModel> all_riends = null;
        private ArrayList<FacebookFriendListModel> products;

        Activity activity;

        LayoutInflater inflater = null;


        ViewHolder holder;

        // Typeface face, face1;

        public OfferAdapter(Activity a, List<FacebookFriendListModel> all_products) {

            this.all_riends = all_products;
            this.products = new ArrayList<FacebookFriendListModel>();
            this.products.addAll(all_products);
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_riends.size();
        }

        @Override
        public FacebookFriendListModel getItem(int position) {
            return all_riends.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(
                        R.layout.facebook_friend_list_item, null);
                holder = new ViewHolder();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addOfferListRow);

                holder.profileImage = (ProfilePictureView) convertView
                        .findViewById(R.id.profilePicture);
                holder.fbName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);

                holder.isOffered = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.selectBtn);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (all_riends.get(position).isOffered()) {
                holder.isOffered.setBackgroundResource(R.drawable.follow_selected);
                holder.isOffered.setTextColor(getResources().getColor(R.color.white));
                holder.isOffered.setText(getResources().getString(R.string.followingLabel));
            } else {
                holder.isOffered.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.isOffered.setText(getResources().getString(R.string.follow));
                holder.isOffered.setTextColor(getResources().getColor(R.color.statusBar));
            }
            holder.fbName.setText(all_riends.get(position).getFbName());
            holder.profileImage.setProfileId(all_riends.get(position).getFbId());


            holder.isOffered.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if (!AppStatus.getInstance(SplashActivity.this)
//                            .isOnline(SplashActivity.this)) {
//                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.noInternet));
//                    } else {
//                        FollowFriendApi(all_riends.get(position).getUserId());
                        if (all_riends.get(position).isOffered()) {
                            all_riends.get(position).setOffered(false);
                            if(userIdsCommaSeparated.contains(all_riends.get(position).getFbId()))
                                userIdsCommaSeparated.remove(all_riends.get(position).getFbId());
                            //followStatus = "0";
                        } else {
                            all_riends.get(position).setOffered(true);
                            if(!userIdsCommaSeparated.contains(all_riends.get(position).getFbId()))
                                userIdsCommaSeparated.add(all_riends.get(position).getFbId());
                            //followStatus = "1";
                        }
                        notifyDataSetChanged();
                   // }
                }

            });

            return convertView;

        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            Log.i("inside filter", charText + "..");

            all_riends.clear();
            if (charText.length() == 0) {
                all_riends.addAll(products);
            } else {
                for (FacebookFriendListModel wp : products) {
                    if (wp.getFbName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_riends.add(wp);
                        Log.i("inside filter", wp.getFbName() + "..");
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    class ViewHolder {
        public RelativeLayout layout;
        ProfilePictureView profileImage;
        CustomTextViewRegularFont fbName, fbId, isOffered;
    }

    /**
     * Foollow Friend Api
     * id_type_flag  1 for facebook
     */
    public void FollowFriendApi() {
        String idList = userIdsCommaSeparated.toString();
        String ids = idList.substring(1, idList.length() - 1).replace(", ", ",");
        Data.loading_box(SplashActivity.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("status", "1");
        params.put("ids", ids);
        params.put("id_type_flag", "1");
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "follow", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));

                                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                                finish();

                            } else {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }

//    /**
//     * faceboook friendlist pagination
//     */
    public void GetFacebookListAPi() {
        RequestParams params = new RequestParams();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post("nextPageUrl", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;



                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {

                        offerListview.setVisibility(View.GONE);

                    }
                });

    }
}
