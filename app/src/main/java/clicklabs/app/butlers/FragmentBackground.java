package clicklabs.app.butlers;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clicklabs.butlers.R;

public class FragmentBackground extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootElement;
		rootElement = inflater.inflate(R.layout.fragment_background, container,
				false);
		return rootElement;
	}
}
