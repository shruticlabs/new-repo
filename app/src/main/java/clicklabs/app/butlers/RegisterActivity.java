//package clicklabs.app.butlers;
//
//import android.annotation.TargetApi;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.ActivityNotFoundException;
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageInfo;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.transition.Explode;
//import android.util.Log;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.afollestad.materialdialogs.MaterialDialog;
//import com.example.clicklabs.butlers.R;
//import com.google.android.gcm.GCMRegistrar;
//import com.loopj.android.http.AsyncHttpClient;
//import com.loopj.android.http.AsyncHttpResponseHandler;
//import com.loopj.android.http.RequestParams;
//import com.squareup.picasso.Picasso;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.InputStream;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.Locale;
//
//import clabs.butlers.utils.AppStatus;
//import clabs.butlers.utils.CircleTransform;
//import clabs.butlers.utils.CommonUtil;
//import clabs.butlers.utils.Config;
//import clabs.butlers.utils.CustomEditTextRegularFont;
//import clabs.butlers.utils.CustomTextViewRegularFont;
//import clabs.butlers.utils.CustomTextViewSemiBold;
//import clabs.butlers.utils.Data;
//import clabs.butlers.utils.Fonts;
//import clabs.butlers.utils.InternetConnectionStatus;
//import clabs.butlers.utils.ProfilePictureView;
//import material.animations.MaterialDesignAnimations;
//import rmn.androidscreenlibrary.ASSL;
//
//
///**
//* Created by click103 on 16/3/15.
//*/
//public class RegisterActivity extends Activity {
//
//    Button countryBtn,changeProfileImage;
//    RadioGroup radioSexGroup;
//    Spinner countrySpinner;
//    TextView userNameLabel,userNameText,emailLabel,contactLabel,countryLabel,statelabel,addressLabel,genderLabel,aboutLabel;
//    EditText emailText,contactText,conntryText,stateText,addressText,passField;
//    CustomEditTextRegularFont stateField;
//    CustomTextViewRegularFont aboutText;
//    ImageView profileImageView;
//    ArrayList<String> coutries;
//    ArrayList<String> coutriescodes;
//    Bundle extras ;
//    Bitmap bitmap;
//    private Uri mImageCaptureUri;
//    private File mFileTemp;
//    ProfilePictureView profilePicture;
//    private static final int PICK_FROM_CAMERA = 1;
//    private static final int PICK_FROM_FILE = 2;
//    RelativeLayout backBtnMain;
//    LinearLayout errorLayout;
//    CustomTextViewSemiBold headerTitle;
//    RelativeLayout NextBtnMain;
//    String name="",emailStr="",fbId="",passwordStr="",phone="",country="",countryCode="",city="",address="",gender="1",
//            profilePic="",aboutYou="",picStatus="",deviceToken="",twiId="";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.register);
//        changeProfileImage=(Button)findViewById(R.id.changeProfileImageBtn);
//        userNameLabel=(TextView)findViewById(R.id.userNameLabel);
//        userNameText=(TextView)findViewById(R.id.usernamefield);
//        emailLabel=(TextView)findViewById(R.id.emailLabel);
//        emailText=(EditText)findViewById(R.id.emailfield);
//        contactLabel=(TextView)findViewById(R.id.contactLabel);
//        contactText=(EditText)findViewById(R.id.contactField);
//        countryLabel=(TextView)findViewById(R.id.countryLabel);
////        conntryText=(TextView)findViewById(R.id.count);
//        statelabel=(TextView)findViewById(R.id.stateLabel);
//        stateText=(EditText)findViewById(R.id.stateField);
//        addressLabel=(TextView)findViewById(R.id.addresslabel);
//        addressText=(EditText)findViewById(R.id.addressField);
//        genderLabel=(TextView)findViewById(R.id.genderLabel);
//        aboutText=(CustomTextViewRegularFont)findViewById(R.id.aboutField);
//
//        aboutLabel=(TextView)findViewById(R.id.aboutLabel);
//        passField=(EditText)findViewById(R.id.passwordfield);
//        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);
//        profileImageView=(ImageView)findViewById(R.id.userImage);
//        backBtnMain=(RelativeLayout)findViewById(R.id.backBtnMain);
//
//        stateField=(CustomEditTextRegularFont)findViewById(R.id.stateField);
//        headerTitle=(CustomTextViewSemiBold)findViewById(R.id.header);
//        NextBtnMain=(RelativeLayout)findViewById(R.id.NextBtnMain);
//        errorLayout=(LinearLayout)findViewById(R.id.errorLayout);
//         profilePicture = (ProfilePictureView) findViewById(R.id.profilePicture);
//
//
//        /***apply fonts****/
//        userNameLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        userNameText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        emailLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        emailText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        contactLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        contactText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        countryLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
////        conntryText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        statelabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        stateText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        addressLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        addressText.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        genderLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        aboutLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//
//        aboutLabel.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//        passField.setTypeface(Fonts.getFontTextMedium(RegisterActivity.this));
//
//
//        headerTitle.setText("Register With facebook");
//
//        try {
//            RegisterWithGCM();
//        } catch (Exception e) {
//            Log.i("device_token ", deviceToken + "..");
//        }
//
//       coutries = new ArrayList<String>();
//        coutriescodes = new ArrayList<String>();
//        extras = getIntent().getExtras();
//       countryBtn=(Button)findViewById(R.id.countryBtn);
//
//
//        /****gallery image**/
//        mFileTemp = CommonUtil.getTempImageFile();
//        if (mFileTemp.exists())
//            mFileTemp.delete();
//        mFileTemp = CommonUtil.getTempImageFile();
//        /*****set data from facebook****/
//
//
//        if (extras != null) {
//            if(extras.getString("loginVia").equals("facebook")) {
//                userNameText.setText(extras.getString("fbUserName"));
//                emailText.setText(extras.getString("fbUserEmail"));
//                fbId = extras.getString("fbUserId");
//                String p = "http://graph.facebook.com/" + extras.getString("fbUserId") + "/picture?width=160&height=160";
//                try {
//                    profileImageView.setVisibility(View.GONE);
//                    profilePicture.setProfileId(extras.getString("fbUserId"));
//                    // Picasso.with(RegisterActivity.this).load(getUserPic(extras.getString("fbUserId"))).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);
//                } catch (Exception e) {
//
//                }
//            }
//            else  if(extras.getString("loginVia").equals("twitter")) {
//
//                userNameText.setText(extras.getString("twtUserName"));
//                emailText.setText(extras.getString("twtUserEmail"));
//                twiId = extras.getString("twtId");
//                String p = "http://graph.facebook.com/" + extras.getString("fbUserId") + "/picture?width=160&height=160";
//                try {
//                   profilePicture.setVisibility(View.GONE);
//                   Picasso.with(RegisterActivity.this).load(extras.getString("twtprofilePicture")).skipMemoryCache().transform(new CircleTransform()).into(profileImageView);
//                } catch (Exception e) {
//
//                }
//            }
//            else  if(extras.getString("loginVia").equals("butlers")) {
//                profilePicture.setVisibility(View.GONE);
//                userNameText.setText(extras.getString("UserName"));
//                emailText.setText(extras.getString("UserEmail"));
//                passField.setText(extras.getString("UserPassword"));
//
//            }
//        }
//        /** chage profile image button***/
//        changeProfileImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showSelection(1);
//
//
//            }
//        });
//
//        /**
//         * Register button click
//         */
//        NextBtnMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if(userNameText.getText().length()==0)
//                    userNameText.setError(getResources().getString(R.string.userRequired));
//                else if(emailText.getText().length()==0)
//                    emailText.setError(getResources().getString(R.string.emailRequired));
//                else if ((Data.validEmail(emailText.getText().toString())) == false)
//                    emailText.setError(getResources().getString(R.string.emailValid));
//                else
//                {
//                if (!AppStatus.getInstance(RegisterActivity.this)
//                        .isOnline(RegisterActivity.this)) {
//                    AppStatus.errorDialog(RegisterActivity.this);
//
//                } else
//                {
//                    RegisterViaAll();
//                }
//                }
//
//
//
//
//
//            }
//        });
//
//        String[] isoCountryCodes = Locale.getISOCountries();
//        coutries.clear();
//        coutriescodes.clear();
//        for (String countryCode : isoCountryCodes) {
//            Locale locale = new Locale("", countryCode);
//            String countryName = locale.getDisplayCountry();
//
//            coutries.add(locale.getDisplayCountry());
//            coutriescodes.add(countryCode);
//        }
//
//        ArrayAdapter coutryAdapter = new ArrayAdapter(RegisterActivity.this,R.layout.country_spinner_item,coutries);
//        coutryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        countrySpinner.setAdapter(coutryAdapter);
//
//
//
//
//        /*** country btn***/
//        countryBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                countrySpinner.performClick();
//
//
//            }
//        });
//        /***about label click***/
//
//        aboutText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                aboutYouPopup(RegisterActivity.this);
//
//
//            }
//        });
//        /****back btn click****/
//        backBtnMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//        finish();
//
//            }
//        });
//
//
//        /**** gender click***/
//        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
//        RadioButton  radioButtonMale = (RadioButton) findViewById(R.id.radioMale);
//        RadioButton  radioButtonFemale= (RadioButton) findViewById(R.id.radioFemale);
//
//        radioButtonMale.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                // get selected radio button from radioGroup
//                int selectedId = radioSexGroup.getCheckedRadioButtonId();
//
//                // find the radiobutton by returned id
//                RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
//
//                if(radioSexButton.getText().equals("Male"))
//                    gender="1";
//                else
//                    gender="2";
//
//            }
//
//        });
//        radioButtonFemale.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                // get selected radio button from radioGroup
//                int selectedId = radioSexGroup.getCheckedRadioButtonId();
//
//                // find the radiobutton by returned id
//                RadioButton radioSexButton = (RadioButton) findViewById(selectedId);
//
//                Toast.makeText(RegisterActivity.this,
//                        radioSexButton.getText(), Toast.LENGTH_SHORT).show();
//
//            }
//
//        });
//    }
//    /***change profile image from gallery***/
//    private void showSelection(int idx) {
//        Intent intent = null;
//        if (idx == 0) {
//            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//            mImageCaptureUri = Uri
//                    .fromFile(new File(android.os.Environment
//                            .getExternalStorageDirectory(),
//                            CommonUtil.GAME_IMAGE_NAME));
//
//            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
//                    mImageCaptureUri);
//
//            try {
//                intent.putExtra("return-data", true);
//
//                startActivityForResult(intent, PICK_FROM_CAMERA);
//            } catch (ActivityNotFoundException e) {
//                e.printStackTrace();
//            }
//        } else if (idx == 1) {
//            intent = new Intent();
//
//            intent.setType("image/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//
//            startActivityForResult(
//                    Intent.createChooser(intent, "Complete action using"),
//                    PICK_FROM_FILE);
//        }
//    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode != RESULT_OK)
//            return;
////        isPicChange = "0";
//        switch (requestCode) {
//
//            case PICK_FROM_FILE:
//                try {
//                    InputStream inputStream = getContentResolver().openInputStream(
//                            data.getData());
//                    FileOutputStream fileOutputStream = new FileOutputStream(
//                            mFileTemp);
//                    CommonUtil.copyStream(inputStream, fileOutputStream);
//                    fileOutputStream.close();
//                    inputStream.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//
//                reload();
//                // new LoadBlurImageChange().execute("");
//                break;
//
//            case PICK_FROM_CAMERA:
//
//                try {
//
//
//                    if (data != null) {
//                        Bundle extras = data.getExtras();
//                        if (extras.containsKey("data")) {
//                            bitmap = (Bitmap) extras.get("data");
//                        } else {
//                            bitmap = getBitmapFromUri();
//                        }
//                    } else {
//                        bitmap = getBitmapFromUri();
//                    }
//
//                 //   new LongOperation().execute("");
//
//                } catch (Exception e) {
//                    Log.v("camera image exception", e.toString());
//                }
//
//                break;
//        }
//    }
//    public Bitmap getBitmapFromUri() {
//        getContentResolver().notifyChange(mImageCaptureUri, null);
//        ContentResolver cr = getContentResolver();
//        Bitmap bitmap;
//        try {
//            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
//                    mImageCaptureUri);
//            return bitmap;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//    public void reload() {
//        //isPicChange = "1";
//
//        Picasso.with(RegisterActivity.this).load(mFileTemp).skipMemoryCache()
//                .transform(new CircleTransform()).resize(300, 300).centerCrop()
//                .skipMemoryCache().into(profileImageView);
//
//        // Picasso.with(profile.this).load(mFileTemp)
//        // .transform(new BlurTransform(profile.this)).resize(300, 300)
//        // .centerCrop().skipMemoryCache().into(reviewUserImgBlured);
//    }
//
//    /***About You Popup******/
//
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void aboutYouPopup(Activity activity) {
//        final EditText aboutDesc;
//        try {
//            final Dialog dialog = new Dialog(activity,
//                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
//            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//            dialog.setContentView(R.layout.about_you_popup);
//            if (Config.isLollyPop())
//            dialog.getWindow().setExitTransition(new Explode());
//            WindowManager.LayoutParams layoutParams = dialog.getWindow()
//                    .getAttributes();
//            layoutParams.dimAmount = 0.6f;
//            dialog.getWindow().addFlags(
//                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            dialog.setCancelable(false);
//            dialog.setCanceledOnTouchOutside(false);
//
//            TextView textHead = (TextView) dialog.findViewById(R.id.headerLabel);
//            textHead.setTypeface(Fonts.getFontTextSemiBold(RegisterActivity.this));
//             aboutDesc=(EditText) dialog.findViewById(R.id.aboutFieldPopup);
//            Button backBtn=(Button) dialog.findViewById(R.id.backBtn);
//            aboutDesc.setText(aboutText.getText());
//            backBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//
//                }
//            });
//
//            Button doneBtn = (Button) dialog.findViewById(R.id.doneBtn);
//            textHead.setTypeface(Fonts.getFontTextRegularG3(RegisterActivity.this));
//            doneBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                    aboutText.setText(aboutDesc.getText());
//                }
//            });
//
//
//
//            dialog.show();
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//    }
//
//
//
//
//
//    /**
//     *
//     * Login via email api
//     */
//    public void RegisterViaAll() {
//        Data.loading_box(RegisterActivity.this, "Loading...");
//        RequestParams params = new RequestParams();
//        try {
//            RegisterWithGCM();
//        } catch (Exception e) {
//            Log.i("device_token ", deviceToken + "..");
//        }
//        String serverUrl="";
//        if(extras.getString("loginVia").equals("facebook")) {
//            params.put("fb_id", fbId);
//            serverUrl=Config.getBaseURL() + "registerUserUsingFb";
//        }
//        else if(extras.getString("loginVia").equals("twitter")) {
//            params.put("twitter_id", twiId);
//            serverUrl=Config.getBaseURL() + "registerUserUsingFb";
//        }
//        else if(extras.getString("loginVia").equals("butlers")) {
//            serverUrl=Config.getBaseURL() + "registerUserUsingFb";
//
//        }
//            params.put("name", userNameText.getText().toString());
//            params.put("email", emailText.getText().toString());
//            params.put("pasword",passField.getText().toString());
//            params.put("phone", contactText.getText().toString());
//            params.put("country", countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString());
//            params.put("country_code", getCountryCode(countrySpinner.getItemAtPosition(countrySpinner.getSelectedItemPosition()).toString()));
//            params.put("city", stateField.getText().toString());
//            params.put("address", addressText.getText().toString());
//            params.put("gender", gender);
//            params.put("device_type", Config.getDEVICE_TYPE());
//            params.put("device_token", deviceToken);
//            params.put("device_name", getDeviceName());
//            params.put("app_version", getVersion(getApplicationContext())+"");
//            params.put("pic_status", picStatus);
//            params.put("profile_pic", profilePic);
//            params.put("about_you", aboutText.getText().toString());
//            AsyncHttpClient client = new AsyncHttpClient();
//            client.setTimeout(Config.getSERVER_TIMEOUT());
//            client.post(serverUrl, params,
//                    new AsyncHttpResponseHandler() {
//                        @Override
//                        public void onSuccess(String response) {
//                            Log.e("request succesfull", "response = " + response);
//                            JSONObject res;
//                            try {
//                                res = new JSONObject(response);
//
//
//                            } catch (JSONException e) {
//
//                                e.printStackTrace();
//                            } catch (Exception e) {
//
//                                e.printStackTrace();
//                            }
//                            Data.loading_box_stop();
//                        }
//
//                        @Override
//                        public void onFailure(Throwable arg0) {
//                            Log.i("request fail", arg0.toString());
//                            Data.loading_box_stop();
//                            new AlertDialog.Builder(RegisterActivity.this)
//                                    .setTitle("Error").setMessage(getResources().getString(R.string.ServerFailure))
//                                    .setPositiveButton(android.R.string.ok, null)
//                                    .show();
//                        }
//                    });
//
//    }
//    /**
//     *
//     * Register GCM for notifications and get the register id
//     */
//    private void RegisterWithGCM() {
//        GCMRegistrar.checkDevice(this);
//        GCMRegistrar.checkManifest(this);
//        String regId;
//        regId = GCMRegistrar.getRegistrationId(this);
//        if (regId.equals("")) {
//            GCMRegistrar.register(this,Config.getGCM_ID());
//            Log.v("reg id ", ".." + regId);
//            deviceToken = regId;
//        } else {
//            deviceToken = regId;
//            Log.v("Registration", "Already registered, regId: " + regId);
//        }
//    }
//  /*
//  get device name
//   */
//    private String getDeviceName()
//    {
//        return android.os.Build.MODEL;
//    }
//
//    /**
//     * get app version
//     * @param context
//     * @return
//     */
//    public int getVersion(Context context) {
//        try {
//            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
//            return pInfo.versionCode;
//        } catch (PackageManager.NameNotFoundException e) {
//            return 0;
//        }
//    }
//    /**
//     * Function loads the users facebook profile pic
//     *
//     * @param userID
//     */
//    public Bitmap getUserPic(String userID) {
//        String imageURL;
//        Bitmap bitmap = null;
//
//        imageURL = "http://graph.facebook.com/"+userID+"/picture?type=small";
//        try {
//            bitmap = BitmapFactory.decodeStream((InputStream) new URL(imageURL).getContent());
//        } catch (Exception e) {
//            Log.d("TAG", "Loading Picture FAILED");
//            e.printStackTrace();
//        }
//        return bitmap;
//    }
//
//
//    /**
//     * get country code
//     */
//
//   private String getCountryCode(String countryName)
//   {
//      int index= coutries.indexOf(countryName);
//       return coutriescodes.get(index).toString();
//   }
//
//
//
//}
