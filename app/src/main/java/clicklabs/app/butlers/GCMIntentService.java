package clicklabs.app.butlers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	/*
	 * public GCMIntentService(String senderId) { super(senderId);
	 * Log.d("GCMIntentService", senderId); }
	 * 
	 * @author showket
	 */


	protected void onError(Context arg0, String arg1) {
		Log.e("Registration", "Got an error1!");
		Log.e("Registration", arg0.toString() + arg1.toString());
	}

	protected boolean onRecoverableError(Context context, String errorId) {
		Log.d("onRecoverableError", errorId);
		return false;
	}

	protected void onMessage(Context context, Intent arg1) {
		Log.e("Registration", "Got an error2!");
		Log.e("MESSAGE", "" + arg1.toString());
	}

	protected void onRegistered(Context arg0, String arg1) {
		Log.e("Registration", "Got an error3!");
		Log.e("Registration", arg0.toString() + arg1.toString());
	}

	protected void onUnregistered(Context arg0, String arg1) {
		Log.e("Registration", "Got an error4!");
		Log.e("Registration", arg0.toString() + arg1.toString());
	}

	private void notificationManager(Context context, String id,
			String message, String sender, String log) {
		long when = System.currentTimeMillis();
	}
}
