package clicklabs.app.butlers;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.CircleTransform;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.ParallaxPageTransformer;
import material.animations.MaterialDesignAnimations;
import models.PromoteUserListModel;
import nl.changer.polypicker.ImagePickerActivity;
import nl.changer.polypicker.utils.ImageInternalFetcher;
import pager.fragment.BazaarFragmentTab;
import pager.fragment.CommentProductFragment;
import pager.fragment.FeedFragmentTab;
import pager.fragment.Followersfragment;
import pager.fragment.FollowingFragment;
import pager.fragment.MutualConnectionsFragment;
import pager.fragment.PromotePeopleFrag;
import pager.fragment.PromoteShopFrag;
import pager.fragment.SearchFragmentTab;

/**
 * Created by click103 on 24/3/15.
 */
public class BuyingProducts extends FragmentActivity implements ObservableScrollViewCallbacks {

    PromoteUserListModel wp;
    ArrayList<PromoteUserListModel> arraylist;
    LinearLayout buyingProdlayout, showPromotedList;
    //    ListView promoteListView;
    RelativeLayout addPromoterRow;
    //    EditText searchBar;
    PromoteAdapter adapter;
    Button nextButton;
    Boolean viewPromoterList = false;
    Bundle extras;
    int ITEMS = 2;
    PromotePagerAdapter promotePagerAdapter;
    ViewPager viewPager;
    CustomTextViewSemiBold header;
    String BACK_STACK = "BACK_STACK";
    LinearLayout followingLayout,errorLayout;
    RelativeLayout profileLayout;
    FrameLayout fragmentContainer;
    String userId = "0";
    ImageView productImageOffer,imageProfile,UserPromterImageview;
    HashSet<Uri> mMedia = new HashSet<Uri>();
    String filePathToUpload = "";
    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    private static final int INTENT_REQUEST_GET_N_IMAGES = 14;
    private Context mContext;
    int chkFlag = 0;
    String cashOnDelivery = "0";
    CustomTextViewRegularFont userName,userRanking,timeValue,addressField,priceProduct,noOfItems,nameLabel;
    CustomTextViewLightFont descriptionField;
    protected MyApplication applicationClass;
    CustomTextViewSemiBold headerLabelPromote;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        mContext = BuyingProducts.this;
        applicationClass = (MyApplication)getApplication();
        if (extras != null) {
            if (extras.getString("layoutShown").equals("promote")) {
                setContentView(R.layout.product_description);

                RelativeLayout backBtnMain=(RelativeLayout)findViewById(R.id.backBtnMain);
                promotePagerAdapter = new PromotePagerAdapter(getSupportFragmentManager());
                viewPager = (ViewPager) findViewById(R.id.pagerPromotion);
                header = (CustomTextViewSemiBold) findViewById(R.id.header);
                viewPager.setAdapter(promotePagerAdapter);
                viewPager.setPageTransformer(true, new ParallaxPageTransformer());
                viewPager.setOffscreenPageLimit(ITEMS);
                PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
                tabsStrip.setViewPager(viewPager);
                header.setText("Promote");
                backBtnMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        finish();
                    }
                });

            } else if (extras.getString("layoutShown").equals("userprofile")) {
                setContentView(R.layout.friend_profile_layout);
                ImageView userImageView = (ImageView) findViewById(R.id.userImage);
                CustomTextViewSemiBold header = (CustomTextViewSemiBold) findViewById(R.id.header);
                final String UserIdProfile=extras.getString("userId");
                header.setText(extras.getString("userProfileName"));
                ObservableScrollView scrollView = (ObservableScrollView) findViewById(R.id.friend_profile_ScrollView);
                scrollView.setScrollViewCallbacks(this);
                try {

                    Picasso.with(BuyingProducts.this).load(extras.getString("userProfilePic")).transform(new CircleTransform()).into(userImageView);
                } catch (Exception e) {

                }
                View userImage = (ImageView) findViewById(R.id.userImage);
                if (Config.isLollyPop())
                    userImage.setTransitionName("userImage");


                RelativeLayout backBtnMain = (RelativeLayout) findViewById(R.id.backBtnMain);
                profileLayout = (RelativeLayout) findViewById(R.id.profileLayout);
                LinearLayout followersBtn = (LinearLayout) findViewById(R.id.followers_layout);
                followingLayout = (LinearLayout) findViewById(R.id.following_layout);
                fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
                CustomTextViewRegularFont mutaualConnections = (CustomTextViewRegularFont) findViewById(R.id.mutual_connections);
                backBtnMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int count = getSupportFragmentManager().getBackStackEntryCount();

                        if (count == 0) {
                            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                            finish();
                        } else {
                            fragmentContainer.setVisibility(View.GONE);
                            profileLayout.setVisibility(View.VISIBLE);
                            getSupportFragmentManager().popBackStack();

                        }
                    }
                });

                mutaualConnections.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        fragmentContainer.setVisibility(View.VISIBLE);
//                        profileLayout.setVisibility(View.GONE);
//                        MutualConnectionsFragment mutualConnectionsFragment=new MutualConnectionsFragment();
//                        mutualConnectionsFragment.setUserId(UserIdProfile);
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.fragmentContainer, mutualConnectionsFragment);
//                        ft.addToBackStack(BACK_STACK);
//                        ft.commit();
                    }
                });
                followersBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        fragmentContainer.setVisibility(View.VISIBLE);
//                        profileLayout.setVisibility(View.GONE);
//                        Followersfragment followersfragment=new Followersfragment();
//                        followersfragment.setUserId(UserIdProfile);
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.fragmentContainer, followersfragment);
//                        ft.addToBackStack(BACK_STACK);
//                        ft.commit();
                    }
                });

                followingLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        fragmentContainer.setVisibility(View.VISIBLE);
//                        profileLayout.setVisibility(View.GONE);
//                        FollowingFragment followingFragment=new FollowingFragment();
//                        followingFragment.setUserId(UserIdProfile);
//                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                        ft.replace(R.id.fragmentContainer, followingFragment);
//                        ft.addToBackStack(BACK_STACK);
//                        ft.commit();
                    }
                });

            } else if (extras.getString("layoutShown").equals("buy")) {
                setContentView(R.layout.product_buying);

                arraylist = new ArrayList<PromoteUserListModel>();
                buyingProdlayout = (LinearLayout) findViewById(R.id.buyingProdlayout);
                showPromotedList = (LinearLayout) findViewById(R.id.showPromotedList);
                addPromoterRow = (RelativeLayout) findViewById(R.id.addPromoterRow);
                header = (CustomTextViewSemiBold) findViewById(R.id.header);
                header.setText("Buying Products");
                nextButton = (Button) findViewById(R.id.nextButton);

                userName=(CustomTextViewRegularFont)findViewById(R.id.userName);
                userRanking=(CustomTextViewRegularFont)findViewById(R.id.ratingValue);
                timeValue=(CustomTextViewRegularFont)findViewById(R.id.timeValue);
                addressField=(CustomTextViewRegularFont)findViewById(R.id.address);
                priceProduct=(CustomTextViewRegularFont)findViewById(R.id.priceProduct);
                nameLabel=(CustomTextViewRegularFont)findViewById(R.id.nameLabel);
                noOfItems=(CustomTextViewRegularFont)findViewById(R.id.noOfItems);
                descriptionField=(CustomTextViewLightFont)findViewById(R.id.descriptionField);
                userName.setText(extras.getString("userName"));
                userRanking.setText(extras.getString("userRank")+getResources().getString(R.string.star));
                timeValue.setText(applicationClass.getExpirationTimeTodisplay(extras.getString("time"))+", ");
                addressField.setText(extras.getString("address"));
                priceProduct.setText(extras.getString("available"));
                noOfItems.setText(extras.getString("price"));
                descriptionField.setText(extras.getString("description"));
                imageProfile = (ImageView) findViewById(R.id.imageProfile);
                UserPromterImageview = (ImageView) findViewById(R.id.UserPromterImageview);
                ImageView productImage = (ImageView) findViewById(R.id.cardImage);


                try {

                    Picasso.with(mContext).load(extras.getString("productImage")).into(productImage);
                    Picasso.with(mContext).load(extras.getString("userImage")).into(imageProfile);
                } catch (Exception e) {

                }

                final View cardImage = (ImageView) findViewById(R.id.cardImage);
                if (Config.isLollyPop())
                    cardImage.setTransitionName("cardImage");


                addPromoterRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPromoterList = true;
                        headerLabelPromote.setText("Promoters list");
                        showPromotedList.setVisibility(View.VISIBLE);
                        buyingProdlayout.setVisibility(View.GONE);


                    }
                });



                final ListView likeListView = (ListView) findViewById(R.id.promoterList);
                 headerLabelPromote = (CustomTextViewSemiBold) findViewById(R.id.headerLabel);

                RelativeLayout backBtn = (RelativeLayout) findViewById(R.id.backPopup);
                RelativeLayout NextBtnMain = (RelativeLayout) findViewById(R.id.NextBtnMain);
                final CustomEditTextRegularFont searchBar = (CustomEditTextRegularFont) findViewById(R.id.searchBar);
                searchBar.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void afterTextChanged(Editable s) {
                        String text = searchBar.getText().toString().toLowerCase();
                        adapter.filter(text);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {

                    }
                });

                String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
                String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                        "http://api.androidhive.info/json/movies/3.jpg",
                        "http://api.androidhive.info/json/movies/2.jpg", "http://api.androidhive.info/json/movies/1.jpg", "showket", "anil", "saif", "showket", "anil"};
                String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

                for (int i = 0; i < order_ids.length; i++) {

                    wp = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
                    // Binds all strings into an array
                    arraylist.add(wp);
                }
                adapter = new PromoteAdapter(BuyingProducts.this, arraylist);
                likeListView.setAdapter(adapter);


                backBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPromotedList.setVisibility(View.GONE);
                        buyingProdlayout.setVisibility(View.VISIBLE);

                    }
                });

                NextBtnMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPromotedList.setVisibility(View.GONE);
                        buyingProdlayout.setVisibility(View.VISIBLE);
                    }
                });

                final RelativeLayout backBtnMain = (RelativeLayout) findViewById(R.id.backBtnMain);
                backBtnMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        finish();

                    }
                });




            } else if (extras.getString("layoutShown").equals("offer")) {
                setContentView(R.layout.product_buying_offer);
                header = (CustomTextViewSemiBold) findViewById(R.id.header);
                Button addProductBtn = (Button) findViewById(R.id.addProductBtn);
                final CustomTextViewRegularFont productName = (CustomTextViewRegularFont) findViewById(R.id.productName);
                final MaterialEditText productQuantity = (MaterialEditText) findViewById(R.id.productQuantity);
                final MaterialEditText productDesc = (MaterialEditText) findViewById(R.id.productDesc);
                final MaterialEditText pricePerProduct = (MaterialEditText) findViewById(R.id.pricePerProduct);
                errorLayout = (LinearLayout) findViewById(R.id.errorLayout);
                final RelativeLayout cashOnDeliverChk = (RelativeLayout) findViewById(R.id.cashOnDeliverChk);
                final RelativeLayout backBtnMain = (RelativeLayout) findViewById(R.id.backBtnMain);
                final ImageView checkBtn = (ImageView) findViewById(R.id.checkBtn);

                header.setText("Make an offer");
                productName.setText(extras.getString("productName"));
                RelativeLayout nextButton = (RelativeLayout) findViewById(R.id.NextBtnMain);

                productImageOffer = (ImageView) findViewById(R.id.productImage);

                cashOnDeliverChk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (chkFlag == 0) {
                            checkBtn.setBackgroundResource(R.drawable.btn_chkbox_pressed);
                            cashOnDelivery = "1";
                            chkFlag = 1;

                        } else {

                            checkBtn.setBackgroundResource(R.drawable.btn_chkbox_normal);
                            cashOnDelivery = "0";
                            chkFlag = 0;
                        }


                    }


                });

                nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (filePathToUpload.length() == 0) {
                            MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.noImageChoosen));

                        } else if (productName.getText().length() == 0) {
                            productName.requestFocus();
                            productName.setError(getResources().getString(R.string.productnameRequired));
                        } else if (productQuantity.getText().length() == 0) {
                            productQuantity.requestFocus();
                            productQuantity.setError(getResources().getString(R.string.productqtyRequired));
                        } else if (productDesc.getText().length() == 0) {
                            productDesc.requestFocus();
                            productDesc.setError(getResources().getString(R.string.productdescriptionRequired));
                        } else if (pricePerProduct.getText().length() == 0) {
                            pricePerProduct.requestFocus();
                            pricePerProduct.setError(getResources().getString(R.string.priceperProductRequired));
                        } else {
                            if (!AppStatus.getInstance(getApplicationContext())
                                    .isOnline(getApplicationContext())) {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.internetConnectionError));

                            } else {
                                Data.hideSoftKeyboard(BuyingProducts.this);

                                MakeAnOfferApi(productName.getText().toString(),
                                        productQuantity.getText().toString(),
                                        productDesc.getText().toString(),
                                        cashOnDelivery,
                                        pricePerProduct.getText().toString());
                            }


                        }
                    }
                });

                addProductBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        getNImages(1);

                    }
                });
                backBtnMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                       finish();

                    }
                });
            }
        }
    }

    public String getUserId() {
        return userId;
    }


    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            finish();
        } else {
            fragmentContainer.setVisibility(View.GONE);
            profileLayout.setVisibility(View.VISIBLE);
            getSupportFragmentManager().popBackStack();

        }

    }


    private void getNImages(int count) {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);

        // limit image pick count to only 3 images.
        intent.putExtra(ImagePickerActivity.EXTRA_SELECTION_LIMIT, count);
        startActivityForResult(intent, INTENT_REQUEST_GET_N_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (resuleCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_REQUEST_GET_IMAGES || requestCode == INTENT_REQUEST_GET_N_IMAGES) {
                Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

                if (parcelableUris == null) {
                    return;
                }

                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
                mMedia.clear();
                if (uris != null) {
                    for (Uri uri : uris) {
                        mMedia.add(uri);
                        filePathToUpload = uri.toString();

                    }


                    showMedia();
                }
            }
        }
    }

    private void showMedia() {
        // Remove all views before
        // adding the new ones.
//        mSelectedImagesContainer.removeAllViews();

        Iterator<Uri> iterator = mMedia.iterator();
        ImageInternalFetcher imageFetcher = new ImageInternalFetcher(this, 500);
        while (iterator.hasNext()) {
            Uri uri = iterator.next();

//            if(mMedia.size() >= 1) {
//                mSelectedImagesContainer.setVisibility(View.VISIBLE);
//            }

            View imageHolder = LayoutInflater.from(this).inflate(R.layout.media_layout, null);

            // View removeBtn = imageHolder.findViewById(R.id.remove_media);
            // initRemoveBtn(removeBtn, imageHolder, uri);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);

            if (!uri.toString().contains("content://")) {
                // probably a relative uri
                uri = Uri.fromFile(new File(uri.toString()));

            }

            imageFetcher.loadImage(uri, productImageOffer);

        }
    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    /**
     * Promote people/shop pager adapter
     */

    public class PromotePagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{getResources().getString(R.string.peopleLabel), getResources().getString(R.string.shopLabel)};

        public PromotePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return PromotePeopleFrag.init(position);
                case 1:
                    return new PromoteShopFrag().init(position);
                default:// Fragment # 2-9 - Will show list
                    return new PromotePeopleFrag().init(position);

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }


    }


//    /**
//     * Promote popup
//     *
//     * @param activity
//     */
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void showPromotedPopup(Activity activity) {
//
//        try {
//            final Dialog dialog = new Dialog(activity,
//                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
//            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//            dialog.setContentView(R.layout.promoted_list_product);
//            if (Config.isLollyPop())
//                dialog.getWindow().setExitTransition(new Explode());
//
//            WindowManager.LayoutParams layoutParams = dialog.getWindow()
//                    .getAttributes();
//            layoutParams.dimAmount = 0.6f;
//            dialog.getWindow().addFlags(
//                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            dialog.setCancelable(false);
//            dialog.setCanceledOnTouchOutside(false);
//            final ListView likeListView = (ListView) dialog.findViewById(R.id.promoterList);
//            CustomTextViewSemiBold headerLabelPromote = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);
//            headerLabelPromote.setText("Promoters list");
//            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backPopup);
//            RelativeLayout NextBtnMain = (RelativeLayout) dialog.findViewById(R.id.NextBtnMain);
//            final CustomEditTextRegularFont searchBar = (CustomEditTextRegularFont) dialog.findViewById(R.id.searchBar);
//            searchBar.addTextChangedListener(new TextWatcher() {
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    String text = searchBar.getText().toString().toLowerCase();
//                    adapter.filter(text);
//                }
//
//                @Override
//                public void beforeTextChanged(CharSequence s, int start,
//                                              int count, int after) {
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start,
//                                          int before, int count) {
//
//                }
//            });
//
//            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
//            String[] order_images = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};
//            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};
//
//            for (int i = 0; i < order_ids.length; i++) {
//
//                wp = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
//                // Binds all strings into an array
//                arraylist.add(wp);
//            }
//            adapter = new PromoteAdapter(activity, arraylist);
//            likeListView.setAdapter(adapter);
//
//
//            backBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Config.setRecordinStatus(0);
//                    dialog.dismiss();
//                }
//            });
//
//            NextBtnMain.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Config.setRecordinStatus(0);
//                    dialog.dismiss();
//                }
//            });
//
//            dialog.show();
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//
//
//    }

    /**
     * shows various orders adapter
     *
     * @author showket
     */
    public class PromoteAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_products = null;
        private ArrayList<PromoteUserListModel> products;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolder holder;

        // Typeface face, face1;

        public PromoteAdapter(Activity a, List<PromoteUserListModel> all_products) {

            this.all_products = all_products;
            check = new boolean[all_products.size()];
            for (int i = 0; i < all_products.size(); i++)
                check[i] = false;

            this.products = new ArrayList<PromoteUserListModel>();
            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolder();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.namefield = (TextView) convertView
                        .findViewById(R.id.userName);

                holder.arrowbtn = (TextView) convertView
                        .findViewById(R.id.selectBtn);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (check[position] == true) {
                holder.arrowbtn.setBackgroundResource(R.drawable.follow_selected);
            }
            holder.namefield.setText(all_products.get(position).getNamefield());
            try {
                Picasso.with(BuyingProducts.this).load("http://api.androidhive.info/json/movies/4.jpg").transform(new CircleTransform()).into(holder.img);
            } catch (Exception e) {

            }

            holder.arrowbtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (check[position] == false)
                        check[position] = true;
                    else
                        check[position] = false;
                    notifyDataSetChanged();

                    Log.i("order idd", all_products.get(position)
                            .getRowId() + "..");



                }

            });
            holder.layout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    nameLabel.setText(all_products.get(position)
                            .getNamefield());
                    try {
                        Picasso.with(BuyingProducts.this).load(all_products.get(position)
                                .getImagefield()).transform(new CircleTransform()).into(UserPromterImageview);
                    } catch (Exception e) {

                    }

//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(searchbar.getWindowToken(), 0);
//                    detailclick = true;
//                    order_idd = all_products.get(position).getProductId();
//

//                    // searchicon.setVisibility(8);
//
//                    if (!AppStatus.getInstance(getApplicationContext())
//                            .isOnline(getApplicationContext())) {
//                        AppStatus.errorDialog(MyOrders.this);
//
//                    } else {
//                        MyOrdersDetails();
//                    }

                }

            });
            return convertView;

        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            Log.i("inside filter", charText + "..");

            all_products.clear();
            if (charText.length() == 0) {
                all_products.addAll(products);
            } else {
                for (PromoteUserListModel wp : products) {
                    if (wp.getNamefield().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_products.add(wp);
                        Log.i("inside filter", wp.getRowId() + "..");
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    class ViewHolder {

        public RelativeLayout layout;
        ImageView img;
        TextView namefield, arrowbtn;


    }

    /**
     * Add offer Product aPI CALL
     */
    public void MakeAnOfferApi(String productName, String productQty, String productDesc, String cashOnDelivery, String productPerPrice) {
        String picStatus = "";
        if (filePathToUpload.length() > 0)
            picStatus = "1";
        else
            picStatus = "0";
        Data.loading_box(BuyingProducts.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_name", productName);
        params.put("quantity", productQty);
        params.put("description", productDesc);
        params.put("price", productPerPrice);
        params.put("cash_on_delivery_flag", cashOnDelivery);
        params.put("pic_status", picStatus);
        try {

            params.put("image", new File(filePathToUpload));

        } catch (Exception e) {
            Log.e("ERROR", "ERROR" + e.toString());
        }
        AsyncHttpClient client =
                new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "addProduct", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                            } else {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                                Data.loading_box_stop();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }
                });
    }

}






























