package clicklabs.app.butlers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.transition.Explode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import material.animations.MaterialDesignAnimations;
import pager.fragment.CommentProductFragment;
import pager.fragment.LikeProductFragment;

/**
 * Created by saify on 4/5/15.
 */
public class ProductDescription extends FragmentActivity  {

    CustomTextViewSemiBold header;
    private MediaRecorder myRecorder;
    MediaPlayer myPlayer;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "ButlerApp";

    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };
    private File mFileTemp;
    String promoteType="0",textNote="";
    ArrayList<String> userIdsCommaSeparated;
    int voiceNoteDuration=0;
    CountDownTimer countDowntimer;
    LinearLayout errorLayout;
    Activity mcontext;
    Bundle extras;
    ImageView imageProfile;
    CustomTextViewRegularFont userName,userRanking,timeValue,addressField,priceProduct,noOfItems,likeBtn,commentBtn;
    CustomTextViewLightFont descriptionField;
    RelativeLayout backBtnMain;
    String UserIdProfile;
    String BACK_STACK = "BACK_STACK";
    FrameLayout fragmentContainer;
    protected MyApplication applicationClass;
    ObservableScrollView scrollView;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_description);
        mcontext=ProductDescription.this;
        applicationClass = (MyApplication)getApplication();
        header = (CustomTextViewSemiBold) findViewById(R.id.header);
        backBtnMain = (RelativeLayout) findViewById(R.id.backBtnMain);
        errorLayout = (LinearLayout) findViewById(R.id.errorLayout);
        RelativeLayout promoteBtnLay=(RelativeLayout)findViewById(R.id.promoteBtnLay);
        RelativeLayout buyBtnLay=(RelativeLayout)findViewById(R.id.buyBtnLay);
        imageProfile=(ImageView)findViewById(R.id.imageProfile);
        userName=(CustomTextViewRegularFont)findViewById(R.id.userName);
        userRanking=(CustomTextViewRegularFont)findViewById(R.id.ratingValue);
        timeValue=(CustomTextViewRegularFont)findViewById(R.id.timeValue);
        addressField=(CustomTextViewRegularFont)findViewById(R.id.addressField);
        priceProduct=(CustomTextViewRegularFont)findViewById(R.id.priceProduct);
        noOfItems=(CustomTextViewRegularFont)findViewById(R.id.noOfItems);
        likeBtn=(CustomTextViewRegularFont)findViewById(R.id.likeBtn);
        commentBtn=(CustomTextViewRegularFont)findViewById(R.id.commentBtn);
        descriptionField=(CustomTextViewLightFont)findViewById(R.id.descriptionField);
        fragmentContainer = (FrameLayout) findViewById(R.id.fragmentContainer);
        scrollView = (ObservableScrollView) findViewById(R.id.profile_ScrollView);

        extras = getIntent().getExtras();
        if (extras != null) {
            header.setText(extras.getString("productName"));
            userName.setText(extras.getString("userName"));
            userRanking.setText(extras.getString("userRank")+getResources().getString(R.string.star));
            timeValue.setText(applicationClass.getExpirationTimeTodisplay(extras.getString("time"))+", ");
            addressField.setText(extras.getString("address"));
            priceProduct.setText(extras.getString("available"));
            noOfItems.setText(extras.getString("price"));
            descriptionField.setText(extras.getString("description"));
            ImageView productImage = (ImageView) findViewById(R.id.cardImage);
            try {

                Picasso.with(mcontext).load(extras.getString("productImage")).into(productImage);
                Picasso.with(mcontext).load(extras.getString("userImage")).into(imageProfile);
            } catch (Exception e) {

            }

            final View cardImage = (ImageView) findViewById(R.id.cardImage);
            if (Config.isLollyPop())
                cardImage.setTransitionName("cardImage");


        }
        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentContainer.setVisibility(View.VISIBLE);
//                scrollView.setVisibility(View.GONE);
//                LikeProductFragment likeProductFragment=new LikeProductFragment();
//                likeProductFragment.setUserId(UserIdProfile);
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.fragmentContainer, likeProductFragment);
//                ft.addToBackStack(BACK_STACK);
//                ft.commit();
            }
        });
        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentContainer.setVisibility(View.VISIBLE);
//                scrollView.setVisibility(View.GONE);
//                CommentProductFragment commentProductFragment=new CommentProductFragment();
//                commentProductFragment.setProductIdId(UserIdProfile);
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//                ft.replace(R.id.fragmentContainer, commentProductFragment);
//                ft.addToBackStack(BACK_STACK);
//                ft.commit();
            }
        });
        backBtnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        promoteBtnLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPromotePopup(ProductDescription.this);
            }
        });

        buyBtnLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ProductDescription.this, BuyingProducts.class);
                intent.putExtra("layoutShown", "offer");
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showPromotePopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.promote_product_popup);
            if(Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final LinearLayout textNoteLayout=(LinearLayout)dialog.findViewById(R.id.textNoteLayout);
            final LinearLayout popupIstTimeView=(LinearLayout)dialog.findViewById(R.id.popupIstTimeView);
            final LinearLayout textNoteButtonLayout=(LinearLayout)dialog.findViewById(R.id.textNoteBtnLayout);
            final CustomTextViewRegularFont textMessage=(CustomTextViewRegularFont)dialog.findViewById(R.id.textMsg);
            final CustomTextViewRegularFont timerRec=(CustomTextViewRegularFont)dialog.findViewById(R.id.timerRecording);
            final Button voiceNoteBtn=(Button)dialog.findViewById(R.id.voiceNoteBtn);
            final RelativeLayout voiceNoteBtnLay=(RelativeLayout)dialog.findViewById(R.id.voiceNoteBtnLay);

            RelativeLayout backBtn=(RelativeLayout)dialog.findViewById(R.id.backPopup);
            CustomTextViewSemiBold headerLabel=(CustomTextViewSemiBold)dialog.findViewById(R.id.headerLabel);
            CustomTextViewRegularFont textNoteBtn=(CustomTextViewRegularFont)dialog.findViewById(R.id.textNoteBtn);
            headerLabel.setText(getResources().getString(R.string.promotionMethod));
            final CustomTextViewRegularFont doneBtn=(CustomTextViewRegularFont)dialog.findViewById(R.id.doneBtn);
            final CustomEditTextRegularFont textNoteText=(CustomEditTextRegularFont) dialog.findViewById(R.id.textNoteText);

            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    stop();

                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                    if(promoteType.equals("1"))
                        textNote = textNoteText.getText().toString();
                    else
                        textNote="";
                    try
                    {
                        Data.hideSoftKeyboard(ProductDescription.this);
                        if (!AppStatus.getInstance(getApplicationContext())
                                .isOnline(getApplicationContext())) {
                            MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            PromotePeopleApi();
                        }
                    }catch (Exception e)
                    {
                        Log.v("Error recording",e.toString());
                    }

                }
            });
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stop();
                    promoteType="0";
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });
            textNoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promoteType="1";
                    doneBtn.setText(getResources().getString(R.string.doneLabel));
                    textNoteLayout.setVisibility(View.VISIBLE);
                    textNoteButtonLayout.setVisibility(View.GONE);
                    popupIstTimeView.setVisibility(View.GONE);


                }
            });
            voiceNoteBtnLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promoteType="2";
                    if(Config.getRecordinStatus()==0) {
                        doneBtn.setText(getResources().getString(R.string.doneLabel));
                        textMessage.setText(getResources().getString(R.string.voiceNote));
                        timerRec.setText("00:00");
                        textNoteButtonLayout.setVisibility(View.GONE);
                        Config.setRecordinStatus(1);
                    }
                    else if(Config.getRecordinStatus()==1) {

                        RecordVoice();
                        start();

                        Config.setRecordinStatus(2);
                        voiceNoteBtn.setBackgroundResource(R.drawable.pause_selecter_btn);

                        try {


                            countDowntimer = new CountDownTimer(30000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    timerRec.setText("00:"+(millisUntilFinished / 1000));
                                    voiceNoteDuration=(int)millisUntilFinished / 1000;

                                }

                                public void onFinish() {
//                                Toast.makeText(getApplicationContext(), "Stop recording Automatically ", Toast.LENGTH_LONG).show();

                                    stop();
                                    voiceNoteBtn.setBackgroundResource(R.drawable.play_recording_selecter_btn);

                                }
                            };
                            countDowntimer.start();

                        }catch (Exception e){
                            Log.e("ERROR","Recording error in stop");
                        }
                    }
                    else if(Config.getRecordinStatus()==2) {
                        countDowntimer.cancel();
                        stop();
                        voiceNoteBtn.setBackgroundResource(R.drawable.play_recording_selecter_btn);

                    }
                    else if(Config.getRecordinStatus()==3) {
                        play();
                        voiceNoteBtn.setBackgroundResource(R.drawable.record_selecter_btn);
                        try {


                            countDowntimer = new CountDownTimer(voiceNoteDuration, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    timerRec.setText("00:"+(millisUntilFinished / 1000));
                                    voiceNoteDuration=(int)millisUntilFinished / 1000;

                                }

                                public void onFinish() {
//                                Toast.makeText(getApplicationContext(), "Stop recording Automatically ", Toast.LENGTH_LONG).show();

                                    stop();


                                }
                            };
                            countDowntimer.start();

                        }catch (Exception e){
                            Log.e("ERROR","Recording error in stop");
                        }
                    }

                }
            });




            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void RecordVoice()
    {
        // store it to sd card
        //outputFile = Environment.getExternalStorageDirectory().
        //       getAbsolutePath() +"/ButlersApp"+ "/butlers.3gpp";

        myPlayer = new MediaPlayer();
        myRecorder = new MediaRecorder();
        myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myRecorder.setOutputFile(getFilename());
        mFileTemp=new File(getFilename());

    }
    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" +"butlerpromoteaudio"+ file_exts[currentFormat]);
    }

    public void start(){
        try {
            myPlayer.stop();

            myRecorder.prepare();
            myRecorder.start();
        } catch (IllegalStateException e) {
            // start:it is called before prepare()
            // prepare: it is called after start() or before setOutputFormat()
            e.printStackTrace();
        } catch (IOException e) {
            // prepare() fails
            e.printStackTrace();
        }



        Toast.makeText(getApplicationContext(), "Start recording...",
                Toast.LENGTH_SHORT).show();
    }

    public void stop(){
        try {
            myRecorder.stop();
            myRecorder.release();
            myRecorder  = null;

//            stopBtn.setEnabled(false);
            Config.setRecordinStatus(3);
//            view.setEnabled(true);
//            text.setText("Recording Point: Stop recording");

            Toast.makeText(getApplicationContext(), "Stop recording...",
                    Toast.LENGTH_SHORT).show();
        } catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
        }
    }
    public void play() {
        try{

            myPlayer.setDataSource(getFilename());
            myPlayer.prepare();
            myPlayer.start();
            Config.setRecordinStatus(1);
//            playBtn.setEnabled(false);
//            stopPlayBtn.setEnabled(true);
//            text.setText("Recording Point: Playing");

            Toast.makeText(getApplicationContext(), "Start play the recording...",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        try {
            myRecorder.stop();
            myRecorder.release();
            myRecorder  = null;
            Config.setRecordinStatus(0);

        } catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * Proote peole Api
     */
    public void PromotePeopleApi() {

        String idList = userIdsCommaSeparated.toString();
        String csv = idList.substring(1, idList.length() - 1).replace(", ", ",");

        Data.loading_box(ProductDescription.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_id",Data.productId);
        params.put("promote_type", promoteType);
        if(promoteType.equals("2")) {
            params.put("voice_note_duration", voiceNoteDuration + "");
        }
        else
        {
            params.put("voice_note_duration", "");
        }
        params.put("text_note", textNote);
        params.put("user_ids", csv);
        try {

            if(promoteType.equals("2")) {
                if (mFileTemp.length() > 0)
                    params.put("voice_note", mFileTemp);
                else {

                    params.put("voice_note", "");
                }
            }
            else
            {
                params.put("voice_note", "");
            }

        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.get(Config.getBaseURL() + "makePromote", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(ProductDescription.this, errorLayout, jObj.getString("message"));

                            }
                            else{
                                MaterialDesignAnimations.fadeIn(ProductDescription.this, errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(ProductDescription.this, errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }
}
