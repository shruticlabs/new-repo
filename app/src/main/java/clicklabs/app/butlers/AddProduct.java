package clicklabs.app.butlers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.InternalStorageContentProvider;
import material.animations.MaterialDesignAnimations;
import nl.changer.polypicker.ImagePickerActivity;
import nl.changer.polypicker.utils.ImageInternalFetcher;
import simplecropimage.CropImage;

public class AddProduct extends Activity {

    ImageView productImage;
    Button addProductBtn;
    public static final String TAG = "AddProduct";

    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    public static final int REQUEST_CODE_GALLERY      = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;
    private File mFileTemp;
    int photoCount=0;
    private Context mContext;
   // ArrayList<File> mMedia = new HashSet<File>();
    ArrayList<String> imagePaths = new ArrayList<String>();
    HashSet<Uri> mMedia = new HashSet<Uri>();
    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    private static final int INTENT_REQUEST_GET_N_IMAGES = 14;
    ViewGroup mSelectedImagesContainer;
    ImageView thumb1,thumb2,thumb3,thumb4,checkBtn;
    Button deleteBtn1,deleteBtn2,deleteBtn3,deleteBtn14;
    RelativeLayout NextBtnMain,cashOnDeliverChk,backBtnMain;
    Context activity;
    LinearLayout errorLayout,cashDeliveryLayout,tabImagesLayout;
    MaterialEditText productName,productQuantity,productDesc,pricePerProduct,tags;
    String productCategory="1",cashOnDelivery="0",picStatus="0";
    RadioGroup buySellRadio;
    CustomTextViewSemiBold header,footerLabel;
    int chkFlag=0;
    Boolean plusTapped=false;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_layout);
        activity=AddProduct.this;
        errorLayout = (LinearLayout) findViewById(R.id.errorLayout);
        tabImagesLayout = (LinearLayout) findViewById(R.id.tabImagesLayout);
        cashDeliveryLayout = (LinearLayout) findViewById(R.id.cashDeliveryLayout);
        productImage=(ImageView)findViewById(R.id.peoductImage);
        checkBtn=(ImageView)findViewById(R.id.checkBtncash);
        cashOnDeliverChk=(RelativeLayout)findViewById(R.id.cashOnDeliverChk);
        NextBtnMain=(RelativeLayout)findViewById(R.id.NextBtnMain);
        backBtnMain=(RelativeLayout)findViewById(R.id.backBtnMain);
        productName=(MaterialEditText)findViewById(R.id.productName);
        productQuantity=(MaterialEditText)findViewById(R.id.productQuantity);
        productDesc=(MaterialEditText)findViewById(R.id.productDesc);
        pricePerProduct=(MaterialEditText)findViewById(R.id.pricePerProduct);
        header=(CustomTextViewSemiBold)findViewById(R.id.header);
        footerLabel=(CustomTextViewSemiBold)findViewById(R.id.footerLabel);
        tags=(MaterialEditText)findViewById(R.id.tags);
        thumb1=(ImageView)findViewById(R.id.thumb1);
        thumb2=(ImageView)findViewById(R.id.thumb2);
        thumb3=(ImageView)findViewById(R.id.thumb3);
        thumb4=(ImageView)findViewById(R.id.thumb4);
        header.setText(getResources().getString(R.string.addProduct));
        footerLabel.setText(getResources().getString(R.string.done));
        addProductBtn=(Button)findViewById(R.id.addProductBtn);
        mContext = AddProduct.this;


        /**** gender click***/
        buySellRadio = (RadioGroup) findViewById(R.id.buySellRadio);
        RadioButton radioBuy = (RadioButton) findViewById(R.id.buy);
        RadioButton  radioSell= (RadioButton) findViewById(R.id.sell);

        radioBuy.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = buySellRadio.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton buySellRadio = (RadioButton) findViewById(selectedId);

                if(buySellRadio.getText().equals("Buy"))
                    productCategory="1";
                else
                    productCategory="0";
                cashDeliveryLayout.setVisibility(View.GONE);
                cashOnDelivery="0";


            }

        });
        radioSell.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = buySellRadio.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton buySellRadio = (RadioButton) findViewById(selectedId);
                if(buySellRadio.getText().equals("Buy"))
                    productCategory="1";
                else
                    productCategory="0";
                cashDeliveryLayout.setVisibility(View.VISIBLE);


            }

        });
/**
 *
 * Add Product
 */
        NextBtnMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imagePaths.size()<=0)
                {
                    MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.noImageChoosen));

                }

                 else if (productName.getText().length() == 0) {
                    productName.requestFocus();
                    productName.setError(getResources().getString(R.string.productnameRequired));
                } else  if (productQuantity.getText().length() == 0) {
                    productQuantity.requestFocus();
                    productQuantity.setError(getResources().getString(R.string.productqtyRequired));
                } else if (productDesc.getText().length() == 0) {
                    productDesc.requestFocus();
                    productDesc.setError(getResources().getString(R.string.productdescriptionRequired));
                }
                else if (pricePerProduct.getText().length() == 0) {
                    pricePerProduct.requestFocus();
                    pricePerProduct.setError(getResources().getString(R.string.priceperProductRequired));
                }else {
                    if (!AppStatus.getInstance(activity)
                            .isOnline(activity)) {
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.internetConnectionError));

                    } else {
                        Data.hideSoftKeyboard(AddProduct.this);

                        AddProductApi();
                    }


                }
            }


        });

        cashOnDeliverChk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

           if(chkFlag==0)
           {
               checkBtn.setBackgroundResource(R.drawable.btn_chkbox_pressed);
               cashOnDelivery="1";
               chkFlag=1;

           }
            else
           {

               checkBtn.setBackgroundResource(R.drawable.btn_chkbox_normal);
               cashOnDelivery="0";
               chkFlag=0;
           }

            }


        });


        addProductBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=true;
                photoCount=0;
               // mMedia.clear();
               // imagePaths.clear();
                getNImages(5);

             //showChooserPopup();
            }


        });
        productImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=false;
                photoCount=0;
                getNImages(1);
            }


        });
        thumb1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=false;
                photoCount=1;
                getNImages(1);
            }


        });

//        thumb1.setOnLongClickListener(new View.OnLongClickListener() {
//
//            @Override
//            public boolean onLongClick(View v) {
//                //your stuff
//                deleteBtn1.setVisibility(View.VISIBLE);
//                return true;
//            }
//        });


        thumb2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=false;
                photoCount=2;
                getNImages(1);
            }


        });
        thumb3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=false;
                photoCount=3;
                getNImages(1);
            }


        });
        thumb4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                plusTapped=false;
                photoCount=4;
                getNImages(1);
            }


        });

        backBtnMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                finish();

                //showChooserPopup();
            }


        });

        Data.hideSoftKeyboard(AddProduct.this);
    }


    public void updatedPath()
    {
        Time time = new Time();
        String timestr=Long.toString(time.toMillis(false));
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), timestr+TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), timestr+TEMP_PHOTO_FILE_NAME);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        finish();
    }

    private void showChooserPopup() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Choose product image from");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

//                if(photoCount==0)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_0.jpg";
//                    updatedPath();
//                }
//                else if(photoCount==1)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_1.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==2)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_2.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==3)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_3.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==4)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_4.jpg";
//                    updatedPath();
//                }
                updatedPath();
                openGallery();

            } });


        adb.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

//                if(photoCount==0)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_0.jpg";
//                    updatedPath();
//                }
//                else if(photoCount==1)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_1.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==2)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_2.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==3)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_3.jpg";
//                    updatedPath();
//                }
//                else  if(photoCount==4)
//                {
//                    TEMP_PHOTO_FILE_NAME = "temp_photo_4.jpg";
//                    updatedPath();
//                }
                updatedPath();
                takePicture();



            } });
        adb.show();
    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            }
            else {
	        	/*
	        	 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        } catch (ActivityNotFoundException e) {

            Log.d(TAG, "cannot take picture", e);
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }
    private void getNImages(int count) {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);

        // limit image pick count to only 3 images.
        intent.putExtra(ImagePickerActivity.EXTRA_SELECTION_LIMIT, count);
        startActivityForResult(intent, INTENT_REQUEST_GET_N_IMAGES);
    }
    @Override
    protected void onActivityResult(int requestCode, int resuleCode, Intent intent) {
        super.onActivityResult(requestCode, resuleCode, intent);

        if (resuleCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_REQUEST_GET_IMAGES || requestCode == INTENT_REQUEST_GET_N_IMAGES) {
                Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

                if(parcelableUris ==null) {
                    return;
                }

                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);
               // mMedia.clear();
                if(uris != null) {
                    int index=0;

                    if(!plusTapped)
                    {
                        for (Uri uri : uris) {

                            imagePaths.remove(photoCount);
                            imagePaths.add(photoCount,uri.toString());

                        }
                    }
                    else {
                        for (Uri uri : uris) {

                            imagePaths.add(index, uri.toString());
                            index++;

                            Log.i(TAG, " uri: " + uri);
//                        if(imagePaths.size()<5) {
//                            if (!imagePaths.contains(uri)) {
//                               // mMedia.add(uri);
//                                imagePaths.add(uri.toString());
//                            }
//                        }
//                        else
//                       if(imagePaths.size()>0) {
//                           for (int j = 0; j < parcelableUris.length; j++) {
//                               imagePaths.remove(j);
//
//                           }
//                       }

//                        if (!imagePaths.contains(uri)) {
//                            for(int j=0;j<parcelableUris.length;j++)
//                            {
//                                imagePaths.add(j,uri.toString());
//
//                            }
//
//                        }


                        }
                    }

                    showMedia();
                }
            }
        }
    }
    private void showMedia()  {
        // Remove all views before
        // adding the new ones.
//        mSelectedImagesContainer.removeAllViews();

      //  Iterator<Uri> iterator = mMedia.iterator();
        ImageInternalFetcher imageFetcher = new ImageInternalFetcher(this, 500);
       // while(iterator.hasNext()) {
        for(int i=0;i<imagePaths.size();i++) {
            //Uri uri = iterator.next();

            // showImage(uri);
          //  Log.i(TAG, " uri: " + uri);
            if(imagePaths.size() >= 1) {
                tabImagesLayout.setVisibility(View.VISIBLE);
            }
            else
            {
                tabImagesLayout.setVisibility(View.GONE);
            }

            View imageHolder = LayoutInflater.from(this).inflate(R.layout.media_layout, null);

            // View removeBtn = imageHolder.findViewById(R.id.remove_media);
            // initRemoveBtn(removeBtn, imageHolder, uri);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);

//            if(!uri.toString().contains("content://")) {
//                // probably a relative uri
//                uri = Uri.fromFile(new File(uri.toString()));
//
//            }

            Uri uri = null;
            if(!imagePaths.get(i).toString().contains("content://")) {
                // probably a relative uri
                uri = Uri.fromFile(new File(imagePaths.get(i).toString()));

            }
            if(i==0)
                {
                    Bitmap bitmap = null;
                    try {
                        bitmap = getThumbnail(uri);
                    }
                    catch(Exception e)
                    {

                    }
//                    productImage.invalidate();
//                    productImage.setImageBitmap(null);
//                    productImage.setImageBitmap(bitmap);
                    imageFetcher.loadImage(uri, productImage);

                }
                else if(i==1)
                {
                    imageFetcher.loadImage(uri, thumb1);
                }
                else  if(i==2)
                {
                    imageFetcher.loadImage(uri, thumb2);
                }
                else  if(i==3)
                {
                    imageFetcher.loadImage(uri, thumb3);
                }
                else  if(i==4)
                {
                    imageFetcher.loadImage(uri, thumb4);
                }

           // photoCount++;
//            mSelectedImagesContainer.addView(imageHolder);
//
//            // set the dimension to correctly
//            // show the image thumbnail.
//            int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
//            int htpx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
//            thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));
        }
    }
    public  Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException{
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither=true;//optional
        onlyBoundsOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 80) ? (originalSize / 80) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither=true;//optional
        bitmapOptions.inPreferredConfig=Bitmap.Config.ARGB_8888;//optional
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }
    private static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }
   // @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (resultCode != RESULT_OK) {
//
//            return;
//        }
//
//        Bitmap bitmap;
//
//        switch (requestCode) {
//
//            case REQUEST_CODE_GALLERY:
//
//                try {
//
//                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
//                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
//                    copyStream(inputStream, fileOutputStream);
//                    fileOutputStream.close();
//                    inputStream.close();
//
//                    startCropImage();
//
//                } catch (Exception e) {
//
//                    Log.e(TAG, "Error while creating temp file", e);
//                }
//
//                break;
//            case REQUEST_CODE_TAKE_PICTURE:
//
//                startCropImage();
//                break;
//            case REQUEST_CODE_CROP_IMAGE:
//
//                String path = data.getStringExtra(CropImage.IMAGE_PATH);
//                if (path == null) {
//
//                    return;
//                }
//
//                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
//                productImage.setImageBitmap(bitmap);
//                photoCount++;
//                mMedia.add(mFileTemp);
//                break;
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }


    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

//    params.put("userimage[" + i + "]", new File(uniquePositions.get(entry.getKey())));


    /**
     * Login via email api
     */
    public void AddProductApi() {
        if(imagePaths.size()>0)
            picStatus="1";
        else
            picStatus="0";
        Data.loading_box(AddProduct.this, "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_name", productName.getText().toString());
        params.put("quantity", productQuantity.getText().toString());
        params.put("description", productDesc.getText().toString());
        params.put("category", productCategory);
        params.put("price", pricePerProduct.getText().toString());
        params.put("tags", tags.getText().toString());
        params.put("cash_on_delivery_flag", cashOnDelivery);
//        params.put("image",imagePaths);
        params.put("pic_status", picStatus);
        try {
            if (imagePaths.size() > 0) {
                for (int i = 0; i < imagePaths.size(); i++)
                    params.put("image[" + i + "]", new File(imagePaths.get(i)));
            }
        }
        catch(Exception e)
        {
            Log.e("ERROR","ERROR"+e.toString());
        }
        AsyncHttpClient client =
                new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "addProduct", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                            }
                            else {
                                MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, jObj.getString("message"));
                                Data.loading_box_stop();
                            }



                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getApplicationContext(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }
                });

    }


}