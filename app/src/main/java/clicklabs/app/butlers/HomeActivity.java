package clicklabs.app.butlers;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by click103 on 12/3/15.
 */
import java.util.HashMap;
import java.util.Map;

import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.ParallaxPageTransformer;
import pager.fragment.AddShopFragment;
import pager.fragment.AllNotificationFrag;
import pager.fragment.BazaarFragmentTab;
import pager.fragment.BazarNotificationFrag;
import pager.fragment.CommentProductFragment;
import pager.fragment.EditProfileFragment;
import pager.fragment.FeedFragmentTab;
import pager.fragment.FindInviteFragment;
import pager.fragment.FriendProductFrag;
import pager.fragment.FriendProfileFrag;
import pager.fragment.HomFragment;
import pager.fragment.InfoFragment;
import pager.fragment.LikeProductFragment;
import pager.fragment.OffersFragmentTab;
import pager.fragment.OptionFragment;
import pager.fragment.ProfileFragmentTab;
import pager.fragment.PromotedProductsFrag;
import pager.fragment.SearchFragmentTab;
import pager.fragment.SearchPeopleFrag;
import pager.fragment.SearchProductsFrag;
import pager.fragment.ShopFragment;


public class HomeActivity extends FragmentActivity  {


    public MenuItem item;


    //    static final int ITEMS = 5;
//    ButlerPagerAdapter butlerPagerAdapter;
//    ViewPager viewPager;
//
//    public static LinearLayout lastSelectedButton;
//    public static int selectedButtonHeight;
//    public static int defaultButtonHeight;
//
//
////    public static final int UNSELECTED = Color.rgb(80, 230, 0);
//
//    public static Fragment holderFrangment;
//
//    public static int fragmentHolderId = 0;
//
//    LinearLayout btnBazar, btnSrch, btnProf,btnOfr,btnFeed;
//
//
//
//    public static Fragment bazaarFragmentTab ;
//    public static Fragment searchFragmentTab;
//    public static Fragment feedFragmentTab ;
//    public static Fragment profileFragmentTab ;
//    public static Fragment offersFragmentTab ;
//
//
//    public static FragmentManager manager;
////    public static FragmentTransaction transaction;
//    private final String BACKSTACK = "MY_BACK_STACK";
//    public static boolean isBackStacked = false;
//    static Map<Integer, Integer> buttonValues;
//    static {
//        buttonValues = new HashMap<Integer, Integer>();
//        buttonValues.put(R.id.btnBazar, 1);
//        buttonValues.put(R.id.btnSrch, 2);
//        buttonValues.put(R.id.btnFeed, 3);
//        buttonValues.put(R.id.btnOfr, 4);
//        buttonValues.put(R.id.btnProf, 5);
//
//    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle presses on the action bar items
        switch (item.getItemId()) {

            case R.id.action_search:
                Intent intent = new Intent(getApplicationContext(), AddProduct.class);
                startActivity(intent);
                return true;
            case R.id.viewNotification:

//                Intent intent = new Intent(getApplicationContext(), AddProduct.class);
//                startActivity(intent);
//                openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_layout, new HomFragment(),"HomFragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

//        ActionBar actionBar = getActionBar();
//        actionBar.setTitle("FEED");
//        if(Config.isLollyPop())
//        getActionBar().setElevation(0);
//        getActionBar().setDisplayHomeAsUpEnabled(false);



//        viewPager.setPageTransformer(true, new ParallaxPageTransformer());
//        viewPager.setOffscreenPageLimit(ITEMS);
//        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
//        tabsStrip.setViewPager(viewPager);







//        // add the custom view to the action bar
////        actionBar.setCustomView(R.layout.top_header_bar);
////        actionBar.setBackgroundDrawable(new ColorDrawable(R.color.blue));
//
//
//
//
//
//        initViews();
////        initFragments();
////        initManager();
//
//        butlerPagerAdapter = new ButlerPagerAdapter(getSupportFragmentManager());
//        viewPager = (ViewPager) findViewById(R.id.pager);
//        viewPager.setAdapter(butlerPagerAdapter);
//        viewPager.setPageTransformer(true, new ParallaxPageTransformer());
//        viewPager.setOffscreenPageLimit(ITEMS);
//        viewPager.setCurrentItem(2);
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            // This method will be invoked when a new page becomes selected.
//
//            @Override
//            public void onPageSelected(int position) {
//
//                lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent));
//                if(position==0) {
//                    lastSelectedButton=btnBazar;
//                }
//                if(position==1) {
//                    lastSelectedButton=btnSrch;
//                }
//                if(position==2) {
//                    lastSelectedButton=btnFeed;
//                }
//                if(position==3) {
//
//                    lastSelectedButton = btnOfr;
//                }
//                if(position==4) {
//                    lastSelectedButton = btnProf;
//                }
//
//                lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
////                Toast.makeText(HomeActivity.this,
////                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
//            }
//
//            // This method will be invoked when the current page is scrolled
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                // Code goes here
//            }
//
//            // Called when the scroll state changes:
//            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                // Code goes here
//            }
//        });
    }




//    private void initFragments() {
//        // TODO Auto-generated method stub
//        bazaarFragmentTab = new BazaarFragmentTab();
//        searchFragmentTab = new SearchFragmentTab();
//        feedFragmentTab = new FeedFragmentTab();
//        profileFragmentTab = new ProfileFragmentTab();
//        offersFragmentTab = new OffersFragmentTab();
//    }
//
////    private void initManager() {
////        // TODO Auto-generated method stub
////        fragmentHolderId = R.id.fragmentHolder;
////        manager = getSupportFragmentManager();
////        btnFeed.performClick();
////    }
//
//    private void initViews() {
//        // TODO Auto-generated method stub
//        btnBazar = (LinearLayout) findViewById(R.id.btnBazar);
//        btnSrch = (LinearLayout) findViewById(R.id.btnSrch);
//        btnFeed = (LinearLayout) findViewById(R.id.btnFeed);
//        btnProf = (LinearLayout) findViewById(R.id.btnProf);
//        btnOfr = (LinearLayout) findViewById(R.id.btnOfr);
//        btnFeed.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
//        btnBazar.setOnClickListener(this);
//        btnSrch.setOnClickListener(this);
//        btnFeed.setOnClickListener(this);
//        btnProf.setOnClickListener(this);
//        btnOfr.setOnClickListener(this);
//
//        lastSelectedButton = btnFeed;
//
//    }
//
//    @Override
//    public void onClick(View v) {
////        // TODO Auto-generated method stub
//        lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent));
//        switch (v.getId()) {
//
//            case R.id.btnBazar:
//                viewPager.setCurrentItem(0);
//                lastSelectedButton = btnBazar;
//                break;
//
//            case R.id.btnSrch:
//                viewPager.setCurrentItem(1);
//                lastSelectedButton = btnSrch;
//                break;
//
//            case R.id.btnFeed:
//                viewPager.setCurrentItem(2);
//                lastSelectedButton = btnFeed;
//                break;
//
//            case R.id.btnOfr:
//                viewPager.setCurrentItem(3);
//                lastSelectedButton = btnOfr;
//                break;
//            case R.id.btnProf:
//                viewPager.setCurrentItem(4);
//                lastSelectedButton = btnProf;
//                break;
//
//            default:
//                break;
//        }
//
//        lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
//    }
//
//    public static class ButlerPagerAdapter extends FragmentPagerAdapter {
//        public ButlerPagerAdapter(FragmentManager fragmentManager) {
//            super(fragmentManager);
//        }
//
//        @Override
//        public int getCount() {
//            return ITEMS;
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//
//            switch (position) {
//                case 0:
//                    return new BazaarFragmentTab().init(position);
//                case 1:
//                    return SearchFragmentTab.init(position);
//                case 2:
//                    return FeedFragmentTab.init(position);
//                case 3:
//                    return SearchFragmentTab.init(position);
//                case 4:
//                    return SearchFragmentTab.init(position);
//
//                default:// Fragment # 2-9 - Will show list
//                    return FeedFragmentTab.init(position);
//            }
//        }
//
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);




    }

//    }

    @Override
    public void onBackPressed() {

        Config.setSKIP_MODE(false);
        Fragment fragment=getCurrentFragment();
        if(fragment!=null) {
            if ((fragment.getClass().getName()).contentEquals(FindInviteFragment.class.getName()))
                ((FindInviteFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(OptionFragment.class.getName()))
                ((OptionFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(InfoFragment.class.getName()))
                ((InfoFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(PromotedProductsFrag.class.getName()))
                ((PromotedProductsFrag) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(FriendProfileFrag.class.getName()))
                ((FriendProfileFrag) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(ShopFragment.class.getName()))
                ((ShopFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(AddShopFragment.class.getName()))
                ((AddShopFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(EditProfileFragment.class.getName()))
                ((EditProfileFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(LikeProductFragment.class.getName()))
                ((LikeProductFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(CommentProductFragment.class.getName()))
                ((CommentProductFragment) fragment).pressBack();
            if ((fragment.getClass().getName()).contentEquals(FriendProductFrag.class.getName()))
                ((FriendProductFrag) fragment).pressBack();
        }
        super.onBackPressed();


    }

    public Fragment getCurrentFragment(){
//        FragmentManager fragmentManager = getSupportFragmentManager();
        HomFragment parent= (HomFragment) getSupportFragmentManager().findFragmentByTag("HomFragment");
//        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.add_shop_fragment_container);
        return currentFragment;
    }
}









//public class HomeActivity extends FragmentActivity implements  {
//
//    LinearLayout notloggedinlayout;
//    ActionBar.Tab bazTab, srchTab, fedTab,profTab,offrTab;
//    Fragment bazaarFragmentTab = new BazaarFragmentTab();
//    Fragment searchFragmentTab = new SearchFragmentTab();
//    Fragment feedFragmentTab = new FeedFragmentTab();
//    Fragment profileFragmentTab = new ProfileFragmentTab();
//    Fragment offersFragmentTab = new OffersFragmentTab();
//    ActionBar actionBar;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        setContentView(R.layout.home);
//
//         actionBar = getActionBar();
//
//        // Screen handling while hiding ActionBar icon.
//        actionBar.setDisplayShowHomeEnabled(false);
//
//        // Screen handling while hiding Actionbar title.
//        actionBar.setDisplayShowTitleEnabled(false);
//
//        // Creating ActionBar tabs.
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//
//        // Setting custom tab icons.
////        bazTab = actionBar.newTab().setIcon(R.drawable.logo);
////        srchTab = actionBar.newTab().setIcon(R.drawable.logo);
////        fedTab = actionBar.newTab().setIcon(R.drawable.logo);
////        offrTab = actionBar.newTab().setIcon(R.drawable.logo);
////        profTab = actionBar.newTab().setIcon(R.drawable.logo);
//        actionBar.setCustomView(R.layout.bottomtabbar);
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
////       View btn= actionBar.getCustomView().findViewById(R.id.btn_1);
//////Button btn =(Button) findViewById(R.id.btn_1);
//////        final ActionBar.Tab tab = actionBar.newTab();
////        btn.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
//////                tab.select();
////                Toast.makeText(getApplication(),"hellp",Toast.LENGTH_LONG).show();
////
////            }
////        });
//        // Setting tab listeners.
////        bazTab.setTabListener(new TabListener(bazaarFragmentTab));
////        srchTab.setTabListener(new TabListener(searchFragmentTab));
////        fedTab.setTabListener(new TabListener(feedFragmentTab));
////        offrTab.setTabListener(new TabListener(offersFragmentTab));
////        profTab.setTabListener(new TabListener(profileFragmentTab));
//////
//////        // Adding tabs to the ActionBar.
////        actionBar.addTab(bazTab);
////        actionBar.addTab(srchTab);
////        actionBar.addTab(fedTab);
////        actionBar.addTab(offrTab);
////        actionBar.addTab(profTab);
//
////        notloggedinlayout=(LinearLayout) findViewById(R.id.notloggedinlayout);
////
////        if(Config.isLOGIN_STATUS())
////            notloggedinlayout.setVisibility(View.GONE);
////        else
////            notloggedinlayout.setVisibility(View.VISIBLE);
//
//    }
//}
