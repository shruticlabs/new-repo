package models;

/**
 * Model class for add promoter
 *
 * @author showket
 */
public class PromoteUserListModel {
    private String namefield, imagefield,rowId;
    int position;
    Boolean isSelected;



    public PromoteUserListModel() {
    }




    public PromoteUserListModel(String namefield, String imagefield,String rowId,Boolean isSelected) {
        this.namefield = namefield;
        this.imagefield = imagefield;
        this.rowId=rowId;
        this.isSelected=isSelected;

    };

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getNamefield() {
        return namefield;
    }

    public void setNamefield(String namefield) {
        this.namefield = namefield;
    }

    public String getImagefield() {
        return imagefield;
    }

    public void setImagefield(String imagefield) {
        this.imagefield = imagefield;
    }
}