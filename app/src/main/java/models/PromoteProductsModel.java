package models;

/**
 * Model class for products
 *
 * @author showket
 */
public class PromoteProductsModel {

    private String userProfileImage;
    private String userName;
    private String userRaings;
    private String productAddedTime;
    private String addressField;
    private String productImage;
    private String productModel;
    private String userWhoRated;
    private String userWhoRatedRatings;
    private String otherUserWhoRatedCount;
    private String likesCount;
    private String commentCount;
    private String productPrice;
    private String availableCount;
    private String userId;
    private String isLiked;
    private String productId;

    public String getVoiveNoteUrl() {
        return voiveNoteUrl;
    }

    public void setVoiveNoteUrl(String voiveNoteUrl) {
        this.voiveNoteUrl = voiveNoteUrl;
    }

    private String voiveNoteUrl;
    int position;
    Boolean isShownLayout;

    public int getPlayBtnBehaviour() {
        return playBtnBehaviour;
    }

    public void setPlayBtnBehaviour(int playBtnBehaviour) {
        this.playBtnBehaviour = playBtnBehaviour;
    }

    int playBtnBehaviour;



    public PromoteProductsModel() {
    }




    public PromoteProductsModel(String userProfileImage, String userName, String userRaings,
                                int position, String productAddedTime, String addressField, String productImage, String productModel, String userWhoRated, String userWhoRatedRatings, String otherUserWhoRatedCount,
                                String likesCount, String commentCount, String productPrice, String availableCount, String userId, String isLiked, String productId, Boolean isShownLayout,int playBtnBehaviour,String voiveNoteUrl) {


        this.userProfileImage = userProfileImage;
        this.userName = userName;
        this.userRaings = userRaings;
        this.position = position;
        this.productAddedTime = productAddedTime;
        this.addressField = addressField;
        this.productImage = productImage;
        this.productModel = productModel;
        this.userWhoRated = userWhoRated;
        this.userWhoRatedRatings = userWhoRatedRatings;
        this.otherUserWhoRatedCount = otherUserWhoRatedCount;
        this.likesCount = likesCount;
        this.commentCount = commentCount;
        this.productPrice = productPrice;
        this.availableCount = availableCount;
        this.userId = userId;
        this.isLiked = isLiked;
        this.productId = productId;
        this.isShownLayout=isShownLayout;
        this.playBtnBehaviour=playBtnBehaviour;
        this.voiveNoteUrl=voiveNoteUrl;
    };

    public Boolean getIsShownLayout() {
        return isShownLayout;
    }

    public void setIsShownLayout(Boolean isShownLayout) {
        this.isShownLayout = isShownLayout;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRaings() {
        return userRaings;
    }

    public void setUserRaings(String userRaings) {
        this.userRaings = userRaings;
    }

    public String getProductAddedTime() {
        return productAddedTime;
    }

    public void setProductAddedTime(String productAddedTime) {
        this.productAddedTime = productAddedTime;
    }

    public String getAddressField() {
        return addressField;
    }

    public void setAddressField(String addressField) {
        this.addressField = addressField;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getUserWhoRated() {
        return userWhoRated;
    }

    public void setUserWhoRated(String userWhoRated) {
        this.userWhoRated = userWhoRated;
    }

    public String getUserWhoRatedRatings() {
        return userWhoRatedRatings;
    }

    public void setUserWhoRatedRatings(String userWhoRatedRatings) {
        this.userWhoRatedRatings = userWhoRatedRatings;
    }

    public String getOtherUserWhoRatedCount() {
        return otherUserWhoRatedCount;
    }

    public void setOtherUserWhoRatedCount(String otherUserWhoRatedCount) {
        this.otherUserWhoRatedCount = otherUserWhoRatedCount;
    }

    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(String availableCount) {
        this.availableCount = availableCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}