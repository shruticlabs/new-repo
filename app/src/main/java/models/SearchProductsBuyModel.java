package models;

/**
 * Model class for products
 *
 * @author showket
 */
public class SearchProductsBuyModel {

    private String userProfileImage;
    private String userName;
    private String userRaings;
    private String productAddedTime;
    private String productDesc;
    private String addressField;
    private String productImage;
    private String dollarSeparatedproductImage;
    private String productModel;
    private String likesCount;
    private String commentCount;
    private String productPrice;
    private String availableCount;
    private String userId;
    private String isLiked;
    private String productId;
    Boolean isShownLayout;


    int playBtnBehaviour;



    public SearchProductsBuyModel() {
    }




    public SearchProductsBuyModel(String userProfileImage, String userName, String userRaings, String productAddedTime, String productImage,String dollarSeparatedproductImage, String productModel,
                                  String likesCount, String commentCount, String productPrice, String userId, String isLiked, String productId,String address,String availableCount,String productDesc, Boolean isShownLayout) {


        this.userProfileImage = userProfileImage;
        this.userName = userName;
        this.userRaings = userRaings;
        this.productAddedTime = productAddedTime;
        this.productImage = productImage;
        this.dollarSeparatedproductImage = dollarSeparatedproductImage;
        this.productModel = productModel;
        this.likesCount = likesCount;
        this.commentCount = commentCount;
        this.productPrice = productPrice;
        this.userId = userId;
        this.isLiked = isLiked;
        this.productId = productId;
        this.addressField = address;
        this.isShownLayout=isShownLayout;
        this.availableCount=availableCount;
        this.productDesc=productDesc;
    };

    public Boolean getIsShownLayout() {
        return isShownLayout;
    }

    public void setIsShownLayout(Boolean isShownLayout) {
        this.isShownLayout = isShownLayout;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRaings() {
        return userRaings;
    }

    public void setUserRaings(String userRaings) {
        this.userRaings = userRaings;
    }

    public String getProductAddedTime() {
        return productAddedTime;
    }

    public void setProductAddedTime(String productAddedTime) {
        this.productAddedTime = productAddedTime;
    }

    public String getAddressField() {
        return addressField;
    }

    public void setAddressField(String addressField) {
        this.addressField = addressField;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getDollarSeparatedproductImage() {
        return dollarSeparatedproductImage;
    }

    public void setDollarSeparatedproductImage(String dollarSeparatedproductImage) {
        this.dollarSeparatedproductImage = dollarSeparatedproductImage;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }


    public String getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(String likesCount) {
        this.likesCount = likesCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getAvailableCount() {
        return availableCount;
    }

    public void setAvailableCount(String availableCount) {
        this.availableCount = availableCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(String isLiked) {
        this.isLiked = isLiked;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }
}