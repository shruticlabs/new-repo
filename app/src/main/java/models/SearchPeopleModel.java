package models;

/**
 * Model class for products
 *
 * @author showket
 */
public class SearchPeopleModel {

    private String userProfileImage,userName,userId;




    public SearchPeopleModel() {
    }




    public SearchPeopleModel(String userProfileImage, String userName,String userId) {


        this.userProfileImage = userProfileImage;
        this.userName = userName;
        this.userId = userId;
    };

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}