package models;

/**
 * Model class for add promoter
 *
 * @author showket
 */
public class HowmanyLikeModel {
    private String namefield, imagefield,rowId;
    int position;
    Boolean hasVoiceNote;



    public HowmanyLikeModel() {
    }




    public HowmanyLikeModel(String namefield, String imagefield,String rowId,Boolean hasVoiceNote) {
        this.namefield = namefield;
        this.imagefield = imagefield;
        this.rowId=rowId;
        this.hasVoiceNote=hasVoiceNote;

    };

    public Boolean getIsSelected() {
        return hasVoiceNote;
    }

    public void setIsSelected(Boolean isSelected) {
        this.hasVoiceNote = isSelected;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getNamefield() {
        return namefield;
    }

    public void setNamefield(String namefield) {
        this.namefield = namefield;
    }

    public String getImagefield() {
        return imagefield;
    }

    public void setImagefield(String imagefield) {
        this.imagefield = imagefield;
    }
}