package models;

/**
 * Created by saify on 29/4/15.
 */
public class FacebookFriendListModel {
    private String fbName, fbId;
    boolean isOffered;

    public FacebookFriendListModel(String fbName, String fbId,Boolean isOffered) {
        this.fbName = fbName;
        this.fbId = fbId;
        this.isOffered=isOffered;
    }



    public boolean isOffered() {
        return isOffered;
    }

    public void setOffered(boolean isOffered) {
        this.isOffered = isOffered;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }



    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }
}
