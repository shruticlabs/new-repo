package models;

/**
 * Created by click103 on 31/3/15.
 */
public class ShopModel {

    private String shopImage,shopName, shopQuantity, shopId,shopType;



    public ShopModel(String shopImage,String shopName,String shopQuantity,String shopId,String shopType){
        this.shopId=shopId;
        this.shopName=shopName;
        this.shopImage=shopImage;
        this.shopQuantity=shopQuantity;
        this.shopType=shopType;

    };

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopQuantity() {
        return shopQuantity;
    }

    public void setShopQuantity(String shopQuantity) {
        this.shopQuantity = shopQuantity;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }
}
