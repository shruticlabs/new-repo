package models;

/**
 * Model class for products
 *
 * @author showket
 */
public class CommentModel {

    private String userProfileImage,userName, userComment, commentTime,productId;
    int position;



    public CommentModel() {
    }




    public CommentModel(String userProfileImage, String userName, String userComment,String commentTime, String productId) {


        this.userProfileImage = userProfileImage;
        this.userName = userName;
        this.userComment = userComment;
        this.commentTime = commentTime;
        this.productId = productId;
    };

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}