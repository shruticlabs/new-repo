package adapters;

/**
 * Created by saify on 30/4/15.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;

import java.util.List;

import clabs.butlers.utils.CustomTextViewRegularFont;
import models.PromoteUserListModel;


/**
 * Comments adapter
 *
 * @author showket
 */
public class LikesAdapter extends BaseAdapter {

    private List<PromoteUserListModel> all_likes = null;

    Activity activity;

    LayoutInflater inflater = null;
    boolean check[];

    ViewHolderLikes holder;

    // Typeface face, face1;

    public LikesAdapter(Activity a, List<PromoteUserListModel> all_likes) {

        this.all_likes = all_likes;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
        activity = a;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return all_likes.size();
    }

    @Override
    public PromoteUserListModel getItem(int position) {
        return all_likes.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(
                    R.layout.list_item_like, null);
            holder = new ViewHolderLikes();
            holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

            holder.img = (ImageView) convertView
                    .findViewById(R.id.userImage);
            holder.nameField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);

            holder.offferLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.selectBtn);


            convertView.setTag(holder);


        } else {
            holder = (ViewHolderLikes) convertView.getTag();
        }



        holder.nameField.setText(all_likes.get(position).getNamefield());
//            holder.comment.setText(all_likes.get(position).getUserComment());


        AQuery aq = new AQuery(convertView);

        try{

            aq.id(holder.img)
                    .image(all_likes.get(position).getImagefield(), true, true,
                            0, 0, null, 0, AQuery.FADE_IN);
        }catch(Exception e){

        }


        holder.layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }

        });
        return convertView;

    }



}

class ViewHolderLikes {

    RelativeLayout layout;
    ImageView img;
    CustomTextViewRegularFont nameField;
    CustomTextViewRegularFont offferLabel;

}
