package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;

import java.util.List;

import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewMeadiumFont;
import clicklabs.app.butlers.MyApplication;
import models.CommentModel;

/**
 * Created by saify on 30/4/15.
 */
public class CommentAdapter extends BaseAdapter {

    private List<CommentModel> all_comments = null;
    MyApplication applicationClass;

    Activity activity;

    LayoutInflater inflater = null;
    boolean check[];

    ViewHolderComments holder;

    // Typeface face, face1;

    public CommentAdapter(Activity a, List<CommentModel> all_comments) {

        this.all_comments = all_comments;
        applicationClass=new MyApplication();

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
        activity = a;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return all_comments.size();
    }

    @Override
    public CommentModel getItem(int position) {
        return all_comments.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(
                    R.layout.list_item_comment_product, null);
            holder = new ViewHolderComments();
            holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

            holder.img = (ImageView) convertView
                    .findViewById(R.id.userImage);
            holder.nameField = (CustomTextViewMeadiumFont) convertView
                    .findViewById(R.id.userName);
            holder.comment = (CustomTextViewMeadiumFont) convertView
                    .findViewById(R.id.comment);
            holder.timeComment = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.timeComment);


            convertView.setTag(holder);


        } else {
            holder = (ViewHolderComments) convertView.getTag();
        }



        holder.nameField.setText(all_comments.get(position).getUserName());
        holder.comment.setText(all_comments.get(position).getUserComment());
        holder.timeComment.setText(all_comments.get(position).getCommentTime());
        holder.timeComment.setText(applicationClass.getExpirationTimeTodisplay(all_comments.get(position).getCommentTime()));



        AQuery aq = new AQuery(convertView);

        try{

            aq.id(holder.img)
                    .image(all_comments.get(position).getUserProfileImage(), true, true,
                            0, 0, null, 0, AQuery.FADE_IN);
        }catch(Exception e){

        }


        holder.layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }

        });
        return convertView;

    }



}

class ViewHolderComments {

    RelativeLayout layout;
    ImageView img;
    CustomTextViewMeadiumFont nameField,comment;
    CustomTextViewLightFont timeComment;

}
