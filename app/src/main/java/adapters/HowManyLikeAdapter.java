package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;

import java.util.List;

import clabs.butlers.utils.CustomTextViewRegularFont;
import models.HowmanyLikeModel;

/**
 * Created by saify on 30/4/15.
 */
public class HowManyLikeAdapter extends BaseAdapter {

    private List<HowmanyLikeModel> all_likes = null;

    Activity activity;

    LayoutInflater inflater = null;

    ViewHowManyHolderLikes holder;

    public HowManyLikeAdapter(Activity a, List<HowmanyLikeModel> all_likes) {
        this.all_likes = all_likes;
        activity = a;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return all_likes.size();
    }

    @Override
    public HowmanyLikeModel getItem(int position) {
        return all_likes.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {

        if (convertView == null) {
            convertView = activity.getLayoutInflater().inflate(
                    R.layout.how_many_list_item_like, null);
            holder = new ViewHowManyHolderLikes();
            holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

            holder.img = (ImageView) convertView
                    .findViewById(R.id.userImage);
            holder.nameField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);

            holder.offferLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.selectBtn);


            convertView.setTag(holder);


        } else {
            holder = (ViewHowManyHolderLikes) convertView.getTag();
        }



        holder.nameField.setText(all_likes.get(position).getNamefield());
//            holder.comment.setText(all_likes.get(position).getUserComment());


        AQuery aq = new AQuery(convertView);

        try{

            aq.id(holder.img)
                    .image(all_likes.get(position).getImagefield(), true, true,
                            0, 0, null, 0, AQuery.FADE_IN);
        }catch(Exception e){

        }


        holder.layout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


            }

        });
        return convertView;

    }



}

class ViewHowManyHolderLikes {

    RelativeLayout layout;
    ImageView img;
    CustomTextViewRegularFont nameField;
    CustomTextViewRegularFont offferLabel;

}
