package pager.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewMeadiumFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clabs.butlers.utils.StreamingMediaPlayer;
import clicklabs.app.butlers.BuyingProducts;
import models.CommentModel;
import models.HowmanyLikeModel;
import adapters.CommentAdapter;
import adapters.HowManyLikeAdapter;
import models.ProductsModel;
import models.PromoteUserListModel;
import pathanimation.ArcTranslateAnimation;

/**
 * Created by click103 on 12/3/15.
 */
public class FeedContactsFrag extends Fragment {

    CustomTextViewRegularFont contactsTab, buyingBtn,sellingBtn,promotedBtn;
    ListView listView;
    ProductsModel productsModel;
    ArrayList<ProductsModel> arraylist;
    ProductsAdapter adapter;
    CommentAdapter commentAdapter;
    PromoteUserListModel likeModel;
    ArrayList<PromoteUserListModel> likeModelArrayList;
    CommentModel commentModel;
    ArrayList<CommentModel> commentModelArrayList;
    HowManyLikeAdapter howManyLikeAdapter;
    HowmanyLikeModel howmanyLikeModel;
    ArrayList<HowmanyLikeModel> howmanyLikeModelArrayList;
    RecyclerView recyclerView;
    HomFragment homeFragment=null;


    int ITEMS = 2;

    static FeedContactsFrag init(int val) {
        FeedContactsFrag feedContactsFrag = new FeedContactsFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        feedContactsFrag.setArguments(args);

        return feedContactsFrag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.feed_contact_layout, container, false);
        buyingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.buyingBtn);
        sellingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.sellingBtn);
     //   listView =(ListView) rootView.findViewById(R.id.listView);
        arraylist = new ArrayList<ProductsModel>();

        String[] order_ids={"1","2","3","4","5","6","7","8","9"};
        String[] order_images={"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                ,"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names={"saif","showket","anil","saif","showket","anil","saif","showket","anil"};

        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i],order_names[i],order_ids[i],i,order_images[i],order_names[i],order_names[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],false);
            // Binds all strings into an array
            arraylist.add(productsModel);
        }
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        final FragmentManager fm = getFragmentManager();
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...

                Log.v("Load More", "Load More" + current_page);
            }

            @Override
            public void onHide() {
                HomFragment parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
                //parent.hideBuySell(buyselltabs);
            }

            @Override
            public void onShow() {
                HomFragment parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
                //  parent.showBuySell(buyselltabs);
            }
        });



        final SwipeRefreshLayout swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        swipeView.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        double f = Math.random();
//                        rndNum.setText(String.valueOf(f));
                    }
                }, 3000);
            }
        });

       // MyRecyclerAdapter adapter=new MyRecyclerAdapter(getActivity(), arraylist);
       // recyclerView.setAdapter(adapter);


//        adapter=new ProductsAdapter(getActivity(),R.id.image, arraylist);
//        listView.setAdapter(adapter);


//        listView.setAdapter(new ProductsAdapter(getActivity(),R.id.image,arraylistnew));


        buyingBtn.setBackgroundResource(R.drawable.buying_normal);
        buyingBtn.setTextColor(getResources().getColor(R.color.white));
        sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));





        buyingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyingBtn.setBackgroundResource(R.drawable.buying_normal);
                sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                buyingBtn.setTextColor(getResources().getColor(R.color.white));
                sellingBtn.setBackgroundResource(0);
            }
        });
        sellingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellingBtn.setBackgroundResource(R.drawable.buying_normal);
                buyingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                sellingBtn.setTextColor(getResources().getColor(R.color.white));
                buyingBtn.setBackgroundResource(0);
            }
        });
        return rootView;
   }

    /********************* ProductsAdapter items list adapter ******************/
    public class ProductsAdapter extends ArrayAdapter<String> {


        private LayoutInflater inflater;
        ViewHolder holder;

        private ArrayList<ProductsModel> products;
        Context context;
        Activity activity;



        //        public ProductsAdapter(Context context, int CustomTextViewRegularFontResourceId, List<ProductsModel> list) {
        public ProductsAdapter(Activity activity, int CustomTextViewRegularFontResourceId, List<ProductsModel> arraylist) {
            super(activity, CustomTextViewRegularFontResourceId);

            this.activity=activity;
//            arraylistnew = arraylist;
            this.products = new ArrayList<ProductsModel>();
            this.products.addAll(arraylist);

//            Log.i("products", products + "....");
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            Log.i("arrlist len", arraylist.size() + ",,");

//            for (int i = 0; i < arraylistnew.size(); i++) {
//                Log.i("wishh..", "..." + arraylistnew.get(i).getAddcartStatus()
//                        + ".." + arraylistnew.get(i).getWishStatus());
        }


        @Override
        public int getCount() {
            return products.size();
//            return 10;
        }

//		@Override
//		public ProductFilter getItem(int position) {
//			return arraylistnew.get(position);
//		}

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;


            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_contacts_buying, null);
                holder = new ViewHolder();
                holder.profileImage = (ImageView) convertView
                        .findViewById(R.id.imageProfile);
                holder.productImage = (ImageView) convertView
                        .findViewById(R.id.cardImage);
                holder.revealBtn = (Button) convertView
                        .findViewById(R.id.revealBtn);
                holder.offerBtn = (Button) convertView
                        .findViewById(R.id.offerIcon);
                holder.descBtn = (Button) convertView
                        .findViewById(R.id.descIcon);
                holder.promoteBtn = (Button) convertView
                        .findViewById(R.id.promoteIcon);
                holder.userName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.likesCount = (CustomTextViewLightFont) convertView
                        .findViewById(R.id.likeBtnLabel);
                holder.commentCount = (CustomTextViewLightFont) convertView
                        .findViewById(R.id.commentBtnLabel);

                holder.productModel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.productName);
                holder.productLabel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.productLabel);
                holder.userWhoRated = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.productBarUserName);
                holder.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userRatingValue);
                holder.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.countPlus);
                holder.buttonLayoutReveal = (LinearLayout) convertView
                        .findViewById(R.id.buttonLayoutReveal);
                holder.howManyLikeLayout = (LinearLayout) convertView
                        .findViewById(R.id.howManyLikeLayout);

                holder.topPart = (LinearLayout) convertView
                        .findViewById(R.id.topPart);

                holder.likeBtn = (RelativeLayout) convertView
                        .findViewById(R.id.likeBtn);
                holder.comentBtn = (RelativeLayout) convertView
                        .findViewById(R.id.commentBtn);
                holder.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
                holder.productAddedTime = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.timeValue);
                holder.rl = (RelativeLayout) convertView.findViewById(R.id.rv);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
//            holder.namefield.setText(arraylistnew.get(position).getName());
//            holder.price.setText(arraylistnew.get(position).getPrice());
//            if (arraylistnew.get(position).getImage().length() != 0) {
            AQuery aq = new AQuery(convertView);

            try{
                aq.id(holder.profileImage)
                        .progress(R.id.imageProgressbar)
                        .image(products.get(position).getUserProfileImage(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
                aq.id(holder.productImage)
                        .progress(R.id.centralProgressbar)
                        .image(products.get(position).getUserProfileImage(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
//                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.profileImage);
//
//                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.productImage);
            }catch(Exception e){

            }
//            }
//            if (arraylistnew.get(position).getWishStatus().equals("0"))
//                holder.favorite.setBackgroundResource(R.drawable.fav_icon);
//            else
//                holder.favorite.setBackgroundResource(R.drawable.fav_icon_on);
//            if (arraylistnew.get(position).getAddcartStatus().equals("0"))
//                holder.addtocart.setBackgroundResource(R.drawable.cart_icon);
//            else
//                holder.addtocart.setBackgroundResource(R.drawable.cart_icon_on);
            if (products.get(position).getIsShownLayout()==false) {
                holder.buttonLayoutReveal.setVisibility(View.GONE);
            }
            else {
                holder.buttonLayoutReveal.setVisibility(View.VISIBLE);
            }
            holder.revealBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                    if (holder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                        products.get(position).setIsShownLayout(false);
                        holder.buttonLayoutReveal.setVisibility(View.GONE);
                    }
                    else {
                        products.get(position).setIsShownLayout(true);
                        holder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                    }
                    notifyDataSetChanged();

                }
            });

            holder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeFragment = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                    homeFragment.viewPager.setVisibility(View.GONE);
                    homeFragment.bottomBar.setVisibility(View.GONE);
                    homeFragment.topBar.setVisibility(View.GONE);
                    homeFragment.fragmentContainer.setVisibility(View.VISIBLE);
                    LikeProductFragment likeProductFragment = new LikeProductFragment();
                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(homeFragment.fragmentContainer.getId(), likeProductFragment);
                    ft.commit();

                }
            });
            holder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    showCommentPopup(getActivity());


                }
            });

            holder.howManyLikeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showHowManyLikePopup(getActivity());




                }
            });
            holder.likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });
            holder.topPart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });


//            final ImageView cardImage = holder.productImage;
            holder.productImage.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
                   if(Config.isLollyPop()) {
                       ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, holder.productImage, "cardImage");
                       Intent intent = new Intent(activity, BuyingProducts.class);
                       getActivity().startActivity(intent, options.toBundle());
                   }
                    else
                    {
                       getActivity().startActivity(new Intent(activity, BuyingProducts.class));
                   }
                }
            });

            holder.offerBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if(Config.isLollyPop()) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, holder.productImage, "cardImage");
                    Intent intent = new Intent(activity, BuyingProducts.class);
                    getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        getActivity().startActivity(new Intent(activity, BuyingProducts.class));
                    }
                }
            });

            holder.promoteBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {



                    Intent intent = new Intent(activity, BuyingProducts.class);
                    intent.putExtra("layoutShown","promote");
                    getActivity().startActivity(intent);
                }
            });
            holder.descBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    ArcTranslateAnimation anim = new ArcTranslateAnimation(0, -820 , 0, 100);
                    anim.setDuration(500);
                    anim.setFillAfter(true);



                    holder.revealBtn.startAnimation(anim);


//                    TranslateAnimation translateAnimation1 = new TranslateAnimation(0,-700,20,0);
//                    translateAnimation1.setDuration(1000);
//                    translateAnimation1.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation arg0) {}
//                        @Override
//                        public void onAnimationRepeat(Animation arg0) {}
//                        @Override
//                        public void onAnimationEnd(Animation arg0) {
////                            TranslateAnimation translateAnimation2 = new TranslateAnimation(
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.8f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.5f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.8f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.5f);
////                            translateAnimation2.setDuration(1000);
////                            AnimationSet animationSet = new AnimationSet(false);
////                            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 2.0f,1.0f,2.0f,ScaleAnimation.RELATIVE_TO_SELF ,0.5f,ScaleAnimation.RELATIVE_TO_SELF,0.5f);
////                            scaleAnimation.setDuration(1000);
////                            animationSet.addAnimation(scaleAnimation);
////                            animationSet.addAnimation(translateAnimation2);
////                            holder.revealBtn.startAnimation(animationSet);
//
//                        }
//                    });
//                    holder.revealBtn.startAnimation(translateAnimation1);
                    notifyDataSetChanged();
//                    Intent intent = new Intent(activity, BuyingProducts.class);
//                    intent.putExtra("layoutShown", "description");
//                    getActivity().startActivity(intent);
                }
            });
//
//            holder.favorite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try{
//                        productID = arraylistnew.get(position).getProductId();
//                    }catch(ArrayIndexOutOfBoundsException e){
//
//                    }
//                    final_position = position;
//                    addcart_wishlistflag = "1";
//                    if (!AppStatus.getInstance(getApplicationContext())
//                            .isOnline(getApplicationContext())) {
//                        AppStatus.errorDialog(HomePage.this);
//                    } else {
//                        AddCartWishList(HomePage.this);
//                    }
//                }
//            });
//            holder.img.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String url = arraylistnew.get(position).getImage();
//                    Data.final_position = position;
//                    productID = arraylistnew.get(position).getProductId();
//                    Log.i("pro d id", "" + position);
//                    Data.productID = arraylistnew.get(position).getProductId();
//
//
//                    Intent i = new Intent(HomePage.this, PagerView.class);
//                    Bundle b = new Bundle();
//                    b.putInt("pos_val", pos);
//                    i.putExtras(b);
//                    startActivity(i);
//                    overridePendingTransition( R.anim.slide_in_right, R.anim.grow_from_center);
//
////					Intent i =new Intent(HomePage.this, PagerView.class);
////					startActivity(i);
//
////					if (!AppStatus.getInstance(HomePage.this).isOnline(HomePage.this)) {
////						AppStatus.errorDialog(HomePage.this);
////						Data.networkError(HomePage.this);
////					}
////					else{
////						Data.product_IDD = arraylistnew.get(position).getProductId();
////						CategoryProduct(HomePage.this, position);
////					}
//
//
//                }
//            });
            return convertView;
        }

//        public void filter(String charText) {
//            charText = charText.toLowerCase();
//            arraylistnew.clear();
//            if (charText.length() == 0) {
//                Log.i("products filrter1111", products + "....");
//                arraylistnew.addAll(products);
//            } else {
//                for (ProductInfo wp : products) {
//                    Log.i("products filrter2222", products + "....");
//                    if (wp.getName().toLowerCase(Locale.getDefault())
//                            .contains(charText)) {
//                        arraylistnew.add(wp);
//                        Log.i("inside filter", wp.getName() + "..");
//                    }
//                }
//            }
//            adapter.notifyDataSetChanged();
//        }


    }

    class ViewHolder {
        ProgressBar profileProgress,productProgress;
        CustomTextViewLightFont likesCount,commentCount;
        ImageView profileImage,productImage;
        CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                productPrice,availableCount,commentBtnLabel;
        RelativeLayout rel,likeBtn,comentBtn;
        LinearLayout buttonLayoutReveal,topPart,howManyLikeLayout;
        Button offerBtn, descBtn,promoteBtn,revealBtn;
        RelativeLayout rl;
        int p;
    }


    public class MyRecyclerAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

        private List<ProductsModel> feedItemList;
        private Context mContext;
        private boolean isPlaying;
        private StreamingMediaPlayer audioStreamer;
//        ArcTranslateAnimation anim  = new ArcTranslateAnimation(0, -820 , 0, 100);;


        public MyRecyclerAdapter(Context context, List<ProductsModel> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;


            // Set up the animation

        }

        @Override
        public FeedListRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contacts_buying, null);
            FeedListRowHolder mh = new FeedListRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, int position) {
            final ProductsModel feedItem = feedItemList.get(position);
            Picasso.with(mContext).load(feedItem.getProductImage()).into(feedListRowHolder.productImage);
            Picasso.with(mContext).load(feedItem.getUserProfileImage()).into(feedListRowHolder.profileImage);

//            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));
//            feedListRowHolder.userRaings.setText(Html.fromHtml(feedItem.getUserRaings()));
//            feedListRowHolder.productAddedTime.setText(Html.fromHtml(feedItem.getProductAddedTime()));
//            feedListRowHolder.productModel.setText(Html.fromHtml(feedItem.getProductModel()));
//            feedListRowHolder.userWhoRated.setText(Html.fromHtml(feedItem.getUserWhoRated()));

            if (feedItem.getIsShownLayout()==false) {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
            }
            else {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
            }

            feedListRowHolder.revealBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                    if (feedListRowHolder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                       feedItem.setIsShownLayout(false);
                        feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                    }
                    else {
                      feedItem.setIsShownLayout(true);
                        feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                    }
                    notifyDataSetChanged();

                }
            });

          feedListRowHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homeFragment = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                    homeFragment.viewPager.setVisibility(View.GONE);
                    homeFragment.bottomBar.setVisibility(View.GONE);
                    homeFragment.topBar.setVisibility(View.GONE);
                    homeFragment.fragmentContainer.setVisibility(View.VISIBLE);
                    LikeProductFragment likeProductFragment = new LikeProductFragment();
                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(homeFragment.fragmentContainer.getId(), likeProductFragment);
                    ft.commit();

                }
            });
          feedListRowHolder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    showCommentPopup(getActivity());


                }
            });

            feedListRowHolder.howManyLikeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showHowManyLikePopup(getActivity());




                }
            });
           feedListRowHolder.likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });
            feedListRowHolder.topPart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });
          feedListRowHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(getActivity(), BuyingProducts.class);
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        getActivity().startActivity(new Intent(getActivity(), BuyingProducts.class));
                    }
                }
            });

           feedListRowHolder.offerBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(getActivity(), BuyingProducts.class);
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        getActivity().startActivity(new Intent(getActivity(), BuyingProducts.class));
                    }
                }
            });

           feedListRowHolder.promoteBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {



                    Intent intent = new Intent(getActivity(), BuyingProducts.class);
                    intent.putExtra("layoutShown","promote");
                    getActivity().startActivity(intent);
                }
            });
          feedListRowHolder.descBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    ArcTranslateAnimation anim = new ArcTranslateAnimation(0, -820 , 0, 100);
                    anim.setDuration(500);
                    anim.setFillAfter(true);



                    feedListRowHolder.revealBtn.startAnimation(anim);


//                    TranslateAnimation translateAnimation1 = new TranslateAnimation(0,-700,20,0);
//                    translateAnimation1.setDuration(1000);
//                    translateAnimation1.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation arg0) {}
//                        @Override
//                        public void onAnimationRepeat(Animation arg0) {}
//                        @Override
//                        public void onAnimationEnd(Animation arg0) {
////                            TranslateAnimation translateAnimation2 = new TranslateAnimation(
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.8f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.5f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.8f,
////                                    TranslateAnimation.RELATIVE_TO_PARENT,
////                                    0.5f);
////                            translateAnimation2.setDuration(1000);
////                            AnimationSet animationSet = new AnimationSet(false);
////                            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 2.0f,1.0f,2.0f,ScaleAnimation.RELATIVE_TO_SELF ,0.5f,ScaleAnimation.RELATIVE_TO_SELF,0.5f);
////                            scaleAnimation.setDuration(1000);
////                            animationSet.addAnimation(scaleAnimation);
////                            animationSet.addAnimation(translateAnimation2);
////                            holder.revealBtn.startAnimation(animationSet);
//
//                        }
//                    });
//                    holder.revealBtn.startAnimation(translateAnimation1);
                    notifyDataSetChanged();
//                    Intent intent = new Intent(activity, BuyingProducts.class);
//                    intent.putExtra("layoutShown", "description");
//                    getActivity().startActivity(intent);
                }
            });
//
//            holder.favorite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    try{
//                        productID = arraylistnew.get(position).getProductId();
//                    }catch(ArrayIndexOutOfBoundsException e){
//
//                    }
//                    final_position = position;
//                    addcart_wishlistflag = "1";
//                    if (!AppStatus.getInstance(getApplicationContext())
//                            .isOnline(getApplicationContext())) {
//                        AppStatus.errorDialog(HomePage.this);
//                    } else {
//                        AddCartWishList(HomePage.this);
//                    }
//                }
//            });
//            holder.img.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String url = arraylistnew.get(position).getImage();
//                    Data.final_position = position;
//                    productID = arraylistnew.get(position).getProductId();
//                    Log.i("pro d id", "" + position);
//                    Data.productID = arraylistnew.get(position).getProductId();
//
//
//                    Intent i = new Intent(HomePage.this, PagerView.class);
//                    Bundle b = new Bundle();
//                    b.putInt("pos_val", pos);
//                    i.putExtras(b);
//                    startActivity(i);
//                    overridePendingTransition( R.anim.slide_in_right, R.anim.grow_from_center);
//
////					Intent i =new Intent(HomePage.this, PagerView.class);
////					startActivity(i);
//
////					if (!AppStatus.getInstance(HomePage.this).isOnline(HomePage.this)) {
////						AppStatus.errorDialog(HomePage.this);
////						Data.networkError(HomePage.this);
////					}
////					else{
////						Data.product_IDD = arraylistnew.get(position).getProductId();
////						CategoryProduct(HomePage.this, position);
////					}
//
//
//                }
//            });

        }

        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }
    }
    class FeedListRowHolder extends RecyclerView.ViewHolder {
        ProgressBar profileProgress,productProgress;
        CustomTextViewLightFont likesCount,commentCount;
        ImageView profileImage,productImage;
        CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                productPrice,availableCount,commentBtnLabel;
        RelativeLayout rel,likeBtn,comentBtn;
        LinearLayout buttonLayoutReveal,topPart,howManyLikeLayout;
        Button offerBtn, descBtn,promoteBtn,revealBtn;
        RelativeLayout rl;
        int p;

        public FeedListRowHolder(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView
                    .findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView
                    .findViewById(R.id.cardImage);
            this.revealBtn = (Button) convertView
                    .findViewById(R.id.revealBtn);
            this.offerBtn = (Button) convertView
                    .findViewById(R.id.offerIcon);
            this.descBtn = (Button) convertView
                    .findViewById(R.id.descIcon);
            this.promoteBtn = (Button) convertView
                    .findViewById(R.id.promoteIcon);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.userWhoRated = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productBarUserName);
            this.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userRatingValue);
            this.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.countPlus);
            this.buttonLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.buttonLayoutReveal);
            this.howManyLikeLayout = (LinearLayout) convertView
                    .findViewById(R.id.howManyLikeLayout);

            this.topPart = (LinearLayout) convertView
                    .findViewById(R.id.topPart);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
            this.productAddedTime = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.timeValue);
            this.rl = (RelativeLayout) convertView.findViewById(R.id.rv);

        }

    }



    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showCommentPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.comment_product);
            if(Config.isLollyPop())
            dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final EditText editView=(EditText)dialog.findViewById(R.id.commentText);
            final ListView commentsListView=(ListView)dialog.findViewById(R.id.comemtsListView);
            RelativeLayout backBtn=(RelativeLayout)dialog.findViewById(R.id.backBtn);

            LinearLayout sendBtn=(LinearLayout)dialog.findViewById(R.id.sendBtnLay);



            String[] order_ids={"1","2","3","4","5","6","7","8","9"};
            String[] order_images={"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    ,"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names={"saif","showket","anil","saif","showket","anil","saif","showket","anil"};

            commentModelArrayList = new ArrayList<CommentModel>();

            for (int i = 0; i < order_ids.length; i++) {

                commentModel = new CommentModel(order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i]);
                // Binds all strings into an array
                commentModelArrayList.add(commentModel);
            }
            commentAdapter=new CommentAdapter(getActivity(), commentModelArrayList);


            commentsListView.setAdapter(commentAdapter);


            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });





            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }





    }




    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showHowManyLikePopup(Activity activity) {

        try {
            final Dialog dialog;
//            if (Config.isLollyPop()) {
//                dialog = new Dialog(activity,
//                        android.R.style.Theme_Material_Dialog_NoActionBar);
//            }
//            else
//            {
            dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            }

            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.how_many_like_product);
            if(Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            final ListView likeListView=(ListView)dialog.findViewById(R.id.howmanylikeListView);
            CustomTextViewMeadiumFont closeBtn=(CustomTextViewMeadiumFont)dialog.findViewById(R.id.closeBtn);
            String[] order_ids={"1","2","3","4","5","6","7","8","9"};
            String[] order_images={"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    ,"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names={"saif","showket","anil","saif","showket","anil","saif","showket","anil"};

            howmanyLikeModelArrayList = new ArrayList<>();

            for (int i = 0; i < order_ids.length; i++) {

                howmanyLikeModel = new HowmanyLikeModel(order_names[i],order_images[i],order_ids[i],false);
                // Binds all strings into an array
                howmanyLikeModelArrayList.add(howmanyLikeModel);
            }
            howManyLikeAdapter=new HowManyLikeAdapter(getActivity(), howmanyLikeModelArrayList);
            likeListView.setAdapter(howManyLikeAdapter);
            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


}


