package pager.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.transition.Explode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomEditTextRegularFontBold;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clicklabs.app.butlers.BuyingProducts;
import clicklabs.app.butlers.MyApplication;
import clicklabs.app.butlers.ProductDescription;
import clicklabs.app.butlers.ViewPagerExampleActivity;
import material.animations.MaterialDesignAnimations;
import models.CommentModel;
import models.PromoteUserListModel;
import models.SearchPeopleModel;
import models.SearchProductsBuyModel;
import models.SearchProductsSellModel;

import static pager.fragment.SearchPeopleFrag.*;

/**
 * Created by click103 on 12/3/15.
 */
public class SearchProductsFrag extends Fragment {
    SearchProductsBuyModel searchProductsBuyModel;
    ArrayList<SearchProductsSellModel> sellArraylist;
    SearchProductsSellModel searchProductsSellModel;
    ArrayList<SearchProductsBuyModel> buyArraylist;
    RelativeLayout buyselltabs;
    CustomTextViewRegularFont  buyingBtn,sellingBtn;
    RecyclerView buyRecyclerView,sellRecyclerView;
    LinearLayoutManager linearLayoutManager,linearLayoutManagersell;
    LinearLayout errorLayout;
    FragmentManager fm;
    int offSetBuY=0,offSetSell=0;
    int contentFlag=1,LOAD_FIRST_TIME_BUY=1,LOAD_FIRST_TIME_SELL=2,NEW_SEARCH=1;
    CustomEditTextRegularFontBold homeSearchBar,peopleSearchBar;
    Button searchcrossBtn;
    String  difference = null;
//    CommentAdapter commentAdapter;
//    PromoteUserListModel likeModel;
//    ArrayList<PromoteUserListModel> likeModelArrayList;
//    ArrayList<CommentModel> commentModelArrayList;
//    CommentModel commentModel;
//    LikesAdapter likesAdapter;
    SearchProductsSellAdapter searchProductsSelAdapter;
    SearchProductsBuyAdapter searchProductsBuyAdapter;
    CustomTextViewRegularFont resultCount;
    HomFragment parent;
    int FROM_SELL_OR_BUY;
    MyApplication applicationClass;
    int sellingCount=0,buyingCount;

    public SearchProductsFrag() {
        FROM_SELL_OR_BUY = 0;
    }


    static SearchProductsFrag init(int val) {
        SearchProductsFrag searchProductsFrag = new SearchProductsFrag();
        Bundle args = new Bundle();
        args.putInt("val", val);
        searchProductsFrag.setArguments(args);

        return searchProductsFrag;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        applicationClass = (MyApplication)getActivity().getApplication();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_product_layout, container, false);
        fm = getFragmentManager();
        parent = (HomFragment)fm.findFragmentByTag("HomFragment");
        errorLayout = (LinearLayout)getActivity(). findViewById(R.id.errorLayout);
        buyingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.buyingBtn);
        sellingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.sellingBtn);
        resultCount =(CustomTextViewRegularFont) rootView.findViewById(R.id.noOfResults);
        buyselltabs =(RelativeLayout) rootView.findViewById(R.id.buyselltabs);
        buyArraylist = new ArrayList<SearchProductsBuyModel>();
        sellArraylist = new ArrayList<SearchProductsSellModel>();
        final ViewPager viewPager=(ViewPager) getActivity().findViewById(R.id.searchPager);



        buyRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewBuy);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        buyRecyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setItemAnimator(new FadeInAnimator());

        buyRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    NEW_SEARCH=2;
                    LOAD_FIRST_TIME_BUY=2;
                    SearchProductApi();
                }
            }

            @Override
            public void onHide() {
                Data.hideSoftKeyboard(getActivity());

                parent.hideBottombarScroll();
                //parent.hideBuySell(buyselltabs);
            }

            @Override
            public void onShow() {
                Data.hideSoftKeyboard(getActivity());
                parent.showBottombarScroll();
                //  parent.showBuySell(buyselltabs);
            }
        });

        /**** Sell Recycleview**/

        sellRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewSell);
        linearLayoutManagersell = new LinearLayoutManager(getActivity());
        sellRecyclerView.setLayoutManager(linearLayoutManagersell);
        sellRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {

            @Override
            public void onLoadMore(int current_page) {
                if (AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    NEW_SEARCH=2;
                    LOAD_FIRST_TIME_SELL=2;
                    SearchProductApi();
                }
            }

            @Override
            public void onHide() {
                Data.hideSoftKeyboard(getActivity());
                HomFragment parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
            }

            @Override
            public void onShow() {
                Data.hideSoftKeyboard(getActivity());
                HomFragment parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
            }
        });


        buyingBtn.setBackgroundResource(R.drawable.buying_normal);
        buyingBtn.setTextColor(getResources().getColor(R.color.white));
        sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));


        buyingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyingBtn.setBackgroundResource(R.drawable.buying_normal);
                sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                buyingBtn.setTextColor(getResources().getColor(R.color.white));
                sellingBtn.setBackgroundResource(0);
                sellRecyclerView.setVisibility(View.GONE);
                buyRecyclerView.setVisibility(View.VISIBLE);
                contentFlag=1;

                if(buyArraylist.size()==0) {
                    if(homeSearchBar.getText().length()==0)
                    {
                        homeSearchBar.requestFocus();
                        homeSearchBar.setError(getResources().getString(R.string.nothingToSearch));
                    }
                    else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            offSetBuY=0;
                            NEW_SEARCH=1;
                            LOAD_FIRST_TIME_BUY=1;
                            SearchProductApi();
                        }
                    }

                }
                else
                {
                    resultCount.setText(buyingCount+" Results Found");
                }
            }
        });
        sellingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellingBtn.setBackgroundResource(R.drawable.buying_normal);
                buyingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                sellingBtn.setTextColor(getResources().getColor(R.color.white));
                buyingBtn.setBackgroundResource(0);
                sellRecyclerView.setVisibility(View.VISIBLE);
                buyRecyclerView.setVisibility(View.GONE);
                contentFlag=0;
                if(sellArraylist.size()==0) {
                    if(homeSearchBar.getText().length()==0)
                    {
                        homeSearchBar.requestFocus();
                        homeSearchBar.setError(getResources().getString(R.string.nothingToSearch));
                    }
                    else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            offSetSell=0;
                            NEW_SEARCH=1;
                            LOAD_FIRST_TIME_SELL=1;
                            SearchProductApi();
                        }
                    }
                }
                else
                {
                    resultCount.setText(sellingCount+" Results Found");
                }
            }
        });

        homeSearchBar = (CustomEditTextRegularFontBold)getActivity(). findViewById(R.id.homeSearchBar);
        peopleSearchBar = (CustomEditTextRegularFontBold)getActivity(). findViewById(R.id.peopleSearchBar);
        homeSearchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(viewPager.getCurrentItem()==1) {
                        if(homeSearchBar.getText().length()==0)
                        {
                            homeSearchBar.requestFocus();
                            homeSearchBar.setError(getResources().getString(R.string.nothingToSearch));
                        }
                        else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {


                            Data.hideSoftKeyboard(getActivity());
                            if(contentFlag==1) {
                                offSetBuY=0;
                                LOAD_FIRST_TIME_BUY = 1;

                            }
                            else if(contentFlag==0) {
                                offSetSell=0;
                                LOAD_FIRST_TIME_SELL = 1;
                            }
                                NEW_SEARCH = 1;

                            SearchProductApi();
                        }
                        }


                    }
                    else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {

//                            Data.hideSoftKeyboard(getActivity());
                           // SearchPeopleApi();
                        }
                    }
                    handled = true;
                }
                return handled;
            }
        });
        homeSearchBar.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                if(homeSearchBar.getText().toString().length()>=1)
                    searchcrossBtn.setVisibility(View.VISIBLE);
                else
                    searchcrossBtn.setVisibility(View.GONE);

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });
        searchcrossBtn = (Button)getActivity(). findViewById(R.id.searchcrossbtn);
        searchcrossBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeSearchBar.setText("");
                peopleSearchBar.setText("");
                searchcrossBtn.setVisibility(View.GONE);

            }
        });

        return rootView;
    }



    /************recycler view*************/
    /**
     * Created by Showket on 2015/28/04.
     */
    public class SearchProductsBuyAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

        //private List<SearchProductsBuyModel> feedItemList;
        private Context mContext;
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        private DisplayImageOptions options;


        public SearchProductsBuyAdapter(Context context) {
           // this.feedItemList = buyArraylist;
            this.mContext = context;
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.logo_small)
                    .showImageForEmptyUri(R.drawable.logo_small)
                    .showImageOnFail(R.drawable.logo_small)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true).build();

        }

        @Override
        public FeedListRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_search_products_buy, null);
            FeedListRowHolder mh = new FeedListRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, final int i) {
            final SearchProductsBuyModel feedItem = buyArraylist.get(i);
           // Picasso.with(mContext).load(feedItem.getProductImage()).into(feedListRowHolder.productImage);
//            Picasso.with(mContext).load(feedItem.getUserProfileImage()).into(feedListRowHolder.profileImage);
            ImageLoader.getInstance().displayImage(feedItem.getProductImage(),feedListRowHolder.productImage, options, animateFirstListener);
            ImageLoader.getInstance().displayImage(feedItem.getUserProfileImage(),feedListRowHolder.profileImage, options, animateFirstListener);

            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));
            feedListRowHolder.userRaings.setText(Html.fromHtml(feedItem.getUserRaings())+" Star )");

            feedListRowHolder.productAddedTime.setText(applicationClass.getExpirationTimeTodisplay(feedItem.getProductAddedTime())+ ", ");


            //feedListRowHolder.productAddedTime.setText(getExpirationTimeTodisplay(currentDate(),feedItem.getProductAddedTime()) + ", ");

            feedListRowHolder.addressField.setText(Html.fromHtml(feedItem.getAddressField()));
            if(Integer.parseInt(feedItem.getLikesCount())==1)
                feedListRowHolder.likesCount.setText(feedItem.getLikesCount()+" Like");
            else
                feedListRowHolder.likesCount.setText(feedItem.getLikesCount()+" Likes");
            if(Integer.parseInt(feedItem.getLikesCount())==1)
                feedListRowHolder.commentCount.setText(feedItem.getCommentCount()+" Comment");
            else
                feedListRowHolder.commentCount.setText(feedItem.getCommentCount()+" Comments");
            feedListRowHolder.availableCount.setText(feedItem.getAvailableCount());
            feedListRowHolder.productPrice.setText(feedItem.getProductPrice());
            feedListRowHolder.description.setText(feedItem.getProductDesc());


            if (feedItem.getIsShownLayout()==false) {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
            }
            else {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
            }
            if (feedItem.getIsLiked().equals("0")) {
                feedListRowHolder.likeHeartBtn.setBackgroundResource(R.drawable.btn_like_small_normal);
            }
            else {
                feedListRowHolder.likeHeartBtn.setBackgroundResource(R.drawable.btn_like_small_pressed);
            }
            feedListRowHolder.revealBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {

                        if (feedListRowHolder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                            feedItem.setIsShownLayout(false);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                        } else {
                            feedItem.setIsShownLayout(true);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                        }
                        notifyDataSetChanged();
                    }

                }
            });


            feedListRowHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));
                        } else {
                            String status="1";
                            if(feedItem.getIsLiked().equals("0"))
                                status="1";
                            else
                                status="0";

                            FROM_SELL_OR_BUY = 1;
                            LikeProductApi(feedItem.getProductId(), i, FROM_SELL_OR_BUY,status);
                        }
                    }
                }
            });
            feedListRowHolder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                        parent.viewPager.setVisibility(View.GONE);
                        parent.bottomBar.setVisibility(View.GONE);
                        parent.topBar.setVisibility(View.GONE);
                        parent.fragmentContainer.setVisibility(View.VISIBLE);
                        CommentProductFragment commentProductFragment = new CommentProductFragment();
                        commentProductFragment.setProductIdId(feedItem.getProductId());
                        commentProductFragment.setLikeCount(feedItem.getLikesCount());


                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), commentProductFragment);
                        ft.addToBackStack(commentProductFragment.getClass().getName());
                        ft.commit();
                    }
                }
            });

            feedListRowHolder.commentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                        parent.viewPager.setVisibility(View.GONE);
                        parent.bottomBar.setVisibility(View.GONE);
                        parent.topBar.setVisibility(View.GONE);
                        parent.fragmentContainer.setVisibility(View.VISIBLE);
                        CommentProductFragment commentProductFragment = new CommentProductFragment();
                        commentProductFragment.setProductIdId(feedItem.getProductId());
                        commentProductFragment.setLikeCount(feedItem.getLikesCount());


                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), commentProductFragment);
                        ft.addToBackStack(commentProductFragment.getClass().getName());
                        ft.commit();
                    }



                }
            });
            feedListRowHolder.likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Integer.parseInt(feedItem.getLikesCount())>0) {
                        if (Config.getSKIP_MODE())
                            parent.showPopupHome();
                        else {

                            parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                            parent.viewPager.setVisibility(View.GONE);
                            parent.bottomBar.setVisibility(View.GONE);
                            parent.topBar.setVisibility(View.GONE);
                            parent.fragmentContainer.setVisibility(View.VISIBLE);
                            LikeProductFragment likeProductFragment = new LikeProductFragment();
                            likeProductFragment.setProductIdId(feedItem.getProductId());

                            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), likeProductFragment);
                            ft.addToBackStack(likeProductFragment.getClass().getName());
                            ft.commit();


                        }
                    }


                }
            });
            feedListRowHolder.topPart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });

            feedListRowHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
//                        if (Config.isLollyPop()) {
//                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
//                            Intent intent = new Intent(mContext, ViewPagerExampleActivity.class);
//                            intent.putExtra("images", feedItem.getDollarSeparatedproductImage());
//                            getActivity().startActivity(intent, options.toBundle());
//                        } else
                        {
                            Intent intent = new Intent(mContext, ViewPagerExampleActivity.class);
                            intent.putExtra("images", feedItem.getDollarSeparatedproductImage());
                            getActivity().startActivity(intent);
                        }
                    }
                }
            });

            feedListRowHolder.offerBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(mContext, BuyingProducts.class);
                        intent.putExtra("layoutShown", "offer");
                        intent.putExtra("productName", feedItem.getProductModel());
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        Intent intent = new Intent(mContext, BuyingProducts.class);
                        intent.putExtra("layoutShown", "offer");
                        intent.putExtra("productName", feedItem.getProductModel());
                        getActivity().startActivity(intent);
                    }
                }
            });

            feedListRowHolder.promoteBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {


                    Config.setPRODUCT_ID(feedItem.getProductId());
                    Intent intent = new Intent(mContext, BuyingProducts.class);
                    intent.putExtra("layoutShown","promote");
                    getActivity().startActivity(intent);
                }
            });
            feedListRowHolder.descBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(mContext, ProductDescription.class);
                        intent.putExtra("layoutShown", "description");
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        Intent intent = new Intent(mContext, ProductDescription.class);
                        intent.putExtra("productName", feedItem.getProductModel());
                        intent.putExtra("productImage", feedItem.getProductImage());
                        intent.putExtra("userImage", feedItem.getUserProfileImage());
                        intent.putExtra("userName", feedItem.getUserName());
                        intent.putExtra("userRank", feedItem.getUserRaings());
                        intent.putExtra("time", feedItem.getProductAddedTime());
                        intent.putExtra("address", feedItem.getAddressField());
                        intent.putExtra("available", feedItem.getAvailableCount());
                        intent.putExtra("price", feedItem.getProductPrice());
                        intent.putExtra("description", feedItem.getProductDesc());
                        getActivity().startActivity(intent);
                    }
                }
            });
//            final ImageView cardImage = holder.productImage;
            feedListRowHolder.productImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ViewPagerExampleActivity.class);
                    intent.putExtra("images", feedItem.getDollarSeparatedproductImage());
                    getActivity().startActivity(intent);

                }
            });
            feedListRowHolder.revealBtn.setTag(feedListRowHolder);

        }

        @Override
        public int getItemCount() {
            return (null != buyArraylist ? buyArraylist.size() : 0);
        }
    }


    /************recycler view*************/
    /**
     * Created by Showket on 2015/28/04.
     */
    public class SearchProductsSellAdapter extends RecyclerView.Adapter<FeedListRowHolderSell> {

        //private List<SearchProductsSellModel> feedItemList;
        private Context mContext;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        private DisplayImageOptions options;



        public SearchProductsSellAdapter(Context context) {
           // this.feedItemList = sellArraylist;
            this.mContext = context;

            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.logo_small)
                    .showImageForEmptyUri(R.drawable.logo_small)
                    .showImageOnFail(R.drawable.logo_small)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true).build();
//                    .displayer(new RoundedBitmapDisplayer(20)).build();


        }

        @Override
        public FeedListRowHolderSell onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_search_products, null);
            FeedListRowHolderSell mh = new FeedListRowHolderSell(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolderSell feedListRowHolder, final int i) {
            final SearchProductsSellModel feedItem = sellArraylist.get(i);
            //Picasso.with(mContext).load(feedItem.getProductImage()).into(feedListRowHolder.productImage);
            ImageLoader.getInstance().displayImage(feedItem.getProductImage(),feedListRowHolder.productImage, options, animateFirstListener);
            ImageLoader.getInstance().displayImage(feedItem.getUserProfileImage(),feedListRowHolder.profileImage, options, animateFirstListener);
            //Picasso.with(mContext).load(feedItem.getUserProfileImage()).into(feedListRowHolder.profileImage);
            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));
            feedListRowHolder.userRaings.setText(Html.fromHtml(feedItem.getUserRaings())+" Star )");
//            feedListRowHolder.productAddedTime.setText(Html.fromHtml(feedItem.getProductAddedTime()) + ", ");
            feedListRowHolder.productAddedTime.setText(applicationClass.getExpirationTimeTodisplay(feedItem.getProductAddedTime()) + ", ");

            feedListRowHolder.productModel.setText(Html.fromHtml(feedItem.getProductModel()));
            feedListRowHolder.addressField.setText(Html.fromHtml(feedItem.getAddressField()));
            if(Integer.parseInt(feedItem.getLikesCount())==1)
            feedListRowHolder.likesCount.setText(feedItem.getLikesCount()+" Like");
            else
            feedListRowHolder.likesCount.setText(feedItem.getLikesCount()+" Likes");
            if(Integer.parseInt(feedItem.getLikesCount())==1)
            feedListRowHolder.commentCount.setText(feedItem.getCommentCount()+" Comment");
            else
            feedListRowHolder.commentCount.setText(feedItem.getCommentCount()+" Comments");
            feedListRowHolder.availableCount.setText(feedItem.getAvailableCount());
            feedListRowHolder.productPrice.setText(feedItem.getProductPrice());
            feedListRowHolder.description.setText(feedItem.getProductDesc());
            if (feedItem.getIsShownLayout()==false) {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
            }
            else {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
            }

            if (feedItem.getIsLiked().equals("0")) {
                feedListRowHolder.likeHeartBtn.setBackgroundResource(R.drawable.btn_like_small_normal);
            }
            else {
                feedListRowHolder.likeHeartBtn.setBackgroundResource(R.drawable.btn_like_small_pressed);
            }

            feedListRowHolder.revealBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {

                        if (feedListRowHolder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                            feedItem.setIsShownLayout(false);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                        } else {
                            feedItem.setIsShownLayout(true);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                        }
                        notifyDataSetChanged();
                    }

                }
            });



            feedListRowHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            String status="1";
                            if(feedItem.getIsLiked().equals("0"))
                                status="1";
                            else
                                status="0";
                            FROM_SELL_OR_BUY = 0;
                            LikeProductApi(feedItem.getProductId(), i, FROM_SELL_OR_BUY, status);
                        }
                    }
                }
            });
            feedListRowHolder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                        parent.viewPager.setVisibility(View.GONE);
                        parent.bottomBar.setVisibility(View.GONE);
                        parent.topBar.setVisibility(View.GONE);
                        parent.fragmentContainer.setVisibility(View.VISIBLE);
                        CommentProductFragment commentProductFragment = new CommentProductFragment();
                        commentProductFragment.setProductIdId(feedItem.getProductId());
                        commentProductFragment.setLikeCount(feedItem.getLikesCount());


                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(parent.fragmentContainer.getId(), commentProductFragment);
                        ft.addToBackStack(commentProductFragment.getClass().getName());
                        ft.commit();





                    }

//                    if(Config.getSKIP_MODE())
//                        parent.showPopupHome();
//                    else {
//                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
//                        parent.viewPager.setVisibility(View.GONE);
//                        parent.bottomBar.setVisibility(View.GONE);
//                        parent.topBar.setVisibility(View.GONE);
//                        parent.fragmentContainer.setVisibility(View.VISIBLE);
//                        CommentProductFragment commentProductFragment = new CommentProductFragment();
//                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), commentProductFragment);
//                        ft.commit();
//                    }

                }
            });

            feedListRowHolder.commentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                        parent.viewPager.setVisibility(View.GONE);
                        parent.bottomBar.setVisibility(View.GONE);
                        parent.topBar.setVisibility(View.GONE);
                        parent.fragmentContainer.setVisibility(View.VISIBLE);
                        CommentProductFragment commentProductFragment = new CommentProductFragment();
                        commentProductFragment.setProductIdId(feedItem.getProductId());
                        commentProductFragment.setLikeCount(feedItem.getLikesCount());


                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), commentProductFragment);
                        ft.addToBackStack(commentProductFragment.getClass().getName());
                        ft.commit();
                    }



                }
            });
            feedListRowHolder.likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Integer.parseInt(feedItem.getLikesCount())>0) {

                        if (Config.getSKIP_MODE())
                            parent.showPopupHome();
                        else {
                            parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                            parent.viewPager.setVisibility(View.GONE);
                            parent.bottomBar.setVisibility(View.GONE);
                            parent.topBar.setVisibility(View.GONE);
                            parent.fragmentContainer.setVisibility(View.VISIBLE);
                            LikeProductFragment likeProductFragment = new LikeProductFragment();
                            likeProductFragment.setProductIdId(feedItem.getProductId());
                            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), likeProductFragment);
                            ft.commit();
                            //showLikePopup(getActivity());
                        }
                    }





                }
            });
            feedListRowHolder.topPart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });

            feedListRowHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    if(Config.getSKIP_MODE())
                        parent.showPopupHome();
                    else {
                        //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
//                        if (Config.isLollyPop()) {
//                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
//                            Intent intent = new Intent(mContext, ViewPagerExampleActivity.class);
//                            intent.putExtra("images", feedItem.getDollarSeparatedproductImage());
//                            getActivity().startActivity(intent, options.toBundle());
//                        } else
                        {
                            Intent intent = new Intent(mContext, ViewPagerExampleActivity.class);
                            intent.putExtra("images", feedItem.getDollarSeparatedproductImage());
                            getActivity().startActivity(intent);
                        }
                    }
                }
            });

            feedListRowHolder.offerBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(mContext, BuyingProducts.class);
                        intent.putExtra("layoutShown", "buy");
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        Intent intent = new Intent(mContext, BuyingProducts.class);
                        intent.putExtra("layoutShown", "buy");
                        intent.putExtra("productName", feedItem.getProductModel());
                        intent.putExtra("productImage", feedItem.getProductImage());
                        intent.putExtra("userImage", feedItem.getUserProfileImage());
                        intent.putExtra("userName", feedItem.getUserName());
                        intent.putExtra("userRank", feedItem.getUserRaings());
                        intent.putExtra("time", feedItem.getProductAddedTime());
                        intent.putExtra("address", feedItem.getAddressField());
                        intent.putExtra("available", feedItem.getAvailableCount());
                        intent.putExtra("price", feedItem.getProductPrice());
                        intent.putExtra("description", feedItem.getProductDesc());
                        getActivity().startActivity(intent);
                    }
                }
            });

            feedListRowHolder.promoteBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {



                    Config.setPRODUCT_ID(feedItem.getProductId());
                    Intent intent = new Intent(mContext, BuyingProducts.class);
                    intent.putExtra("layoutShown","promote");
                    getActivity().startActivity(intent);
                }
            });
            feedListRowHolder.descBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    Data.productId=feedItem.getProductId();
                    if(Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(mContext, ProductDescription.class);
                       // intent.putExtra("layoutShown", "description");
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        Intent intent = new Intent(mContext, ProductDescription.class);
                        intent.putExtra("productName", feedItem.getProductModel());
                        intent.putExtra("productImage", feedItem.getProductImage());
                        intent.putExtra("userImage", feedItem.getUserProfileImage());
                        intent.putExtra("userName", feedItem.getUserName());
                        intent.putExtra("userRank", feedItem.getUserRaings());
                        intent.putExtra("time", feedItem.getProductAddedTime());
                        intent.putExtra("address", feedItem.getAddressField());
                        intent.putExtra("available", feedItem.getAvailableCount());
                        intent.putExtra("price", feedItem.getProductPrice());
                        intent.putExtra("description", feedItem.getProductDesc());
                        getActivity().startActivity(intent);
                    }



                }
            });


            feedListRowHolder.revealBtn.setTag(feedListRowHolder);

        }

        @Override
        public int getItemCount() {
            return (null != sellArraylist ? sellArraylist.size() : 0);
        }
    }


    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

    class FeedListRowHolderSell extends RecyclerView.ViewHolder {
        ProgressBar profileProgress,productProgress;
        CustomTextViewLightFont likesCount,commentCount,description;
        ImageView profileImage,productImage,likeHeartBtn;
        CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                productPrice,availableCount,commentBtnLabel;
        RelativeLayout rel,likeBtn,comentBtn,seeRelative;
        LinearLayout buttonLayoutReveal,topPart;
        Button revealBtn,promoteBtn,descBtn,offerBtn;
        RelativeLayout rl;

        public FeedListRowHolderSell(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView.findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView.findViewById(R.id.cardImage);

            this.productImage = (ImageView) convertView
                    .findViewById(R.id.cardImage);
            this.likeHeartBtn = (ImageView) convertView
                    .findViewById(R.id.likeHeartBtn);

            this.revealBtn = (Button) convertView
                    .findViewById(R.id.revealBtn);
            this.promoteBtn = (Button) convertView
                    .findViewById(R.id.promoteIcon);
            this.descBtn = (Button) convertView
                    .findViewById(R.id.descIcon);
            this.offerBtn = (Button) convertView
                    .findViewById(R.id.offerIcon);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.description = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.descriptionText);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.userWhoRated = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productBarUserName);
            this.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userRatingValue);
            this.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.countPlus);
            this.buttonLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.buttonLayoutReveal);
            this.topPart = (LinearLayout) convertView
                    .findViewById(R.id.topPart);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.seeRelative = (RelativeLayout) convertView
                    .findViewById(R.id.seeRelative);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
            this.productAddedTime = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.timeValue);
            this.addressField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.addressField);
            this.availableCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.noOfItems);
            this.productPrice = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.priceProduct);
            this.rl = (RelativeLayout) convertView.findViewById(R.id.rv);

        }

    }
    class FeedListRowHolder extends RecyclerView.ViewHolder {
        ProgressBar profileProgress,productProgress;
        CustomTextViewLightFont likesCount,commentCount,description;
        ImageView profileImage,productImage,likeHeartBtn;
        CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                productPrice,availableCount,commentBtnLabel;
        RelativeLayout rel,likeBtn,comentBtn,seeRelative;
        LinearLayout buttonLayoutReveal,topPart;
        Button revealBtn,promoteBtn,descBtn,offerBtn;
        RelativeLayout rl;

        public FeedListRowHolder(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView.findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView.findViewById(R.id.cardImage);

            this.productImage = (ImageView) convertView
                    .findViewById(R.id.cardImage);

            this.revealBtn = (Button) convertView
                    .findViewById(R.id.revealBtn);
            this.likeHeartBtn = (ImageView) convertView
                    .findViewById(R.id.likeHeartBtn);
            this.promoteBtn = (Button) convertView
                    .findViewById(R.id.promoteIcon);
            this.descBtn = (Button) convertView
                    .findViewById(R.id.descIcon);
            this.offerBtn = (Button) convertView
                    .findViewById(R.id.offerIcon);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.description = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.descriptionText);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.userWhoRated = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productBarUserName);
            this.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userRatingValue);
            this.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.countPlus);
            this.buttonLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.buttonLayoutReveal);
            this.topPart = (LinearLayout) convertView
                    .findViewById(R.id.topPart);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.seeRelative = (RelativeLayout) convertView
                    .findViewById(R.id.seeRelative);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
            this.productAddedTime = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.timeValue);
            this.addressField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.addressField);
            this.availableCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.noOfItems);
            this.productPrice = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.priceProduct);
            this.rl = (RelativeLayout) convertView.findViewById(R.id.rv);

        }

    }

//    /**
//     * search People Api
//     */
//    public void SearchPeopleApi() {
//        Data.loading_box(getActivity(), "Loading...");
//        RequestParams params = new RequestParams();
//        params.put("search_param", homeSearchBar.getText().toString());
//        params.put("offset", offSet);
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.setTimeout(Config.getSERVER_TIMEOUT());
//        client.get(Config.getBaseURL() + "searchPeople", params,
//                new AsyncHttpResponseHandler() {
//                    @Override
//                    public void onSuccess(String response) {
//                        Log.e("request succesfull", "response = " + response);
//                        JSONObject jObj;
//                        try {
//                            jObj = new JSONObject(response);
//                            int status = jObj.getInt("status");
//                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
//
//                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getJSONObject("data").getString("total_results")+ "results found");
//                                JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");
//
//                                SearchPeopleModel searchPeopleModel = null;
//                                SearchPeopleFrag.arraylist.clear();
//                                for (int i = 0; i < userResults.length(); i++) {
//                                    searchPeopleModel = new SearchPeopleModel(userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("user_id"));
//                                    SearchPeopleFrag.arraylist.add(searchPeopleModel);
//
//                                }
//                                SearchPeopleFrag.searchPeopleAdapter.notifyDataSetChanged();
//
//
//
//                            }
//                            else{
//                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
//
//                            }
//
//
//                        } catch (JSONException e) {
//
//                            e.printStackTrace();
//                        } catch (Exception e) {
//
//                            e.printStackTrace();
//                        }
//                        Data.loading_box_stop();
//                    }
//
//                    @Override
//                    public void onFailure(Throwable arg0) {
//                        Log.i("request fail", arg0.toString());
//                        Data.loading_box_stop();
//                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
//
//                    }
//                });
//
//    }

    /**
     * like product
     */
    public void LikeProductApi(String productId, final int position, final int FROM_SELL_OR_BUY,final String likeStatus) {

        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_id", productId);
        params.put("status", likeStatus);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "likeProduct", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
                                if(FROM_SELL_OR_BUY==0) {
                                    if(likeStatus.equals("1")) {
                                        sellArraylist.get(position).setIsLiked("1");
                                        sellArraylist.get(position).setLikesCount((Integer.parseInt(sellArraylist.get(position).getLikesCount()) + 1) + "");
                                    }
                                    else
                                    {
                                        sellArraylist.get(position).setIsLiked("0");

                                        if(!(Integer.parseInt(sellArraylist.get(position).getLikesCount())<0))
                                        {
                                            sellArraylist.get(position).setLikesCount((Integer.parseInt(sellArraylist.get(position).getLikesCount()) -1) + "");

                                        }
                                    }
                                    searchProductsSelAdapter.notifyDataSetChanged();
                                }
                                else
                                {

                                    if(likeStatus.equals("1")) {
                                        buyArraylist.get(position).setIsLiked("1");
                                        buyArraylist.get(position).setLikesCount((Integer.parseInt(buyArraylist.get(position).getLikesCount()) + 1) + "");
                                    }
                                    else
                                    {
                                        buyArraylist.get(position).setIsLiked("0");
                                        if(!(Integer.parseInt(buyArraylist.get(position).getLikesCount())<0))
                                        {
                                            buyArraylist.get(position).setLikesCount((Integer.parseInt(buyArraylist.get(position).getLikesCount()) - 1) + "");

                                        }
                                    }
                                    searchProductsBuyAdapter.notifyDataSetChanged();


//                                    buyArraylist.get(position).setLikesCount((Integer.parseInt(buyArraylist.get(position).getLikesCount())+1)+"");

//                                    searchProductsBuyAdapter.notifyDataSetChanged();
                                }

                            }
                            else{
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }

    /**
     * search Products Api
     */
    public void SearchProductApi() {
//        if ((offSetBuY < Config.getOFFSET() && contentFlag==1) || (offSetSell < Config.getOFFSET() && contentFlag==0) || NEW_SEARCH==1)
        if ((contentFlag==1 && NEW_SEARCH==1) || (contentFlag==0 && NEW_SEARCH==1))
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("search_param", homeSearchBar.getText().toString());
        if(contentFlag==1)
        params.put("offset", offSetBuY+"");
        else
        params.put("offset", offSetSell+"");
        params.put("sell_buy_flag", contentFlag+"");
        String serverUrl="";
        if(Config.getSKIP_MODE())
        {
            serverUrl=Config.getBaseURL() + "searchProducts";

        }
        else
        {
            params.put("access_token", Config.getAPP_ACCESS_TOKEN());
            serverUrl=Config.getBaseURL() + "accessTokenSearchProducts";
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        if(Config.getSKIP_MODE()) {
            client.get(serverUrl, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            Log.e("request succesfull", "response = " + response);
                            JSONObject jObj;
                            try {
                                jObj = new JSONObject(response);
                                int status = jObj.getInt("status");
                                if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                    JSONArray productResults = jObj.getJSONObject("data").getJSONArray("results");
                                    /**
                                     * Get data for Buy Listview
                                     * contentFlag=1

                                     */

                                    if (contentFlag == 1) {

                                        if (buyArraylist.size()> 0) {
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);
                                                searchProductsBuyModel = new SearchProductsBuyModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                buyArraylist.add(searchProductsBuyModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            buyingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(buyingCount + " "+getResources().getString(R.string.resultsFound));

                                            searchProductsBuyAdapter.notifyDataSetChanged();
                                        } else {
                                            buyArraylist.clear();
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);
                                                searchProductsBuyModel = new SearchProductsBuyModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                buyArraylist.add(searchProductsBuyModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            buyingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(buyingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsBuyAdapter = new SearchProductsBuyAdapter(getActivity());
                                            buyRecyclerView.setAdapter(searchProductsBuyAdapter);
                                        }
                                    } else {
                                        if (sellArraylist.size()> 0) {

                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);

                                                searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                sellArraylist.add(searchProductsSellModel);

                                            }
                                            offSetSell = offSetSell + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            sellingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(sellingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsSelAdapter.notifyDataSetChanged();
                                        } else {
                                            sellArraylist.clear();
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);

                                                searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                sellArraylist.add(searchProductsSellModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            sellingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(sellingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsSelAdapter = new SearchProductsSellAdapter(getActivity());
                                            sellRecyclerView.setAdapter(searchProductsSelAdapter);
                                        }
                                    }

                                } else {
//                                    if ((offSetBuY < Config.getOFFSET() && contentFlag == 1) || (offSetSell < Config.getOFFSET() && contentFlag == 0) || NEW_SEARCH == 1)
                                        if ((contentFlag==1 && NEW_SEARCH==1) || (contentFlag==0 && NEW_SEARCH==1))
                                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                                }


                            } catch (JSONException e) {

                                e.printStackTrace();
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                            Data.loading_box_stop();
                        }

                        @Override
                        public void onFailure(Throwable arg0) {
                            if ((contentFlag==1 && NEW_SEARCH==1) || (contentFlag==0 && NEW_SEARCH==1)) {
                                Data.loading_box_stop();
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                            }

                        }
                    });
        }
        else
        {
            client.post(serverUrl, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            Log.e("request succesfull", "response = " + response);
                            JSONObject jObj;
                            try {
                                jObj = new JSONObject(response);
                                int status = jObj.getInt("status");
                                if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                    JSONArray productResults = jObj.getJSONObject("data").getJSONArray("results");
                                    /**
                                     * Get data for Buy Listview
                                     * contentFlag=1

                                     */

                                    if (contentFlag == 1) {

                                        if (buyArraylist.size()>0) {
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);
                                                searchProductsBuyModel = new SearchProductsBuyModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        productResults.getJSONObject(i).getString("like_status"), productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                buyArraylist.add(searchProductsBuyModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            buyingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(buyingCount + " "+getResources().getString(R.string.resultsFound));

                                            searchProductsBuyAdapter.notifyDataSetChanged();
                                        } else {
                                            buyArraylist.clear();
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);
                                                searchProductsBuyModel = new SearchProductsBuyModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        productResults.getJSONObject(i).getString("like_status"), productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                buyArraylist.add(searchProductsBuyModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            buyingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(buyingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsBuyAdapter = new SearchProductsBuyAdapter(getActivity());
                                            buyRecyclerView.setAdapter(searchProductsBuyAdapter);
                                        }
                                    } else {
                                        if (sellArraylist.size()>0) {

                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);

                                                searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        productResults.getJSONObject(i).getString("like_status"), productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                sellArraylist.add(searchProductsSellModel);

                                            }
                                            offSetSell = offSetSell + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            sellingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(sellingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsSelAdapter.notifyDataSetChanged();
                                        } else {
                                            sellArraylist.clear();
                                            for (int i = 0; i < productResults.length(); i++) {
                                                String productUrls = "";
                                                for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                    productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                                productUrls = productUrls.substring(0, productUrls.length() - 1);

                                                searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
                                                        productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"), productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
                                                        productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
                                                        productResults.getJSONObject(i).getString("like_status"), productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"), productResults.getJSONObject(i).getString("description"), false);

                                                sellArraylist.add(searchProductsSellModel);

                                            }
                                            offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                            sellingCount = Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
                                            resultCount.setText(sellingCount + " "+getResources().getString(R.string.resultsFound));
                                            searchProductsSelAdapter = new SearchProductsSellAdapter(getActivity());
                                            sellRecyclerView.setAdapter(searchProductsSelAdapter);
                                        }
                                    }

                                } else {
                                    if ((contentFlag==1 && NEW_SEARCH==1) || (contentFlag==0 && NEW_SEARCH==1))
//                                        if ((offSetBuY < Config.getOFFSET() && contentFlag == 1) || (offSetSell < Config.getOFFSET() && contentFlag == 0) || NEW_SEARCH == 1)
                                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                                }


                            } catch (JSONException e) {

                                e.printStackTrace();
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                            Data.loading_box_stop();
                        }

                        @Override
                        public void onFailure(Throwable arg0) {
                            if ((contentFlag==1 && NEW_SEARCH==1) || (contentFlag==0 && NEW_SEARCH==1)){
//                                if ((offSetBuY < Config.getOFFSET() && contentFlag == 1) || (offSetSell < Config.getOFFSET() && contentFlag == 0) || NEW_SEARCH == 1) {
                                Data.loading_box_stop();
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                            }

                        }
                    });
        }

    }

    /**
     * Show Likes of Product
     */
    public void ShowProductLikesApi(String offset,String productId) {
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_id", productId);
        params.put("offset", offset);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.get(Config.getBaseURL() + "getProductLikes", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }
                            else{
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }


}

