package pager.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.clicklabs.butlers.R;

import clabs.butlers.utils.CustomTextViewSemiBold;

/**
 * Created by clicklabs107 on 4/27/15.
 */
public class InfoFragment extends Fragment {

    RelativeLayout backBtn;
    HomFragment parent;
    FragmentManager fm;
    CustomTextViewSemiBold header;

    static InfoFragment init(int val) {
        InfoFragment infoFragment = new InfoFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
       infoFragment.setArguments(args);

        return infoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView =inflater.inflate(R.layout.info_layout, container, false);
        backBtn = (RelativeLayout) rootView.findViewById(R.id.backBtnMain);
        header = (CustomTextViewSemiBold) rootView.findViewById(R.id.header);
        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.topBar.setVisibility(View.GONE);
        header.setText(getResources().getString(R.string.info));

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(fm.popBackStackImmediate()) {
                    parent.topBar.setVisibility(View.VISIBLE);
                }
            }
        });

        return rootView;
    }
/*
handles device back pressed
 */
    public void pressBack() {
        parent.topBar.setVisibility(View.VISIBLE);

    }
}
