package pager.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clicklabs.butlers.R;

/**
 * Created by click103 on 12/3/15.
 */
public class BazarNotificationFrag extends Fragment {

    public static BazarNotificationFrag init(int val) {
        BazarNotificationFrag bazarNotificationFrag = new BazarNotificationFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        bazarNotificationFrag.setArguments(args);

        return bazarNotificationFrag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_people_layout, container, false);
        return rootView;
    }
}
