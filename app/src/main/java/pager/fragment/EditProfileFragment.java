package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.clicklabs.butlers.R;

import java.util.ArrayList;
import java.util.Locale;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import material.animations.MaterialDesignAnimations;

/**
 * Created by clicklabs107 on 4/23/15.
 */
public class EditProfileFragment extends Fragment {

    RelativeLayout bottomGreenBar, nextBtn, backBtn;
    CustomTextViewSemiBold header, footer;
    MaterialDesignAnimations animations;
    HomFragment parent;
    CustomTextViewRegularFont aboutText;
    Spinner countrySpinner;
    ArrayList<String> coutries;
    ArrayList<String> coutriescodes;
    Button countryBtn;

    static EditProfileFragment init(int val) {
        EditProfileFragment editProfileFragment = new EditProfileFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
      editProfileFragment.setArguments(args);

        return editProfileFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View rootView = inflater.inflate(R.layout.edit_profile, container, false);
        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.bottomBar.setVisibility(View.GONE);
        parent.topBar.setVisibility(View.GONE);
        parent.fragmentContainer.setVisibility(View.VISIBLE);
        nextBtn = (RelativeLayout) rootView.findViewById(R.id.NextBtnMain);
        backBtn = (RelativeLayout) rootView.findViewById(R.id.backBtnMain);
        bottomGreenBar = (RelativeLayout) rootView.findViewById(R.id.green_bar);
        header = (CustomTextViewSemiBold) rootView.findViewById(R.id.header);
        footer = (CustomTextViewSemiBold) rootView.findViewById(R.id.footerLabel);
        aboutText = (CustomTextViewRegularFont) rootView.findViewById(R.id.aboutfieldRegister);
        countrySpinner = (Spinner) rootView.findViewById(R.id.countrySpinner);
        countryBtn = (Button) rootView.findViewById(R.id.countryBtn);


        animations=new MaterialDesignAnimations();
        header.setText(getResources().getString(R.string.edit_profile));
        footer.setText(getResources().getString(R.string.doneLabel));
        coutries = new ArrayList<String>();
        coutriescodes = new ArrayList<String>();
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getFragmentManager().popBackStackImmediate()) {
                    parent.fragmentContainer.setVisibility(View.GONE);
                    parent.bottomBar.setVisibility(View.VISIBLE);
                    parent.topBar.setVisibility(View.VISIBLE);
                    Data.hideSoftKeyboard(getActivity());
                }
            }
        });
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getFragmentManager().popBackStackImmediate()) {
                    parent.fragmentContainer.setVisibility(View.GONE);
                    parent.bottomBar.setVisibility(View.VISIBLE);
                    parent.topBar.setVisibility(View.VISIBLE);
                    Data.hideSoftKeyboard(getActivity());
                }
            }
        });
        aboutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutYouPopup(getActivity());
            }
        });

        String[] isoCountryCodes = Locale.getISOCountries();
        coutries.clear();
        coutriescodes.clear();
        for (String countryCode : isoCountryCodes) {
            Locale locale = new Locale("", countryCode);
            String countryName = locale.getDisplayCountry();

            coutries.add(locale.getDisplayCountry());
            coutriescodes.add(countryCode);
        }

        ArrayAdapter coutryAdapter = new ArrayAdapter(getActivity(), R.layout.country_spinner_item, coutries);
        coutryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(coutryAdapter);


        /*** country btn***/
        countryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySpinner.performClick();
            }
        });


        return rootView;
    }

  public void pressBack() {

        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);
        parent.bottomBar.setVisibility(View.VISIBLE);
        parent.topBar.setVisibility(View.VISIBLE);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void aboutYouPopup(Activity activity) {
        final EditText aboutDesc;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.about_you_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            TextView textHead = (TextView) dialog.findViewById(R.id.headerLabel);
            textHead.setText(getResources().getString(R.string.aboutYou));
            aboutDesc = (EditText) dialog.findViewById(R.id.aboutFieldPopup);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backPopup);
            aboutDesc.setText(aboutText.getText());
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            Button doneBtn = (Button) dialog.findViewById(R.id.doneBtn);
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();
                    aboutText.setText(aboutDesc.getText());
                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}
