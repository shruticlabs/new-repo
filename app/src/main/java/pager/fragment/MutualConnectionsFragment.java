package pager.fragment;

        import android.app.Activity;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.FrameLayout;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.RelativeLayout;

        import com.androidquery.AQuery;
        import com.example.clicklabs.butlers.R;
        import com.loopj.android.http.AsyncHttpClient;
        import com.loopj.android.http.AsyncHttpResponseHandler;
        import com.loopj.android.http.RequestParams;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.List;
        import clabs.butlers.utils.ApiResponseFlags;
        import clabs.butlers.utils.AppStatus;
        import clabs.butlers.utils.Config;
        import clabs.butlers.utils.CustomTextViewRegularFont;
        import clabs.butlers.utils.CustomTextViewSemiBold;
        import clabs.butlers.utils.Data;
        import clicklabs.app.butlers.BuyingProducts;
        import material.animations.MaterialDesignAnimations;
        import models.CommentModel;
        import models.PromoteUserListModel;

/**
 * Created by clicklabs107 on 4/23/15.
 */
public class MutualConnectionsFragment extends Fragment {

    RelativeLayout bottomGreenBar, nextBtn, backBtn;
    CustomTextViewSemiBold header, footer;
    MaterialDesignAnimations animations;
    HomFragment parent;
    PromoteUserListModel mutualConnectionsModel;
    ArrayList<PromoteUserListModel> mutualArrayList;
    MutualAdapter mutualAdapter;
    int offSet=0;
    String userId="",followStatus="0";
    LinearLayout errorLayout;
    ListView likeListView;
    String String="";


    static MutualConnectionsFragment init(int val) {
        MutualConnectionsFragment mutualConnectionsFragment = new MutualConnectionsFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        mutualConnectionsFragment.setArguments(args);

        return mutualConnectionsFragment;
    }
    public void setUserId(String id)
    {
        String=id;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.followers_layout, container, false);
        likeListView=(ListView)rootView.findViewById(R.id.mutualList);
        errorLayout=(LinearLayout)rootView.findViewById(R.id.errorLayout);
        header=(CustomTextViewSemiBold)rootView.findViewById(R.id.header);
        header.setText(getResources().getString(R.string.mutualConnections));
        BuyingProducts activity = (BuyingProducts) getActivity();
        userId = activity.getUserId();

        LayoutInflater inflaterheader = getActivity().getLayoutInflater();
        View header = inflater.inflate(R.layout.like_product_header_view, null);

        likeListView.addHeaderView(header, null, false);
        final RelativeLayout loadPreviousLikes=(RelativeLayout)rootView.findViewById(R.id.loadPreviousLikes);
        loadPreviousLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                } else {
                    MutualConnectionApi();

                }
            }
        });
        RelativeLayout backBtn=(RelativeLayout)rootView.findViewById(R.id.backBtnMain);
        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
        } else {
            offSet=1;
            MutualConnectionApi();

        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(MutualConnectionsFragment.this).commit();
                FrameLayout fragmentContainer=(FrameLayout)getActivity().findViewById(R.id.fragmentContainer);
                RelativeLayout profileLayout=(RelativeLayout)getActivity().findViewById(R.id.profileLayout);
                fragmentContainer.setVisibility(View.GONE);
                profileLayout.setVisibility(View.VISIBLE);
            }
        });

        return rootView;
    }

    /**
     * Product Comments Api
     */
    public void MutualConnectionApi() {
        if (offSet< Config.getOFFSET())
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("offset", offSet+"");
        params.put("user_id", userId);
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getMutualFriends", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                JSONArray userResults = jObj.getJSONArray("data");
                                if (offSet == 1) {


                                    mutualArrayList = new ArrayList<>();
                                    for (int i = 0; i < userResults.length(); i++) {
                                        mutualConnectionsModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                        mutualArrayList.add(mutualConnectionsModel);

                                    }
                                    mutualAdapter = new MutualAdapter(getActivity(), mutualArrayList);
                                    likeListView.setAdapter(mutualAdapter);
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                } else {
                                    ArrayList<PromoteUserListModel> prevLikes = new ArrayList<>();
                                    prevLikes.addAll(mutualArrayList);
                                    mutualArrayList.clear();
                                    for (int i = 0; i < userResults.length(); i++) {
                                        mutualConnectionsModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                        mutualArrayList.add(mutualConnectionsModel);

                                    }
                                    mutualArrayList.addAll(prevLikes);

                                    mutualAdapter.notifyDataSetChanged();
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                }
                            } else {
                                if (offSet < Config.getOFFSET())
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        if (offSet < Config.getOFFSET())
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                      }
                });

    }





    public class MutualAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_friends = null;
        Activity activity;
        LayoutInflater inflater = null;
        ViewHolderLikes holder;


        public MutualAdapter(Activity a, List<PromoteUserListModel> all_friends) {
            this.all_friends = all_friends;
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_friends.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_friends.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(
                        R.layout.list_item_like, null);
                holder = new ViewHolderLikes();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);

                holder.offferLabel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.selectBtn);


                convertView.setTag(holder);


            } else {
                holder = (ViewHolderLikes) convertView.getTag();
            }



            holder.nameField.setText(all_friends.get(position).getNamefield());
//            holder.comment.setText(all_friends.get(position).getUserComment());


            AQuery aq = new AQuery(convertView);

            try{

                aq.id(holder.img)
                        .image(all_friends.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            }catch(Exception e){

            }

            if (all_friends.get(position).getIsSelected()) {
                holder.offferLabel.setBackgroundResource(R.drawable.follow_selected);
                holder.offferLabel.setTextColor(getResources().getColor(R.color.white));
                holder.offferLabel.setText(getResources().getString(R.string.followingLabel));
            }
            else
            {
                holder.offferLabel.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.offferLabel.setText(getResources().getString(R.string.follow));
                holder.offferLabel.setTextColor(getResources().getColor(R.color.statusBar));
            }

            holder.offferLabel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (all_friends.get(position).getIsSelected() == true) {
                        followStatus="0";
                    } else {
                        followStatus="1";
                    }

                    if (!AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                    } else {
                        FollowApi(all_friends.get(position).getRowId(),position);

                    }


                }

            });
            holder.layout.setTag(holder);
            holder.offferLabel.setTag(holder);
            return convertView;

        }



    }

    class ViewHolderLikes {

        RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField;
        CustomTextViewRegularFont offferLabel;

    }

    /**
     * Get Folowing List API
     */
    public void FollowApi(String userId, final int position) {
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("user_id", userId);
        params.put("status", followStatus);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "follow", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) ||(ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {

                                if (followStatus.equals("1")) {
                                    mutualArrayList.get(position).setIsSelected(true);
                                    followStatus="0";
                                } else {
                                    mutualArrayList.get(position).setIsSelected(false);
                                    followStatus="1";
                                }
                                mutualAdapter.notifyDataSetChanged();
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            } else {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }

}
