package pager.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.ParallaxPageTransformer;
import clicklabs.app.butlers.HomeActivity;

/**
 * Created by click103 on 12/3/15.
 */
public class SearchFragmentTab extends Fragment {


    int ITEMS = 2;
    SearchPagerAdapter searchPagerAdapter;
    ViewPager viewPager;
    EditText peopleSearchBar, homeSearchbar;

    int PagerPosition = 0;

    public static SearchFragmentTab init(int val) {
        SearchFragmentTab searchFragmentTab = new SearchFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        searchFragmentTab.setArguments(args);

        return searchFragmentTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_layout, container, false);

        searchPagerAdapter = new SearchPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager = (ViewPager) rootView.findViewById(R.id.searchPager);
        viewPager.setAdapter(searchPagerAdapter);
        viewPager.setPageTransformer(true, new ParallaxPageTransformer());
        viewPager.setOffscreenPageLimit(ITEMS);
        peopleSearchBar = (EditText) getActivity().findViewById(R.id.peopleSearchBar);
        homeSearchbar = (EditText) getActivity().findViewById(R.id.homeSearchBar);

        peopleSearchBar.setVisibility(View.VISIBLE);
        homeSearchbar.setVisibility(View.GONE);
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabsStrip.setViewPager(viewPager);
        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (viewPager.getCurrentItem() == 0) {
                    peopleSearchBar.setVisibility(View.VISIBLE);
                    homeSearchbar.setVisibility(View.GONE);
                } else {
                    peopleSearchBar.setVisibility(View.GONE);
                    homeSearchbar.setVisibility(View.VISIBLE);
                }
            }
        });


        return rootView;

    }


    /**
     * Promote people/shop pager adapter
     */

    public class SearchPagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{getResources().getString(R.string.peopleLabel), getResources().getString(R.string.products)};

        public SearchPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return SearchPeopleFrag.init(position);
                case 1:
                    return new SearchProductsFrag().init(position);
                default:
                    return new SearchProductsFrag().init(position);

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }


    }
}
