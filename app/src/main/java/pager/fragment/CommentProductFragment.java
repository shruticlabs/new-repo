package pager.fragment;

        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.RelativeLayout;

        import com.example.clicklabs.butlers.R;
        import com.loopj.android.http.AsyncHttpClient;
        import com.loopj.android.http.AsyncHttpResponseHandler;
        import com.loopj.android.http.RequestParams;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;

        import adapters.CommentAdapter;
        import clabs.butlers.utils.ApiResponseFlags;
        import clabs.butlers.utils.AppStatus;
        import clabs.butlers.utils.Config;
        import clabs.butlers.utils.CustomEditTextRegularFont;
        import clabs.butlers.utils.CustomTextViewRegularFont;
        import clabs.butlers.utils.CustomTextViewSemiBold;
        import clabs.butlers.utils.Data;
        import material.animations.MaterialDesignAnimations;
        import models.CommentModel;
        import models.PromoteUserListModel;

/**
 * Created by clicklabs107 on 4/23/15.
 */
public class CommentProductFragment extends Fragment {

    LinearLayout errorLayout;
    RelativeLayout backBtn;
    CustomTextViewSemiBold header, footer;
    MaterialDesignAnimations animations;
    HomFragment parent;
    ArrayList<CommentModel> commentModelArrayList;
    CommentModel commentModel;
    CommentAdapter commentAdapter;
    String productIdId="",likeCount;
    int offSet=0;
    ListView commentsListView;
    Button sendBtn;
    CustomEditTextRegularFont editView;
    CustomTextViewSemiBold howManyLike;
    CustomTextViewRegularFont noComents;
     RelativeLayout loadPreviousComments;

     CommentProductFragment init(int val) {
        CommentProductFragment commentProductFragment = new CommentProductFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);


        commentProductFragment.setArguments(args);

        return commentProductFragment;
    }
    public void setProductIdId(String productIdId) {
       this. productIdId = productIdId;
    }
    public void setLikeCount(String likeCount) {
        this. likeCount = likeCount;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.comment_product, container, false);
         commentsListView=(ListView)rootView.findViewById(R.id.comemtsListView);
         errorLayout=(LinearLayout)rootView.findViewById(R.id.errorLayout);
        sendBtn=(Button)rootView.findViewById(R.id.sendBtn);


        commentModelArrayList=new ArrayList<>();
        LayoutInflater inflaterheader = getActivity().getLayoutInflater();
        View header = inflater.inflate(R.layout.comment_top_header_view, null);

        commentsListView.addHeaderView(header, null, false);
        howManyLike=(CustomTextViewSemiBold)rootView.findViewById(R.id.howManyLike);
        noComents=(CustomTextViewRegularFont)rootView.findViewById(R.id.noComents);
        howManyLike.setText(likeCount+" people like this");


        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
        } else {
            offSet=0;
            ProductCommentsApi();

        }
/**
 * called to reload data incase of error
 */
        noComents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                } else {
                    ProductCommentsApi();

                }
            }
        });


         editView=(CustomEditTextRegularFont)rootView.findViewById(R.id.commentText);
      
        RelativeLayout backBtn=(RelativeLayout)rootView.findViewById(R.id.backBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(editView.getText().length()==0) {
                    editView.requestFocus();
                    editView.setError(getResources().getString(R.string.nothingToComment));
                }
                else {

                    if (!AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                    } else {
                        AddCommentApi();

                    }
                }
            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                parent.fragmentContainer.setVisibility(View.GONE);
                parent.viewPager.setVisibility(View.VISIBLE);
                parent.bottomBar.setVisibility(View.VISIBLE);
                parent.topBar.setVisibility(View.VISIBLE);
            }
        });


        loadPreviousComments=(RelativeLayout)rootView.findViewById(R.id.loadPreviousLikes);
        loadPreviousComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                } else {
                    ProductCommentsApi();

                }



            }
        });
 

        return rootView;
    }

    private void onBackPressed() {

        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);
        parent.bottomBar.setVisibility(View.VISIBLE);
        parent.topBar.setVisibility(View.VISIBLE);

//        animations.applyHideAnimation(parent.fragmentContainer);
//        animations.appllyLoadInAnimation(parent.mainHomeFragmentLayout);
    }


    /**
     * Product Comments Api
     */
    public void ProductCommentsApi() {
        if(commentModelArrayList.size()==0)
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("offset", offSet+"");
        params.put("product_id", productIdId);
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getProductComments", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status))
                            {

                                JSONArray productComments = jObj.getJSONObject("data").getJSONArray("results");
                                if (commentModelArrayList.size() > 0) {
                                    ArrayList<CommentModel> precomments = new ArrayList<>();
                                    precomments.addAll(commentModelArrayList);
                                    commentModelArrayList.clear();
                                    for (int i = productComments.length()-1; i >=0; i--) {

                                        commentModel = new CommentModel(productComments.getJSONObject(i).getString("profile_pic_link"), productComments.getJSONObject(i).getString("name"), productComments.getJSONObject(i).getString("comment_body"), productComments.getJSONObject(i).getString("created_at"), productComments.getJSONObject(i).getString("user_id"));
                                        commentModelArrayList.add(commentModel);

                                    }
                                    commentModelArrayList.addAll(precomments);
                                    commentAdapter.notifyDataSetChanged();
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));

                                } else {

//                                    commentModelArrayList = new ArrayList<>();
                                    for (int i = productComments.length()-1; i >=0; i--) {

                                        commentModel = new CommentModel(productComments.getJSONObject(i).getString("profile_pic_link"), productComments.getJSONObject(i).getString("name"), productComments.getJSONObject(i).getString("comment_body"), productComments.getJSONObject(i).getString("created_at"), productComments.getJSONObject(i).getString("user_id"));
                                        commentModelArrayList.add(commentModel);

                                    }
                                    commentAdapter = new CommentAdapter(getActivity(), commentModelArrayList);
                                    commentsListView.setAdapter(commentAdapter);
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));

                                    if (commentModelArrayList.size() == 0 && status == ApiResponseFlags.NO_MORE_RESULTS.getOrdinal()) {
                                        noComents.setVisibility(View.VISIBLE);
                                        noComents.setEnabled(false);
                                        noComents.setClickable(false);
                                    } else if (commentModelArrayList.size() == 0 && status == ApiResponseFlags.ERROR_IN_EXECUTION.getOrdinal()) {
                                        noComents.setVisibility(View.VISIBLE);
                                        noComents.setText(getResources().getString(R.string.tapToReload));
                                        noComents.setEnabled(true);
                                        noComents.setClickable(true);
                                    } else {
                                        noComents.setVisibility(View.GONE);
                                    }

                                }

                                noComents.setVisibility(View.GONE);
                                if ((Integer.parseInt(jObj.getJSONObject("data").getString("count"))) < (Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"))))
                                    loadPreviousComments.setVisibility(View.GONE);
                                else
                                    loadPreviousComments.setVisibility(View.VISIBLE);


                            } else {

                                if (commentModelArrayList.size() == 0 && status == ApiResponseFlags.NO_MORE_RESULTS.getOrdinal()) {
                                    noComents.setVisibility(View.VISIBLE);
                                    noComents.setEnabled(false);
                                    noComents.setClickable(false);
                                } else if (commentModelArrayList.size() == 0 && status == ApiResponseFlags.ERROR_IN_EXECUTION.getOrdinal()) {
                                    noComents.setVisibility(View.VISIBLE);
                                    noComents.setText(getResources().getString(R.string.tapToReload));
                                    noComents.setEnabled(true);
                                    noComents.setClickable(true);
                                } else {
                                    noComents.setVisibility(View.GONE);
                                }
                                if (commentModelArrayList.size() == 0)
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        if (commentModelArrayList.size() == 0) {
                               noComents.setVisibility(View.VISIBLE);
                               noComents.setText(getResources().getString(R.string.tapToReload));
                               noComents.setEnabled(true);
                               noComents.setClickable(true);

                            Data.loading_box_stop();
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                        }

                    }
                });

    }



    /**
     * Product Comments Api
     */
    public void AddCommentApi() {
//        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("comment_body", editView.getText().toString());
        params.put("product_id", productIdId);
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "makeComment", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                                JSONObject newComment = jObj.getJSONObject("data");

                                // String  j=newComment.getString(0)
                                commentModel = new CommentModel(newComment.getString("profile_pic_link"), newComment.getString("name"), newComment.getString("comment_body"), newComment.getString("created_at"), newComment.getString("user_id"));

                                if(commentModelArrayList.size()==0)
                                {
                                    commentModelArrayList.add(commentModel);
                                    commentAdapter = new CommentAdapter(getActivity(), commentModelArrayList);
                                    commentsListView.setAdapter(commentAdapter);
                                    editView.setText("");
                                    noComents.setVisibility(View.GONE);
                                    loadPreviousComments.setVisibility(View.GONE);
                                }
                                else
                                {
                                    commentModelArrayList.add(commentModel);
                                    commentAdapter.notifyDataSetChanged();
                                    editView.setText("");
                                }

                            } else {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
//                        Log.i("request fail", arg0.toString());
//                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));


                    }
                });

    }
    public void pressBack() {

        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);
        parent.bottomBar.setVisibility(View.VISIBLE);
        parent.topBar.setVisibility(View.VISIBLE);

    }

}
