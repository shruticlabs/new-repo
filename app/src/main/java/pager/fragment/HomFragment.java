package pager.fragment;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.example.clicklabs.butlers.R;

import java.util.HashMap;
import java.util.Map;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.ParallaxPageTransformer;
import clicklabs.app.butlers.AddProduct;
import clicklabs.app.butlers.HomeActivity;
import clicklabs.app.butlers.SplashActivity;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link pager.fragment.HomFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    static final int ITEMS = 5;
    static final int NOTITEMS = 2;
     public ButlerPagerAdapter butlerPagerAdapter;
    ViewPager viewPager;
    CustomTextViewSemiBold headerText;
    LinearLayout cameraNotificationLayout, searchbarLayout;
    RelativeLayout mainHomeFragmentLayout;
    FrameLayout fragmentContainer;


    public static LinearLayout lastSelectedButton;
    public static int selectedButtonHeight;
    public static int defaultButtonHeight;


//    public static final int UNSELECTED = Color.rgb(80, 230, 0);

    public static Fragment holderFrangment;

    public static int fragmentHolderId = 0;

    LinearLayout btnBazar, btnSrch, btnProf, btnOfr, btnFeed, bottomBar, topBar;
    View hiddenPanel;
    RelativeLayout shadowbehind;
    RelativeLayout slideUp, notificationView, backBtn, optionBtn;

    NotificationPagerAdapter searchPagerAdapter;
    ViewPager notViewPager;
    CustomTextViewRegularFont allBtn, bazarBtn;


    //    public static Fragment bazaarFragmentTab ;
//    public static Fragment searchFragmentTab;
//    public static Fragment feedFragmentTab ;
    Fragment profileFragmentTab;
//    public static Fragment offersFragmentTab ;


    public static FragmentManager manager;
    //    public static FragmentTransaction transaction;
    private final String BACKSTACK = "MY_BACK_STACK";
    public static boolean isBackStacked = false;
    static Map<Integer, Integer> buttonValues;

    static {
        buttonValues = new HashMap<Integer, Integer>();
        buttonValues.put(R.id.btnBazar, 1);
        buttonValues.put(R.id.btnSrch, 2);
        buttonValues.put(R.id.btnFeed, 3);
        buttonValues.put(R.id.btnOfr, 4);
        buttonValues.put(R.id.btnProf, 5);

    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle presses on the action bar items
//        switch (item.getItemId()) {
//            case R.id.action_search:
////                openSearch();
//                return true;
//            case R.id.action_settings:
////                openSettings();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomFragment newInstance(String param1, String param2) {
        HomFragment fragment = new HomFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public HomFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
//            setHasOptionsMenu(true);
        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater =getActivity().getMenuInflater();
//        inflater.inflate(R.menu.main_activity_actions, menu);
//        return true;
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu items for use in the action bar
//        MenuInflater inflater = getActivity().getMenuInflater();
//        inflater.inflate(R.menu.main_activity_actions, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater =  getActivity().getMenuInflater();
//        inflater.inflate(R.menu.main_activity_actions, menu);
//        return true;
//    }
//   @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle presses on the action bar items
//        switch (item.getItemId()) {
//            case R.id.action_search:
////                openSearch();
//                return true;
//            case R.id.action_settings:
////                openSettings();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
    View view;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        view = inflater.inflate(R.layout.home, container, false);
        initViews();
//        initFragments();
//        initManager();
        mainHomeFragmentLayout = (RelativeLayout) view.findViewById(R.id.main_home_fragment_layout);
        fragmentContainer = (FrameLayout) view.findViewById(R.id.add_shop_fragment_container);
        headerText = (CustomTextViewSemiBold) view.findViewById(R.id.header);
        hiddenPanel = view.findViewById(R.id.hidden_panel);
        allBtn = (CustomTextViewRegularFont) view.findViewById(R.id.allBtnNot);
        bazarBtn = (CustomTextViewRegularFont) view.findViewById(R.id.bazarBtnNot);
        shadowbehind = (RelativeLayout) view.findViewById(R.id.shadowbehind);
        slideUp = (RelativeLayout) view.findViewById(R.id.slideUp);
        notificationView = (RelativeLayout) view.findViewById(R.id.notificationView);
        RelativeLayout cameraBtn = (RelativeLayout) view.findViewById(R.id.camerabtnlLay);
        searchbarLayout = (LinearLayout) view.findViewById(R.id.searchbarLayout);
        cameraNotificationLayout = (LinearLayout) view.findViewById(R.id.cameraNotificationLayout);
        bottomBar = (LinearLayout) view.findViewById(R.id.bottomBarLay);
        topBar = (LinearLayout) view.findViewById(R.id.topbarlay);
        backBtn = (RelativeLayout) view.findViewById(R.id.backBtnMain);
        optionBtn = (RelativeLayout) view.findViewById(R.id.moreBtn);
        searchPagerAdapter = new NotificationPagerAdapter(getActivity().getSupportFragmentManager());
        notViewPager = (ViewPager) view.findViewById(R.id.notificationPager);
        notViewPager.setAdapter(searchPagerAdapter);
        allBtn.setBackgroundResource(R.drawable.buying_normal);
        allBtn.setTextColor(getResources().getColor(R.color.white));
        bazarBtn.setTextColor(getResources().getColor(R.color.rowtext));
        profileFragmentTab = new ProfileFragmentTab();
        backBtn.setVisibility(View.GONE);
        optionBtn.setVisibility(View.GONE);


        allBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allBtn.setBackgroundResource(R.drawable.buying_normal);
                bazarBtn.setTextColor(getResources().getColor(R.color.rowtext));
                allBtn.setTextColor(getResources().getColor(R.color.white));
                bazarBtn.setBackgroundResource(0);
                viewPager.setCurrentItem(0);
            }
        });
        bazarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bazarBtn.setBackgroundResource(R.drawable.buying_normal);
                allBtn.setTextColor(getResources().getColor(R.color.rowtext));
                bazarBtn.setTextColor(getResources().getColor(R.color.white));
                allBtn.setBackgroundResource(0);
                viewPager.setCurrentItem(1);
            }
        });

        slideUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUpDown(hiddenPanel);
            }
        });


        notificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Config.getSKIP_MODE())
                    showPopupHome();
                else
                    slideUpDown(hiddenPanel);
            }
        });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.getSKIP_MODE())
                    showPopupHome();
                else
                {
                Intent intent = new Intent(getActivity(), AddProduct.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }
            }
        });

        optionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Config.getSKIP_MODE())
                    showPopupHome();
                else {
                    // fragmentContainer.setVisibility(View.VISIBLE);
                    OptionFragment optionFragment = new OptionFragment();
                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(R.id.add_shop_fragment_container, optionFragment);
                    ft.addToBackStack(optionFragment.getClass().getName());
                    ft.commit();
                }

            }
        });

        butlerPagerAdapter = new ButlerPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(butlerPagerAdapter);
        viewPager.setPageTransformer(true, new ParallaxPageTransformer());
        viewPager.setOffscreenPageLimit(ITEMS);
        viewPager.setCurrentItem(2);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.

            @Override
            public void onPageSelected(int position) {

                lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent));
                if (position == 0) {
                    optionBtn.setVisibility(View.GONE);
                    backBtn.setVisibility(View.GONE);
                    headerText.setVisibility(View.VISIBLE);
                    searchbarLayout.setVisibility(View.INVISIBLE);
                    cameraNotificationLayout.setVisibility(View.VISIBLE);
                    headerText.setText(getResources().getString(R.string.bazaarLabel));
                    fragmentContainer.setVisibility(View.GONE);
                    topBar.setVisibility(View.VISIBLE);
                    lastSelectedButton = btnBazar;
                }
                if (position == 1) {
                    headerText.setVisibility(View.GONE);
                    backBtn.setVisibility(View.GONE);
                    optionBtn.setVisibility(View.GONE);
                    searchbarLayout.setVisibility(View.VISIBLE);
                    cameraNotificationLayout.setVisibility(View.INVISIBLE);
                    headerText.setText(getResources().getString(R.string.searchLabel));
                    fragmentContainer.setVisibility(View.GONE);
                    topBar.setVisibility(View.VISIBLE);
                    lastSelectedButton = btnSrch;

                }
                if (position == 2) {
                    optionBtn.setVisibility(View.GONE);
                    backBtn.setVisibility(View.GONE);
                    headerText.setVisibility(View.VISIBLE);
                    searchbarLayout.setVisibility(View.INVISIBLE);
                    cameraNotificationLayout.setVisibility(View.VISIBLE);
                    headerText.setText(getResources().getString(R.string.feedLabel));
                    fragmentContainer.setVisibility(View.GONE);
                    topBar.setVisibility(View.VISIBLE);
                    lastSelectedButton = btnFeed;

                }
                if (position == 3) {
                    optionBtn.setVisibility(View.GONE);
                    backBtn.setVisibility(View.GONE);
                    headerText.setVisibility(View.VISIBLE);
                    searchbarLayout.setVisibility(View.INVISIBLE);
                    cameraNotificationLayout.setVisibility(View.VISIBLE);
                    headerText.setText(getResources().getString(R.string.offerLabel));
                    fragmentContainer.setVisibility(View.GONE);
                    topBar.setVisibility(View.VISIBLE);
                    lastSelectedButton = btnOfr;

                }
                if (position == 4) {

                    backBtn.setVisibility(View.GONE);
                    optionBtn.setVisibility(View.VISIBLE);
                    headerText.setVisibility(View.VISIBLE);
                    searchbarLayout.setVisibility(View.INVISIBLE);
                    cameraNotificationLayout.setVisibility(View.VISIBLE);
                    headerText.setText(getResources().getString(R.string.profileLabel));
                    fragmentContainer.setVisibility(View.GONE);
                    topBar.setVisibility(View.VISIBLE);
                    lastSelectedButton = btnProf;

                    Fragment fragment= getActivity().getSupportFragmentManager().findFragmentById(R.id.pager);
                    if(fragment!=null) {
                        if ((fragment.getClass().getName()).contentEquals(ProfileFragmentTab.class.getName()))
                            ((ProfileFragmentTab) fragment).mainLayout.setVisibility(View.VISIBLE);
                        ((ProfileFragmentTab) fragment).viewPagerProducts.setVisibility(View.GONE);
                    }

                }

                showPopupHome();

                lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });

        return view;


    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void hideBottombarScroll() {
        Animation slide_down = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down_bar);
        bottomBar.setAnimation(slide_down);
        bottomBar.setVisibility(View.GONE);
    }

    public void showBottombarScroll() {
        Animation slide_up = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up_bar);
        bottomBar.setAnimation(slide_up);
        bottomBar.setVisibility(View.VISIBLE);
    }


    public void hideBuySell(View view) {
        Animation slide_down = AnimationUtils.loadAnimation(getActivity(),
                R.anim.hide_buy_sell);
        view.setAnimation(slide_down);
        view.setVisibility(View.GONE);
    }

    public void showBuySell(View view) {
        Animation slide_up = AnimationUtils.loadAnimation(getActivity(),
                R.anim.show_buy_sell);
        view.setAnimation(slide_up);
        view.setVisibility(View.VISIBLE);

    }

    public void onPromotedProductsClick() {

        backBtn.setVisibility(View.VISIBLE);
        optionBtn.setVisibility(View.GONE);
        headerText.setVisibility(View.VISIBLE);
        searchbarLayout.setVisibility(View.INVISIBLE);
        cameraNotificationLayout.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getString(R.string.my_promoted_products));
    }


    /*  sets header bar on pressing My products button */
    public void onProductsClick() {

        backBtn.setVisibility(View.VISIBLE);
        optionBtn.setVisibility(View.VISIBLE);
        headerText.setVisibility(View.VISIBLE);
        searchbarLayout.setVisibility(View.INVISIBLE);
        cameraNotificationLayout.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getString(R.string.productsbtnLabel));
    }

    //sets header bar on pressing options button on top bar
    public void onOptionsClick() {

        backBtn.setVisibility(View.VISIBLE);
        optionBtn.setVisibility(View.GONE);
        headerText.setVisibility(View.VISIBLE);
        searchbarLayout.setVisibility(View.INVISIBLE);
        cameraNotificationLayout.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getString(R.string.options));
    }

    //sets header bar on pressing listitem in profile
    public void onShopClick() {

        backBtn.setVisibility(View.VISIBLE);
        optionBtn.setVisibility(View.GONE);
        headerText.setVisibility(View.VISIBLE);
        searchbarLayout.setVisibility(View.INVISIBLE);
        cameraNotificationLayout.setVisibility(View.VISIBLE);
        //  headerText.setText(getResources().getString(R.string.options));
    }

    public void showPopupHome()
    {
        if(Config.getSKIP_MODE())
            showSkipPopup(getActivity());
    }

    /* sets Profile  header bar on pressing back button on products Fragment */
    public void backPressed() {

        backBtn.setVisibility(View.GONE);
        optionBtn.setVisibility(View.VISIBLE);
        headerText.setVisibility(View.VISIBLE);
        searchbarLayout.setVisibility(View.INVISIBLE);
        cameraNotificationLayout.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getString(R.string.profileLabel));

    }

    private void initViews() {
        // TODO Auto-generated method stub

        btnBazar = (LinearLayout) view.findViewById(R.id.btnBazar);
        btnSrch = (LinearLayout) view.findViewById(R.id.btnSrch);
        btnFeed = (LinearLayout) view.findViewById(R.id.btnFeed);
        btnProf = (LinearLayout) view.findViewById(R.id.btnProf);
        btnOfr = (LinearLayout) view.findViewById(R.id.btnOfr);
        btnFeed.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
        btnBazar.setOnClickListener(this);
        btnSrch.setOnClickListener(this);
        btnFeed.setOnClickListener(this);
        btnProf.setOnClickListener(this);
        btnOfr.setOnClickListener(this);
        btnOfr.setOnClickListener(this);

        lastSelectedButton = btnFeed;

    }

    @Override
    public void onClick(View v) {
//        // TODO Auto-generated method stub
        lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.transparent));
        switch (v.getId()) {

            case R.id.btnBazar:
                fragmentContainer.setVisibility(View.GONE);
                optionBtn.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                headerText.setVisibility(View.VISIBLE);
                searchbarLayout.setVisibility(View.INVISIBLE);
                cameraNotificationLayout.setVisibility(View.VISIBLE);
                topBar.setVisibility(View.VISIBLE);
                headerText.setText(getResources().getString(R.string.bazaarLabel));
                viewPager.setCurrentItem(0);
                lastSelectedButton = btnBazar;
                break;

            case R.id.btnSrch:
                fragmentContainer.setVisibility(View.GONE);
                headerText.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                optionBtn.setVisibility(View.GONE);
                searchbarLayout.setVisibility(View.VISIBLE);
                cameraNotificationLayout.setVisibility(View.INVISIBLE);
                topBar.setVisibility(View.VISIBLE);
                // headerText.setText(getResources().getString(R.string.searchLabel));
                viewPager.setCurrentItem(1);
                lastSelectedButton = btnSrch;
                break;

            case R.id.btnFeed:
                fragmentContainer.setVisibility(View.GONE);
                optionBtn.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                headerText.setVisibility(View.VISIBLE);
                searchbarLayout.setVisibility(View.INVISIBLE);
                cameraNotificationLayout.setVisibility(View.VISIBLE);
                topBar.setVisibility(View.VISIBLE);
                headerText.setText(getResources().getString(R.string.feedLabel));
                viewPager.setCurrentItem(2);
                lastSelectedButton = btnFeed;
                break;

            case R.id.btnOfr:
                fragmentContainer.setVisibility(View.GONE);
                optionBtn.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                headerText.setVisibility(View.VISIBLE);
                searchbarLayout.setVisibility(View.INVISIBLE);
                cameraNotificationLayout.setVisibility(View.VISIBLE);
                topBar.setVisibility(View.VISIBLE);
                headerText.setText(getResources().getString(R.string.offerLabel));
                viewPager.setCurrentItem(3);
                lastSelectedButton = btnOfr;
                break;

            case R.id.btnProf:
                fragmentContainer.setVisibility(View.GONE);
                backBtn.setVisibility(View.GONE);
                optionBtn.setVisibility(View.VISIBLE);
                headerText.setVisibility(View.VISIBLE);
                searchbarLayout.setVisibility(View.INVISIBLE);
                cameraNotificationLayout.setVisibility(View.VISIBLE);
                topBar.setVisibility(View.VISIBLE);
                headerText.setText(getResources().getString(R.string.profileLabel));
                viewPager.setCurrentItem(4);
                lastSelectedButton = btnProf;

                Fragment fragment= getActivity().getSupportFragmentManager().findFragmentById(R.id.pager);
                if(fragment!=null) {
                    if ((fragment.getClass().getName()).contentEquals(ProfileFragmentTab.class.getName()))
                        ((ProfileFragmentTab) fragment).mainLayout.setVisibility(View.VISIBLE);
                    ((ProfileFragmentTab) fragment).viewPagerProducts.setVisibility(View.GONE);
                }


                break;

            default:
                break;
        }



        lastSelectedButton.setBackgroundColor(getResources().getColor(R.color.blacktransparent));
    }

    public static class ButlerPagerAdapter extends FragmentPagerAdapter {
        public ButlerPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new BazaarFragmentTab().init(position);
                case 1:

                    return new SearchFragmentTab().init(position);
                case 2:
                    return new FeedFragmentTab().init(position);
                case 3:
                    return new OffersFragmentTab().init(position);
                case 4:
                    return new ProfileFragmentTab().init(position);

                default:// Fragment # 2-9 - Will show list
                    return new FeedFragmentTab().init(position);
            }
        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater = getActivity().getMenuInflater();
//        inflater.inflate(R.menu.main_activity_actions, menu);
//        return super.onCreateOptionsMenu(menu, inflater);
//    }
//       @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
//        // Inflate the menu items for use in the action bar
//         inflater = getActivity().getMenuInflater();
//        inflater.inflate(R.menu.main_activity_actions, menu);
//        setHasOptionsMenu(true);
//        return super.onCreateOptionsMenu(menu,inflater);
//    }

    public void slideUpDown(final View view) {
        if (!isPanelShown()) {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.slide_down);

            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            shadowbehind.setVisibility(View.VISIBLE);
        } else {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.slide_up);

            hiddenPanel.startAnimation(bottomDown);
            hiddenPanel.setVisibility(View.GONE);
            shadowbehind.setVisibility(View.GONE);
        }
    }

    private boolean isPanelShown() {
        return hiddenPanel.getVisibility() == View.VISIBLE;
    }


    /**
     * Notification pager adapter
     */

    public class NotificationPagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{getResources().getString(R.string.all), getResources().getString(R.string.bazar)};

        public NotificationPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NOTITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return AllNotificationFrag.init(position);
                case 1:
                    return new BazarNotificationFrag().init(position);
                default:// Fragment # 2-9 - Will show list
                    return new AllNotificationFrag().init(position);

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }


    }

    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showSkipPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.not_logged_in);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            CustomTextViewLightFont cancel=(CustomTextViewLightFont)dialog.findViewById(R.id.cancel);
            CustomTextViewLightFont signinfb=(CustomTextViewLightFont)dialog.findViewById(R.id.signinfb);
            CustomTextViewLightFont signingmail=(CustomTextViewLightFont)dialog.findViewById(R.id.signingmail);
            CustomTextViewLightFont signintwitter=(CustomTextViewLightFont)dialog.findViewById(R.id.signintwitter);
            CustomTextViewSemiBold signin=(CustomTextViewSemiBold)dialog.findViewById(R.id.signin);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    dialog.dismiss();
                }
            });
            signinfb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), SplashActivity.class);
                    intent.putExtra("loginVia", "facebook");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    getActivity().finish();
                }
            });
            signingmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), SplashActivity.class);
                    intent.putExtra("loginVia", "email");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    getActivity().finish();
                }
            });
            signintwitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), SplashActivity.class);
                    intent.putExtra("loginVia", "twitter");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    getActivity().finish();
                }
            });
            signin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), SplashActivity.class);
                    intent.putExtra("loginVia", "login");
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    getActivity().finish();
                }
            });




            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }





    }


}
