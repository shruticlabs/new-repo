package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Helper;
import clabs.butlers.utils.InternalStorageContentProvider;
import models.ProductsModel;
import models.PromoteUserListModel;
import models.ShopModel;
import simplecropimage.CropImage;

/**
 * Created by clicklabs107 on 4/17/15.
 */
public class AddShopFragment extends Fragment {

    LinearLayout topBarLayout, addShopLayout, shopsAddedLayout, addProductsInShopLayout;
    RelativeLayout bottomGreenBar, nextBtn, backBtn;
    CustomTextViewSemiBold header, footer;
    ImageView shopImage;
    MaterialEditText aboutShopdescription, shopName;
    CustomTextViewRegularFont addshopImgeTxt, editProductBtn;
    CustomEditTextRegularFont searchBar;
    HomFragment parent;
    static int countNext = 0, countBack = 0;
    ProductsModel productsModel;
    ArrayList<ProductsModel> arraylist;
    ArrayList<ProductsModel> addProductListToShop;
    AddProductAdapter adapter;
    AddedProductsAdapter addedProductsAdapter;
    Button searchBtn;
    GridView productGridView, productsInShopGrid;
    public static final String TAG = "AddProduct";
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private File mFileTemp;
    int margin;

    static AddShopFragment init(int val) {
        AddShopFragment addShopFragment = new AddShopFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        addShopFragment.setArguments(args);

        return addShopFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        View rootView = inflater.inflate(R.layout.add_shop, container, false);
        topBarLayout = (LinearLayout) rootView.findViewById(R.id.topbar);
        addShopLayout = (LinearLayout) rootView.findViewById(R.id.addshop_layout);
        shopsAddedLayout = (LinearLayout) rootView.findViewById(R.id.shops_added);
        addProductsInShopLayout = (LinearLayout) rootView.findViewById(R.id.add_products_in_shop);
        bottomGreenBar = (RelativeLayout) rootView.findViewById(R.id.green_bar);

        header = (CustomTextViewSemiBold) rootView.findViewById(R.id.header);
        shopImage = (ImageView) rootView.findViewById(R.id.image_shop);
        shopName = (MaterialEditText) rootView.findViewById(R.id.shop_name);
        aboutShopdescription = (MaterialEditText) rootView.findViewById(R.id.about_shop_edit);

        footer = (CustomTextViewSemiBold) rootView.findViewById(R.id.footerLabel);
        nextBtn = (RelativeLayout) rootView.findViewById(R.id.NextBtnMain);
        backBtn = (RelativeLayout) rootView.findViewById(R.id.backBtnMain);
        addshopImgeTxt = (CustomTextViewRegularFont) rootView.findViewById(R.id.addshp_image_text);
        editProductBtn = (CustomTextViewRegularFont) rootView.findViewById(R.id.edit_products_in_shop_btn);
        searchBar = (CustomEditTextRegularFont) rootView.findViewById(R.id.searchBarProducts);
        searchBtn = (Button) rootView.findViewById(R.id.btn_search);


        productGridView = (GridView) rootView.findViewById(R.id.add_productList_grid);
        productsInShopGrid = (GridView) rootView.findViewById(R.id.grid_products_added_in_shop);

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
        };
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};


        arraylist = new ArrayList<ProductsModel>();
        addProductListToShop = new ArrayList<ProductsModel>();
        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i], order_names[i], order_ids[i], i, order_images[i], order_names[i], order_names[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], false);
            arraylist.add(productsModel);
        }
        margin = getResources().getDimensionPixelSize(R.dimen.spacinggrid);
        adapter = new AddProductAdapter(getActivity(), arraylist);
        productGridView.setAdapter(adapter);


        Helper.setGridViewHeightBasedOnChildren(productGridView, 3, margin);


        addedProductsAdapter = new AddedProductsAdapter(getActivity(), addProductListToShop);


        footer.setText(getResources().getString(R.string.addProducts));
        addshopImgeTxt.setVisibility(View.VISIBLE);
        addShop();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          /* back pressed Add shop

           */
                if (countBack == 0) {
                    parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                    parent.viewPager.setVisibility(View.VISIBLE);
                    parent.bottomBar.setVisibility(View.VISIBLE);
                    parent.topBar.setVisibility(View.VISIBLE);
                    parent.fragmentContainer.setVisibility(View.GONE);
                    getFragmentManager().popBackStack();

                } else if (countBack == 1) {
            /*back pressed Add products in shop

             */
                    addshopImgeTxt.setVisibility(View.GONE);
                    addShop();
                    countNext--;
                    countBack--;
                } else if (countBack == 2) {
             /* back pressed *Shop Name*

              */
                    parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                    parent.viewPager.setVisibility(View.VISIBLE);
                    parent.bottomBar.setVisibility(View.VISIBLE);
                    parent.topBar.setVisibility(View.VISIBLE);
                    parent.fragmentContainer.setVisibility(View.GONE);
                    shopsAddedLayout.setVisibility(View.GONE);
                    getFragmentManager().popBackStack();
                    countNext = 0;
                    countBack = 0;

                }


            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        /*
        next pressed in Add Shop
         */
                if (countNext == 0) {
                    viewAddProductsInShop();
                    countBack++;
                    countNext++;

                } else if (countNext == 1) {
        /*
           next pressed in Add products in shop
        */
                    addShop();
                    addshopImgeTxt.setVisibility(View.GONE);

                    shopsAddedLayout.setVisibility(View.VISIBLE);
                    footer.setText("NEXT");

                    if (addProductListToShop.size() != 0) {
                        productsInShopGrid.setAdapter(addedProductsAdapter);
                        Helper.setGridViewHeightBasedOnChildren(productsInShopGrid, 3, margin);


                    }
                    countBack++;
                    countNext++;

                } else if (countNext == 2) {

       /*
           next pressed in "ShopName"
        */
                    if (validation()) {
                        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                        parent.viewPager.setVisibility(View.VISIBLE);
                        parent.bottomBar.setVisibility(View.VISIBLE);
                        parent.topBar.setVisibility(View.VISIBLE);
                        parent.fragmentContainer.setVisibility(View.GONE);
                        shopsAddedLayout.setVisibility(View.GONE);
                        addToMyShop();
                        getFragmentManager().popBackStack();
                        countNext = 0;
                        countBack = 0;

                    }

                }

            }

        });


        editProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewAddProductsInShop();
                shopsAddedLayout.setVisibility(View.GONE);
                countBack--;
                countNext--;

            }
        });

        shopImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChooserPopup();
            }
        });


        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getActivity().getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                String text = searchBar.getText().toString().toLowerCase();
                adapter.filter(text);


            }
        });


        return rootView;
    }

    /*
    adds shop and its products to my shops in profile screen
     */
    public void addToMyShop() {

        ShopModel model = new ShopModel("http://api.androidhive.info/json/movies/4.jpg", shopName.getText().toString(), String.valueOf(addedProductsAdapter.getCount()), "", shopName.getText().toString());
        ProfileFragmentTab fragmentTab = (ProfileFragmentTab) getFragmentManager().findFragmentById(R.id.pager);
        fragmentTab.shopArraylist.add(model);
        fragmentTab.shopRecyclerAdapter.notifyDataSetChanged();
        Helper.setRecyclerGridViewHeightBasedOnChildren(fragmentTab.recyclerViewGrid,3,fragmentTab.margin);

    }

    /*
    validation for adding shop
     */
    public boolean validation() {

        if ((shopName.getText().toString().isEmpty())) {
            Toast.makeText(getActivity(), "Shop name can't be left empty", Toast.LENGTH_SHORT).show();
            shopName.requestFocus();
        } else if (aboutShopdescription.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "About shop can't be left empty", Toast.LENGTH_SHORT).show();
            aboutShopdescription.requestFocus();
        } else if (shopImage.getDrawable() == null) {
            Toast.makeText(getActivity(), "Shop Image can't be left empty", Toast.LENGTH_SHORT).show();
            shopImage.requestFocus();
        } else
            return true;

        return false;
    }

    /*
    handles back pressed on device
     */

    public void pressBack() {

        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.viewPager.setVisibility(View.VISIBLE);
        parent.bottomBar.setVisibility(View.VISIBLE);
        parent.topBar.setVisibility(View.VISIBLE);
        parent.fragmentContainer.setVisibility(View.GONE);

    }


    private void showChooserPopup() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle("Choose shop image from");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                openGallery();

            }
        });


        adb.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                takePicture();


            }
        });
        adb.show();
    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                /*
	        	 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
            // overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        } catch (ActivityNotFoundException e) {

            Log.d(TAG, "cannot take picture", e);
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void startCropImage() {

        Intent intent = new Intent(getActivity(), CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
        //  overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != getActivity().RESULT_OK) {

            return;
        }

        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:


                try {

                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.e(TAG, "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                shopImage.setImageBitmap(bitmap);
                addshopImgeTxt.setVisibility(View.GONE);

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }


    // views added to fragment when next is pressed in Add Shop
    private void viewAddProductsInShop() {

        header.setText("ADD PRODUCT IN SHOP");
        footer.setText("NEXT");
        addShopLayout.setVisibility(View.GONE);
        addProductsInShopLayout.setVisibility(View.VISIBLE);

    }


    // layout shown when fragment created
    private void addShop() {

        header.setText(getResources().getString(R.string.addshop));
        shopImage.getBackground().setAlpha(50);
        addShopLayout.setVisibility(View.VISIBLE);
        addProductsInShopLayout.setVisibility(View.GONE);

    }

/*
adapter for choosing products to be added in shop
 */
    public class AddProductAdapter extends BaseAdapter {

        private List<ProductsModel> all_products = null;
        private ArrayList<ProductsModel> products;


        Activity activity;

        LayoutInflater inflater = null;

        public AddProductAdapter(Activity a, List<ProductsModel> all_products) {

            this.all_products = all_products;

            this.products = new ArrayList<ProductsModel>();
            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public ProductsModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolderProducts holder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_add_product_inshop, null);
                holder = new ViewHolderProducts();


                holder.productImage = (ImageView) convertView
                        .findViewById(R.id.img_product_list_item);
                holder.productName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.product_name);

                holder.checkBtn = (ImageView) convertView
                        .findViewById(R.id.checkbox_btn);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderProducts) convertView.getTag();
            }


            holder.productName.setText(all_products.get(position).getUserName());
            holder.productImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (all_products.get(position).getIsShownLayout() == false) {
                        holder.checkBtn.setVisibility(View.VISIBLE);
                        all_products.get(position).setIsShownLayout(true);
                        productsModel = getItem(position);
                        //   productsModel=new ProductsModel(all_products.get(position).getUserProfileImage(), all_products.get(position).getUserName(), all_products.get(position).getUserRaings(), position, all_products.get(position).getProductAddedTime(), all_products.get(position).getAddressField(), all_products.get(position).getProductImage(), all_products.get(position).getProductModel(), all_products.get(position).getUserWhoRated(), all_products.get(position).getUserWhoRatedRatings(), all_products.get(position).getOtherUserWhoRatedCount(), all_products.get(position).getLikesCount(), all_products.get(position).getCommentCount(), all_products.get(position).getProductPrice(), all_products.get(position).getAvailableCount(), all_products.get(position).getUserId(), all_products.get(position).getIsLiked(), all_products.get(position).getProductId(), true);
                        addProductListToShop.add(productsModel);
                        addedProductsAdapter.notifyDataSetChanged();

                    } else {
                        holder.checkBtn.setVisibility(View.GONE);
                        all_products.get(position).setIsShownLayout(false);
                        productsModel = getItem(position);
                        int index = addProductListToShop.indexOf(productsModel);
                        addProductListToShop.remove(index);
                        addedProductsAdapter.notifyDataSetChanged();

                    }
                }
            });


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.productImage)
                        .image("http://api.androidhive.info/json/movies/4.jpg", true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }

            return convertView;

        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            all_products.clear();
            if (charText.length() == 0) {
                all_products.addAll(products);
            } else {
                for (ProductsModel wp : products) {
                    if (wp.getUserName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_products.add(wp);
                        //           Log.i("inside filter", wp.getRowId() + "..");

                    }
                }
            }
            notifyDataSetChanged();
            if(getCount()!=0)
                Helper.setGridViewHeightBasedOnChildren(productGridView, 3, margin);
        }


    }

    class ViewHolderProducts {

        ImageView checkBtn;
        ImageView productImage;
        CustomTextViewRegularFont productName;
    }


    /*
    adapter for added products in shop
     */
    public class AddedProductsAdapter extends BaseAdapter {

        private List<ProductsModel> all_products = null;


        Activity activity;

        LayoutInflater inflater = null;


        public AddedProductsAdapter(Activity a, List<ProductsModel> all_products) {

            this.all_products = all_products;


            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public ProductsModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolderProductsAdded holder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_add_product_inshop, null);
                holder = new ViewHolderProductsAdded();


                holder.productImage = (ImageView) convertView
                        .findViewById(R.id.img_product_list_item);
                holder.productName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.product_name);


                convertView.setTag(holder);


            } else {
                holder = (ViewHolderProductsAdded) convertView.getTag();
            }


            holder.productName.setText(all_products.get(position).getUserName());


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.productImage)
                        .image("http://api.androidhive.info/json/movies/4.jpg", true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }

            return convertView;

        }
    }

    class ViewHolderProductsAdded {


        ImageView productImage;
        CustomTextViewRegularFont productName;
    }
}
