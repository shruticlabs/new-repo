package pager.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Data;
import material.animations.MaterialDesignAnimations;
import models.ShopModel;

/**
 * Created by click103 on 12/3/15.
 */
public class PromoteShopFrag extends Fragment {


    ShopModel shopModel;
    ArrayList<ShopModel> arraylist;
    PromoteShopAdapter adapter;
    GridView shopGridview;
    LinearLayout errorLayout;
    public static PromoteShopFrag init(int val) {
        PromoteShopFrag promoteShopFrag = new PromoteShopFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        promoteShopFrag.setArguments(args);

        return promoteShopFrag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.promote_shop_layout, container, false);

        shopGridview=(GridView)rootView.findViewById(R.id.shopGridview);
        errorLayout=(LinearLayout)rootView.findViewById(R.id.errorLayout);


        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

        } else {
           GetShopsApi();
        }


//        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
//        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//        };
//        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};
//
//        arraylist = new ArrayList<ShopModel>();
//
//        for (int i = 0; i < order_ids.length; i++) {
//
//            shopModel = new ShopModel("http://api.androidhive.info/json/movies/4.jpg", order_ids[i], order_ids[i] ,order_ids[i], order_ids[i]);
//            // Binds all strings into an array
//            arraylist.add(shopModel);
//        }
//        adapter = new PromoteShopAdapter(getActivity(), arraylist);
//        shopGridview.setAdapter(adapter);

        return rootView;
    }


    /**
     * shows various orders adapter
     *
     * @author showket
     */
    public class PromoteShopAdapter extends BaseAdapter {

        private List<ShopModel> all_products = null;
        private ArrayList<ShopModel> products;

        Activity activity;


        LayoutInflater inflater = null;
        boolean check[];

        ViewHolder holder;

        // Typeface face, face1;

        public PromoteShopAdapter(Activity a, List<ShopModel> all_products) {
            this.all_products = all_products;
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public ShopModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_myshops, null);
                holder = new ViewHolder();
                holder.layout = (LinearLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.shopImage = (ImageView) convertView
                        .findViewById(R.id.list_item_img_view);
                holder.shopType = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.shopType);

                holder.quantity = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.noofshops);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            holder.shopType.setText(all_products.get(position).getShopType());


            AQuery aq = new AQuery(convertView);

            try{

                aq.id(holder.shopImage)
                        .image("http://api.androidhive.info/json/movies/4.jpg", true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            }catch(Exception e){

            }


            holder.shopImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    Toast.makeText(getActivity(),"clicked",Toast.LENGTH_LONG).show();
                }

            });
            return convertView;

        }


    }

    class ViewHolder {

         LinearLayout layout;
        ImageView shopImage;
        CustomTextViewRegularFont shopType, quantity;


    }



    /**
     * Get Folowing List API
     */
    public void GetShopsApi() {
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("user_id", Config.getUSER_ID());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getShops", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {


                                    JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");

                                    for (int i = 0; i < userResults.length(); i++) {
                                        //shopModel = new ShopModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                        //arraylist.add(shopModel);

                                    }
                                adapter = new PromoteShopAdapter(getActivity(), arraylist);
                                shopGridview.setAdapter(adapter);




                            } else {
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
                                }



                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {

                            Data.loading_box_stop();
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));


                    }
                });

    }
}
