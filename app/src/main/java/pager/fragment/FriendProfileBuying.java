package pager.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.clicklabs.butlers.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import models.ProductsModel;

/**
 * Created by clicklabs107 on 5/11/15.
 */
public class FriendProfileBuying extends Fragment {
    CustomTextViewRegularFont listedBtn,promotedBtn;
    RecyclerView recyclerView;
    ArrayList<ProductsModel> arraylist;
    ProductsModel productsModel;
    Boolean otherUser;


    public static FriendProfileBuying init(int val, Boolean OtherUser) {
       FriendProfileBuying profileBuyingFrag = new FriendProfileBuying();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        args.putBoolean("otherUser", OtherUser);
        profileBuyingFrag.setArguments(args);

        return profileBuyingFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // position = getArguments() != null ? getArguments().getInt("val") : 1;
        otherUser = getArguments() != null ? getArguments().getBoolean("otherUser") : false;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.myproducts_buying, container, false);
        listedBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.listedBtn);
        promotedBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.promotedBtn);
        recyclerView =(RecyclerView) rootView.findViewById(R.id.recycler_view);


            listedBtn.setText("Listed");
            promotedBtn.setText("Promoted");


        arraylist = new ArrayList<ProductsModel>();

        String[] order_ids={"1","2","3","4","5","6","7","8","9"};
        String[] order_images={"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                ,"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names={"saif","showket","anil","saif","showket","anil","saif","showket","anil"};

        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i],order_names[i],order_ids[i],i,order_images[i],order_names[i],order_names[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i],false);
            // Binds all strings into an array
            arraylist.add(productsModel);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.setItemAnimator(new FadeInAnimator());


//        adapter=new ProductsAdapter(getActivity(),R.id.image, arraylist);
        RecyclerAdapter adapter=new RecyclerAdapter(getActivity(), arraylist);
        recyclerView.setAdapter(adapter);

        listedBtn.setBackgroundResource(R.drawable.buying_normal);
        listedBtn.setTextColor(getResources().getColor(R.color.white));
        promotedBtn.setTextColor(getResources().getColor(R.color.rowtext));

        listedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listedBtn.setBackgroundResource(R.drawable.buying_normal);
                promotedBtn.setTextColor(getResources().getColor(R.color.rowtext));
                listedBtn.setTextColor(getResources().getColor(R.color.white));
                promotedBtn.setBackgroundResource(0);
            }
        });
        promotedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promotedBtn.setBackgroundResource(R.drawable.buying_normal);
                listedBtn.setTextColor(getResources().getColor(R.color.rowtext));
                promotedBtn.setTextColor(getResources().getColor(R.color.white));
                listedBtn.setBackgroundResource(0);
            }
        });



        return rootView;
    }



    public class RecyclerAdapter extends RecyclerView.Adapter<ProductsListRowHolder>{

        private List<ProductsModel> productItemList;
        private Context mContext;

        public RecyclerAdapter(Context context, List<ProductsModel> productItemList) {
            this.productItemList = productItemList;
            this.mContext = context;


            // Set up the animation

        }


        @Override
        public ProductsListRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_buying_mylisted, null);
            ProductsListRowHolder holder = new ProductsListRowHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(ProductsListRowHolder productListRowHolder, int position) {
            final ProductsModel productItem = productItemList.get(position);
            Picasso.with(mContext).load(productItem.getProductImage()).into(productListRowHolder.productImage);


        }

        @Override
        public int getItemCount() {
            return (null != productItemList ? productItemList.size() : 0);
        }
    }





    class ProductsListRowHolder extends RecyclerView.ViewHolder {
        ProgressBar productProgress;
        CustomTextViewLightFont likesCount,commentCount,description;
        ImageView productImage;
        CustomTextViewRegularFont  productLabel,productModel,
                productPrice,availableCount;
        RelativeLayout likeBtn,comentBtn;




        public ProductsListRowHolder(View convertView) {
            super(convertView);
            this.productImage = (ImageView) convertView.findViewById(R.id.cardImage);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.description=(CustomTextViewLightFont)convertView.findViewById(R.id.descriptionText);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.availableCount=(CustomTextViewRegularFont)convertView.findViewById(R.id.noOfItems);
            this.productPrice=(CustomTextViewRegularFont)convertView.findViewById(R.id.priceProduct);

        }

    }

}


