package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Helper;
import models.PromoteUserListModel;


public class FindInviteFragment extends Fragment implements ObservableScrollViewCallbacks {

    HomFragment parent;
    FragmentManager fm;
    ListView followersListView;
    ArrayList<PromoteUserListModel> followerArrayList;
    PromoteUserListModel followersModel;
    FollowersAdapter followersAdapter;
    RelativeLayout backBtn;
    CustomTextViewSemiBold header;
    CustomEditTextRegularFont searchBar;
    ImageView checkBox;
    ObservableScrollView scrollView;

    static FindInviteFragment init(int val) {
        FindInviteFragment findInviteFragment = new FindInviteFragment();
        Bundle args = new Bundle();
        args.putInt("val", val);
        findInviteFragment.setArguments(args);
        return findInviteFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.find_invite_friends, container, false);
        backBtn = (RelativeLayout) rootView.findViewById(R.id.backBtnMain);
        header = (CustomTextViewSemiBold) rootView.findViewById(R.id.header);
        searchBar = (CustomEditTextRegularFont) rootView.findViewById(R.id.search_friends_invite);
        checkBox = (ImageView) rootView.findViewById(R.id.checkbox_follow);
        scrollView=(ObservableScrollView)rootView.findViewById(R.id.find_invite_scrollView);
        scrollView.setScrollViewCallbacks(this);
        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.topBar.setVisibility(View.GONE);
        header.setText(getResources().getString(R.string.findInvite));
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fm.popBackStackImmediate()) {
                    parent.topBar.setVisibility(View.VISIBLE);
                    fm.getBackStackEntryCount();
                }
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
//                if (checkBox.getBackground() == getResources().getDrawable(R.drawable.btn_chkbox_normal)) {
//                    checkBox.setBackground(getResources().getDrawable(R.drawable.btn_chkbox_pressed));
//
//                } else {
//                    checkBox.setBackground((getResources().getDrawable(R.drawable.btn_chkbox_normal)));
//                }
            }
        });

        followersListView = (ListView) rootView.findViewById(R.id.friends_to_follow_list);


        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                , "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

        followerArrayList = new ArrayList<>();

        for (int i = 0; i < order_ids.length; i++) {

            followersModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
            // Binds all strings into an array
            followerArrayList.add(followersModel);
        }
        followersAdapter = new FollowersAdapter((getActivity()), followerArrayList);


        followersListView.setAdapter(followersAdapter);
        Helper.getListViewSize(followersListView);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = searchBar.getText().toString().toLowerCase();
                followersAdapter.filter(text);

            }
        });

        return rootView;
    }


    public void pressBack() {
        parent.topBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (scrollState == ScrollState.UP) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (parent.bottomBar.isShown())
                parent.hideBottombarScroll();
        } else if (scrollState == ScrollState.DOWN) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (!parent.bottomBar.isShown())
                parent.showBottombarScroll();
        }
    }


    public class FollowersAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_followers = null;
        ArrayList<PromoteUserListModel> products;

        Activity activity;

        LayoutInflater inflater = null;


        ViewHolderFollowers holder;



        public FollowersAdapter(Activity a, List<PromoteUserListModel> all_comments) {

            this.all_followers = all_comments;

            this.products = new ArrayList<>();
            this.products.addAll(all_followers);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_followers.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_followers.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolderFollowers();
                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.followBtn = (CustomTextViewRegularFont) convertView.findViewById(R.id.selectBtn);
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderFollowers) convertView.getTag();

            }

            if (all_followers.get(position).getIsSelected() == false) {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.followBtn.setText("Follow");
                holder.followBtn.setTextColor(getResources().getColor(R.color.statusBar));
            } else {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selected);
                holder.followBtn.setText("Following");
                holder.followBtn.setTextColor(getResources().getColor(R.color.white));


            }
            holder.nameField.setText(all_followers.get(position).getNamefield());

            holder.followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (all_followers.get(position).getIsSelected() == true) {
                        all_followers.get(position).setIsSelected(false);
                    } else {
                        all_followers.get(position).setIsSelected(true);
                    }

                    notifyDataSetChanged();
                }

            });


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_followers.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }
            return convertView;

        }


        public void filter(String charText) {
            charText = charText.toLowerCase();
            all_followers.clear();
            if (charText.length() == 0) {
                all_followers.addAll(products);
            } else {
                for (PromoteUserListModel wp : products) {
                    if (wp.getNamefield().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_followers.add(wp);

                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    class ViewHolderFollowers {
        ImageView img;
        CustomTextViewRegularFont nameField, followBtn;
        TextView header;
    }
}
