package pager.fragment;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewMeadiumFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.ParallaxPageTransformer;
import clicklabs.app.butlers.BuyingProducts;
import models.CommentModel;
import models.ProductsModel;
import models.PromoteUserListModel;

/**
 * Created by click103 on 12/3/15.
 */
public class FeedFragmentTab extends Fragment {



    int ITEMS = 2;
    FeedPagerAdapter feedPagerAdapter;
    ViewPager viewPager;
    ImageView noProductFound;
    LinearLayout ListLayout;

    public static FeedFragmentTab init(int val) {
        FeedFragmentTab feedFragmentTab = new FeedFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        feedFragmentTab.setArguments(args);
        return feedFragmentTab;
    }
    public static List<ProductsModel> arraylistnew = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.feed_layout, container, false);
        feedPagerAdapter = new FeedPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager = (ViewPager)rootView.findViewById(R.id.feedPagerView);
        noProductFound = (ImageView)rootView.findViewById(R.id.noProductFound);
        ListLayout = (LinearLayout)rootView.findViewById(R.id.ListLayout);
        if(Config.getSKIP_MODE())
        {
            noProductFound.setVisibility(View.VISIBLE);
            ListLayout.setVisibility(View.GONE);
        }
        else {
            noProductFound.setVisibility(View.GONE);
            ListLayout.setVisibility(View.VISIBLE);
            viewPager.setAdapter(feedPagerAdapter);
            viewPager.setPageTransformer(true, new ParallaxPageTransformer());
            viewPager.setOffscreenPageLimit(ITEMS);
            PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
            tabsStrip.setViewPager(viewPager);
        }
        return rootView;
    }

    /**
     * Promote people/shop pager adapter
     */

    public class FeedPagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[] { getResources().getString(R.string.contact), getResources().getString(R.string.promote) };
        public FeedPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return FeedContactsFrag.init(position);
                case 1:
                    return new FeedPromoteFrag().init(position);
                default:// Fragment # 2-9 - Will show list
                    return new FeedContactsFrag().init(position);

            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }


    }




}