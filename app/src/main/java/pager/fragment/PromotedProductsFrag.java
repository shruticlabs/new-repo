package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewMeadiumFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clabs.butlers.utils.Helper;
import clabs.butlers.utils.StreamingMediaPlayer;
import clicklabs.app.butlers.BuyingProducts;
import models.CommentModel;
import models.ProductsModel;
import models.PromoteUserListModel;

/**
 * Created by clicklabs107 on 4/29/15.
 */
public class PromotedProductsFrag extends Fragment {


    FragmentManager fm;
    HomFragment parent;
    ProductsModel productsModel;
    ArrayList<ProductsModel> arraylist;
    CommentModel commentModel;
    ArrayList<CommentModel> commentModelArrayList;
    CommentAdapter commentAdapter;
    LikesAdapter LikesAdapter;
    PromoteUserListModel likeModel;
    ArrayList<PromoteUserListModel> likeModelArrayList;
    CustomEditTextRegularFont searchBar;

    static PromotedProductsFrag init(int val) {
        PromotedProductsFrag promotedProductsFrag = new PromotedProductsFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        promotedProductsFrag.setArguments(args);

        return promotedProductsFrag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.promoted_products_layout, container, false);
        searchBar = (CustomEditTextRegularFont) rootView.findViewById(R.id.search_friends_product);

        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.onPromotedProductsClick();
        parent.topBar.setVisibility(View.VISIBLE);

        arraylist = new ArrayList<ProductsModel>();

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                , "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i], order_names[i], order_ids[i], i, order_images[i], order_names[i], order_names[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], false);
            // Binds all strings into an array
            arraylist.add(productsModel);
        }
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycleView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        final FragmentManager fm = getFragmentManager();
        recyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...

                Log.v("Load More", "Load More" + current_page);
            }

            @Override
            public void onHide() {
                HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
                //parent.hideBuySell(buyselltabs);
            }

            @Override
            public void onShow() {
                HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
                //  parent.showBuySell(buyselltabs);
            }
        });


        final SwipeRefreshLayout swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        swipeView.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        double f = Math.random();
//                        rndNum.setText(String.valueOf(f));
                    }
                }, 3000);
            }
        });

        final MyRecyclerAdapter adapter = new MyRecyclerAdapter(getActivity(), arraylist);
        recyclerView.setAdapter(adapter);

        parent.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fm.popBackStackImmediate()) {
                    if (fm.getBackStackEntryCount() == 1)
                        parent.onOptionsClick();
                    else {
                        parent.backPressed();
                        fm.getBackStackEntryCount();
                        parent.viewPager.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String text = searchBar.getText().toString().toLowerCase();
                adapter.filter(text);

            }
        });


        return rootView;
    }

    /*
handles device back pressed
 */
    public void pressBack() {

        parent.onOptionsClick();

    }
/*
recyclerview adapter
 */

    public class MyRecyclerAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

        private List<ProductsModel> feedItemList;
        private Context mContext;
        private ArrayList<ProductsModel> products;
        int count=0;

//        ArcTranslateAnimation anim  = new ArcTranslateAnimation(0, -820 , 0, 100);;


        public MyRecyclerAdapter(Context context, List<ProductsModel> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
            this.products = new ArrayList<ProductsModel>();
            this.products.addAll(feedItemList);


            // Set up the animation

        }

        @Override
        public FeedListRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_promoted_products, null);
            FeedListRowHolder mh = new FeedListRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, final int position) {
            final ProductsModel feedItem = feedItemList.get(position);
            Picasso.with(mContext).load(feedItem.getProductImage()).into(feedListRowHolder.productImage);
            Picasso.with(mContext).load(feedItem.getUserProfileImage()).into(feedListRowHolder.profileImage);
            //            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));
//            feedListRowHolder.userRaings.setText(Html.fromHtml(feedItem.getUserRaings()));
//            feedListRowHolder.productAddedTime.setText(Html.fromHtml(feedItem.getProductAddedTime()));
//            feedListRowHolder.productModel.setText(Html.fromHtml(feedItem.getProductModel()));


            if (feedItem.getIsShownLayout() == false) {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
            } else {
                feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
            }

            feedListRowHolder.revealBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                        feedItem.setIsShownLayout(true);
                        feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                        feedListRowHolder.textBtnLayoutReveal.setVisibility(View.GONE);
                        feedListRowHolder.revealBtn.setVisibility(View.GONE);
                        feedListRowHolder.smallerRevealBtn.setVisibility(View.VISIBLE);
                        feedListRowHolder.textBtn.setVisibility(View.GONE);

                       feedListRowHolder.smallerRevealBtn.setBackgroundResource(R.drawable.btn_text_note_sel);
                    notifyDataSetChanged();

                }
            });

            feedListRowHolder.smallerRevealBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {


                    if (feedListRowHolder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                        if(count==0) {
                            feedItem.setIsShownLayout(false);
                            feedListRowHolder.smallerRevealBtn.setBackgroundResource(R.drawable.butler_selecter_btn);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                            feedListRowHolder.textBtnLayoutReveal.setVisibility(View.VISIBLE);


                            count++;
                        }

                      else  if(count==1){
                            feedItem.setIsShownLayout(false);
                            feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                            feedListRowHolder.textBtn.setVisibility(View.VISIBLE);
                            feedListRowHolder.smallerRevealBtn.setVisibility(View.GONE);
                            feedListRowHolder.revealBtn.setVisibility(View.VISIBLE);
                            count=0;

                        }
                    } else {

                        if (feedListRowHolder.textBtnLayoutReveal.getVisibility() == View.VISIBLE) {
                            if(count==1) {
                                feedItem.setIsShownLayout(false);
                                feedListRowHolder.textBtnLayoutReveal.setVisibility(View.GONE);
                                feedListRowHolder.textBtn.setVisibility(View.VISIBLE);
                                feedListRowHolder.smallerRevealBtn.setVisibility(View.GONE);
                                feedListRowHolder.revealBtn.setVisibility(View.VISIBLE);
                                count=0;
                            }
                           else if(count==0){
                                feedItem.setIsShownLayout(true);
                                feedListRowHolder.smallerRevealBtn.setBackgroundResource(R.drawable.btn_text_note_sel);
                               feedListRowHolder.textBtnLayoutReveal.setVisibility(View.GONE);
                                feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);


                                count++;
                            }
                        }
                    }
                }
            });

            feedListRowHolder.textBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {

                    feedItem.setIsShownLayout(false);
                    feedListRowHolder.smallerRevealBtn.setBackgroundResource(R.drawable.butler_selecter_btn);
                    feedListRowHolder.textBtnLayoutReveal.setVisibility(View.VISIBLE);
                    feedListRowHolder.buttonLayoutReveal.setVisibility(View.GONE);
                    feedListRowHolder.revealBtn.setVisibility(View.GONE);
                    feedListRowHolder.smallerRevealBtn.setVisibility(View.VISIBLE);
                    feedListRowHolder.textBtn.setVisibility(View.GONE);

                    notifyDataSetChanged();

                }
            });

            feedListRowHolder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    showCommentPopup(getActivity());


                }
            });
            feedListRowHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showLikePopup(getActivity());

                }
            });

            feedListRowHolder.buyBtn.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if (Config.isLollyPop()) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(getActivity(), BuyingProducts.class);
                        getActivity().startActivity(intent, options.toBundle());
                    } else {
                        getActivity().startActivity(new Intent(getActivity(), BuyingProducts.class));
                    }
                }
            });

            feedListRowHolder.nameBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                  FriendProfileFrag friendProfileFrag = new FriendProfileFrag();
                     friendProfileFrag.init(position);
                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(parent.fragmentContainer.getId(), friendProfileFrag);
                    ft.addToBackStack(friendProfileFrag.getClass().getName());
                    ft.commit();
                }
            });

            feedListRowHolder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditTextNotePopUp(getActivity());
                }
            });

            feedListRowHolder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deletePromotionNotePoup(getActivity());
                }
            });

            feedListRowHolder.delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deletePromotionNotePoup(getActivity());
                }
            });

        }

        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }

        public void filter(String charText) {
            charText = charText.toLowerCase();
            feedItemList.clear();
            if (charText.length() == 0) {
                feedItemList.addAll(products);
            } else {
                for (ProductsModel wp : products) {
                    if (wp.getUserName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        feedItemList.add(wp);
                        //           Log.i("inside filter", wp.getRowId() + "..");
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showEditTextNotePopUp(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.edit_textnote_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            Button backBtn = (Button) dialog.findViewById(R.id.backBtn);
            CustomTextViewSemiBold header = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);
            Button doneBtn=(Button)dialog.findViewById(R.id.doneBtn);
            header.setText(getResources().getString(R.string.edit_text_note));


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });

            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    class FeedListRowHolder extends RecyclerView.ViewHolder {
        ProgressBar profileProgress, productProgress;
        CustomTextViewLightFont likesCount, commentCount;
        ImageView profileImage, productImage;
        CustomTextViewRegularFont userName, userRaings, productAddedTime, addressField, productLabel, productModel,
                commentBtnLabel;
        RelativeLayout rel, likeBtn, comentBtn;
        LinearLayout buttonLayoutReveal, textBtnLayoutReveal, topPart, howManyLikeLayout,nameBar;
        Button buyBtn, descBtn, deleteBtn, revealBtn, viewBtn, editBtn, delBtn, textBtn,smallerRevealBtn;
        RelativeLayout rl;

        int p;

        public FeedListRowHolder(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView
                    .findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView
                    .findViewById(R.id.cardImage);
            this.revealBtn = (Button) convertView
                    .findViewById(R.id.revealBtn);
            this.smallerRevealBtn = (Button) convertView
                    .findViewById(R.id.smallRevealBtn);
            textBtn = (Button) convertView.findViewById(R.id.txtButton);
            this.buyBtn = (Button) convertView
                    .findViewById(R.id.buyIcon);
            this.descBtn = (Button) convertView
                    .findViewById(R.id.descIcon);
            this.deleteBtn = (Button) convertView
                    .findViewById(R.id.deleteIcon);

            this.viewBtn = (Button) convertView
                    .findViewById(R.id.viewIcon);
            this.editBtn = (Button) convertView
                    .findViewById(R.id.editIcon);
            delBtn = (Button) convertView.findViewById(R.id.del_Icon);

            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);

            this.buttonLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.buttonLayoutReveal);
            this.textBtnLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.textbuttonLayoutReveal);

            this.topPart = (LinearLayout) convertView
                    .findViewById(R.id.topPart);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
            this.productAddedTime = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.timeValue);
            addressField = (CustomTextViewRegularFont) convertView.findViewById(R.id.addressValue);
            this.rl = (RelativeLayout) convertView.findViewById(R.id.rv);
            this.nameBar=(LinearLayout) convertView.findViewById(R.id.nameBar);
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showCommentPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.comment_product);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final EditText editView = (EditText) dialog.findViewById(R.id.commentText);
            final ListView commentsListView = (ListView) dialog.findViewById(R.id.comemtsListView);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backBtn);

            LinearLayout sendBtn = (LinearLayout) dialog.findViewById(R.id.sendBtn);


            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    , "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

            commentModelArrayList = new ArrayList<CommentModel>();

            for (int i = 0; i < order_ids.length; i++) {

                commentModel = new CommentModel(order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i]);
                // Binds all strings into an array
                commentModelArrayList.add(commentModel);
            }
            commentAdapter = new CommentAdapter(getActivity(), commentModelArrayList);


            commentsListView.setAdapter(commentAdapter);


            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }


    }
/***
 *
 * CommentAdapter
 */
    /**
     * Comments adapter
     *
     * @author showket
     */
    public class CommentAdapter extends BaseAdapter {

        private List<CommentModel> all_comments = null;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolderComments holder;

        // Typeface face, face1;

        public CommentAdapter(Activity a, List<CommentModel> all_comments) {

            this.all_comments = all_comments;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_comments.size();
        }

        @Override
        public CommentModel getItem(int position) {
            return all_comments.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_comment_product, null);
                holder = new ViewHolderComments();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewMeadiumFont) convertView
                        .findViewById(R.id.userName);
                holder.comment = (CustomTextViewMeadiumFont) convertView
                        .findViewById(R.id.comment);
                holder.timeComment = (CustomTextViewLightFont) convertView
                        .findViewById(R.id.timeComment);


                convertView.setTag(holder);


            } else {
                holder = (ViewHolderComments) convertView.getTag();
            }


            holder.nameField.setText(all_comments.get(position).getUserName());
            holder.comment.setText(all_comments.get(position).getUserComment());
            holder.timeComment.setText(all_comments.get(position).getCommentTime());


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_comments.get(position).getUserProfileImage(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }


            holder.layout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }

            });
            return convertView;

        }


    }

    class ViewHolderComments {

        RelativeLayout layout;
        ImageView img;
        CustomTextViewMeadiumFont nameField, comment;
        CustomTextViewLightFont timeComment;

    }

    /**
     * Promote popup
     *
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showLikePopup(Activity activity) {

        try {
            final Dialog dialog;
//            if (Config.isLollyPop()) {
//                dialog = new Dialog(activity,
//                        android.R.style.Theme_Material_NoActionBar);
//            }
//            else
//            {
            dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            // }

//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.like_product);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            final ListView likeListView = (ListView) dialog.findViewById(R.id.likeListView);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backBtn);
            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    , "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

            likeModelArrayList = new ArrayList<>();

            for (int i = 0; i < order_ids.length; i++) {

                likeModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
                // Binds all strings into an array
                likeModelArrayList.add(likeModel);
            }
            LikesAdapter = new LikesAdapter(getActivity(), likeModelArrayList);
            likeListView.setAdapter(LikesAdapter);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    /**
     * Comments adapter
     *
     * @author showket
     */
    public class LikesAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_likes = null;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolderLikes holder;

        // Typeface face, face1;

        public LikesAdapter(Activity a, List<PromoteUserListModel> all_likes) {

            this.all_likes = all_likes;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_likes.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_likes.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_like, null);
                holder = new ViewHolderLikes();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);

                holder.offferLabel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.selectBtn);


                convertView.setTag(holder);


            } else {
                holder = (ViewHolderLikes) convertView.getTag();
            }


            holder.nameField.setText(all_likes.get(position).getNamefield());
//            holder.comment.setText(all_likes.get(position).getUserComment());


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_likes.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }


            holder.layout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }

            });
            return convertView;

        }

    }

    class ViewHolderLikes {

        RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField;
        CustomTextViewRegularFont offferLabel;

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void deletePromotionNotePoup(Activity activity) {
        final CustomTextViewRegularFont cameraBtn,galleryBtn;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.gallery_camera_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            CustomTextViewSemiBold textHead = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);
            cameraBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.camerabtn);
            galleryBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.galleryBtn);
            textHead.setText(getResources().getString(R.string.delete_promotion_note));
            cameraBtn.setText(getResources().getString(R.string.noLabel));
            galleryBtn.setText(getResources().getString(R.string.yesLabel));


            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            galleryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();


                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


}
