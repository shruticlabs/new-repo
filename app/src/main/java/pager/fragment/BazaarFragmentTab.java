package pager.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clicklabs.butlers.R;

/**
 * Created by click103 on 12/3/15.
 */
public class BazaarFragmentTab extends Fragment {
    public static BazaarFragmentTab init(int val) {
        BazaarFragmentTab bazaarFragmentTab = new BazaarFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        bazaarFragmentTab.setArguments(args);

        return bazaarFragmentTab;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bazaar_layout, container, false);
        return rootView;
    }
}
