package pager.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clabs.butlers.utils.StreamingMediaPlayer;
import clicklabs.app.butlers.BuyingProducts;
import clicklabs.app.butlers.MyApplication;
import material.animations.MaterialDesignAnimations;
import models.CommentModel;
import models.PromoteProductsBuyModel;
import models.PromoteProductsSellModel;
import models.PromoteUserListModel;
import models.SearchProductsBuyModel;
import models.SearchProductsSellModel;
import pathanimation.ArcTranslateAnimation;
import adapters.CommentAdapter;


/**
 * Created by click103 on 12/3/15.
 */
public class FeedPromoteFrag extends Fragment  {


    CustomTextViewRegularFont contactsTab, buyingBtn,sellingBtn,promotedBtn;
    ListView listView;
    PromoteProductsBuyModel promoteProductsBuyModel;
    ArrayList<PromoteProductsBuyModel> buyarraylist;
    ProductsBuyAdapter buyadapter;

    PromoteProductsSellModel promoteProductsSellModel;
    ArrayList<PromoteProductsSellModel> sellarraylist;
  //  ProductsSellAdapter selladapter;

    CommentAdapter commentAdapter;
    PromoteUserListModel likeModel;
    ArrayList<PromoteUserListModel> likeModelArrayList;
    CommentModel commentModel;
    ArrayList<CommentModel> commentModelArrayList;
     RecyclerView recyclerView;
     Animation animAccelerate;
    SwipeRefreshLayout swipe;
    RelativeLayout buyselltabs;
    HomFragment homFragment;
    int contentFlag=1;
    int offSetBuY=0,offSetSell=0;
     RecyclerView buyRecyclerView,sellRecyclerView;
    LinearLayout errorLayout;
    FragmentManager fm;
    HomFragment parent;
     SwipeRefreshLayout swipeViewSell,swipeView;
    MyApplication applicationClass;

    static FeedPromoteFrag init(int val) {
        FeedPromoteFrag feedPromoteFrag = new FeedPromoteFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        feedPromoteFrag.setArguments(args);

        return feedPromoteFrag;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        applicationClass = (MyApplication)getActivity().getApplication();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



          animAccelerate = AnimationUtils.loadAnimation(getActivity(), R.anim.accelerate);
        errorLayout = (LinearLayout)getActivity(). findViewById(R.id.errorLayout);
        View rootView = inflater.inflate(R.layout.feed_promote_layout, container, false);
        buyingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.buyingBtn);
        sellingBtn =(CustomTextViewRegularFont) rootView.findViewById(R.id.sellingBtn);
        buyselltabs =(RelativeLayout) rootView.findViewById(R.id.buyselltabs);
        buyarraylist = new ArrayList<PromoteProductsBuyModel>();

        buyRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewBuy);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        buyRecyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setItemAnimator(new FadeInAnimator());
        fm = getFragmentManager();
        buyRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...

                Log.v("Load More","Load More"+current_page);
            }

            @Override
            public void onHide() {
                parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
                //parent.hideBuySell(buyselltabs);
            }

            @Override
            public void onShow() {
                parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
              //  parent.showBuySell(buyselltabs);
            }
        });



        sellRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleViewSell);

        LinearLayoutManager linearLayoutManagersell = new LinearLayoutManager(getActivity());
        sellRecyclerView.setLayoutManager(linearLayoutManagersell);
//        recyclerView.setItemAnimator(new FadeInAnimator());

        sellRecyclerView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...

                Log.v("Load More","Load More"+current_page);
            }

            @Override
            public void onHide() {
                parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
                //parent.hideBuySell(buyselltabs);
            }

            @Override
            public void onShow() {
                parent = (HomFragment)fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
                //  parent.showBuySell(buyselltabs);
            }
        });

        if (AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {

           // PromoteFeedsApi();
        }


        swipeView = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeBuy);
        swipeView.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(false);
                        double f = Math.random();
//                        rndNum.setText(String.valueOf(f));
                    }
                }, 3000);
            }
        });



        swipeViewSell = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeSell);
        swipeViewSell.setColorScheme(android.R.color.holo_blue_dark, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_green_light);
        swipeViewSell.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                Log.d("Swipe", "Refreshing Number");
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeViewSell.setRefreshing(false);
                        double f = Math.random();
//                        rndNum.setText(String.valueOf(f));
                    }
                }, 3000);
            }
        });

//        buyadapter=new ProductsBuyAdapter(getActivity(), buyarraylist);
//        recyclerView.setAdapter(buyadapter);

//        listView.setAdapter(new ProductsBuyAdapter(getActivity(),R.id.image,arraylistnew));


        buyingBtn.setBackgroundResource(R.drawable.buying_normal);
        buyingBtn.setTextColor(getResources().getColor(R.color.white));
        sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));





        buyingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyingBtn.setBackgroundResource(R.drawable.buying_normal);
                sellingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                buyingBtn.setTextColor(getResources().getColor(R.color.white));
                sellingBtn.setBackgroundResource(0);
                sellRecyclerView.setVisibility(View.GONE);
                swipeViewSell.setVisibility(View.GONE);
                swipeView.setVisibility(View.VISIBLE);
                buyRecyclerView.setVisibility(View.VISIBLE);
                contentFlag=1;
                if (AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {

                   // PromoteFeedsApi();
                }
            }
        });
        sellingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellingBtn.setBackgroundResource(R.drawable.buying_normal);
                buyingBtn.setTextColor(getResources().getColor(R.color.rowtext));
                sellingBtn.setTextColor(getResources().getColor(R.color.white));
                buyingBtn.setBackgroundResource(0);
                swipeViewSell.setVisibility(View.VISIBLE);
                swipeView.setVisibility(View.GONE);
                sellRecyclerView.setVisibility(View.VISIBLE);
                buyRecyclerView.setVisibility(View.GONE);
                contentFlag=0;
                if (AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {

                   // PromoteFeedsApi();
                }
            }
        });
        return rootView;
    }
    /********************* ProductsBuyAdapter items list adapter ******************/
//    public class ProductsBuyAdapter extends ArrayAdapter<String> {

//    public class ProductsBuyAdapter extends ArrayAdapter<String> {
//
//
//        private LayoutInflater inflater;
//        ViewHolder holder;
//
//        private ArrayList<PromoteProductsBuyModel> products;
//        Context context;
//        Activity activity;
//        MediaPlayer mediaPlayer;
//        private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class
//
//        private Handler handler;
//        private boolean isPlaying;
//
//        private StreamingMediaPlayer audioStreamer;
//
//
//
//        //        public ProductsBuyAdapter(Context context, int CustomTextViewRegularFontResourceId, List<PromoteProductsModel> list) {
//        public ProductsBuyAdapter(Activity activity, int CustomTextViewRegularFontResourceId, List<PromoteProductsBuyModel> arraylist) {
//            super(activity, CustomTextViewRegularFontResourceId);
//
//            this.activity=activity;
////            arraylistnew = arraylist;
//            this.products = new ArrayList<PromoteProductsBuyModel>();
//            mediaPlayer = new MediaPlayer();
//            this.products.addAll(arraylist);
//
//            mediaPlayer = new MediaPlayer();
//            handler = new Handler();
//
////            Log.i("products", products + "....");
//            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////            Log.i("arrlist len", arraylist.size() + ",,");
//
////            for (int i = 0; i < arraylistnew.size(); i++) {
////                Log.i("wishh..", "..." + arraylistnew.get(i).getAddcartStatus()
////                        + ".." + arraylistnew.get(i).getWishStatus());
//        }
//
//
//        @Override
//        public int getCount() {
//            return products.size();
////            return 10;
//        }
//
////		@Override
////		public ProductFilter getItem(int position) {
////			return arraylistnew.get(position);
////		}
//
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            final ViewHolder holder;
//
//
//            if (convertView == null) {
//                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_promote, null);
//                holder = new ViewHolder();
//                holder.profileImage = (ImageView) convertView
//                        .findViewById(R.id.imageProfile);
//                holder.productImage = (ImageView) convertView
//                        .findViewById(R.id.cardImage);
//                holder.playVoiceNoteBtn = (ImageButton) convertView
//                        .findViewById(R.id.playVoiceNoteBtn);
//                holder.streamButton = (Button) convertView
//                        .findViewById(R.id.streamButton);
//                holder.userName = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.userName);
//                holder.likesCount = (CustomTextViewLightFont) convertView
//                        .findViewById(R.id.likeBtnLabel);
//                holder.commentCount = (CustomTextViewLightFont) convertView
//                        .findViewById(R.id.commentBtnLabel);
//
//                holder.productModel = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.productName);
//                holder.productLabel = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.productLabel);
//                holder.userWhoRated = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.productBarUserName);
//                holder.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.userRatingValue);
//                holder.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.countPlus);
//                holder.buttonLayoutReveal = (LinearLayout) convertView
//                        .findViewById(R.id.buttonLayoutReveal);
//                holder.topPart = (LinearLayout) convertView
//                        .findViewById(R.id.topPart);
//
//                holder.likeBtn = (RelativeLayout) convertView
//                        .findViewById(R.id.likeBtn);
//                holder.comentBtn = (RelativeLayout) convertView
//                        .findViewById(R.id.commentBtn);
//                holder.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
//                holder.productAddedTime = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.timeValue);
//               // holder.seekBar=(ProgressBar)convertView.findViewById(R.id.seekbar);
//                holder.seekBar1=(SeekBar)convertView.findViewById(R.id.seekBar1);
//                holder.seekBar1.setMax(100);
//                holder.rl = (RelativeLayout) convertView.findViewById(R.id.rv);
//                convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
////            holder.namefield.setText(arraylistnew.get(position).getName());
////            holder.price.setText(arraylistnew.get(position).getPrice());
////            if (arraylistnew.get(position).getImage().length() != 0) {
//            AQuery aq = new AQuery(convertView);
//
//            try{
//                aq.id(holder.profileImage)
//                        .progress(R.id.imageProgressbar)
//                        .image(products.get(position).getUserProfileImage(), true, true,
//                                0, 0, null, 0, AQuery.FADE_IN);
////                aq.id(holder.productImage)
////                        .progress(R.id.centralProgressbar)
////                        .image(products.get(position).getUserProfileImage(), true, true,
////                                0, 0, null, 0, AQuery.FADE_IN);
////                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.profileImage);
////
//                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.productImage);
//            }catch(Exception e){
//
//            }
//
//            if (products.get(position).getIsShownLayout()==false) {
////                holder.buttonLayoutReveal.setVisibility(View.GONE);
//            }
//            else {
//                holder.buttonLayoutReveal.setVisibility(View.VISIBLE);
//            }
//
//            holder.streamButton.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View view) {
//
//
//                    holder.seekBar.setProgress(5);
//                    startStreamingAudio(position);
//
//                }
//
//                private void startStreamingAudio(int position) {
//                    try {
////                        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
////                        if (audioStreamer != null) {
////                            audioStreamer.interrupt();
////                        }
//
//                        audioStreamer = new StreamingMediaPlayer(getActivity(),holder.playVoiceNoteBtn, holder.streamButton, holder.seekBar1);
//                        audioStreamer.ReleaseMediaPlayer();
//
//
//                        if(position==0)
//                            audioStreamer.startStreaming("http://www.pocketjourney.com/downloads/pj/tutorials/audio.mp3",1717, 214);
//                        else
//                            audioStreamer.startStreaming("http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3", 5208, 216);
////                        holder.playVoiceNoteBtn.setEnabled(false);
//                    } catch (IOException e) {
//                        Log.e(getClass().getName(), "Error starting to stream audio.", e);
//                    }
//
//                }
//
//
//            });
////            holder.seekBar1.setOnTouchListener(new View.OnTouchListener() {
////                @Override
////                public boolean onTouch(View v, MotionEvent event) {
////                    if(StreamingMediaPlayer.mediaPlayer.isPlaying()){
////                        SeekBar sb = (SeekBar)v;
//////                        int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
////                        StreamingMediaPlayer.mediaPlayer.seekTo((int) StreamingMediaPlayer.loadProgress);
////                    }
////                    return false;
////                }
////            });
//            holder.playVoiceNoteBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View v) {
//                    holder.seekBar.setSecondaryProgress(0);
//
//                    if (holder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
//                        products.get(position).setIsShownLayout(false);
////                        holder.buttonLayoutReveal.setVisibility(View.GONE);
//                    } else {
//                        products.get(position).setIsShownLayout(true);
//                        holder.buttonLayoutReveal.setVisibility(View.VISIBLE);
//
//
//
//
//
//
//
////                        if (((Button) v).getText().toString().equals("Play")) {
////                            try {
//////                                mediaPlayer.reset();
////                                String uri = "http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3";
////                                mediaPlayer.setDataSource(getActivity(), Uri.parse(uri));
//////                            mediaPlayer = new MediaPlayer();
//////                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//////                            mediaPlayer.setDataSource("http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3");
////                                mediaPlayer.prepareAsync();
////                                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
////                                    @Override
////                                    public void onPrepared(MediaPlayer mp) {
////                                        mp.start();
////                                        ((Button) v).setText("Pause");
////                                        mediaFileLengthInMilliseconds = mp.getDuration();
////                                        primarySeekBarProgressUpdater();
////                                    }
////                                });
////                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
////                                    @Override
////                                    public void onCompletion(MediaPlayer mp) {
////                                        //mp.release();
////                                        //  mp = null;
////                                        ((Button) v).setText("Play");
////                                    }
////                                });
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
////                        } else {
////                            mediaPlayer.release();
////                            mediaPlayer = null;
////                            ((Button) v).setText("Play");
////                        }
//
//
//                    }
//                    if (audioStreamer.getMediaPlayer().isPlaying()) {
//                        audioStreamer.getMediaPlayer().pause();
//                        holder.playVoiceNoteBtn.setImageResource(R.drawable.play);
//                    } else {
//                        audioStreamer.getMediaPlayer().start();
//                        audioStreamer.startPlayProgressUpdater();
//                        holder.playVoiceNoteBtn.setImageResource(R.drawable.pause);
//                    }
//                    isPlaying = !isPlaying;
//
//                    // if(((Button)v).getText().toString().equals("Play")){
//
////                    }else{
////                        mediaPlayer.release();
////                        mediaPlayer = null;
////                        ((Button)v).setText("Play");
////                    }
//                    notifyDataSetChanged();
//
//
////                    try {
////                        mediaPlayer.setDataSource("http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3"); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
////                        mediaPlayer.prepareAsync(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
////
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
//////                    mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
////
////                    if(!mediaPlayer.isPlaying()){
////                        mediaPlayer.start();
////
////                    }else {
////                        mediaPlayer.pause();
////                        holder.playVoiceNoteBtn.setBackgroundResource(R.drawable.pause);
////                    }
//
////                    primarySeekBarProgressUpdater();
//
//
////                    notifyDataSetChanged();
//
//                }
//
//
//
//
//                private void primarySeekBarProgressUpdater() {
//
//                    holder.seekBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song length"
//                    if (mediaPlayer != null) {
//                        Runnable notification = new Runnable() {
//                            public void run() {
//                                primarySeekBarProgressUpdater();
//                            }
//                        };
//                        handler.postDelayed(notification, 1000);
//                    }
//                }
//            });
//
//
//
//            holder.likeBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    homFragment = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
//                    homFragment.viewPager.setVisibility(View.GONE);
//                    homFragment.bottomBar.setVisibility(View.GONE);
//                    homFragment.topBar.setVisibility(View.GONE);
//                    homFragment.fragmentContainer.setVisibility(View.VISIBLE);
//                    LikeProductFragment likeProductFragment = new LikeProductFragment();
//                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(homFragment.fragmentContainer.getId(), likeProductFragment);
//                    ft.commit();
//
//                }
//            });
//            holder.comentBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//
//                    showCommentPopup(getActivity());
//
//
//                }
//            });
//
//            holder.commentCount.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//
//
//
//
//                }
//            });
//            holder.likesCount.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//
//
//
//
//                }
//            });
//            holder.topPart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//
//
//
//
//                }
//            });
//
//
////            final ImageView cardImage = holder.productImage;
//            holder.productImage.setOnClickListener(new View.OnClickListener() {
//                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//                @Override
//                public void onClick(View view) {
//                    //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
//                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, holder.productImage, "cardImage");
//                    Intent intent = new Intent(activity, BuyingProducts.class);
//                    getActivity().startActivity(intent, options.toBundle());
//                }
//            });
//
//
//
//            return convertView;
//        }
//
//
////        @Override
////        public void onCompletion(MediaPlayer mp) {
////        }
////
////        @Override
////        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
////
////        }
////
////        @Override
////        public void onStartTrackingTouch(SeekBar seekBar) {
////
////        }
////
////        @Override
////        public void onStopTrackingTouch(SeekBar seekBar) {
////
////        }
////
////        @Override
////        public boolean onTouch(View v, MotionEvent event) {
////            if(v.getId() == R.id.playVoiceNoteBtn){
////                /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
////                if(mediaPlayer.isPlaying()){
////                    SeekBar sb = (SeekBar)v;
////                    int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
////                    mediaPlayer.seekTo(playPositionInMillisecconds);
////                }
////            }
////            return false;
////        }
////
////        @Override
////        public void onBufferingUpdate(MediaPlayer mp, int percent) {
////            holder.seekBar.setSecondaryProgress(percent);
////        }
//    }

    class ViewHolder {
        ProgressBar profileProgress,productProgress;
        CustomTextViewLightFont likesCount,commentCount;
        ImageView profileImage,productImage;
        CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                productPrice,availableCount,commentBtnLabel;
        RelativeLayout rel,likeBtn,comentBtn;
        LinearLayout buttonLayoutReveal,topPart;
        Button streamButton;
        ImageButton playVoiceNoteBtn;
        RelativeLayout rl;
        ProgressBar seekBar;
        SeekBar seekBar1;
        TextView textStreamed;
        int p;
    }





    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showCommentPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.comment_product);
            if (Config.isLollyPop())
            dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final EditText editView=(EditText)dialog.findViewById(R.id.commentText);
            final ListView commentsListView=(ListView)dialog.findViewById(R.id.comemtsListView);
            RelativeLayout backBtn=(RelativeLayout)dialog.findViewById(R.id.backBtn);

            LinearLayout sendBtn=(LinearLayout)dialog.findViewById(R.id.sendBtnLay);



            String[] order_ids={"1","2","3","4","5","6","7","8","9"};
            String[] order_images={"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    ,"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names={"saif","showket","anil","saif","showket","anil","saif","showket","anil"};

            commentModelArrayList = new ArrayList<CommentModel>();

            for (int i = 0; i < order_ids.length; i++) {

                commentModel = new CommentModel(order_images[i],order_names[i],order_ids[i],order_images[i],order_names[i]);
                // Binds all strings into an array
                commentModelArrayList.add(commentModel);
            }
            commentAdapter=new CommentAdapter(getActivity(), commentModelArrayList);


            commentsListView.setAdapter(commentAdapter);


            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });





            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }





    }
/***
 *
 * CommentAdapter
 */
    /**
     * Comments adapter
     *
     * @author showket
     */






    /************recycler view*************/
    /**
     * Created by Wasabeef on 2015/01/03.
     */
    public class ProductsBuyAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

        private List<PromoteProductsBuyModel> feedItemList;
        private Context mContext;
        private boolean isPlaying;
        private StreamingMediaPlayer audioStreamer;
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        private DisplayImageOptions options;
//        ArcTranslateAnimation anim  = new ArcTranslateAnimation(0, -820 , 0, 100);;



        public ProductsBuyAdapter(Context context, List<PromoteProductsBuyModel> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.ic_launcher)
                    .showImageForEmptyUri(R.drawable.ic_launcher)
                    .showImageOnFail(R.drawable.ic_launcher)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true).build();

            // Set up the animation

        }

        @Override
        public FeedListRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_promote, null);
            FeedListRowHolder mh = new FeedListRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, final int i) {
            final PromoteProductsBuyModel feedItem = feedItemList.get(i);
            ImageLoader.getInstance().displayImage(feedItem.getProductImage(),feedListRowHolder.productImage, options, animateFirstListener);
            ImageLoader.getInstance().displayImage(feedItem.getUserProfileImage(),feedListRowHolder.profileImage, options, animateFirstListener);

            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));
            feedListRowHolder.userRaings.setText(Html.fromHtml(feedItem.getUserRaings())+" Star )");

            feedListRowHolder.productAddedTime.setText(applicationClass.getExpirationTimeTodisplay(feedItem.getProductAddedTime()) + ", ");


            //feedListRowHolder.productAddedTime.setText(getExpirationTimeTodisplay(currentDate(),feedItem.getProductAddedTime()) + ", ");

            feedListRowHolder.addressField.setText(Html.fromHtml(feedItem.getAddressField()));
            feedListRowHolder.likesCount.setText(feedItem.getLikesCount() + " Likes");
            feedListRowHolder.commentCount.setText(feedItem.getCommentCount() + " Comments");


            feedListRowHolder.description.setText(feedItem.getDescription());
            feedListRowHolder.productModel.setText(Html.fromHtml(feedItem.getProductModel()));
            feedListRowHolder.userWhoRated.setText(Html.fromHtml(feedItem.getUserWhoRated()));


            if (feedItem.getTextNote().equals("")) {
                feedListRowHolder.textNoteBtn.setVisibility(View.GONE);
            }
            else {
                feedListRowHolder.textNoteBtn.setVisibility(View.VISIBLE);
            }

            if (feedItem.getVoiveNoteUrl().equals("")) {
                feedListRowHolder.playVoiceNoteBtnLay.setVisibility(View.GONE);
            }
            else {
                feedListRowHolder.playVoiceNoteBtnLay.setVisibility(View.VISIBLE);
            }



//            feedListRowHolder.streamButton.setOnClickListener(new View.OnClickListener() {
//                public void onClick(View view) {
//
//
//                    feedListRowHolder.seekBar.setProgress(5);
//                    startStreamingAudio(i);
//
//                }
//
//                private void startStreamingAudio(int position) {
//                    try {
////                        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
////                        if (audioStreamer != null) {
////                            audioStreamer.interrupt();
////                        }
//
//                        audioStreamer = new StreamingMediaPlayer(getActivity(),  feedListRowHolder.textStreamed, feedListRowHolder.playVoiceNoteBtn, feedListRowHolder.streamButton, feedListRowHolder.seekBar1);
//                        audioStreamer.ReleaseMediaPlayer();
//
//
//                        if(position==0)
//                            audioStreamer.startStreaming("http://www.pocketjourney.com/downloads/pj/tutorials/audio.mp3",1717, 214);
//                        else
//                            audioStreamer.startStreaming("http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3", 5208, 216);
////                        holder.playVoiceNoteBtn.setEnabled(false);
//                    } catch (IOException e) {
//                        Log.e(getClass().getName(), "Error starting to stream audio.", e);
//                    }
//
//                }
//
//
//            });
//            feedListRowHolder.seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                    audioStreamer.changeSeekBarOnTouch(seekBar);
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//
//                }
//            });
//            feedListRowHolder.seekBar1.setOnSeekBarChangeListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    if (StreamingMediaPlayer.mediaPlayer.isPlaying()) {
//                        SeekBar sb = (SeekBar) v;
//                        audioStreamer.changeSeekBarOnTouch(feedListRowHolder.seekBar1);
//
////                        int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
////                        StreamingMediaPlayer.mediaPlayer.seekTo((int) StreamingMediaPlayer.loadProgress);
//                    }
//                    return false;
//                }
//            });
//
//            feedListRowHolder.animatedPlay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });


            feedListRowHolder.playVoiceNoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final FeedListRowHolder feedListRowHolder =(FeedListRowHolder)v.getTag();

                   // feedListRowHolder.seekBar.setSecondaryProgress(0);

                    /*****working*****/
                    feedItem.setIsShownLayout(true);

                    feedListRowHolder.animatedPlay.setVisibility(View.VISIBLE);
                    int width = (feedListRowHolder.seeRelative.getWidth())-30;
                    int thumbPos = feedListRowHolder.seekBar1.getPaddingLeft()
                            + width
                            * feedListRowHolder.seekBar1.getProgress()
                            / feedListRowHolder.seekBar1.getMax();
                    ArcTranslateAnimation anim = new ArcTranslateAnimation(0, -width , 0, 110);
                    anim.setDuration(500);
                    anim.setFillAfter(true);
                    feedItem.setIsShownLayout(true);

                    feedListRowHolder.animatedPlay.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                        feedListRowHolder.animatedPlay.setVisibility(View.GONE);
                        feedListRowHolder.seekBar1.setVisibility(View.VISIBLE);
                        notifyDataSetChanged();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    /*****working end*****/

                 //   feedListRowHolder.animatedPlay.animate().translationY(-100).setDuration(500).start();

//                    TranslateAnimation translateAnimation1 = new TranslateAnimation(
//                            TranslateAnimation.RELATIVE_TO_PARENT,
//                            0.0f,
//                            TranslateAnimation.RELATIVE_TO_PARENT,
//                            0.8f,
//                            TranslateAnimation.RELATIVE_TO_PARENT,
//                            0.0f,
//                            TranslateAnimation.RELATIVE_TO_PARENT,
//                            0.8f);
//                    translateAnimation1.setDuration(1000);
//                    translateAnimation1.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation arg0) {}
//                        @Override
//                        public void onAnimationRepeat(Animation arg0) {}
//                        @Override
//                        public void onAnimationEnd(Animation arg0) {
//                            TranslateAnimation translateAnimation2 = new TranslateAnimation(
//                                    TranslateAnimation.RELATIVE_TO_PARENT,
//                                    0.8f,
//                                    TranslateAnimation.RELATIVE_TO_PARENT,
//                                    0.5f,
//                                    TranslateAnimation.RELATIVE_TO_PARENT,
//                                    0.8f,
//                                    TranslateAnimation.RELATIVE_TO_PARENT,
//                                    0.5f);
//                            translateAnimation2.setDuration(1000);
//                            AnimationSet animationSet = new AnimationSet(false);
//                            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 2.0f,1.0f,2.0f,ScaleAnimation.RELATIVE_TO_SELF ,0.5f,ScaleAnimation.RELATIVE_TO_SELF,0.5f);
//                            scaleAnimation.setDuration(1000);
//                            animationSet.addAnimation(scaleAnimation);
//                            animationSet.addAnimation(translateAnimation2);
//                            feedListRowHolder.animatedPlay.startAnimation(animationSet);
//
//                        }
//                    });
//                    feedListRowHolder.animatedPlay.startAnimation(translateAnimation1);





////
//                    int left=feedListRowHolder.buttonLayoutReveal.getWidth();
//
//                    ObjectAnimator anim = ObjectAnimator.ofFloat(feedListRowHolder.animatedPlay, "translationX",740,0);
//
//                    //You dont need rotation
//                    ObjectAnimator rotate = ObjectAnimator.ofFloat(feedListRowHolder.animatedPlay, "rotation", 0, 360);
//                    rotate.setRepeatCount(Animation.INFINITE);
//                    rotate.setRepeatMode(Animation.REVERSE);
//                    rotate.setDuration(350);
//
//            /*
//             * Button needs to be removed after animation ending
//             * When we have added the view to the ViewOverlay,
//             * it was removed from its original parent.
//             */
//                    anim.addListener(new Animator.AnimatorListener() {
//
//                        @Override
//                        public void onAnimationStart(Animator arg0) {
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animator arg0) {
//                        }
//
//                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
//                        @Override
//                        public void onAnimationEnd(Animator arg0) {
//                            feedListRowHolder.buttonLayoutReveal.getOverlay().remove(feedListRowHolder.animatedPlay);
//                        }
//
//                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
//                        @Override
//                        public void onAnimationCancel(Animator arg0) {
//                            feedListRowHolder.buttonLayoutReveal.getOverlay().remove(feedListRowHolder.animatedPlay);
//                        }
//                    });
//
//                    anim.setDuration(1000);
//
//                    AnimatorSet set = new AnimatorSet();
//                    set.playTogether(anim, rotate);
//                    set.start();








                    if (feedListRowHolder.buttonLayoutReveal.getVisibility() == View.VISIBLE) {
                        feedListRowHolder.playVoiceNoteBtn.setEnabled(true);
//                            if (audioStreamer.getMediaPlayer().isPlaying() && audioStreamer!=null) {
//
//                                audioStreamer.getMediaPlayer().pause();
//                                feedListRowHolder.playVoiceNoteBtn.setImageResource(R.drawable.btn_voice_note_normal);
//                            }
//                            else
                            {
                                feedListRowHolder.seekBar.setProgress(5);
                                startStreamingAudio(i);
                                feedListRowHolder.playVoiceNoteBtn.setImageResource(R.drawable.btn_voice_note_normal);
                            }

                        feedItem.setIsShownLayout(false);

                    } else {
                        feedListRowHolder.playVoiceNoteBtn.setEnabled(false);
                        startStreamingAudio(i);
                        for(int i=0;i<feedItemList.size();i++)
                            feedItemList.get(i).setIsShownLayout(false);
                        feedItem.setIsShownLayout(true);

                        feedListRowHolder.buttonLayoutReveal.setVisibility(View.VISIBLE);
                    }

                 //   notifyDataSetChanged();
                }

                private void startStreamingAudio(int position) {
                    try {
//                        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
//                        if (audioStreamer != null) {
//                            audioStreamer.interrupt();
//                        }

                        audioStreamer = new StreamingMediaPlayer(getActivity(),feedListRowHolder.playVoiceNoteBtn, feedListRowHolder.textNoteBtn, feedListRowHolder.seekBar1);
                        audioStreamer.ReleaseMediaPlayer();

                        audioStreamer.startStreaming(feedItem.getVoiveNoteUrl(),1717, 214);


                    } catch (IOException e) {
                        Log.e(getClass().getName(), "Error starting to stream audio.", e);
                    }

                }

            });

            feedListRowHolder.likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    homFragment = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                    homFragment.viewPager.setVisibility(View.GONE);
                    homFragment.bottomBar.setVisibility(View.GONE);
                    homFragment.topBar.setVisibility(View.GONE);
                    homFragment.fragmentContainer.setVisibility(View.VISIBLE);
                    LikeProductFragment likeProductFragment = new LikeProductFragment();
                    android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(homFragment.fragmentContainer.getId(), likeProductFragment);
                    ft.commit();

                }
            });
            feedListRowHolder.comentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    showCommentPopup(getActivity());


                }
            });

            feedListRowHolder.commentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });
            feedListRowHolder.likesCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });
            feedListRowHolder.topPart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {






                }
            });


//            final ImageView cardImage = holder.productImage;
            feedListRowHolder.productImage.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    if(Config.isLollyPop()) {
                        //This is where the magic happens. makeSceneTransitionAnimation takes a context, view, a name for the target view.
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.productImage, "cardImage");
                        Intent intent = new Intent(mContext, BuyingProducts.class);
                        getActivity().startActivity(intent, options.toBundle());
                    }
                    else
                    {
                        getActivity().startActivity(new Intent(mContext,BuyingProducts.class));
                    }
                }
            });
            feedListRowHolder.playVoiceNoteBtn.setTag(feedListRowHolder);

        }

        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }
    }
     class FeedListRowHolder extends RecyclerView.ViewHolder {
         CustomTextViewLightFont likesCount,commentCount,description;
         ImageView profileImage,productImage;
         CustomTextViewRegularFont userName, userRaings, productAddedTime,addressField,productLabel,productModel, userWhoRated,userWhoRatedRatings,otherUserWhoRatedCount,
                 commentBtnLabel;
         RelativeLayout likeBtn,comentBtn,seeRelative;
         LinearLayout buttonLayoutReveal,topPart;
         Button textNoteBtn;
         ImageButton playVoiceNoteBtn,animatedPlay;
         RelativeLayout playVoiceNoteBtnLay;
         RelativeLayout rl;
         ProgressBar seekBar;
         SeekBar seekBar1;
         TextView textStreamed;

        public FeedListRowHolder(View convertView) {
            super(convertView);
            this.productImage = (ImageView) convertView.findViewById(R.id.cardImage);
            this.profileImage = (ImageView) convertView
                    .findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView
                    .findViewById(R.id.cardImage);
            this.playVoiceNoteBtn = (ImageButton) convertView
                    .findViewById(R.id.playVoiceNoteBtn);
            this.playVoiceNoteBtnLay = (RelativeLayout) convertView
                    .findViewById(R.id.playVoiceNoteBtnLay);
            this.animatedPlay = (ImageButton) convertView
                    .findViewById(R.id.animationPlay);
            this.textNoteBtn = (Button) convertView
                    .findViewById(R.id.textNoteBtn);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.description = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.descriptionText);
            this.likesCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.likeBtnLabel);
            this.commentCount = (CustomTextViewLightFont) convertView
                    .findViewById(R.id.commentBtnLabel);
            this.addressField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.addressField);

            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.userWhoRated = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productBarUserName);
            this.userWhoRatedRatings = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userRatingValue);
            this.otherUserWhoRatedCount = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.countPlus);
            this.buttonLayoutReveal = (LinearLayout) convertView
                    .findViewById(R.id.buttonLayoutReveal);
            this.topPart = (LinearLayout) convertView
                    .findViewById(R.id.topPart);

            this.likeBtn = (RelativeLayout) convertView
                    .findViewById(R.id.likeBtn);
            this.seeRelative = (RelativeLayout) convertView
                    .findViewById(R.id.seeRelative);
            this.comentBtn = (RelativeLayout) convertView
                    .findViewById(R.id.commentBtn);
            this.userRaings = (CustomTextViewRegularFont) convertView.findViewById(R.id.ratingValue);
            this.productAddedTime = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.timeValue);
//            this.seekBar=(ProgressBar)convertView.findViewById(R.id.seekbar);
            this.seekBar1=(SeekBar)convertView.findViewById(R.id.seekBar1);
            this.seekBar1.setMax(100);
            this.rl = (RelativeLayout) convertView.findViewById(R.id.rv);

        }

    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
    /**
     * search Products Api
     */
    public void PromoteFeedsApi() {
        //      Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        if(contentFlag==1) {
            params.put("offset", offSetBuY + "");
            params.put("sell_buy_flag", "1");
        }
        else {
            params.put("offset", offSetSell + "");
            params.put("sell_buy_flag", "0");
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getPromotedProducts", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                JSONArray productResults = jObj.getJSONObject("data").getJSONArray("results");
                                /**
                                 * Get data for Buy Listview
                                 * contentFlag=1

                                 */

                                if (contentFlag == 1) {

                                    if (buyarraylist.size() > 0) {
                                        for (int i = 0; i < productResults.length(); i++) {
                                            String productUrls = "";
                                            for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                            productUrls = productUrls.substring(0, productUrls.length() - 1);
                                            promoteProductsBuyModel = new PromoteProductsBuyModel(
                                                    productResults.getJSONObject(i).getString("profile_pic_link"),
                                                    productResults.getJSONObject(i).getString("name"),
                                                    productResults.getJSONObject(i).getString("rating"),
                                                    productResults.getJSONObject(i).getString("time_ago"),
                                                    productResults.getJSONObject(i).getString("address"),
                                                    productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"),
                                                    productUrls,
                                                    productResults.getJSONObject(i).getString("product_name"),
                                                    productResults.getJSONObject(i).getString("promoter_name"),
                                                    productResults.getJSONObject(i).getString("promoter_rating"),
                                                    productResults.getJSONObject(i).getString("no_of_promotes"),
                                                    productResults.getJSONObject(i).getString("no_of_likes"),
                                                    productResults.getJSONObject(i).getString("no_of_comments"),
                                                    productResults.getJSONObject(i).getString("price"),
                                                    productResults.getJSONObject(i).getString("user_id"),
                                                    productResults.getJSONObject(i).getString("like_status"),
                                                    productResults.getJSONObject(i).getString("product_id"),
                                                    false,
                                                    0,
                                                    productResults.getJSONObject(i).getString("description"),
                                                    productResults.getJSONObject(i).getString("voice_note_duration"),
                                                    productResults.getJSONObject(i).getString("text_note"),
                                                    productResults.getJSONObject(i).getString("voice_note"));

                                            buyarraylist.add(promoteProductsBuyModel);

                                        }
                                        offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));


                                        buyadapter.notifyDataSetChanged();
                                    } else {
                                        buyarraylist.clear();
                                        for (int i = 0; i < productResults.length(); i++) {
                                            String productUrls = "";
                                            for (int j = 0; j < productResults.getJSONObject(i).getJSONArray("images").length(); j++)
                                                productUrls = productUrls + productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link") + "$";
                                            promoteProductsBuyModel = new PromoteProductsBuyModel(
                                                    productResults.getJSONObject(i).getString("profile_pic_link"),
                                                    productResults.getJSONObject(i).getString("name"),
                                                    productResults.getJSONObject(i).getString("rating"),
                                                    productResults.getJSONObject(i).getString("time_ago"),
                                                    productResults.getJSONObject(i).getString("address"),
                                                    productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"),
                                                    productUrls,
                                                    productResults.getJSONObject(i).getString("product_name"),
                                                    productResults.getJSONObject(i).getString("promoter_name"),
                                                    productResults.getJSONObject(i).getString("promoter_rating"),
                                                    productResults.getJSONObject(i).getString("no_of_promotes"),
                                                    productResults.getJSONObject(i).getString("no_of_likes"),
                                                    productResults.getJSONObject(i).getString("no_of_comments"),
                                                    productResults.getJSONObject(i).getString("price"),
                                                    productResults.getJSONObject(i).getString("user_id"),
                                                    productResults.getJSONObject(i).getString("like_status"),
                                                    productResults.getJSONObject(i).getString("product_id"),
                                                    false,
                                                    0,
                                                    productResults.getJSONObject(i).getString("description"),
                                                    productResults.getJSONObject(i).getString("voice_note_duration"),
                                                    productResults.getJSONObject(i).getString("text_note"),
                                                    productResults.getJSONObject(i).getString("voice_note"));      buyarraylist.add(promoteProductsBuyModel);

                                        }
                                        offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));

                                        buyadapter = new ProductsBuyAdapter(getActivity(),buyarraylist);
                                        buyRecyclerView.setAdapter(buyadapter);
                                        buyRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                }
//                                else
//                                {
//                                    if(LOAD_FIRST_TIME_SELL!=1) {
//
//                                        for (int i = 0; i < productResults.length(); i++) {
//                                            String productUrls="";
//                                            for(int j=0;j<productResults.getJSONObject(i).getJSONArray("images").length();j++)
//                                                productUrls=productUrls+productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link")+"$";
//                                            productUrls=productUrls.substring(0, productUrls.length() - 1);
//
//                                            searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
//                                                    productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"),productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
//                                                    productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
//                                                    "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"),productResults.getJSONObject(i).getString("description"), false);
//
//                                            sellArraylist.add(searchProductsSellModel);
//
//                                        }
//                                        offSetSell = offSetSell + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
//                                        sellingCount=Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
//                                        resultCount.setText(sellingCount + " Resuls Found");
//                                        searchProductsSelAdapter.notifyDataSetChanged();
//                                    }
//                                    else
//                                    {
//                                        sellArraylist.clear();
//                                        for (int i = 0; i < productResults.length(); i++) {
//                                            String productUrls="";
//                                            for(int j=0;j<productResults.getJSONObject(i).getJSONArray("images").length();j++)
//                                                productUrls=productUrls+productResults.getJSONObject(i).getJSONArray("images").getJSONObject(j).getString("image_link")+"$";
//                                            productUrls=productUrls.substring(0, productUrls.length() - 1);
//
//                                            searchProductsSellModel = new SearchProductsSellModel(productResults.getJSONObject(i).getString("profile_pic_link"), productResults.getJSONObject(i).getString("name"), productResults.getJSONObject(i).getString("rating"), productResults.getJSONObject(i).getString("created_at"),
//                                                    productResults.getJSONObject(i).getJSONArray("images").getJSONObject(0).getString("image_link"),productUrls, productResults.getJSONObject(i).getString("product_name"), productResults.getJSONObject(i).getString("no_of_likes"),
//                                                    productResults.getJSONObject(i).getString("no_of_comments"), productResults.getJSONObject(i).getString("price"), productResults.getJSONObject(i).getString("user_id"),
//                                                    "0", productResults.getJSONObject(i).getString("product_id"), productResults.getJSONObject(i).getString("address"), productResults.getJSONObject(i).getString("quantity"),productResults.getJSONObject(i).getString("description"), false);
//
//                                            sellArraylist.add(searchProductsSellModel);
//
//                                        }
//                                        offSetBuY = offSetBuY + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
//                                        sellingCount=Integer.parseInt(jObj.getJSONObject("data").getString("total_results"));
//                                        resultCount.setText(sellingCount + " Resuls Found");
//                                        searchProductsSelAdapter = new SearchProductsSellAdapter(getActivity());
//                                        sellRecyclerView.setAdapter(searchProductsSelAdapter);
//                                    }
//                                }

                            } else {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
//                            Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }


}