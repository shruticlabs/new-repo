package pager.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.transition.Explode;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;


import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Helper;
import clabs.butlers.utils.MyLinearLayout;
import clabs.butlers.utils.ProductOfferAdapter;
import models.ProductsModel;


public class OffersFragmentTab extends Fragment implements ObservableScrollViewCallbacks {

    public ListView offersListView;

    public static ViewPager viewPager;
    public static ArrayList<ProductsModel> arraylist;
    ProductsModel productsModel;
    ProductOfferAdapter productOfferAdapter;
    public MyOffersAdapter myOfferAdapter;


    private boolean swipedLeft = false;
    private int lastPage;
    private MyLinearLayout cur = null;
    private MyLinearLayout next = null;
    private MyLinearLayout prev = null;
    private MyLinearLayout prevprev = null;
    private MyLinearLayout nextnext = null;
    private Context context;
    private FragmentManager fm;
    private float scale;
    private boolean IsBlured;
    private static float minAlpha = 0.6f;
    private static float maxAlpha = 1f;
    private static float minDegree = 60.0f;
    private int counter = 0;
    ObservableScrollView offerScrollView;
    ImageView noProductFound;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    static OffersFragmentTab init(int val) {
        OffersFragmentTab offersFragmentTab = new OffersFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        offersFragmentTab.setArguments(args);

        return offersFragmentTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.offers_layout, container, false);
        noProductFound = (ImageView) rootView.findViewById(R.id.noProductFound);
        offerScrollView = (ObservableScrollView) rootView.findViewById(R.id.offerScrollView);
        if(Config.getSKIP_MODE())
        {

            noProductFound.setVisibility(View.VISIBLE);
            offerScrollView.setVisibility(View.GONE);
        }
        else {
        noProductFound.setVisibility(View.GONE);
        offerScrollView.setVisibility(View.VISIBLE);
        offerScrollView.setScrollViewCallbacks(this);
        offersListView = (ListView) rootView.findViewById(R.id.offersList);
        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        viewPager.setFocusable(true);
        fm = getFragmentManager();

        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        float width = metrics.widthPixels;
        float height = metrics.heightPixels;

        arraylist = new ArrayList<ProductsModel>();

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                , "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i], order_names[i], order_ids[i], i, order_images[i], order_names[i], order_names[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], false);
            // Binds all strings into an array
            arraylist.add(productsModel);
        }
        productOfferAdapter = new ProductOfferAdapter(getActivity(), getFragmentManager());
        viewPager.setAdapter(productOfferAdapter);
        //  viewPager.setOnPageChangeListener(productOfferAdapter);

        /*
            * margin for carousel view animation according to screen size
            *
             */
        viewPager.setOffscreenPageLimit(3);
        if (width == 480) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin480)));

        }
        if (width == 640) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin640)));

        }
        if (width == 720) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin720)));

        }
        if (width == 768) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin768)));

        }
        if (width == 800) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin800)));

        }

        if (width == 1080) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin1080)));
        }
        if (width == 1200) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin1200)));

        }
        if (width == 1440) {

        }
        if (width == 1280) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin1280)));

        }
        if (width == 1920) {

        }
        if (width == 2048) {

        }
        if (width == 320) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin320)));

        }
        if (width == 240) {
            viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.margin240)));

        }

        //  viewPager.setPageMargin(Integer.parseInt(getResources().getString(R.string.offerPagermargin)));
        viewPager.setCurrentItem(MyLinearLayout.FIRST_PAGE);

        myOfferAdapter = new MyOffersAdapter(getActivity(), R.id.image, arraylist);
        offersListView.setAdapter(myOfferAdapter);
        Helper.getListViewSize(offersListView);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                updateCarosel(position, positionOffset, positionOffsetPixels);


            }

            @Override
            public void onPageSelected(int position) {
                if (lastPage <= position) {
                    swipedLeft = true;
                } else if (lastPage > position) {
                    swipedLeft = false;
                }
                lastPage = position;


            }

            @Override
            public void onPageScrollStateChanged(int state) {

                if (state == viewPager.SCROLL_STATE_IDLE) {

                    productOfferAdapter.notifyDataSetChanged();


                }


            }
        });


        offersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /* popup view on click list item */
                showOfferPopUp(position);
            }
        });
        }
        return rootView;
    }

    /*
    * method for updating carousel view when page is scrolled
    *
     */
    public void updateCarosel(int position, float positionOffset, int positionOffsetPixels) {

        if (positionOffset >= 0f && positionOffset <= 1f) {
            lastPage = arraylist.size();
            positionOffset = positionOffset * positionOffset;
            cur = productOfferAdapter.getRootView(position);
            next = productOfferAdapter.getRootView(position + 1);
            prev = productOfferAdapter.getRootView(position - 1);
            nextnext = productOfferAdapter.getRootView(position + 2);

            // ViewHelper.setAlpha(cur, maxAlpha - 0.5f * positionOffset);
            //  ViewHelper.setAlpha(next, minAlpha+0.5f*positionOffset);
            // ViewHelper.setAlpha(prev, minAlpha+0.5f*positionOffset);


            if (nextnext != null) {
                //ViewHelper.setAlpha(nextnext, minAlpha);
                ViewHelper.setRotationY(nextnext, -minDegree);
            }
            if (cur != null) {
                cur.setScaleBoth(MyLinearLayout.BIG_SCALE
                        - MyLinearLayout.DIFF_SCALE * positionOffset);

                ViewHelper.setRotationY(cur, 0);
            }

            if (next != null) {
                next.setScaleBoth(MyLinearLayout.SMALL_SCALE
                        + MyLinearLayout.DIFF_SCALE * positionOffset);
                ViewHelper.setRotationY(next, -minDegree);
            }
            if (prev != null) {
                ViewHelper.setRotationY(prev, minDegree);
            }


			/*To animate it properly we must understand swipe direction
             * this code adjusts the rotation according to direction.
			 */
            if (swipedLeft) {
                if (next != null)
                    ViewHelper.setRotationY(next, -minDegree + minDegree * positionOffset);
                if (cur != null)
                    ViewHelper.setRotationY(cur, 0 + minDegree * positionOffset);
            } else {
                if (next != null)
                    ViewHelper.setRotationY(next, -minDegree + minDegree * positionOffset);
                if (cur != null) {
                    ViewHelper.setRotationY(cur, 0 + minDegree * positionOffset);
                }
            }
        }
        if (positionOffset >= 1f) {
            // ViewHelper.setAlpha(cur, maxAlpha);
        }
    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    /*
    method for hiding and showing bottom bar on list scroll
     */
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

        if (scrollState == ScrollState.UP) {
            HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
            if (parent.bottomBar.isShown())
                parent.hideBottombarScroll();
        } else if (scrollState == ScrollState.DOWN) {
            HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
            if (!parent.bottomBar.isShown())
                parent.showBottombarScroll();

        }
    }

     /*
     carousel view fragment
      */
    public static class OfferProductFragment extends Fragment {
        int pos;
        ImageView imageProduct;
        CustomTextViewRegularFont productName, available_quantity, price;

        public static OfferProductFragment init(Context context, int pos,
                                                float scale, boolean IsBlured) {
            OfferProductFragment offerFrag = new OfferProductFragment();
            // Supply  input as an argument.

            Bundle b = new Bundle();
            b.putInt("pos", pos);
            b.putFloat("scale", scale);
            b.putBoolean("IsBlured", IsBlured);
            offerFrag.setArguments(b);
            return offerFrag;

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            pos = getArguments() != null ? getArguments().getInt("pos") : 1;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View fragView = inflater.inflate(R.layout.product_item_layout, container, false);
            imageProduct = (ImageView) fragView.findViewById(R.id.imageProduct);
            productName = (CustomTextViewRegularFont) fragView.findViewById(R.id.productName);
            available_quantity = (CustomTextViewRegularFont) fragView.findViewById(R.id.available_quantity);
            price = (CustomTextViewRegularFont) fragView.findViewById(R.id.price);
            MyLinearLayout root = (MyLinearLayout) fragView.findViewById(R.id.root);
            float scale = this.getArguments().getFloat("scale");
            root.setScaleBoth(scale);
            AQuery aq = new AQuery(fragView);

            try {

                aq.id(imageProduct)
                        .progress(R.id.centralProgressbar)
                        .image(OffersFragmentTab.arraylist.get(pos).getUserProfileImage(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }

            return fragView;


        }
    }


    public class MyOffersAdapter extends ArrayAdapter<String> {


        private LayoutInflater inflater;
        ViewHolder holder;

        private ArrayList<ProductsModel> products;
        Context context;
        Activity activity;



        public MyOffersAdapter(Activity activity, int CustomTextViewRegularFontResourceId, List<ProductsModel> arraylist) {
            super(activity, CustomTextViewRegularFontResourceId);

            this.activity = activity;
            this.products = new ArrayList<ProductsModel>();
            this.products.addAll(arraylist);


            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;


            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_related_offers, null);
                holder = new ViewHolder();
                holder.profileImage = (ImageView) convertView
                        .findViewById(R.id.imageProfile);
                holder.userName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.addressField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.address);
                holder.productModel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.productName);
                holder.productLabel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.productLabel);
                holder.userRatings = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.ratingValue);
                holder.productPrice = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.countPlus);
                holder.productAddedTime = (CustomTextViewRegularFont) convertView.findViewById(R.id.timeValue);

                holder.availableCount = (CustomTextViewRegularFont) convertView.findViewById(R.id.available_quantity);
                holder.offeredDiscount = (CustomTextViewRegularFont) convertView.findViewById(R.id.offeredDiscount);
                holder.offeredPrice = (CustomTextViewRegularFont) convertView.findViewById(R.id.offeredPrice);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            AQuery aq = new AQuery(convertView);

            try {
                aq.id(holder.profileImage)
                        .progress(R.id.imageProgressbar)
                        .image(products.get(position).getUserProfileImage(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);


//                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.profileImage);
//
//                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.productImage);
            } catch (Exception e) {

            }

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return products.size();
        }


    }


    class ViewHolder {
        ProgressBar profileProgress, productProgress;
        ImageView profileImage;
        CustomTextViewRegularFont userName, userRatings, productAddedTime, addressField, productLabel, productModel,
                productPrice, availableCount, offeredDiscount, offeredPrice;


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showOfferPopUp(int position) {
        try {
            final Dialog dialog = new Dialog(getActivity(),
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.offers_popup_layout);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);




            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final ImageView imageProfile = (ImageView) dialog.findViewById(R.id.imageProfile);
            final CustomTextViewRegularFont userName = (CustomTextViewRegularFont) dialog.findViewById(R.id.userName);
            final CustomTextViewRegularFont rating = (CustomTextViewRegularFont) dialog.findViewById(R.id.ratingValue);
            final CustomTextViewRegularFont time = (CustomTextViewRegularFont) dialog.findViewById(R.id.timeValue);
            final CustomTextViewRegularFont address = (CustomTextViewRegularFont) dialog.findViewById(R.id.address);
            final ImageView productImage = (ImageView) dialog.findViewById(R.id.cardImage);
            final CustomTextViewRegularFont productName = (CustomTextViewRegularFont) dialog.findViewById(R.id.productName);
            final CustomTextViewRegularFont offeredQuantity = (CustomTextViewRegularFont) dialog.findViewById(R.id.quantity);
            final CustomTextViewLightFont description = (CustomTextViewLightFont) dialog.findViewById(R.id.descriptionText);
            final CustomTextViewRegularFont yourPrice = (CustomTextViewRegularFont) dialog.findViewById(R.id.your_price);
            final CustomTextViewRegularFont offeredPrice = (CustomTextViewRegularFont) dialog.findViewById(R.id.offeredPrice);
            final CustomTextViewRegularFont offeredDiscount = (CustomTextViewRegularFont) dialog.findViewById(R.id.offeredDiscount);
            final CustomTextViewRegularFont decline = (CustomTextViewRegularFont) dialog.findViewById(R.id.decline);
            final CustomTextViewRegularFont accept = (CustomTextViewRegularFont) dialog.findViewById(R.id.accept);
            final LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.layout);

            layout.getBackground().setAlpha(50);
            userName.setText(myOfferAdapter.products.get(position).getUserName());
            Picasso.with(getActivity()).load(myOfferAdapter.products.get(position).getUserProfileImage()).into(imageProfile);

            Picasso.with(getActivity()).load(myOfferAdapter.products.get(position).getUserProfileImage()).into(productImage);


            decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

}