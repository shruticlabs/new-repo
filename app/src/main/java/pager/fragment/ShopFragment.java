package pager.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Helper;
import models.ProductsModel;


public class ShopFragment extends Fragment implements ObservableScrollViewCallbacks {

    FragmentManager fm;
    HomFragment parent;
    ProductsModel productsModel;
    ArrayList<ProductsModel> arraylist;
    ProductAdapter adapter;
    GridView productGridView;
    ObservableScrollView scrollView;
    int position;
    ImageView shopImage;
    CustomEditTextRegularFont searchBar;
    LinearLayout layout, results;
    CustomTextViewRegularFont resultNo;


    public static ShopFragment init(int val) {
        ShopFragment shopFragment = new ShopFragment();
        Bundle args = new Bundle();
        args.putInt("val", val);
        shopFragment.setArguments(args);

        return shopFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments() != null ? getArguments().getInt("val") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.shop_description_layout, container, false);
        productGridView = (GridView) rootView.findViewById(R.id.gridView);
        scrollView = (ObservableScrollView) rootView.findViewById(R.id.shop_ScrollView);
        shopImage = (ImageView) rootView.findViewById(R.id.image_shop);
        searchBar = (CustomEditTextRegularFont) rootView.findViewById(R.id.search_own_product);
        layout = (LinearLayout) rootView.findViewById(R.id.shop_layout);
        results = (LinearLayout) rootView.findViewById(R.id.results_found_layout);
        resultNo = (CustomTextViewRegularFont) rootView.findViewById(R.id.no_of_results);
        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.VISIBLE);
        scrollView.setScrollViewCallbacks(this);

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
        };
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};


        arraylist = new ArrayList<ProductsModel>();
        for (int i = 0; i < order_ids.length; i++) {

            productsModel = new ProductsModel(order_images[i], order_names[i], order_ids[i], i, order_images[i], order_names[i], order_names[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], false);

            // Binds all strings into an array
            arraylist.add(productsModel);
        }

        final int margin = getResources().getDimensionPixelSize(R.dimen.spacinggrid);
        adapter = new ProductAdapter(getActivity(), arraylist);
        productGridView.setAdapter(adapter);
        Helper.setGridViewHeightBasedOnChildren(productGridView, 3, margin);
        shopImage.getBackground().setAlpha(40);


        parent.onShopClick();
        parent.headerText.setText(arraylist.get(position).getUserName());
        parent.topBar.setVisibility(View.VISIBLE);
        parent.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (layout.getVisibility() == View.VISIBLE) {
                    parent.backPressed();
                    parent.fragmentContainer.setVisibility(View.GONE);
                    parent.viewPager.setVisibility(View.VISIBLE);
                    fm.popBackStackImmediate();
                }
                searchBar.getText().clear();
                results.setVisibility(View.GONE);
                layout.setVisibility(View.VISIBLE);
            }
        });

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                layout.setVisibility(View.GONE);
                results.setVisibility(View.VISIBLE);
                String text = searchBar.getText().toString().toLowerCase();
                adapter.filter(text);
                int count = adapter.getCount();
                resultNo.setText(count + " Results found");
                if (!adapter.all_products.isEmpty())
                    Helper.setGridViewHeightBasedOnChildren(productGridView, 3, margin);
            }
        });

        return rootView;
    }

    /*
    handles device back pressed
     */
    public void pressBack() {

        parent.backPressed();
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);
        layout.setVisibility(View.VISIBLE);
        results.setVisibility(View.GONE);
        searchBar.getText().clear();

    }

    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
//        if (scrollState == ScrollState.UP) {
//            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
//            if (parent.bottomBar.isShown())
//                parent.hideBottombarScroll();
//        } else if (scrollState == ScrollState.DOWN) {
//            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
//            if (!parent.bottomBar.isShown())
//                parent.showBottombarScroll();
//        }

    }

    public class ProductAdapter extends BaseAdapter {

        private List<ProductsModel> all_products = null;
        private ArrayList<ProductsModel> products;


        Activity activity;

        LayoutInflater inflater = null;


        public ProductAdapter(Activity a, List<ProductsModel> all_products) {

            this.all_products = all_products;

            this.products = new ArrayList<ProductsModel>();
            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public ProductsModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolderProducts holder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_product, null);
                holder = new ViewHolderProducts();

                holder.productImage = (ImageView) convertView
                        .findViewById(R.id.list_item_img_view);
                holder.productName = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.product_name);

                convertView.setTag(holder);


            } else {
                holder = (ViewHolderProducts) convertView.getTag();
            }


            holder.productName.setText(all_products.get(position).getUserName());


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.productImage)
                        .image("http://api.androidhive.info/json/movies/4.jpg", true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }

            return convertView;

        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            all_products.clear();
            if (charText.length() == 0) {
                all_products.addAll(products);
            } else {
                for (ProductsModel wp : products) {
                    if (wp.getUserName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_products.add(wp);
                        //           Log.i("inside filter", wp.getRowId() + "..");
                    }
                }
            }
            notifyDataSetChanged();

        }

    }

    class ViewHolderProducts {
        ImageView productImage;
        CustomTextViewRegularFont productName;
    }

}
