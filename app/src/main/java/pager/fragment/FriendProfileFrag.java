package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clabs.butlers.utils.Helper;
import clabs.butlers.utils.SpacesItemDecoration;
import models.ProfileProductsModel;
import models.PromoteUserListModel;
import models.ShopModel;


public class FriendProfileFrag extends Fragment implements ObservableScrollViewCallbacks {

    RelativeLayout backBtn;
    CustomTextViewSemiBold header;
    FragmentManager fm;
    HomFragment parent;
    CustomTextViewRegularFont followBtn, mutualConnBtn, shopsBtn, productsBtn;
    ArrayList<PromoteUserListModel> arraylist;
    ArrayList<ShopModel> shopArrayList;
    PromoteUserListModel model;
    int position;
    ShopModel shopModel;
    RecyclerView recyclerView;
    ObservableScrollView scrollView;
    RelativeLayout moreBtnProfile;
    ArrayList<PromoteUserListModel> followerArrayList;
    PromoteUserListModel followersModel;
    FollowersAdapter followersAdapter;
    Spinner spinner;


    public static FriendProfileFrag init(int val) {
        FriendProfileFrag friendProfileFrag = new FriendProfileFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        friendProfileFrag.setArguments(args);

        return friendProfileFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments() != null ? getArguments().getInt("val") : 1;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.friend_profile_layout, container, false);
        backBtn = (RelativeLayout) rootView.findViewById(R.id.backBtnMain);
        header = (CustomTextViewSemiBold) rootView.findViewById(R.id.header);
        followBtn = (CustomTextViewRegularFont) rootView.findViewById(R.id.following_btn);
        mutualConnBtn = (CustomTextViewRegularFont) rootView.findViewById(R.id.mutual_connections);
        shopsBtn = (CustomTextViewRegularFont) rootView.findViewById(R.id.shops_btn);
        productsBtn = (CustomTextViewRegularFont) rootView.findViewById(R.id.product_btn);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_grid_view);
        scrollView = (ObservableScrollView) rootView.findViewById(R.id.friend_profile_ScrollView);
        moreBtnProfile = (RelativeLayout) rootView.findViewById(R.id.more_button_profile);

        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.topBar.setVisibility(View.GONE);
        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        String[] dropdownlist = {"Block user"};
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_item, dropdownlist);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        scrollView.setScrollViewCallbacks(this);



        arraylist = new ArrayList<PromoteUserListModel>();
        shopArrayList = new ArrayList<>();

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                , "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

        for (int i = 0; i < order_ids.length; i++) {

            model = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
            // Binds all strings into an array
            arraylist.add(model);
        }

        header.setText(arraylist.get(position).getNamefield());

        for (int i = 0; i < order_ids.length; i++) {

            shopModel = new ShopModel("http://api.androidhive.info/json/movies/4.jpg", order_ids[i], order_ids[i], order_ids[i], order_ids[i]);
            // Binds all strings into an array
            shopArrayList.add(shopModel);
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (fm.popBackStackImmediate()) {
                    parent.topBar.setVisibility(View.VISIBLE);
                }
            }
        });

        followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arraylist.get(position).getIsSelected() == false) {
                    followBtn.setBackgroundResource(R.drawable.following_selected);
                    followBtn.setText("Following");
                    followBtn.setTextColor(getResources().getColor(R.color.white));
                    arraylist.get(position).setIsSelected(true);
                } else {
                    followBtn.setBackgroundResource(R.drawable.following_normal);
                    followBtn.setText("Follow");
                    followBtn.setTextColor(getResources().getColor(R.color.statusBar));
                    arraylist.get(position).setIsSelected(false);

                }
            }
        });

        mutualConnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFollowersPopup(getActivity());
            }
        });

        shopsBtn.setBackgroundResource(R.drawable.buying_normal);
        shopsBtn.setTextColor(getResources().getColor(R.color.white));

        shopsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopsBtn.setBackgroundResource(R.drawable.buying_normal);
                shopsBtn.setTextColor(getResources().getColor(R.color.white));
                productsBtn.setBackgroundResource(0);
                productsBtn.setTextColor(getResources().getColor(R.color.rowtext));
                //  parent.fragmentContainer.setVisibility(View.VISIBLE);


            }
        });

        productsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productsBtn.setBackgroundResource(R.drawable.buying_normal);
                productsBtn.setTextColor(getResources().getColor(R.color.white));
                shopsBtn.setBackgroundResource(0);
                shopsBtn.setTextColor(getResources().getColor(R.color.rowtext));
//                FriendProductFrag friendProductFrag = new FriendProductFrag();
//                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(parent.fragmentContainer.getId(), friendProductFrag);
//                ft.addToBackStack(friendProductFrag.getClass().getName());
//                ft.commit();


            }
        });


        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.horizontal_spacing);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        final ShopRecyclerAdapter shopRecyclerAdapter = new ShopRecyclerAdapter(getActivity(), shopArrayList);
        int margin = getResources().getDimensionPixelSize(R.dimen.spacing_large);
        recyclerView.setAdapter(shopRecyclerAdapter);
        Helper.setRecyclerGridViewHeightBasedOnChildren(recyclerView, 3, margin);


        return rootView;
    }
/*
observable scrollview implementation
 */
    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {


        if (scrollState == ScrollState.UP) {
            HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
            if (parent.bottomBar.isShown())
                parent.hideBottombarScroll();
        } else if (scrollState == ScrollState.DOWN) {
            HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
            if (!parent.bottomBar.isShown())
                parent.showBottombarScroll();
        }
    }


    public class CustomSpinnerAdapter extends ArrayAdapter<String> {

        String[] strings;

        public CustomSpinnerAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
            this.strings = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, null);

            TextView label = (TextView) row.findViewById(R.id.spinner_text);
            label.setText("");
            return row;

        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, null);

            TextView label = (TextView) row.findViewById(R.id.spinner_text);
            label.setText(strings[position]);
            return row;
        }


    }

    public class ShopRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ShopModel> shopItemList;
        private Context mContext;


        public ShopRecyclerAdapter(Context context, List<ShopModel> shopItemList) {
            this.shopItemList = shopItemList;
            this.mContext = context;

        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myshops, null);
            ShopListRowHolder holder = new ShopListRowHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ShopModel shopItem = shopItemList.get(position);
            Picasso.with(mContext).load("http://api.androidhive.info/json/movies/4.jpg").into(((ShopListRowHolder) holder).shopImage);
            ((ShopListRowHolder) holder).shopType.setText(Html.fromHtml(shopItem.getShopType()));

        }


        @Override
        public int getItemCount() {
            return (null != shopItemList ? shopItemList.size() : 0);
        }
    }


    class ShopListRowHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        ImageView shopImage;
        TextView shopType, quantity;


        public ShopListRowHolder(View convertView) {
            super(convertView);
            this.layout = (LinearLayout) convertView.findViewById(R.id.addPrmoteListRow);

            this.shopImage = (ImageView) convertView
                    .findViewById(R.id.list_item_img_view);
            this.shopType = (TextView) convertView
                    .findViewById(R.id.shopType);

            this.quantity = (TextView) convertView
                    .findViewById(R.id.noofshops);

        }
    }


    public void pressBack() {
        parent.topBar.setVisibility(View.VISIBLE);

    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showFollowersPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.mutual_connection_layout);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            final ListView followersListView = (ListView) dialog.findViewById(R.id.mutualList);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backBtn);
            CustomTextViewSemiBold header = (CustomTextViewSemiBold) dialog.findViewById(R.id.popupheader);
            header.setText("Mutual Connections");

            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    , "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

            followerArrayList = new ArrayList<PromoteUserListModel>();

            for (int i = 0; i < order_ids.length; i++) {

                followersModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
                // Binds all strings into an array
                followerArrayList.add(followersModel);
            }
            followersAdapter = new FollowersAdapter((getActivity()), followerArrayList);


            followersListView.setAdapter(followersAdapter);


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public class FollowersAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_followers = null;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolderFollowers holder;

        // Typeface face, face1;

        public FollowersAdapter(Activity a, List<PromoteUserListModel> all_comments) {

            this.all_followers = all_comments;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_followers.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_followers.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolderFollowers();
                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.followBtn = (CustomTextViewRegularFont) convertView.findViewById(R.id.selectBtn);
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderFollowers) convertView.getTag();

            }

            if (all_followers.get(position).getIsSelected() == false) {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.followBtn.setText("Follow");
                holder.followBtn.setTextColor(getResources().getColor(R.color.statusBar));
            } else {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selected);
                holder.followBtn.setText("Following");
                holder.followBtn.setTextColor(getResources().getColor(R.color.white));


            }
            //   holder.header.setText("FOLLOWERS");
            holder.nameField.setText(all_followers.get(position).getNamefield());

            holder.followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (all_followers.get(position).getIsSelected() == true) {
                        all_followers.get(position).setIsSelected(false);
                    } else {
                        all_followers.get(position).setIsSelected(true);
                    }

                    notifyDataSetChanged();
                }

            });


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_followers.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }
            return convertView;

        }

    }

    class ViewHolderFollowers {

        //   RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField, followBtn;
        TextView header;


    }
}


