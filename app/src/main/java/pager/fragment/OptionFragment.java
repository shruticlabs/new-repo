package pager.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clicklabs.app.butlers.HomeActivity;

public class OptionFragment extends Fragment implements ObservableScrollViewCallbacks {

    FragmentManager fm;
    HomFragment parent;
    CustomTextViewRegularFont findInviteTab, termsOfService, butlerHelp, reportProblem, privacyPolicy, signOut, promotedProducts, deleteAccount;
    ObservableScrollView scrollView;

    static OptionFragment init(int val) {
        OptionFragment optionFragment = new OptionFragment();
        Bundle args = new Bundle();
        args.putInt("val", val);
        optionFragment.setArguments(args);

        return optionFragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.options_layout, container, false);
        fm = getFragmentManager();
        parent = (HomFragment) fm.findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.VISIBLE);
        findInviteTab = (CustomTextViewRegularFont) rootView.findViewById(R.id.find_and_invite_row);
        promotedProducts = (CustomTextViewRegularFont) rootView.findViewById(R.id.promoted_products_row);
        termsOfService = (CustomTextViewRegularFont) rootView.findViewById(R.id.terms_of_service_row);
        butlerHelp = (CustomTextViewRegularFont) rootView.findViewById(R.id.butlers_help_center_row);
        reportProblem = (CustomTextViewRegularFont) rootView.findViewById(R.id.report_problem_row);
        privacyPolicy = (CustomTextViewRegularFont) rootView.findViewById(R.id.privacy_policy_row);
        signOut = (CustomTextViewRegularFont) rootView.findViewById(R.id.sign_out);
        deleteAccount = (CustomTextViewRegularFont) rootView.findViewById(R.id.delete_acnt);
        scrollView = (ObservableScrollView) rootView.findViewById(R.id.option_scrollView);
        scrollView.setScrollViewCallbacks(this);
        scrollView.setTouchInterceptionViewGroup((ViewGroup) getActivity().findViewById(R.id.add_shop_fragment_container));

        parent.onOptionsClick();
        parent.topBar.setVisibility(View.VISIBLE);
        /*
        top bar customisation in optionFragment
       */
        parent.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fm.popBackStackImmediate()) {
                    parent.backPressed();
                    parent.fragmentContainer.setVisibility(View.GONE);
                    parent.viewPager.setVisibility(View.VISIBLE);
                }
            }
        });

        findInviteTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FindInviteFragment findInviteFragment = new FindInviteFragment();
                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), findInviteFragment);
                ft.setCustomAnimations(R.anim.fragment_left_in,R.anim.fragment_right_out);
                ft.addToBackStack(findInviteFragment.getClass().getName());
                ft.commit();

            }
        });


        promotedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PromotedProductsFrag promotedProductsFrag = new PromotedProductsFrag();
                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), promotedProductsFrag);
                ft.addToBackStack(promotedProductsFrag.getClass().getName());
                ft.commit();

            }
        });

        termsOfService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                InfoFragment infoFragment = new InfoFragment();
                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), infoFragment);
                ft.addToBackStack(infoFragment.getClass().getName());
                ft.commit();
            }
        });


        butlerHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        reportProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportProblemPopup(getActivity());

            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOutPoup(getActivity(), true);
            }
        });

        deleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signOutPoup(getActivity(), false);

            }
        });


        return rootView;
    }

/*
handles device back pressed
 */
    public void pressBack() {
        parent.backPressed();
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void signOutPoup(Activity activity, Boolean signOut) {
        final CustomTextViewRegularFont cameraBtn, galleryBtn;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.gallery_camera_popup);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            CustomTextViewSemiBold textHead = (CustomTextViewSemiBold) dialog.findViewById(R.id.headerLabel);
            cameraBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.camerabtn);
            galleryBtn = (CustomTextViewRegularFont) dialog.findViewById(R.id.galleryBtn);

            if (signOut) {
                textHead.setText(getResources().getString(R.string.signout));
                cameraBtn.setText(getResources().getString(R.string.cancel_label));
                galleryBtn.setText(getResources().getString(R.string.sign_out_label));
            } else {

                textHead.setText(getResources().getString(R.string.delete_account));
                cameraBtn.setText(getResources().getString(R.string.cancel_label));
                galleryBtn.setText(getResources().getString(R.string.delet_acnt_label));

            }


            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            galleryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();


                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void reportProblemPopup(Activity activity) {
        final CustomTextViewRegularFont reportSpam, sendFeedBack, reportProblem;
        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.report_problem);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);


            reportSpam = (CustomTextViewRegularFont) dialog.findViewById(R.id.report_spam_row);
            sendFeedBack = (CustomTextViewRegularFont) dialog.findViewById(R.id.send_feedback_row);
            reportProblem = (CustomTextViewRegularFont) dialog.findViewById(R.id.report_problem_popup_row);


            reportSpam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            sendFeedBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });

            reportProblem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();


                }
            });


            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    /*
    method implementing observable scrollview
     */
    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

        if (scrollState == ScrollState.UP) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (parent.bottomBar.isShown())
                parent.hideBottombarScroll();
        } else if (scrollState == ScrollState.DOWN) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (!parent.bottomBar.isShown())
                parent.showBottombarScroll();
        }

    }
}
