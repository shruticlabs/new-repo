package pager.fragment;
 /*
 created by shruti
  */

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomTextViewLightFont;
import clabs.butlers.utils.CustomTextViewMeadiumFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewRegularItalicFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Helper;
import clabs.butlers.utils.ParallaxPageTransformer;
import clabs.butlers.utils.SpacesItemDecoration;
import material.animations.MaterialDesignAnimations;
import models.CommentModel;
import models.ProfileProductsModel;
import models.PromoteProductsModel;
import models.PromoteUserListModel;
import models.ShopModel;


public class ProfileFragmentTab extends Fragment implements ObservableScrollViewCallbacks {


    RecyclerView recyclerViewList, recyclerViewGrid;
    CustomTextViewRegularFont myShops, myOrders, myProducts, editProfile;
    //    MyShopAdapter myShopAdapter;
//    MyProductsAdapter myProductsAdapter;
    FollowersAdapter followersAdapter;
    FollowingAdapter followingAdapter;
    int ITEMS = 2;
    ArrayList<ProfileProductsModel> arraylist;
    public ArrayList<ShopModel> shopArraylist;
    ArrayList<PromoteUserListModel> followerArrayList, followingArrayList;
    RelativeLayout gridViewLayout, listViewLayout;
    Button okButton;
    ProfileProductsModel ProfileProductsModel;
    ShopModel shopModel;
    PromoteUserListModel followersModel, followingModel;
    ImageView userProfileImage;
    LinearLayout followersLayout, followingLayout, mainLayout;
    ViewPager viewPagerProducts;
    PagerSlidingTabStrip tabStrip;
    FragmentManager fm;
    HomFragment parent;
    ObservableScrollView scrollView;
    public ShopRecyclerAdapter shopRecyclerAdapter;
    public int margin;

    public static ProfileFragmentTab init(int val) {
        ProfileFragmentTab profileFragmentTab = new ProfileFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        profileFragmentTab.setArguments(args);

        return profileFragmentTab;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile_layout, container, false);
        mainLayout = (LinearLayout) rootView.findViewById(R.id.main_layout);
        gridViewLayout = (RelativeLayout) rootView.findViewById(R.id.grid_view_layout);
        listViewLayout = (RelativeLayout) rootView.findViewById(R.id.list_view_layout);
        followersLayout = (LinearLayout) rootView.findViewById(R.id.followers_layout);
        followingLayout = (LinearLayout) rootView.findViewById(R.id.following_layout);
        recyclerViewGrid = (RecyclerView) rootView.findViewById(R.id.recycler_grid_view);
        recyclerViewList = (RecyclerView) rootView.findViewById(R.id.recycler_list_view);
        viewPagerProducts = (ViewPager) rootView.findViewById(R.id.productPager);
        tabStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);


        userProfileImage = (ImageView) rootView.findViewById(R.id.userImage);
        myShops = (CustomTextViewRegularFont) rootView.findViewById(R.id.my_shops);
        myOrders = (CustomTextViewRegularFont) rootView.findViewById(R.id.my_orders);
        myProducts = (CustomTextViewRegularFont) rootView.findViewById(R.id.my_products);
        editProfile = (CustomTextViewRegularFont) rootView.findViewById(R.id.edit_profile);
        scrollView=(ObservableScrollView)rootView.findViewById(R.id.profile_ScrollView);
        scrollView.setScrollViewCallbacks(this);
        myShops.setBackgroundResource(R.drawable.buying_normal);
        myShops.setTextColor(getResources().getColor(R.color.white));

        arraylist = new ArrayList<ProfileProductsModel>();

        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"
                , "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg",
                "http://api.androidhive.info/json/movies/4.jpg"};
        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

        for (int i = 0; i < order_ids.length; i++) {

            ProfileProductsModel = new ProfileProductsModel(order_images[i], order_names[i], order_ids[i], i, order_images[i], order_names[i], order_names[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], order_ids[i], order_images[i], order_names[i], false);
            // Binds all strings into an array
            arraylist.add(ProfileProductsModel);
        }

        shopArraylist = new ArrayList<ShopModel>();

        for (int i = 0; i < order_ids.length; i++) {

            shopModel = new ShopModel("http://api.androidhive.info/json/movies/4.jpg", order_ids[i], order_ids[i], order_ids[i], order_ids[i]);
            // Binds all strings into an array
            shopArraylist.add(shopModel);
        }


        recyclerViewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        final OrdersRecyclerAdapter ordersAdapter = new OrdersRecyclerAdapter(getActivity(), arraylist);




        recyclerViewGrid.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.horizontal_spacing);
        recyclerViewGrid.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        shopRecyclerAdapter = new ShopRecyclerAdapter(getActivity(), shopArraylist);
        margin=getResources().getDimensionPixelSize(R.dimen.spacing);
        recyclerViewGrid.setAdapter(shopRecyclerAdapter);
        Helper.setRecyclerGridViewHeightBasedOnChildren(recyclerViewGrid, 3,margin);


        ProfilePagerAdapter profilePagerAdapter = new ProfilePagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerProducts.setAdapter(profilePagerAdapter);
        viewPagerProducts.setPageTransformer(true, new ParallaxPageTransformer());
        viewPagerProducts.setOffscreenPageLimit(ITEMS);
        tabStrip.setViewPager(viewPagerProducts);
        recyclerViewList.setAdapter(ordersAdapter);
        Helper.getRecyclerViewSize(recyclerViewList);
        listViewLayout.setVisibility(View.GONE);


        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDesignAnimations animations=new MaterialDesignAnimations();

                parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");

                EditProfileFragment editProfileFragment = new EditProfileFragment();
                android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(parent.fragmentContainer.getId(), editProfileFragment);
                ft.addToBackStack(editProfileFragment.getClass().getName());
                ft.commit();


            }
        });


        myShops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myShops.setTextColor(getResources().getColor(R.color.white));
                myShops.setBackgroundResource(R.drawable.buying_normal);
                gridViewLayout.setVisibility(View.VISIBLE);
                listViewLayout.setVisibility(View.GONE);
                myOrders.setTextColor(getResources().getColor(R.color.rowtext));
                myProducts.setTextColor(getResources().getColor(R.color.rowtext));
                myOrders.setBackgroundResource(0);
                myProducts.setBackgroundResource(0);


            }
        });

        myOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myOrders.setTextColor(getResources().getColor(R.color.white));
                listViewLayout.setVisibility(View.VISIBLE);
                gridViewLayout.setVisibility(View.GONE);
                myShops.setTextColor(getResources().getColor(R.color.rowtext));
                myProducts.setTextColor(getResources().getColor(R.color.rowtext));
                myShops.setBackgroundResource(0);
                myProducts.setBackgroundResource(0);
                myOrders.setBackgroundResource(R.drawable.buying_normal);

            }
        });

        myProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myProducts.setBackgroundResource(R.drawable.buying_normal);
                myProducts.setTextColor(getResources().getColor(R.color.white));
                myOrders.setTextColor(getResources().getColor(R.color.rowtext));
                myShops.setTextColor(getResources().getColor(R.color.rowtext));
                myOrders.setBackgroundResource(0);
                myShops.setBackgroundResource(0);
                mainLayout.setVisibility(View.GONE);
                viewPagerProducts.setVisibility(View.VISIBLE);
                tabStrip.setVisibility(View.VISIBLE);
                fm = getFragmentManager();
                parent = (HomFragment) fm.findFragmentByTag("HomFragment");

                parent.onProductsClick();

                parent.backBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPagerProducts.setVisibility(View.GONE);
                        tabStrip.setVisibility(View.GONE);
                        mainLayout.setVisibility(View.VISIBLE);
                        parent.backPressed();
                        myShops.setBackgroundResource(R.drawable.buying_normal);
                        myShops.setTextColor(getResources().getColor(R.color.white));
                        myOrders.setTextColor(getResources().getColor(R.color.rowtext));
                        myProducts.setTextColor(getResources().getColor(R.color.rowtext));
                        myOrders.setBackgroundResource(0);
                        myProducts.setBackgroundResource(0);
                        gridViewLayout.setVisibility(View.VISIBLE);
                        listViewLayout.setVisibility(View.GONE);
                    }
                });


            }
        });
        followersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFollowersPopup(getActivity());
            }
        });

        followingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFollowingPopup(getActivity());
            }
        });

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

/*
        listview adapter for user's orders
 */
//    public class MyProductsAdapter extends ArrayAdapter<String> {
//
//
//        private LayoutInflater inflater;
//        ViewHolder holder;
//
//        private ArrayList<ProfileProductsModel> products;
//        Context context;
//        Activity activity;
//
//
//
//        //        public ProductsAdapter(Context context, int CustomTextViewRegularFontResourceId, List<ProfileProductsModel> list) {
//        public MyProductsAdapter(Activity activity, int CustomTextViewRegularFontResourceId, List<ProfileProductsModel> arraylist) {
//            super(activity, CustomTextViewRegularFontResourceId);
//
//            this.activity=activity;
//            this.products = new ArrayList<ProfileProductsModel>();
//            this.products.addAll(arraylist);
//
//
//            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        }
//
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            final ViewHolder holder;
//
//
//            if (convertView == null) {
//                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_myproducts, null);
//                holder = new ViewHolder();
//                holder.profileImage = (ImageView) convertView
//                        .findViewById(R.id.imageProfile);
//                holder.productImage = (ImageView) convertView
//                        .findViewById(R.id.imageProduct);
//                holder.userName = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.userName);
//                holder.addressField = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.address_seller);
//                holder.productModel = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.productName);
//                holder.productLabel = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.productLabel);
//                holder.userRatings = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.userRatingValue);
//                holder.productPrice = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.countPlus);
//                holder.ratingLayout = (RelativeLayout) convertView
//                        .findViewById(R.id.ratingLayout);
//                holder.productPurchasedTime = (CustomTextViewRegularFont) convertView
//                        .findViewById(R.id.timeValue);
//                holder.callBtn=(Button)convertView.findViewById(R.id.btn_call);
//                holder.msgBtn=(Button)convertView.findViewById(R.id.btn_msg);
//                holder.okButton=(Button)convertView.findViewById(R.id.ok_btn);
//                holder.availableCount=(CustomTextViewRegularFont)convertView.findViewById(R.id.available_quantity);
//                holder.offeredDiscount=(CustomTextViewRegularFont)convertView.findViewById(R.id.offeredDiscount);
//                holder.offeredPrice=(CustomTextViewRegularFont)convertView.findViewById(R.id.offeredPrice);
//
//                convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
//
//            if (products.get(position).getIsShownLayout()==false) {
//                holder.ratingLayout.setVisibility(View.GONE);
//            }
//            else {
//                holder.ratingLayout.setVisibility(View.VISIBLE);
//            }
//
//            AQuery aq = new AQuery(convertView);
//
//            try{
//                aq.id(holder.profileImage)
//                        .progress(R.id.imageProgressbar)
//                        .image(products.get(position).getUserProfileImage(), true, true,
//                                0, 0, null, 0, AQuery.FADE_IN);
//                aq.id(holder.productImage)
//                        .progress(R.id.centralProgressbar)
//                        .image(products.get(position).getUserProfileImage(), true, true,
//                                0, 0, null, 0, AQuery.FADE_IN);
//
////                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.profileImage);
////
////                Picasso.with(getActivity()).load(products.get(position).getUserProfileImage()).into(holder.productImage);
//            }catch(Exception e){
//
//            }
//
//            return convertView;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public int getCount() {
//            return products.size();
//        }
//
//
//    }
//
//
//    class ViewHolder {
//        ProgressBar profileProgress,productProgress;
//        ImageView profileImage,productImage;
//        CustomTextViewRegularFont userName, userRatings, productAddedTime,addressField,productLabel,productModel,
//                productPrice,productPurchasedTime,availableCount,offeredDiscount,offeredPrice;
//
//        RelativeLayout ratingLayout;
//        Button okButton, callBtn,msgBtn;
//
//
//    }

/*
         adapter for grid of shops

 */
//    public class MyShopAdapter extends BaseAdapter {
//
//        private List<ShopModel> all_products = null;
//        private ArrayList<ShopModel> products;
//
//        Activity activity;
//
//        LayoutInflater inflater = null;
//        boolean check[];
//
//        ViewHolder holder;
//
//        // Typeface face, face1;
//
//        public MyShopAdapter(Activity a, List<ShopModel> all_products) {
//
//            this.all_products = all_products;
//
////            this.products = new ArrayList<ShopModel>();
////            this.products.addAll(all_products);
//            activity = a;
//
//            inflater = (LayoutInflater) activity
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        }
//
//        @Override
//        public int getCount() {
//            return all_products.size();
//        }
//
//        @Override
//        public ShopModel getItem(int position) {
//            return all_products.get(position);
//        }
//
//        public long getItemId(int position) {
//            return position;
//        }
//
//        public View getView(final int position, View convertView,
//                            ViewGroup parent) {
//            final ViewHolder holder;
//
//            if (convertView == null) {
//                convertView = getActivity().getLayoutInflater().inflate(
//                        R.layout.list_item_myshops, null);
//                holder = new ViewHolder();
//                holder.layout = (LinearLayout) convertView.findViewById(R.id.addPrmoteListRow);
//
//                holder.shopImage = (ImageView) convertView
//                        .findViewById(R.id.list_item_img_view);
//                holder.shopType = (TextView) convertView
//                        .findViewById(R.id.shopType);
//
//                holder.quantity = (TextView) convertView
//                        .findViewById(R.id.noofshops);
//                convertView.setTag(holder);
//
//
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
//
//
//            holder.shopType.setText(all_products.get(position).getShopType());
//
//
//            AQuery aq = new AQuery(convertView);
//
//            try {
//
//                aq.id(holder.shopImage)
//                        .image("http://api.androidhive.info/json/movies/4.jpg", true, true,
//                                0, 0, null, 0, AQuery.FADE_IN);
//            } catch (Exception e) {
//
//            }
//
//
//            holder.layout.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//
//
//                }
//
//            });
//            return convertView;
//
//        }
//
//
//        class ViewHolder {
//
//            LinearLayout layout;
//            ImageView shopImage;
//            TextView shopType, quantity;
//
//
//        }
//    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showFollowersPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.followers_layout);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            final ListView followersListView = (ListView) dialog.findViewById(R.id.followerList);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backBtnMain);
            CustomTextViewSemiBold header = (CustomTextViewSemiBold) dialog.findViewById(R.id.header);
            header.setText("Followers");

            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    , "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

            followerArrayList = new ArrayList<PromoteUserListModel>();

            for (int i = 0; i < order_ids.length; i++) {

                followersModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
                // Binds all strings into an array
                followerArrayList.add(followersModel);
            }
            followersAdapter = new FollowersAdapter((getActivity()), followerArrayList);


            followersListView.setAdapter(followersAdapter);


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }


    /*
    methods implementing obsevable scroll view handling hiding and viewing bottom bar

     */
    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

        if (scrollState == ScrollState.UP) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (parent.bottomBar.isShown())
                parent.hideBottombarScroll();
        } else if (scrollState == ScrollState.DOWN) {
            HomFragment parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
            if (!parent.bottomBar.isShown())
                parent.showBottombarScroll();
        }

    }


    public class FollowersAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_followers = null;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolderFollowers holder;

        // Typeface face, face1;

        public FollowersAdapter(Activity a, List<PromoteUserListModel> all_comments) {

            this.all_followers = all_comments;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_followers.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_followers.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolderFollowers();
                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.followBtn = (CustomTextViewRegularFont) convertView.findViewById(R.id.selectBtn);
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderFollowers) convertView.getTag();

            }

            if (all_followers.get(position).getIsSelected() == false) {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.followBtn.setText("Follow");
                holder.followBtn.setTextColor(getResources().getColor(R.color.statusBar));
            } else {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selected);
                holder.followBtn.setText("Following");
                holder.followBtn.setTextColor(getResources().getColor(R.color.white));


            }
            //   holder.header.setText("FOLLOWERS");
            holder.nameField.setText(all_followers.get(position).getNamefield());

            holder.followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (all_followers.get(position).getIsSelected() == true) {
                        all_followers.get(position).setIsSelected(false);
                    } else {
                        all_followers.get(position).setIsSelected(true);
                    }

                    notifyDataSetChanged();
                }

            });


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_followers.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }
            return convertView;

        }

    }

    class ViewHolderFollowers {

        //   RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField, followBtn;
        TextView header;


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showFollowingPopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.setContentView(R.layout.followers_layout);
            if (Config.isLollyPop())
                dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            final ListView followingListView = (ListView) dialog.findViewById(R.id.followerList);
            RelativeLayout backBtn = (RelativeLayout) dialog.findViewById(R.id.backBtnMain);
            CustomTextViewSemiBold header = (CustomTextViewSemiBold) dialog.findViewById(R.id.header);
            header.setText("Following");

            String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"
                    , "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg",
                    "http://api.androidhive.info/json/movies/4.jpg"};
            String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};

            followingArrayList = new ArrayList<PromoteUserListModel>();

            for (int i = 0; i < order_ids.length; i++) {

                followersModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i], false);
                // Binds all strings into an array
                followingArrayList.add(followersModel);
            }
            followingAdapter = new FollowingAdapter((getActivity()), followingArrayList);


            followingListView.setAdapter(followingAdapter);


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public class FollowingAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_following = null;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolderFollowers holder;

        // Typeface face, face1;

        public FollowingAdapter(Activity a, List<PromoteUserListModel> all_following) {

            this.all_following = all_following;

//            this.products = new ArrayList<CommentModel>();
//            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_following.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_following.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolderFollowers();
                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.followBtn = (CustomTextViewRegularFont) convertView.findViewById(R.id.selectBtn);
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderFollowers) convertView.getTag();

            }

            holder.followBtn.setBackgroundResource(R.drawable.follow_selected);
            holder.followBtn.setText("Following");
            holder.followBtn.setTextColor(getResources().getColor(R.color.white));


            //   holder.header.setText("FOLLOWING");
            holder.nameField.setText(all_following.get(position).getNamefield());

//            holder.followBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                    if (all_following.get(position).getIsSelected()==true) {
//                        all_following.get(position).setIsSelected(false);
//                    }
//                    else
//                    {
//                        all_following.get(position).setIsSelected(true);
//                    }
//
//                    notifyDataSetChanged();
//                }
//
//            });


            AQuery aq = new AQuery(convertView);

            try {

                aq.id(holder.img)
                        .image(all_following.get(position).getImagefield(), true, true,
                                0, 0, null, 0, AQuery.FADE_IN);
            } catch (Exception e) {

            }
            return convertView;

        }

    }

    /*
    recycler view adapter for orders
     */
    public class OrdersRecyclerAdapter extends RecyclerView.Adapter<OrdersListRowHolder> {

        private List<ProfileProductsModel> ordersItemList;
        private Context mContext;

        public OrdersRecyclerAdapter(Context context, List<ProfileProductsModel> orderItemList) {
            this.ordersItemList = orderItemList;
            this.mContext = context;


            // Set up the animation

        }


        @Override
        public OrdersListRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myproducts, null);
            OrdersListRowHolder holder = new OrdersListRowHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(final OrdersListRowHolder ordersListRowHolder, int position) {

            final ProfileProductsModel orderItem = ordersItemList.get(position);
            Picasso.with(mContext).load(orderItem.getProductImage()).into(ordersListRowHolder.productImage);
            Picasso.with(mContext).load(orderItem.getUserProfileImage()).into(ordersListRowHolder.profileImage);

            ordersListRowHolder.starBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ordersListRowHolder.ratingLayout.setVisibility(View.VISIBLE);
                }
            });

            ordersListRowHolder.okButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ordersListRowHolder.ratingLayout.setVisibility(View.GONE);
                }
            });



//            if (orderItem.getIsShownLayout()==true) {
//                ordersListRowHolder.ratingLayout.setVisibility(View.VISIBLE);
//            }
//            else {
//                ordersListRowHolder.ratingLayout.setVisibility(View.GONE);
//            }

        }

        @Override
        public int getItemCount() {
            return (null != ordersItemList ? ordersItemList.size() : 0);
        }
    }

    class OrdersListRowHolder extends RecyclerView.ViewHolder {
        ProgressBar profileProgress, productProgress;
        ImageView profileImage, productImage;
        CustomTextViewRegularFont userName, userRatings, productAddedTime, addressField, productLabel, productModel,
                productPrice, productPurchasedMonth,productPurchasedDate,productPurchasedYear, availableCount, offeredDiscount, offeredPrice;
        LinearLayout rateProduct;
        RelativeLayout ratingLayout, okButton;
        Button  callBtn, msgBtn, starBtn;


        public OrdersListRowHolder(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView
                    .findViewById(R.id.imageProfile);
            this.productImage = (ImageView) convertView
                    .findViewById(R.id.imageProduct);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);
            this.addressField = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.address_seller);
            this.productModel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productName);
            this.productLabel = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.productLabel);
            this.userRatings = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userRatingValue);
            this.productPrice = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.countPlus);
            this.ratingLayout = (RelativeLayout) convertView
                    .findViewById(R.id.ratingLayout);
            this.productPurchasedMonth = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.month);
            this.productPurchasedDate = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.date);
            this.productPurchasedYear = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.year);
            this.callBtn = (Button) convertView.findViewById(R.id.btn_call);
            this.msgBtn = (Button) convertView.findViewById(R.id.btn_msg);
            this.okButton = (RelativeLayout) convertView.findViewById(R.id.ok_btn);
            this.starBtn = (Button) convertView.findViewById(R.id.star_btn);
            this.availableCount = (CustomTextViewRegularFont) convertView.findViewById(R.id.available_quantity);
            this.offeredDiscount = (CustomTextViewRegularFont) convertView.findViewById(R.id.offeredDiscount);
            this.offeredPrice = (CustomTextViewRegularFont) convertView.findViewById(R.id.offeredPrice);
            this.rateProduct=(LinearLayout)convertView.findViewById(R.id.rate_product);

        }

    }

    /*
    recycler view adapter for shops
     */

    public class ShopRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ShopModel> shopItemList;
        private Context mContext;
        private static final int TYPE_FOOTER = 0;
        private static final int TYPE_ITEM = 1;

        public ShopRecyclerAdapter(Context context, List<ShopModel> shopItemList) {
            this.shopItemList = shopItemList;
            this.mContext = context;


            // Set up the animation

        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_ITEM) {
                //inflate your layout and pass it to view holder
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myshops, null);
                return new ShopListRowHolder(v);
            } else if (viewType == TYPE_FOOTER) {
                //inflate your layout and pass it to view holder
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_add_shop, null);
                return new VHFooter(v);
            }

            throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
//            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_myshops, null);
//            ShopListRowHolder holder = new ShopListRowHolder(v);
//            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            if (holder instanceof ShopListRowHolder) {
                // String dataItem = getItem(position);
                //cast holder to VHItem and set data
                final ShopModel shopItem = shopItemList.get(position);
                Picasso.with(mContext).load("http://api.androidhive.info/json/movies/4.jpg").into(((ShopListRowHolder) holder).shopImage);
                ((ShopListRowHolder) holder).shopType.setText(Html.fromHtml(shopItem.getShopType()));
                parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                ((ShopListRowHolder) holder).shopImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ShopFragment shopFragment = new ShopFragment();
                        shopFragment.init(position);
                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().add(R.id.add_shop_fragment_container, shopFragment);
                        ft.addToBackStack(shopFragment.getClass().getName());
                        ft.commit();
                    }
                });

            } else if (holder instanceof VHFooter) {
                //cast holder to VHHeader and set data for header.
                ((VHFooter) holder).addShopBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       parent.topBar.setVisibility(View.GONE);
                        parent.bottomBar.setVisibility(View.GONE);
                      parent.fragmentContainer.setVisibility(View.VISIBLE);
                        AddShopFragment addShopFragment = new AddShopFragment();
                        android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction().replace(parent.fragmentContainer.getId(), addShopFragment);
                        ft.addToBackStack(addShopFragment.getClass().getName());
                        ft.commit();


                    }
                });
            }


            // Picasso.with(mContext).load("http://api.androidhive.info/json/movies/4.jpg").into(holder.shopImage);

        }

        @Override
        public int getItemCount() {
            return (null != shopItemList ? shopItemList.size() + 1 : 0);
        }

        @Override
        public int getItemViewType(int position) {
            if (isPositionFooter(position))
                return TYPE_FOOTER;

            return TYPE_ITEM;
        }

        private boolean isPositionFooter(int position) {
            return position == getItemCount() - 1;
        }

    }


    class VHFooter extends RecyclerView.ViewHolder {
        CustomTextViewRegularFont addShopBtn;

        public VHFooter(View itemView) {
            super(itemView);
            this.addShopBtn = (CustomTextViewRegularFont) itemView.findViewById(R.id.add_new_shop);
        }
    }


    class ShopListRowHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        ImageView shopImage;
        TextView shopType, quantity;


        public ShopListRowHolder(View convertView) {
            super(convertView);
            this.layout = (LinearLayout) convertView.findViewById(R.id.addPrmoteListRow);

            this.shopImage = (ImageView) convertView
                    .findViewById(R.id.list_item_img_view);
            this.shopType = (TextView) convertView
                    .findViewById(R.id.shopType);

            this.quantity = (TextView) convertView
                    .findViewById(R.id.noofshops);

        }
    }

 /*
 fragment adapter for  my products
  */
    public class ProfilePagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{getResources().getString(R.string.buying), getResources().getString(R.string.selling)};

        public ProfilePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return ProfileBuyingFrag.init(position,false);
                case 1:
                    return new ProfileBuyingFrag().init(position,false);
                default:// Fragment # 2-9 - Will show list
                    return new ProfileBuyingFrag().init(position,false);

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }

    }
}

