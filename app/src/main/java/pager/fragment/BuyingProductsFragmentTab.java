package pager.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clicklabs.butlers.R;

/**
 * Created by click103 on 12/3/15.
 */
public class BuyingProductsFragmentTab extends Fragment {

    static BuyingProductsFragmentTab init(int val) {
        BuyingProductsFragmentTab profileFragmentTab = new BuyingProductsFragmentTab();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        profileFragmentTab.setArguments(args);

        return profileFragmentTab;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.product_buying, container, false);
        TextView cardPayment;
        RelativeLayout addPromoterRow;

        cardPayment=(TextView) rootView.findViewById(R.id.cardPayment);
        addPromoterRow=(RelativeLayout) rootView.findViewById(R.id.addPromoterRow);

        cardPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        addPromoterRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return rootView;
    }
}
