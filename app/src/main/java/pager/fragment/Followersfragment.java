package pager.fragment;

        import android.app.Activity;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AbsListView;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.FrameLayout;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import com.androidquery.AQuery;
        import com.example.clicklabs.butlers.R;
        import com.loopj.android.http.AsyncHttpClient;
        import com.loopj.android.http.AsyncHttpResponseHandler;
        import com.loopj.android.http.RequestParams;
        import com.squareup.picasso.Picasso;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;
        import java.util.ArrayList;
        import java.util.List;
        import clabs.butlers.utils.ApiResponseFlags;
        import clabs.butlers.utils.AppStatus;
        import clabs.butlers.utils.CircleTransform;
        import clabs.butlers.utils.Config;
        import clabs.butlers.utils.CustomEditTextRegularFont;
        import clabs.butlers.utils.CustomTextViewRegularFont;
        import clabs.butlers.utils.CustomTextViewSemiBold;
        import clabs.butlers.utils.Data;
        import material.animations.MaterialDesignAnimations;
        import models.PromoteUserListModel;

/**
 * Created by clicklabs107 on 4/23/15.
 */
public class Followersfragment extends Fragment {

    LinearLayout errorLayout;
    RelativeLayout backBtn;
    CustomTextViewSemiBold header, footer;
    MaterialDesignAnimations animations;
    HomFragment parent;
    ArrayList<PromoteUserListModel> followerArrayList;
    PromoteUserListModel followersListModel;
    FollowersAdapter followersAdapter;
    ListView followerList;
    CustomEditTextRegularFont editView;
    String followStatus="0";
    String UserId="";
    int offSet=0;
    Button reLoad;
    View footerView;
    Boolean loadingMore=true;


    Followersfragment init(int val) {
        Followersfragment followersfragment = new Followersfragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);


        followersfragment.setArguments(args);

        return followersfragment;
    }
    public void setUserId(String id)
    {
        UserId=id;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.followers_layout, container, false);
        followerList=(ListView)rootView.findViewById(R.id.followerList);
        errorLayout=(LinearLayout)rootView.findViewById(R.id.errorLayout);
        header=(CustomTextViewSemiBold)rootView.findViewById(R.id.header);
        reLoad=(Button)rootView.findViewById(R.id.reLoad);
        header.setText(getResources().getString(R.string.followers));
        followerArrayList = new ArrayList<>();
        footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.load_more_view, null, false);

        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
        } else {
            GetFollowersListApi();

        }

        editView=(CustomEditTextRegularFont)rootView.findViewById(R.id.commentText);

        RelativeLayout backBtn=(RelativeLayout)rootView.findViewById(R.id.backBtnMain);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getActivity().getSupportFragmentManager().beginTransaction().remove(Followersfragment.this).commit();
                FrameLayout fragmentContainer=(FrameLayout)getActivity().findViewById(R.id.fragmentContainer);
                RelativeLayout profileLayout=(RelativeLayout)getActivity().findViewById(R.id.profileLayout);
                fragmentContainer.setVisibility(View.GONE);
                profileLayout.setVisibility(View.VISIBLE);
            }
        });
        followerList.setOnScrollListener(new AbsListView.OnScrollListener() {
            //useless here, skip!
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            //dumdumdum
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //what is the bottom iten that is visible
                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already ? Load more !
                if ((lastInScreen == totalItemCount) && !(loadingMore)) {
                    if (AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        GetFollowersListApi();

                    }
                }
            }
        });
        return rootView;
    }

    /**
     * Get Folowing List API
     */
    public void GetFollowersListApi() {
        if(offSet<Config.getOFFSETOTHER())
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("user_id", UserId);
        params.put("offset", offSet+"");
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getFollowersList", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {

                               if(followerArrayList.size()>0)
                               {
                                   JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");

                                   for (int i = 0; i < userResults.length(); i++) {
                                       followersListModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                       followerArrayList.add(followersListModel);

                                   }
                                   offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                   followersAdapter.notifyDataSetChanged();
                               }
                                else {
                                   JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");

                                   for (int i = 0; i < userResults.length(); i++) {
                                       followersListModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                       followerArrayList.add(followersListModel);

                                   }
                                   offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                   followersAdapter = new FollowersAdapter(getActivity(), followerArrayList);
                                   followerList.setAdapter(followersAdapter);
                                   reLoad.setVisibility(View.GONE);
                                   followerList.setVisibility(View.VISIBLE);
                               }

                                if (followerArrayList.size() < 10) {
                                    followerList.removeFooterView(footerView);
                                }
                                else
                                {
                                    followerList.addFooterView(footerView);
                                }
                                loadingMore=false;

                            } else {
                                if(offSet<Config.getOFFSETOTHER()) {
                                    reLoad.setVisibility(View.VISIBLE);
                                    followerList.setVisibility(View.GONE);
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
                                }
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        if (offSet < Config.getOFFSETOTHER())
                        {
                            reLoad.setVisibility(View.VISIBLE);
                        followerList.setVisibility(View.GONE);
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                    }

                    }
                });

    }

    /**
     * Get Folowing List API
     */
    public void FollowApi(String userId, final int position) {
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("user_id", userId);
        params.put("status", followStatus);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "follow", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) ||(ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {

                                if (followStatus.equals("0")) {
                                    followerArrayList.get(position).setIsSelected(false);
                                    followStatus="1";
                                } else {
                                    followerArrayList.get(position).setIsSelected(true);
                                    followStatus="0";
                                }
                                followersAdapter.notifyDataSetChanged();
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            } else {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }

    public class FollowersAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_followers = null;
        Activity activity;
        LayoutInflater inflater = null;
        ViewHolderFollowers holder;
        public FollowersAdapter(Activity a, List<PromoteUserListModel> all_comments) {
            this.all_followers = all_comments;
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_followers.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_followers.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolderFollowers();
                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);
                holder.followBtn = (CustomTextViewRegularFont) convertView.findViewById(R.id.selectBtn);
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);
                holder.header = (TextView) convertView.findViewById(R.id.header);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolderFollowers) convertView.getTag();

            }

            if (all_followers.get(position).getIsSelected() == false) {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.followBtn.setText("Follow");
                holder.followBtn.setTextColor(getResources().getColor(R.color.statusBar));
            } else {
                holder.followBtn.setBackgroundResource(R.drawable.follow_selected);
                holder.followBtn.setText("Following");
                holder.followBtn.setTextColor(getResources().getColor(R.color.white));
            }
            //   holder.header.setText("FOLLOWERS");
            holder.nameField.setText(all_followers.get(position).getNamefield());

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (all_followers.get(position).getIsSelected() == true) {
                        followStatus="0";
                    } else {
                        followStatus="1";
                    }

                    if (!AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                    } else {
                        FollowApi(all_followers.get(position).getRowId(),position);

                    }


                }

            });
            holder.layout.setTag(holder);
            holder.followBtn.setTag(holder);



            try {
                Picasso.with(getActivity()).load(all_followers.get(position).getImagefield()).transform(new CircleTransform()).into(holder.img);

            } catch (Exception e) {

            }
            return convertView;

        }

    }

    class ViewHolderFollowers {

        RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField, followBtn;
        TextView header;


    }
}
