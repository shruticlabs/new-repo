package pager.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.example.clicklabs.butlers.R;
import clabs.butlers.utils.ParallaxPageTransformer;


public class FriendProductFrag extends Fragment {

    PagerSlidingTabStrip tabStrip;
    ViewPager viewPagerProducts;
    int ITEMS = 2,position;
    HomFragment parent;
    FragmentManager fm;

    public static FriendProductFrag init(int val) {
       FriendProductFrag friendProductFrag = new FriendProductFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        friendProductFrag.setArguments(args);

        return friendProductFrag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments() != null ? getArguments().getInt("val") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_products, container, false);
        tabStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        viewPagerProducts = (ViewPager) rootView.findViewById(R.id.productPager_user);
        parent= (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.topBar.setVisibility(View.VISIBLE);
        fm=getFragmentManager();
        parent.headerText.setText("Sean's Products");
        ProfilePagerAdapter profilePagerAdapter = new ProfilePagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerProducts.setAdapter(profilePagerAdapter);
        viewPagerProducts.setPageTransformer(true, new ParallaxPageTransformer());
        viewPagerProducts.setOffscreenPageLimit(ITEMS);
        tabStrip.setViewPager(viewPagerProducts);

        parent.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fm.popBackStackImmediate()) {
                    if(fm.getBackStackEntryCount()==3) {
                        parent.topBar.setVisibility(View.GONE);
                        parent.headerText.setText("My promoted Products");
                    }
                  else  if (fm.getBackStackEntryCount() == 1)
                        parent.onOptionsClick();
                    else {
                        parent.backPressed();
                    }
                }
            }
        });
        return rootView;

    }

    public void pressBack() {

        parent.topBar.setVisibility(View.GONE);
        parent.headerText.setText("My promoted Products");

    }

    public class ProfilePagerAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{getResources().getString(R.string.buying), getResources().getString(R.string.selling)};

        public ProfilePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return ITEMS;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return FriendProfileBuying.init(position, true);
                case 1:
                    return new FriendProfileBuying().init(position,true);
                default:// Fragment # 2-9 - Will show list
                    return new  FriendProfileBuying().init(position,true);

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }

    }
}
