package pager.fragment;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.sip.SipSession;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.CircleTransform;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.CustomTextViewSemiBold;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.Fonts;
import material.animations.MaterialDesignAnimations;
import models.PromoteUserListModel;

/**
 * Created by click103 on 12/3/15.
 */
public class PromotePeopleFrag extends Fragment {

    PromoteUserListModel promoteUserListModel;
    ArrayList<PromoteUserListModel> arraylist;
    PromoteAdapter adapter;
    ListView listView;
    CustomEditTextRegularFont searchBar;
    private MediaRecorder myRecorder;
    MediaPlayer myPlayer;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "ButlerApp";

    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };
    CustomTextViewSemiBold nextLabel;
    RelativeLayout nextButton,followAll;
    LinearLayout buyingHeader,errorLayout;
    private File mFileTemp;
    String promoteType="0",textNote="",promoteAll="0";
    ArrayList<String> userIdsCommaSeparated;
    int voiceNoteDuration=0;
    CountDownTimer countDowntimer;
    int offSet = 0;
    ImageView checkBtn;
    Button reLoad;
    Boolean loadingMore = true;
    View footerView;
    int followAllflag=0;
    public static PromotePeopleFrag init(int val) {
        PromotePeopleFrag promotePeopleFrag = new PromotePeopleFrag();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        promotePeopleFrag.setArguments(args);

        return promotePeopleFrag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        buyingHeader=(LinearLayout)getActivity().findViewById(R.id.buyingHeader);

        errorLayout=(LinearLayout)getActivity().findViewById(R.id.errorLayout);
        reLoad=(Button)getActivity().findViewById(R.id.reLoad);
        View rootView = inflater.inflate(R.layout.promote_people_layout, container, false);
        userIdsCommaSeparated = new ArrayList<String>();


//        String[] order_ids = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
//        String[] order_images = {"http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                "http://api.androidhive.info/json/movies/4.jpg",
//                };
//        String[] order_names = {"saif", "showket", "anil", "saif", "showket", "anil", "saif", "showket", "anil"};
        followAll=(RelativeLayout)rootView.findViewById(R.id.promoteAllLayy);
        checkBtn = (ImageView) rootView.findViewById(R.id.checkBtnPromote);
        arraylist = new ArrayList<PromoteUserListModel>();
        listView=(ListView) rootView.findViewById(R.id.promoterList);
        searchBar = (CustomEditTextRegularFont) rootView.findViewById(R.id.searchBarPeople);
        nextButton = (RelativeLayout) rootView.findViewById(R.id.NextBtnMain);
        nextLabel = (CustomTextViewSemiBold) rootView.findViewById(R.id.footerLabel);
        nextLabel.setText(getResources().getString(R.string.promote));
        footerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.load_more_view, null, false);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            //useless here, skip!
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            //dumdumdum
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                //what is the bottom iten that is visible
                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already ? Load more !
                if ((lastInScreen == totalItemCount) && !(loadingMore)) {
                    if (AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        GetFollowingListAPi();

                    }
                }
            }
        });


        searchBar.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    String text = searchBar.getText().toString().toLowerCase();

                    if (text.length() > 0) {
                        hideBuySell(buyingHeader);
                    } else {
                        showBuySell(buyingHeader);
                    }


                    adapter.filter(text);
                }
                catch(Exception e){}
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        followAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(arraylist.size()>0) {
                    if (followAllflag == 0) {
                        checkBtn.setBackgroundResource(R.drawable.btn_chkbox_pressed);
                        followAllflag = 1;

                        for (int i = 0; i < arraylist.size(); i++) {
                            arraylist.get(i).setIsSelected(true);
                            if(!userIdsCommaSeparated.contains(arraylist.get(i).getRowId()))
                                userIdsCommaSeparated.add(arraylist.get(i).getRowId());
                        }

                    } else {

                        checkBtn.setBackgroundResource(R.drawable.btn_chkbox_normal);
                        followAllflag = 0;
                        for (int i = 0; i < arraylist.size(); i++) {
                            arraylist.get(i).setIsSelected(false);

                        }
                        userIdsCommaSeparated.clear();
                    }
                    adapter.notifyDataSetChanged();
                }

            }


        });

        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

        } else {
            GetFollowingListAPi();
        }
//        for (int i = 0; i < order_ids.length; i++) {
//
//            promoteUserListModel = new PromoteUserListModel(order_names[i], order_images[i], order_ids[i],false);
//            // Binds all strings into an array
//            arraylist.add(promoteUserListModel);
//        }
//        adapter = new PromoteAdapter(getActivity(), arraylist);
//        listView.setAdapter(adapter);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arraylist.size()>0) {
                    promoteType = "0";
                    showPromotePopup(getActivity());
                }
                else
                {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.nothingToPromote));
                }
            }
        });
        Data.hideSoftKeyboard(getActivity());
        return rootView;
    }




    /**
     * shows various orders adapter
     *
     * @author showket
     */
    public class PromoteAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_products = null;
        private ArrayList<PromoteUserListModel> products;

        Activity activity;

        LayoutInflater inflater = null;
        boolean check[];

        ViewHolder holder;

        // Typeface face, face1;

        public PromoteAdapter(Activity a, List<PromoteUserListModel> all_products) {

            this.all_products = all_products;

            this.products = new ArrayList<PromoteUserListModel>();
            this.products.addAll(all_products);
            activity = a;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_products.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_products.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.add_proted_user_list_item, null);
                holder = new ViewHolder();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.namefield = (TextView) convertView
                        .findViewById(R.id.userName);

                holder.arrowbtn = (TextView) convertView
                        .findViewById(R.id.selectBtn);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (all_products.get(position).getIsSelected()==false) {
                holder.arrowbtn.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.arrowbtn.setText("Select");
                holder.arrowbtn.setTextColor(getResources().getColor(R.color.statusBar));
            }
            else{
                holder.arrowbtn.setBackgroundResource(R.drawable.follow_selected);
                holder.arrowbtn.setText("Selected");
                holder.arrowbtn.setTextColor(getResources().getColor(R.color.white));


            }

            holder.namefield.setText(all_products.get(position).getNamefield());

            try {
                Picasso.with(getActivity()).load(products.get(position).getImagefield()).transform(new CircleTransform()).into(holder.img);

            } catch (Exception e) {

            }


            holder.arrowbtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (all_products.get(position).getIsSelected()==true) {
                        all_products.get(position).setIsSelected(false);
                        if(userIdsCommaSeparated.contains(all_products.get(position).getRowId()))
                            userIdsCommaSeparated.remove(all_products.get(position).getRowId());
                    }
                    else
                    {
                        all_products.get(position).setIsSelected(true);
                        if(!userIdsCommaSeparated.contains(all_products.get(position).getRowId()))
                        userIdsCommaSeparated.add(all_products.get(position).getRowId());
                    }

                    notifyDataSetChanged();



                }

            });
            holder.layout.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }

            });
            return convertView;

        }

        // Filter Class
        public void filter(String charText) {
            charText = charText.toLowerCase();
            all_products.clear();
            if (charText.length() == 0) {
                all_products.addAll(products);
            } else {
                for (PromoteUserListModel wp : products) {
                    if (wp.getNamefield().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        all_products.add(wp);
                        Log.i("inside filter", wp.getRowId() + "..");
                    }
                }
            }
            notifyDataSetChanged();
        }

    }

    class ViewHolder {

        public RelativeLayout layout;
        ImageView img;
        TextView namefield, arrowbtn;


    }

    /**
     *
     * Promote popup
     * @param activity
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showPromotePopup(Activity activity) {

        try {
            final Dialog dialog = new Dialog(activity,
                    android.R.style.Theme_Translucent_NoTitleBar);
            dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_LoadingDialogFade;
            dialog.getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.setContentView(R.layout.promote_product_popup);
            if(Config.isLollyPop())
            dialog.getWindow().setExitTransition(new Explode());

            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = 0.6f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            final LinearLayout textNoteLayout=(LinearLayout)dialog.findViewById(R.id.textNoteLayout);
            final LinearLayout popupIstTimeView=(LinearLayout)dialog.findViewById(R.id.popupIstTimeView);
            final LinearLayout textNoteButtonLayout=(LinearLayout)dialog.findViewById(R.id.textNoteBtnLayout);
            final CustomTextViewRegularFont textMessage=(CustomTextViewRegularFont)dialog.findViewById(R.id.textMsg);
            final CustomTextViewRegularFont timerRec=(CustomTextViewRegularFont)dialog.findViewById(R.id.timerRecording);
            final Button voiceNoteBtn=(Button)dialog.findViewById(R.id.voiceNoteBtn);
            final RelativeLayout voiceNoteBtnLay=(RelativeLayout)dialog.findViewById(R.id.voiceNoteBtnLay);

            RelativeLayout backBtn=(RelativeLayout)dialog.findViewById(R.id.backPopup);
            CustomTextViewSemiBold headerLabel=(CustomTextViewSemiBold)dialog.findViewById(R.id.headerLabel);
            CustomTextViewRegularFont textNoteBtn=(CustomTextViewRegularFont)dialog.findViewById(R.id.textNoteBtn);
            headerLabel.setText(getResources().getString(R.string.promotionMethod));
            final CustomTextViewRegularFont doneBtn=(CustomTextViewRegularFont)dialog.findViewById(R.id.doneBtn);
            final CustomEditTextRegularFont textNoteText=(CustomEditTextRegularFont) dialog.findViewById(R.id.textNoteText);

            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   stop();

                    Config.setRecordinStatus(0);
                    dialog.dismiss();
                    if(promoteType.equals("1"))
                     textNote = textNoteText.getText().toString();
                    else
                     textNote="";
                    try
                    {
                        Data.hideSoftKeyboard(getActivity());
                        if (!AppStatus.getInstance(getActivity())
                                .isOnline(getActivity())) {
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                        } else {
                            PromotePeopleApi();
                        }
                    }catch (Exception e)
                    {
                        Log.v("Error recording",e.toString());
                    }

                }
            });
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stop();
                    promoteType="0";
                    Config.setRecordinStatus(0);
                    dialog.dismiss();

                }
            });
            textNoteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promoteType="1";
                    doneBtn.setText(getResources().getString(R.string.doneLabel));
                    textNoteLayout.setVisibility(View.VISIBLE);
                    textNoteButtonLayout.setVisibility(View.GONE);
                    popupIstTimeView.setVisibility(View.GONE);


                }
            });
            voiceNoteBtnLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    promoteType="2";
                    if(Config.getRecordinStatus()==0) {
                        doneBtn.setText(getResources().getString(R.string.doneLabel));
                        textMessage.setText(getResources().getString(R.string.voiceNote));
                        timerRec.setText("00:00");
                        textNoteButtonLayout.setVisibility(View.GONE);
                        Config.setRecordinStatus(1);
                    }
                    else if(Config.getRecordinStatus()==1) {

                        RecordVoice();
                        start();

                        Config.setRecordinStatus(2);
                        voiceNoteBtn.setBackgroundResource(R.drawable.pause_selecter_btn);

                        try {


                             countDowntimer = new CountDownTimer(30000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    timerRec.setText("00:"+(millisUntilFinished / 1000));
                                    voiceNoteDuration=(int)millisUntilFinished / 1000;

                                }

                                public void onFinish() {
//                                Toast.makeText(getActivity(), "Stop recording Automatically ", Toast.LENGTH_LONG).show();

                                    stop();
                                    voiceNoteBtn.setBackgroundResource(R.drawable.play_recording_selecter_btn);

                                }
                            };
                            countDowntimer.start();

                        }catch (Exception e){
                            Log.e("ERROR","Recording error in stop");
                        }
                    }
                    else if(Config.getRecordinStatus()==2) {
                         countDowntimer.cancel();
                        stop();
                        voiceNoteBtn.setBackgroundResource(R.drawable.play_recording_selecter_btn);

                    }
                    else if(Config.getRecordinStatus()==3) {
                        play();
                        voiceNoteBtn.setBackgroundResource(R.drawable.record_selecter_btn);
                        try {


                            countDowntimer = new CountDownTimer(voiceNoteDuration, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    timerRec.setText("00:"+(millisUntilFinished / 1000));
                                    voiceNoteDuration=(int)millisUntilFinished / 1000;

                                }

                                public void onFinish() {
//                                Toast.makeText(getActivity(), "Stop recording Automatically ", Toast.LENGTH_LONG).show();

                                    stop();


                                }
                            };
                            countDowntimer.start();

                        }catch (Exception e){
                            Log.e("ERROR","Recording error in stop");
                        }
                    }

                }
            });




            dialog.show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void RecordVoice()
    {
        // store it to sd card
        //outputFile = Environment.getExternalStorageDirectory().
         //       getAbsolutePath() +"/ButlersApp"+ "/butlers.3gpp";

        myPlayer = new MediaPlayer();
        myRecorder = new MediaRecorder();
        myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myRecorder.setOutputFile(getFilename());
        mFileTemp=new File(getFilename());

    }
    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" +"butlerpromoteaudio"+ file_exts[currentFormat]);
    }

    public void start(){
        try {
            myPlayer.stop();

            myRecorder.prepare();
            myRecorder.start();
        } catch (IllegalStateException e) {
            // start:it is called before prepare()
            // prepare: it is called after start() or before setOutputFormat()
            e.printStackTrace();
        } catch (IOException e) {
            // prepare() fails
            e.printStackTrace();
        }



        Toast.makeText(getActivity(), "Start recording...",
                Toast.LENGTH_SHORT).show();
    }

    public void stop(){
        try {
            myRecorder.stop();
            myRecorder.release();
            myRecorder  = null;

//            stopBtn.setEnabled(false);
            Config.setRecordinStatus(3);
//            view.setEnabled(true);
//            text.setText("Recording Point: Stop recording");

            Toast.makeText(getActivity(), "Stop recording...",
                    Toast.LENGTH_SHORT).show();
        } catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
        }
    }
    public void play() {
        try{

            myPlayer.setDataSource(getFilename());
            myPlayer.prepare();
            myPlayer.start();
            Config.setRecordinStatus(1);
//            playBtn.setEnabled(false);
//            stopPlayBtn.setEnabled(true);
//            text.setText("Recording Point: Playing");

            Toast.makeText(getActivity(), "Start play the recording...",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        try {
            myRecorder.stop();
            myRecorder.release();
            myRecorder  = null;
            Config.setRecordinStatus(0);

        } catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void hideBuySell(View view)
    {
//        Animation slide_down     = AnimationUtils.loadAnimation(getActivity(),
//                R.anim.hide_buy_sell);
//        view.setAnimation(slide_down);
        view.setVisibility(View.GONE);

    }

    public void showBuySell(View view)
    {
//        Animation slide_up     = AnimationUtils.loadAnimation(getActivity(),
//                R.anim.show_buy_sell);
//        view.setAnimation(slide_up);

        view.setVisibility(View.VISIBLE);

    }


    /**
     * Proote peole Api
     */
    public void PromotePeopleApi() {

        String idList = userIdsCommaSeparated.toString();
        String csv = idList.substring(1, idList.length() - 1).replace(", ", ",");

        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("product_id",Config.getPRODUCT_ID());
        params.put("promote_type", promoteType);
        params.put("promote_all_flag", promoteAll);
        params.put("promote_in_shop_flag", "0");
        params.put("shop_id", "");
        if(promoteType.equals("2")) {
            params.put("voice_note_duration", Config.getVOICENOTEDURATION()-voiceNoteDuration + "");
        }
        else
        {
            params.put("voice_note_duration", "");
        }
        params.put("text_note", textNote);
        params.put("user_ids", csv);
        try {

            if(promoteType.equals("2")) {
                if (mFileTemp.length() > 0)
                    params.put("voice_note", mFileTemp);
                else {

                    params.put("voice_note", "");
                }
            }
            else
            {
                params.put("voice_note", "");
            }

        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "makePromote", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }
                            else{
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Log.i("request fail", arg0.toString());
                        Data.loading_box_stop();
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                    }
                });

    }
    /**
     * Get Folowing List API
     */
    public void GetFollowingListAPi() {
      //  if (offSet < Config.getOFFSETOTHER())
           // Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        params.put("user_id", Config.getUSER_ID());
        params.put("offset", offSet + "");
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getFollowingsList", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status))
                            {



                                if (arraylist.size() > 0) {
                                    JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");

                                    for (int i = 0; i < userResults.length(); i++) {
                                        promoteUserListModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                        arraylist.add(promoteUserListModel);

                                    }

                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                    adapter.notifyDataSetChanged();
                                    listView.setVisibility(View.VISIBLE);

                                } else {


                                    JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");

                                    for (int i = 0; i < userResults.length(); i++) {
                                        promoteUserListModel = new PromoteUserListModel(userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("user_id"), false);
                                        arraylist.add(promoteUserListModel);

                                    }

                                    adapter = new PromoteAdapter(getActivity(), arraylist);
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));
                                    listView.setAdapter(adapter);
                                   reLoad.setVisibility(View.GONE);
                                    listView.setVisibility(View.VISIBLE);

                                }

                                if (arraylist.size() < 10) {
                                    listView.removeFooterView(footerView);
                                }
                                else
                                {
                                    listView.addFooterView(footerView);
                                }
                                loadingMore=false;

                            } else {

                               if(offSet<Config.getOFFSETOTHER()) {
                                    reLoad.setVisibility(View.VISIBLE);
                                   listView.setVisibility(View.GONE);
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
                               }


                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                       // Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        //if (offSet < Config.getOFFSETOTHER())
                        //{
                            //reLoad.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.GONE);
                         //   Data.loading_box_stop();
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                       // }
                    }
                });

    }
}
