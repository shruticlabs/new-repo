package pager.fragment;


/**
 * Created by Showket on 12/3/15.
 * The class will search people user type in seach bar
 */
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.clicklabs.butlers.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import clabs.butlers.utils.ApiResponseFlags;
import clabs.butlers.utils.AppStatus;
import clabs.butlers.utils.CircleTransform;
import clabs.butlers.utils.Config;
import clabs.butlers.utils.CustomEditTextRegularFont;
import clabs.butlers.utils.CustomEditTextRegularFontBold;
import clabs.butlers.utils.CustomTextViewRegularFont;
import clabs.butlers.utils.Data;
import clabs.butlers.utils.EndlessRecyclerOnScrollListener;
import clicklabs.app.butlers.BuyingProducts;
import material.animations.MaterialDesignAnimations;
import models.SearchPeopleModel;

public class SearchPeopleFrag extends Fragment {


    SearchPeopleModel promoteUserListModel;
    ArrayList<SearchPeopleModel> arraylist;
    SearchPeopleAdapter adapter;
    RecyclerView listView;
    LinearLayoutManager linearLayoutManager;
    FragmentManager fm;
    CustomEditTextRegularFontBold peopleSearchBar;
    LinearLayout errorLayout,listLayout;
    String offSet = "0";
    HomFragment parent;
    public static SearchPeopleAdapter searchPeopleAdapter;
    CustomTextViewRegularFont noOfResults,searchForPeopleLabel;

    static SearchPeopleFrag init(int val) {
        SearchPeopleFrag searchPeopleFrag = new SearchPeopleFrag();
        Bundle args = new Bundle();
        args.putInt("val", val);
        searchPeopleFrag.setArguments(args);

        return searchPeopleFrag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_people_layout, container, false);
        errorLayout = (LinearLayout) getActivity().findViewById(R.id.errorLayout);
        listLayout = (LinearLayout) rootView.findViewById(R.id.listLayout);
        noOfResults = (CustomTextViewRegularFont) rootView.findViewById(R.id.noOfResults);
        searchForPeopleLabel = (CustomTextViewRegularFont) rootView.findViewById(R.id.searchForPeopleLabel);
        final ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.searchPager);
        fm = getFragmentManager();
        parent = (HomFragment)fm.findFragmentByTag("HomFragment");
        peopleSearchBar = (CustomEditTextRegularFontBold) getActivity().findViewById(R.id.peopleSearchBar);
        peopleSearchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.internetConnectionError));

                    } else {
                        Data.hideSoftKeyboard(getActivity());
                        arraylist.clear();
                        offSet = 0 + "";
                        SearchPeopleApi();
                    }
                    handled = true;
                }
                return handled;
            }
        });
        final Button searchcrossBtn = (Button)getActivity(). findViewById(R.id.searchcrossbtn);
        peopleSearchBar.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                if(peopleSearchBar.getText().toString().length()>=1)
                    searchcrossBtn.setVisibility(View.VISIBLE);
                else
                    searchcrossBtn.setVisibility(View.GONE);

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        arraylist = new ArrayList<SearchPeopleModel>();
        listView = (RecyclerView) rootView.findViewById(R.id.searchPeopleListView);
        promoteUserListModel = new SearchPeopleModel("", "", "");
        arraylist.add(promoteUserListModel);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
//        recyclerView.setItemAnimator(new FadeInAnimator());
        fm = getFragmentManager();
        listView.setOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {

                if (AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    SearchPeopleApi();
                }

            }

            @Override
            public void onHide() {
                Data.hideSoftKeyboard(getActivity());
                HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
                parent.hideBottombarScroll();
            }

            @Override
            public void onShow() {
                Data.hideSoftKeyboard(getActivity());
                HomFragment parent = (HomFragment) fm.findFragmentByTag("HomFragment");
                parent.showBottombarScroll();
            }
        });
        searchPeopleAdapter = new SearchPeopleAdapter(getActivity(), arraylist);
        listView.setAdapter(searchPeopleAdapter);

        return rootView;
    }


    /************recycler view*************/
    /**
     * Created by Showket on 2015/28/04.
     * Search people adapter
     */
    public class SearchPeopleAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

        private List<SearchPeopleModel> feedItemList;
        private Context mContext;


        public SearchPeopleAdapter(Context context, List<SearchPeopleModel> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;


        }

        @Override
        public FeedListRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_search_people, null);
            FeedListRowHolder mh = new FeedListRowHolder(v);
            return mh;
        }

        @Override
        public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, final int i) {
            final SearchPeopleModel feedItem = feedItemList.get(i);
            if (feedItem.getUserProfileImage() != "")
                Picasso.with(mContext).load(feedItem.getUserProfileImage()).transform(new CircleTransform()).into(feedListRowHolder.profileImage);
            feedListRowHolder.userName.setText(Html.fromHtml(feedItem.getUserName()));


            feedListRowHolder.layout.setOnClickListener(new View.OnClickListener() {

                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    if(Config.getSKIP_MODE())
                    {
                        parent.showPopupHome();
                    }
                    else
                    {
                        if (Config.isLollyPop()) {
                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity) mContext, feedListRowHolder.profileImage, "userImage");
                            Intent intent = new Intent(mContext, BuyingProducts.class);
                            intent.putExtra("userProfilePic", feedItem.getUserProfileImage());
                            intent.putExtra("userProfileName", feedItem.getUserName());
                            intent.putExtra("layoutShown", "userprofile");
                            intent.putExtra("userId", feedItem.getUserId());
                            getActivity().startActivity(intent, options.toBundle());
                        } else {
                            Intent intent = new Intent(mContext, BuyingProducts.class);
                            intent.putExtra("userProfilePic", feedItem.getUserProfileImage());
                            intent.putExtra("userProfileName", feedItem.getUserName());
                            intent.putExtra("layoutShown", "userprofile");
                            intent.putExtra("userId", feedItem.getUserId());
                            getActivity().startActivity(intent);
                        }
                    }
                }
            });
            feedListRowHolder.profileImage.setTag(feedListRowHolder);
            feedListRowHolder.layout.setTag(feedListRowHolder);

        }

        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }
    }

    class FeedListRowHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        CustomTextViewRegularFont userName;
        RelativeLayout layout;

        public FeedListRowHolder(View convertView) {
            super(convertView);
            this.profileImage = (ImageView) convertView
                    .findViewById(R.id.userImage);
            this.layout = (RelativeLayout) convertView
                    .findViewById(R.id.addPrmoteListRow);
            this.userName = (CustomTextViewRegularFont) convertView
                    .findViewById(R.id.userName);

        }

    }

    /**
     * search People Api
     */
    public void SearchPeopleApi() {

        if (Integer.parseInt(offSet) < Config.getOFFSET())
            Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("search_param", peopleSearchBar.getText().toString());
        params.put("offset", offSet);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.get(Config.getBaseURL() + "searchPeople", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status)) {

                                JSONArray userResults = jObj.getJSONObject("data").getJSONArray("results");
                                SearchPeopleModel searchPeopleModel = null;
                                for (int i = 0; i < userResults.length(); i++) {
                                    searchPeopleModel = new SearchPeopleModel(userResults.getJSONObject(i).getString("profile_pic_link"), userResults.getJSONObject(i).getString("name"), userResults.getJSONObject(i).getString("user_id"));
                                    arraylist.add(searchPeopleModel);

                                }

                                    searchForPeopleLabel.setVisibility(View.GONE);
                                    listLayout.setVisibility(View.VISIBLE);
//                                    noOfResults.setVisibility(View.VISIBLE);
                                    noOfResults.setText(jObj.getJSONObject("data").getString("total_results") +" "+ getResources().getString(R.string.resultsFound));

                                offSet = offSet + jObj.getJSONObject("data").getString("limit_val");
                                searchPeopleAdapter.notifyDataSetChanged();


                            } else {
                                if (!(ApiResponseFlags.NO_MORE_RESULTS.getOrdinal() == status))
                                {
                                    listLayout.setVisibility(View.GONE);
                                   // noOfResults.setVisibility(View.GONE);
                                }
                                if (Integer.parseInt(offSet) < Config.getOFFSET())
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        if (Integer.parseInt(offSet) < Config.getOFFSET()) {
                            Data.loading_box_stop();
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));
                        }
                    }
                });

    }
}
