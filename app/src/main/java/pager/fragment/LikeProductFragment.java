package pager.fragment;

        import android.app.Activity;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.FrameLayout;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.RelativeLayout;

        import com.androidquery.AQuery;
        import com.example.clicklabs.butlers.R;
        import com.loopj.android.http.AsyncHttpClient;
        import com.loopj.android.http.AsyncHttpResponseHandler;
        import com.loopj.android.http.RequestParams;
        import com.squareup.picasso.Picasso;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.List;
        import clabs.butlers.utils.ApiResponseFlags;
        import clabs.butlers.utils.AppStatus;
        import clabs.butlers.utils.CircleTransform;
        import clabs.butlers.utils.Config;
        import clabs.butlers.utils.CustomTextViewRegularFont;
        import clabs.butlers.utils.CustomTextViewSemiBold;
        import clabs.butlers.utils.Data;
        import material.animations.MaterialDesignAnimations;
        import models.CommentModel;
        import models.PromoteUserListModel;

/**
 * Created by clicklabs107 on 4/23/15.
 */
public class LikeProductFragment extends Fragment {

    RelativeLayout bottomGreenBar, nextBtn, backBtn;
    CustomTextViewSemiBold header, footer;
    MaterialDesignAnimations animations;
    HomFragment parent;
    PromoteUserListModel likeModel;
    ArrayList<PromoteUserListModel> likeModelArrayList;
    LikesAdapter likesAdapter;
    int offSet=0;
    String productId="";
    LinearLayout errorLayout;
    ListView likeListView;
    String UserId="";
    CustomTextViewRegularFont reLoad;
    CustomTextViewSemiBold headerLikes;
    CustomTextViewRegularFont noLikes;
     RelativeLayout loadPreviousLikes;

    static LikeProductFragment init(int val) {
        LikeProductFragment editProfileFragment = new LikeProductFragment();

        // Supply val input as an argument.
        Bundle args = new Bundle();
        args.putInt("val", val);
        editProfileFragment.setArguments(args);

        return editProfileFragment;
    }
    public void setUserId(String id){
        UserId=id;
    }
    public void setProductIdId(String productId) {
        this. productId = productId;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.like_product, container, false);
        likeListView=(ListView)rootView.findViewById(R.id.likeListView);
        reLoad=(CustomTextViewRegularFont)rootView.findViewById(R.id.reLoad);

        errorLayout=(LinearLayout)rootView.findViewById(R.id.errorLayout);
        likeModelArrayList = new ArrayList<>();

        LayoutInflater inflaterheader = getActivity().getLayoutInflater();
        View header = inflater.inflate(R.layout.like_product_header_view, null);

        likeListView.addHeaderView(header, null, false);

        headerLikes=(CustomTextViewSemiBold)rootView.findViewById(R.id.headerLabel);
        headerLikes.setText(getResources().getString(R.string.wholike));
       loadPreviousLikes=(RelativeLayout)rootView.findViewById(R.id.loadPreviousLikes);
        loadPreviousLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                } else {
                    ProductLikeApi();

                }
            }
        });




        RelativeLayout backBtn=(RelativeLayout)rootView.findViewById(R.id.backPopup);
        if (!AppStatus.getInstance(getActivity())
                .isOnline(getActivity())) {
            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
        } else {
            offSet=0;
            ProductLikeApi();

        }

        reLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!AppStatus.getInstance(getActivity())
                        .isOnline(getActivity())) {
                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                } else {
                    offSet=0;
                    ProductLikeApi();

                }

            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
                parent.fragmentContainer.setVisibility(View.GONE);
                parent.viewPager.setVisibility(View.VISIBLE);
                parent.bottomBar.setVisibility(View.VISIBLE);
                parent.topBar.setVisibility(View.VISIBLE);

            }
        });

        return rootView;
    }


    /**
     * Product Comments Api
     */
    public void ProductLikeApi() {
        if(likeModelArrayList.size()==0)
            Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("offset", offSet+"");
        params.put("product_id", productId);
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "getProductLikes", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {
                                JSONArray productLikes = jObj.getJSONObject("data").getJSONArray("results");
                                if(likeModelArrayList.size()>0) {
                                    ArrayList<PromoteUserListModel> prevLikes = new ArrayList<>();
                                    if(likeModelArrayList.size()>0)
                                        prevLikes.addAll(likeModelArrayList);
                                    likeModelArrayList.clear();
                                    for (int i = 0; i < productLikes.length(); i++) {

                                        likeModel = new PromoteUserListModel(productLikes.getJSONObject(i).getString("name"), productLikes.getJSONObject(i).getString("profile_pic_link"), productLikes.getJSONObject(i).getString("user_id"), false);
                                        likeModelArrayList.add(likeModel);
                                    }
                                    likeModelArrayList.addAll(prevLikes);

                                    likesAdapter.notifyDataSetChanged();
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));



                                }
                                else
                                {
                                    for (int i = 0; i < productLikes.length(); i++) {
                                        likeModel = new PromoteUserListModel(productLikes.getJSONObject(i).getString("name"), productLikes.getJSONObject(i).getString("profile_pic_link"), productLikes.getJSONObject(i).getString("user_id"), false);
                                        likeModelArrayList.add(likeModel);
                                    }
                                    likesAdapter = new LikesAdapter(getActivity(), likeModelArrayList);
                                    likeListView.setAdapter(likesAdapter);
                                    offSet = offSet + Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"));

                                }
                                if ((Integer.parseInt(jObj.getJSONObject("data").getString("count"))) < (Integer.parseInt(jObj.getJSONObject("data").getString("limit_val"))))
                                    loadPreviousLikes.setVisibility(View.GONE);
                                else
                                    loadPreviousLikes.setVisibility(View.VISIBLE);


                                reLoad.setVisibility(View.GONE);
                                likeListView.setVisibility(View.VISIBLE);
                            } else {
                                if (likeModelArrayList.size()==0) {
                                    reLoad.setVisibility(View.VISIBLE);
                                    likeListView.setVisibility(View.GONE);
                                    MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));
                                }
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }

                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        if(likeModelArrayList.size()==0) {
                            reLoad.setVisibility(View.VISIBLE);
                            likeListView.setVisibility(View.GONE);
                            Data.loading_box_stop();
                            MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));

                        }

                    }
                });

    }


    /**
     * Offer Api Call
     */
    public void OfferLikeApi(final int position,String likeId, final String offerStatus) {
        Data.loading_box(getActivity(), "Loading...");
        RequestParams params = new RequestParams();
        params.put("like_id", likeId);
        params.put("offer_status", offerStatus);
        params.put("access_token", Config.getAPP_ACCESS_TOKEN());
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(Config.getSERVER_TIMEOUT());
        client.post(Config.getBaseURL() + "productOfferToUser", params,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.e("request succesfull", "response = " + response);
                        JSONObject jObj;
                        try {
                            jObj = new JSONObject(response);
                            int status = jObj.getInt("status");
                            if ((ApiResponseFlags.ON_SUCCESS.getOrdinal() == status) || (ApiResponseFlags.ON_UPDATE.getOrdinal() == status)) {
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                                if(offerStatus.equals("0"))
                                {
                                    likeModelArrayList.get(position).setIsSelected(true);

                                }
                                else
                                {
                                    likeModelArrayList.get(position).setIsSelected(false);

                                }
                                likesAdapter.notifyDataSetChanged();


                            }
                            else{
                                MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, jObj.getString("message"));

                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                        Data.loading_box_stop();
                    }

                    @Override
                    public void onFailure(Throwable arg0) {
                        Data.loading_box_stop();

                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.ServerFailure));


                    }
                });

    }


    public class LikesAdapter extends BaseAdapter {

        private List<PromoteUserListModel> all_likes = null;
        Activity activity;
        LayoutInflater inflater = null;
        ViewHolderLikes holder;


        public LikesAdapter(Activity a, List<PromoteUserListModel> all_likes) {
            this.all_likes = all_likes;
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return all_likes.size();
        }

        @Override
        public PromoteUserListModel getItem(int position) {
            return all_likes.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = activity.getLayoutInflater().inflate(
                        R.layout.list_item_like, null);
                holder = new ViewHolderLikes();
                holder.layout = (RelativeLayout) convertView.findViewById(R.id.addPrmoteListRow);

                holder.img = (ImageView) convertView
                        .findViewById(R.id.userImage);
                holder.nameField = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.userName);

                holder.offferLabel = (CustomTextViewRegularFont) convertView
                        .findViewById(R.id.selectBtn);


                convertView.setTag(holder);


            } else {
                holder = (ViewHolderLikes) convertView.getTag();
            }



            holder.nameField.setText(all_likes.get(position).getNamefield());
//            holder.comment.setText(all_likes.get(position).getUserComment());





            try {
                Picasso.with(getActivity()).load(all_likes.get(position).getImagefield()).into(holder.img);

            } catch (Exception e) {

            }

            if (all_likes.get(position).getIsSelected()) {
                holder.offferLabel.setBackgroundResource(R.drawable.follow_selected);
                holder.offferLabel.setTextColor(getResources().getColor(R.color.white));
                holder.offferLabel.setText(getResources().getString(R.string.followingLabel));
            }
            else
            {
                holder.offferLabel.setBackgroundResource(R.drawable.follow_selecter_btn);
                holder.offferLabel.setText(getResources().getString(R.string.follow));
                holder.offferLabel.setTextColor(getResources().getColor(R.color.statusBar));
            }

            holder.offferLabel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String offerStatus="1";
                    if (all_likes.get(position).getIsSelected()) {

                        offerStatus="0";
                    }
                    else
                    {

                        offerStatus="1";
                    }
                    if (!AppStatus.getInstance(getActivity())
                            .isOnline(getActivity())) {
                        MaterialDesignAnimations.fadeIn(getActivity(), errorLayout, getResources().getString(R.string.noInternet));
                    } else {
                        OfferLikeApi(position,all_likes.get(position).getRowId(),offerStatus);

                    }


                }

            });
            return convertView;

        }



    }

    class ViewHolderLikes {

        RelativeLayout layout;
        ImageView img;
        CustomTextViewRegularFont nameField;
        CustomTextViewRegularFont offferLabel;

    }
    public void pressBack() {



        getActivity().getSupportFragmentManager().beginTransaction().remove(LikeProductFragment.this).commit();

        parent = (HomFragment) getFragmentManager().findFragmentByTag("HomFragment");
        parent.fragmentContainer.setVisibility(View.GONE);
        parent.viewPager.setVisibility(View.VISIBLE);
        parent.bottomBar.setVisibility(View.VISIBLE);
        parent.topBar.setVisibility(View.VISIBLE);

    }
}
